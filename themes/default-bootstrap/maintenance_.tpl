{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Conexão Evangelica</title>

<link href="{$css_dir}maintenance.css" rel="stylesheet" type="text/css" />
<script src="scripts.js"></script>
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700' rel='stylesheet' type='text/css'>
</head>

<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&appId=286888154810689&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="centraliza">
	<img src="/../images/logo.png" height="168" width="580">

	<h1>Em breve a loja virtual da sua livraria evangélica será inaugurada.</h1>
	<div id="box">
		<div class="container">
    
			<div class="facebook">
				<div class="fb-like-box" data-href="https://www.facebook.com/livrariaconexaoevangelica" data-width="285" data-height="220" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>

			<p><i class="icones-location"></i> Av. Amazonas 1261 - Betim/MG</p>
			<p><i class="icones-phone"></i> Ligue-nos agora: (31) 3531-6130</p>
			<p><i class="icones-time"></i> Horário de Atendimento: de 9:00 às 18:00</p>
			<p><i class="icones-email"></i><a a href="{$link->getPageLink('teste', true)|escape:'html':'UTF-8'}"> E-mail: contato@conexaoevangelica.com.br</a></p>
			<p><a href="https://www.facebook.com/livrariaconexaoevangelica" target="_blank" title="Facebook"><i class="icones-facebook"></i> /livrariaconexaoevangelica</a></p>
			<p><a href="http://instagram.com/conexaoevangelica" target="_blank" title="Instagram"><i class="icones-instagram"></i> /conexaoevangelica</a></p>
		</div>
	</div>

</div>

</body>
</html>
