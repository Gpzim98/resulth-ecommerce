{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
    <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
        {l s='My account'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe}
    </span>
    <span class="navigation_page">
        {l s='Your personal information'}
    </span>
{/capture}
<div class="box">
    <h1 class="page-subheading">
        {l s='Your personal information'}
    </h1>

    {include file="$tpl_dir./errors.tpl"}

    {if isset($confirmation) && $confirmation}
        <p class="alert alert-success">
            {l s='Your personal information has been successfully updated.'}
            {if isset($pwd_changed)}<br />{l s='Your password has been sent to your email:'} {$email}{/if}
        </p>
    {else}
        <p class="info-title">
            {l s='Please be sure to update your personal information if it has changed.'}
        </p>
        <p class="required">
            <sup>*</sup>{l s='Required field'}
        </p>

{* BEGIN ATS *}
        <script type="text/javascript" src="{$content_dir}modules/atsmodule/js/maskedinput.js"></script>
        <script type="text/javascript" src="{$content_dir}modules/atsmodule/js/customer-functions.js"></script>
{* END ATS *}

        <form action="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" method="post" class="std">
            <fieldset>
{* BEGIN ATS - Adicionado Tipo, CPF, RG *}
    			{if $b2b_enable}
    				<div class="clearfix">
    					<label>Tipo de pessoa</label>
    					<br />

    					<div class="radio-inline">
    						<label for="tipo_pessoa_f" class="top">
    							<input type="radio" name="tipo_pessoa" id="tipo_pessoa_f" value="fisica"
    							{if !isset($smarty.post.tipo_pessoa) || $smarty.post.tipo_pessoa === "fisica"} checked="checked"{/if} />
    							Física
    						</label>
    					</div>
    					<div class="radio-inline">
    						<label for="tipo_pessoa_J" class="top">
    							<input type="radio" name="tipo_pessoa" id="tipo_pessoa_j" value="juridica" {if isset($smarty.post.tipo_pessoa) && $smarty.post.tipo_pessoa === "juridica"} checked="checked"{/if} />
    							Jurídica
    						</label>
    					</div>
    				</div>
    			{else}
    	            <input type="hidden" name="tipo_pessoa" value="{if !isset($smarty.post.tipo_pessoa)}fisica{else}{$smarty.post.tipo_pessoa}{/if}" />
    			{/if}

    			<div class="required form-group">
    				<label for="cpf" class="required">
                        CPF
                    </label>
    				<input type="text" class="is_required form-control" id="cpf" name="cpf"
    					value="{if isset($smarty.post.cpf)}{$smarty.post.cpf}{/if}" />

    				<small style="display:block">
    	    			<span id="alertcpf" class="alertcpf"></span>
    	    		</small>
    			</div>

    			<div class="required form-group">
    				<label for="rg">RG </label>
    				<input type="text" class="is_required form-control" id="rg" name="rg"
    					value="{if isset($smarty.post.rg)}{$smarty.post.rg}{/if}" />
    			</div>
{* END ATS *}

                <div class="required form-group">
                    <label>{l s='Title'}</label>
                    <br />
                    {foreach from=$genders key=k item=gender}
                        <div class="radio-inline">
                            <label for="id_gender{$gender->id}" class="top">
                            <input type="radio" name="id_gender" id="id_gender{$gender->id}" value="{$gender->id|intval}" {if isset($smarty.post.id_gender) && $smarty.post.id_gender == $gender->id}checked="checked"{/if} />
                            {$gender->name}</label>
                        </div>
                    {/foreach}
                </div>
                <div class="required form-group">
                    <label for="firstname" class="required">
                        {l s='First name'}
                    </label>
                    <input class="is_required validate form-control" data-validate="isName" type="text" id="firstname" name="firstname" value="{$smarty.post.firstname}" />
                </div>
                <div class="required form-group">
                    <label for="lastname" class="required">
                        {l s='Last name'}
                    </label>
                    <input class="is_required validate form-control" data-validate="isName" type="text" name="lastname" id="lastname" value="{$smarty.post.lastname}" />
                </div>
                <div class="required form-group">
                    <label for="email" class="required">
                        {l s='E-mail address'}
                    </label>
                    <input class="is_required validate form-control" data-validate="isEmail" type="email" name="email" id="email" value="{$smarty.post.email}" />
                </div>
                <div class="form-group">
                    <label>
                        {l s='Date of Birth'}
                    </label>
                    <div class="row">
                        <div class="col-xs-4">
                            <select name="days" id="days" class="form-control">
                                <option value="">-</option>
                                {foreach from=$days item=v}
                                    <option value="{$v}" {if ($sl_day == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
                                {/foreach}
                            </select>

                        </div>
                        <div class="col-xs-4">
                            <select id="months" name="months" class="form-control">
                                <option value="">-</option>
                                {foreach from=$months key=k item=v}
                                    <option value="{$k}" {if ($sl_month == $k)}selected="selected"{/if}>{l s=$v}&nbsp;</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-xs-4">
                            <select id="years" name="years" class="form-control">
                                <option value="">-</option>
                                {foreach from=$years item=v}
                                    <option value="{$v}" {if ($sl_year == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="required form-group">
                    <label for="old_passwd" class="required">
                        {l s='Current Password'}
                    </label>
                    <input class="is_required validate form-control" type="password" data-validate="isPasswd" name="old_passwd" id="old_passwd" />
                </div>
                <div class="password form-group">
                    <label for="passwd">
                        {l s='New Password'}
                    </label>
                    <input class="is_required validate form-control" type="password" data-validate="isPasswd" name="passwd" id="passwd" />
                </div>
                <div class="password form-group">
                    <label for="confirmation">
                        {l s='Confirmation'}
                    </label>
                    <input class="is_required validate form-control" type="password" data-validate="isPasswd" name="confirmation" id="confirmation" />
                </div>
                {if $newsletter}
                    <div class="checkbox">
                        <label for="newsletter">
                            <input type="checkbox" id="newsletter" name="newsletter" value="1" {if isset($smarty.post.newsletter) && $smarty.post.newsletter == 1} checked="checked"{/if}/>
                            {l s='Sign up for our newsletter!'}
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="optin">
                            <input type="checkbox" name="optin" id="optin" value="1" {if isset($smarty.post.optin) && $smarty.post.optin == 1} checked="checked"{/if}/>
                            {l s='Receive special offers from our partners!'}
                        </label>
                    </div>
                {/if}

{* BEGIN ATS - Adicionado os span de alertas, nome fantasia, o "*" nas labels e classes da div principal *}
			{if $b2b_enable}
                <div class="b2b_information hide">
    				<h1 class="page-subheading">
    					{l s='Your company information'}
    				</h1>
    				<div class="form-group">
    					<label for="company" class="required">
                            {l s='Company'}
                        </label>
    					<input type="text" class="form-control" id="company" name="company" value="{if isset($smarty.post.company)}{$smarty.post.company}{/if}" />
    				</div>
    				<div class="form-group">
    					<label for="nome_fantasia">
                            {l s='Nome fantasia'}
                        </label>
    					<input type="text" class="form-control" id="nome_fantasia" name="nome_fantasia" value="{if isset($smarty.post.nome_fantasia)}{$smarty.post.nome_fantasia}{/if}" />
    				</div>
    				<div class="form-group">
    					<label for="siret" class="required">
                            {l s='SIRET'}
                        </label>
    					<input type="text" class="form-control" id="siret" name="siret" value="{if isset($smarty.post.siret)}{$smarty.post.siret}{/if}" />

                        <small style="display:block">
    	        			<span id="alertsiret" class="alertsiret"></span>
    	        		</small>
    				</div>
    				<div class="form-group">
    					<label for="ape" class="required">
                            {l s='APE'}
                        </label>
    					<input type="text" class="form-control" id="ape" name="ape" value="{if isset($smarty.post.ape)}{$smarty.post.ape}{/if}" />

    					<small style="display:block">
    	        			<span class="alertape">(Inscrição Estadual, CFDF ou "ISENTO")</span>
    	        		</small>
    				</div>
    				<div class="form-group">
    					<label for="website">{l s='Website'}</label>
    					<input type="text" class="form-control" id="website" name="website" value="{if isset($smarty.post.website)}{$smarty.post.website}{/if}" />
    				</div>
                </div>
			{/if}
{* END ATS *}

                <div class="form-group">
                    <button type="submit" name="submitIdentity" class="btn btn-default button button-medium">
                        <span>{l s='Save'}<i class="icon-chevron-right right"></i></span>
                    </button>
                </div>
                <p id="security_informations" class="text-right">
                    <i>{l s='[Insert customer data privacy clause here, if applicable]'}</i>
                </p>
            </fieldset>
        </form> <!-- .std -->
    {/if}
</div>
<ul class="footer_links clearfix">
	<li>
        <a class="btn btn-default button button-small" href="{$link->getPageLink('my-account', true)}">
            <span>
                <i class="icon-chevron-left"></i>{l s='Back to your account'}
            </span>
        </a>
    </li>
	<li>
        <a class="btn btn-default button button-small" href="{$base_dir}">
            <span>
                <i class="icon-chevron-left"></i>{l s='Home'}
            </span>
        </a>
    </li>
</ul>
