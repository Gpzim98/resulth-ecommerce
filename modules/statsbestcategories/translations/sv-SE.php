<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{statsbestcategories}prestashop>statsbestcategories_4f29d8c727dcf2022ac241cb96c31083'] = 'Inga poster hittades';
$_MODULE['<{statsbestcategories}prestashop>statsbestcategories_f5c493141bb4b2508c5938fd9353291a'] = 'Visar %1$s av %2$s';
$_MODULE['<{statsbestcategories}prestashop>statsbestcategories_49ee3087348e8d44e1feda1917443987'] = 'Namn';
$_MODULE['<{statsbestcategories}prestashop>statsbestcategories_eebfd2d9a7ea25b9e61e8260bcd4849c'] = 'Total mängd sålda';
$_MODULE['<{statsbestcategories}prestashop>statsbestcategories_f3547ae5e06426d87312eff7dda775aa'] = 'Totalpris sålda';
$_MODULE['<{statsbestcategories}prestashop>statsbestcategories_c13329e42ec01a10f84c0f950274b404'] = 'Totalt visade';
$_MODULE['<{statsbestcategories}prestashop>statsbestcategories_6e3b3150807da868ebd33ad2c991b8d7'] = 'Bästa kategorier';
$_MODULE['<{statsbestcategories}prestashop>statsbestcategories_e5510869e31bbf721ca15dff21cf1169'] = 'Lägger till en lista på de bästa kategorierna i Statistiken.';
$_MODULE['<{statsbestcategories}prestashop>statsbestcategories_998e4c5c80f27dec552e99dfed34889a'] = 'CSV-export';


return $_MODULE;
