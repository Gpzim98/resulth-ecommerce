<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockcart}prestashop>blockcart_c2e1362a9710a3dd86f937c2ea1f336d'] = '장바구니 블럭';
$_MODULE['<{blockcart}prestashop>blockcart_e03093a5753b436ee1de63b6e3e1bd02'] = '고객의 장바구니를 묶음으로 포함시킴';
$_MODULE['<{blockcart}prestashop>blockcart_a21e5718d2a196280b729438933501c7'] = '';
$_MODULE['<{blockcart}prestashop>blockcart_02c793e3df4632db20e4d6e146095d62'] = '';
$_MODULE['<{blockcart}prestashop>blockcart_c888438d14855d7d96a2724ee9c306bd'] = '설정 업데이트';
$_MODULE['<{blockcart}prestashop>blockcart_f4f70727dc34561dfde1a3c529b6205c'] = '설정';
$_MODULE['<{blockcart}prestashop>blockcart_614a8820aa4ac08ce2ee398a41b10778'] = 'AJAX 장바구니';
$_MODULE['<{blockcart}prestashop>blockcart_eefd19ecf1f6d94a308dcfc95981bbf9'] = '';
$_MODULE['<{blockcart}prestashop>blockcart_00d23a76e43b46dae9ec7aa9dcbebb32'] = '사용';
$_MODULE['<{blockcart}prestashop>blockcart_b9f5c797ebbf55adccdd8539a65a0241'] = '사용안함';
$_MODULE['<{blockcart}prestashop>blockcart_a87a2d4a45b5a7e682089167c1d5232a'] = '';
$_MODULE['<{blockcart}prestashop>blockcart_89e535251f4749df29ad3a288deaa017'] = '';
$_MODULE['<{blockcart}prestashop>blockcart_ce8bd2479bb85218eb304a9a2903a157'] = '';
$_MODULE['<{blockcart}prestashop>blockcart_5f2effb52d25d197793288dfa94c27ce'] = '';
$_MODULE['<{blockcart}prestashop>blockcart_c9cc8cce247e49bae79f15173ce97354'] = '저장';
$_MODULE['<{blockcart}prestashop>blockcart_20351b3328c35ab617549920f5cb4939'] = '사용자 정의 #';
$_MODULE['<{blockcart}prestashop>blockcart_ed6e9a09a111035684bb23682561e12d'] = '장바구니에서 삭제';
$_MODULE['<{blockcart}prestashop>blockcart_c6995d6cc084c192bc2e742f052a5c74'] = '무료배송!';
$_MODULE['<{blockcart}prestashop>blockcart_e7a6ca4e744870d455a57b644f696457'] = '무료!';
$_MODULE['<{blockcart}prestashop>blockcart_f2a6c498fb90ee345d997f888fce3b18'] = '삭제';
$_MODULE['<{blockcart}prestashop>blockcart_0c3bf3014aafb90201805e45b5e62881'] = '내 장바구니 보기';
$_MODULE['<{blockcart}prestashop>blockcart_a85eba4c6c699122b2bb1387ea4813ad'] = '장바구니:';
$_MODULE['<{blockcart}prestashop>blockcart_068f80c7519d0528fb08e82137a72131'] = '제품';
$_MODULE['<{blockcart}prestashop>blockcart_deb10517653c255364175796ace3553f'] = '상품';
$_MODULE['<{blockcart}prestashop>blockcart_9e65b51e82f2a9b9f72ebe3e083582bb'] = '';
$_MODULE['<{blockcart}prestashop>blockcart_0da4d96cad73748e2f608d31cfb3247c'] = '장바구니에서 삭제';
$_MODULE['<{blockcart}prestashop>blockcart_4b7d496eedb665d0b5f589f2f874e7cb'] = '상품 상세';
$_MODULE['<{blockcart}prestashop>blockcart_3d9e3bae9905a12dae384918ed117a26'] = '';
$_MODULE['<{blockcart}prestashop>blockcart_09dc02ecbb078868a3a86dded030076d'] = '비어있음';
$_MODULE['<{blockcart}prestashop>blockcart_ea9cf7e47ff33b2be14e6dd07cbcefc6'] = '배송비';
$_MODULE['<{blockcart}prestashop>blockcart_ba794350deb07c0c96fe73bd12239059'] = '포장비';
$_MODULE['<{blockcart}prestashop>blockcart_4b78ac8eb158840e9638a3aeb26c4a9d'] = '세금';
$_MODULE['<{blockcart}prestashop>blockcart_96b0141273eabab320119c467cdcaf17'] = '합계';
$_MODULE['<{blockcart}prestashop>blockcart_0d11c2b75cf03522c8d97938490466b2'] = '세금포함 가격';
$_MODULE['<{blockcart}prestashop>blockcart_41202aa6b8cf7ae885644717dab1e8b4'] = '세금제외 가격';
$_MODULE['<{blockcart}prestashop>blockcart_377e99e7404b414341a9621f7fb3f906'] = '확인';
$_MODULE['<{blockcart}prestashop>crossselling_ef2b66b0b65479e08ff0cce29e19d006'] = '';
$_MODULE['<{blockcart}prestashop>crossselling_dd1f775e443ff3b9a89270713580a51b'] = '이전';
$_MODULE['<{blockcart}prestashop>crossselling_4351cfebe4b61d8aa5efa1d020710005'] = '보기';
$_MODULE['<{blockcart}prestashop>crossselling_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'Suivant';
$_MODULE['<{blockcart}prestashop>blockcart_98b3009e61879600839e1ee486bb3282'] = '';
$_MODULE['<{blockcart}prestashop>blockcart_544c3bd0eac526113a9c66542be1e5bc'] = '';
$_MODULE['<{blockcart}prestashop>blockcart_694e8d1f2ee056f98ee488bdc4982d73'] = '수량';
$_MODULE['<{blockcart}prestashop>blockcart_e5694b7726ceaf2f057e5f06cf86209e'] = '';
$_MODULE['<{blockcart}prestashop>blockcart_fc86c43dbffcadc193832a310f7a444a'] = '';
$_MODULE['<{blockcart}prestashop>blockcart_db205f01b4fd580fb5daa9072d96849d'] = 'Total produits :';
$_MODULE['<{blockcart}prestashop>blockcart_21034ae6d01a83e702839a72ba8a77b0'] = '(세금별도)';
$_MODULE['<{blockcart}prestashop>blockcart_1f87346a16cf80c372065de3c54c86d9'] = '';
$_MODULE['<{blockcart}prestashop>blockcart_f4e8b53a114e5a17d051ab84d326cae5'] = 'Total des frais de port :';
$_MODULE['<{blockcart}prestashop>blockcart_300225ee958b6350abc51805dab83c24'] = '쇼핑 계속하기';
$_MODULE['<{blockcart}prestashop>blockcart_7e0bf6d67701868aac3116ade8fea957'] = '';


return $_MODULE;
