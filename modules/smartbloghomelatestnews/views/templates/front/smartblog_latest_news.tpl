<div class="home_blog">
    <h2 class="title_block hidden">
		<a href="{smartblog::GetSmartBlogLink('smartblog')}">
			{l s='Latest News' mod='smartbloghomelatestnews'}
		</a>
	</h2>
    <div class="block_content">
		<div class="home_blog_sld">
        {if isset($view_data) AND !empty($view_data)}
            {assign var='i' value=1}
            {foreach from=$view_data item=post}
                    {assign var="options" value=null}
                    {$options.id_post = $post.id}
                    {$options.slug = $post.link_rewrite}
                    <div class="blog_item">
                        <div class="blog_img_holder">
                             <a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}"><img alt="{$post.title}" class="feat_img_small img-responsive" src="{$modules_dir}smartblog/images/{$post.post_img}-home-default.jpg"></a>
                        </div>
						<div class="blog_info">
							<a class="post_title" href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}">{$post.title}</a>
							<p>
								{$post.short_description|escape:'htmlall':'UTF-8'|truncate:100:'...'}
							</p>
							<span class="post_date"><i class="icon-calendar"></i>{$post.date_added}</span>
						</div>
                    </div>
                
                {$i=$i+1}
            {/foreach}
        {/if}
		</div>
    </div>
</div>
{if isset($view_data) AND !empty($view_data)}
	<script>
		$(document).ready(function() {
			var blog_sld = $(".home_blog_sld");
			blog_sld.owlCarousel({
				items : 1,
				itemsDesktop : [1199,1],
				itemsDesktopSmall : [991,1],
				itemsTablet: [767,1],
				itemsMobile : [480,1],
				autoPlay :  false,
				stopOnHover: false,
			});

			// Custom Navigation Events
			$(".home_blog .nexttab").click(function(){
				blog_sld.trigger('owl.next');})
			$(".home_blog .prevtab").click(function(){
				blog_sld.trigger('owl.prev');})
		});
	</script>
{/if}