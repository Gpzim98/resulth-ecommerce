<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blocklink}prestashop>blocklink_fc738410141e4ec0c0319a81255a1431'] = 'Blloku i lidhjeve';
$_MODULE['<{blocklink}prestashop>blocklink_baa2ae9622a47c3217d725d1537e5187'] = 'Shton një bllok me lidhjet shtesë.';
$_MODULE['<{blocklink}prestashop>blocklink_8d85948ef8fda09c2100de886e8663e5'] = '';
$_MODULE['<{blocklink}prestashop>blocklink_33476c93475bba83cdcaac18e09b95ec'] = 'Ky modul nevojitet të jetë i varur (\'hook\') në një kolonë, kurse tema juaj nuk e mbështet një gjë të tillë';
$_MODULE['<{blocklink}prestashop>blocklink_666f6333e43c215212b916fef3d94af0'] = '';
$_MODULE['<{blocklink}prestashop>blocklink_9394bb34611399534ffac4f0ece96b7f'] = 'URL jovalide';
$_MODULE['<{blocklink}prestashop>blocklink_3da9d5745155a430aac6d7de3b6de0c8'] = 'Lidhjet janë shtuar.';
$_MODULE['<{blocklink}prestashop>blocklink_898536ebd630aa3a9844e9cd9d1ebb9b'] = 'U shfaq një gabim gjatë krijimit të lidhjes.';
$_MODULE['<{blocklink}prestashop>blocklink_b18032737875f7947443c98824103a1f'] = 'Fusha "titulli" nuk mund të mbetet e zbrazët.';
$_MODULE['<{blocklink}prestashop>blocklink_43b38b9a2fe65059bed320bd284047e3'] = 'Fusha \\"titulli\\" është jovalide';
$_MODULE['<{blocklink}prestashop>blocklink_eb74914f2759760be5c0a48f699f8541'] = 'U shfaq një gabim gjatë përditësimit të titullit.';
$_MODULE['<{blocklink}prestashop>blocklink_5c0f7e2db8843204f422a369f2030b37'] = 'Titulli i bllokut u përditësua.';
$_MODULE['<{blocklink}prestashop>blocklink_5d73d4c0bcb035a1405e066eb0cdf832'] = 'U shfaq një gabim gjatë fshirjes së lidhjes.';
$_MODULE['<{blocklink}prestashop>blocklink_9bbcafcc85be214aaff76dffb8b72ce9'] = 'Kjo lidhje është fshirë.';
$_MODULE['<{blocklink}prestashop>blocklink_7e5748d8c44f33c9cde08ac2805e5621'] = 'U përditësua mënyra e sortimit';
$_MODULE['<{blocklink}prestashop>blocklink_46cff2568b00bc09d66844849d0b1309'] = 'U shfaq një gabim gjatë vendosjes së mënyrës së sortimit.';
$_MODULE['<{blocklink}prestashop>blocklink_449f6d82cde894eafd3c85b6fa918f89'] = '';
$_MODULE['<{blocklink}prestashop>blocklink_63a11faa3a692d4e00fa8e03bbe8a0d6'] = '';
$_MODULE['<{blocklink}prestashop>blocklink_e6b391a8d2c4d45902a23a8b6585703d'] = 'URL-ja';
$_MODULE['<{blocklink}prestashop>blocklink_387a8014f530f080bf2f3be723f8c164'] = 'Lista e lidhjeve';
$_MODULE['<{blocklink}prestashop>blocklink_58e9b25bb2e2699986a3abe2c92fc82e'] = 'Shto një lidhje të re';
$_MODULE['<{blocklink}prestashop>blocklink_e124f0a8a383171357b9614a45349fb5'] = '';
$_MODULE['<{blocklink}prestashop>blocklink_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'E aktivizuar';
$_MODULE['<{blocklink}prestashop>blocklink_b9f5c797ebbf55adccdd8539a65a0241'] = 'E çaktivizuar';
$_MODULE['<{blocklink}prestashop>blocklink_c9cc8cce247e49bae79f15173ce97354'] = 'Ruaje';
$_MODULE['<{blocklink}prestashop>blocklink_9d55fc80bbb875322aa67fd22fc98469'] = 'Asocioni i dyqanit';
$_MODULE['<{blocklink}prestashop>blocklink_d4bc72be9854ed72e4a1da1509021bf4'] = '';
$_MODULE['<{blocklink}prestashop>blocklink_b22c8f9ad7db023c548c3b8e846cb169'] = 'Shërbimi RSS';
$_MODULE['<{blocklink}prestashop>blocklink_85c75f5424ba98bfc9dfc05ea8c08415'] = '';
$_MODULE['<{blocklink}prestashop>blocklink_b408f3291aaca11c030e806d8b66ee6d'] = '';
$_MODULE['<{blocklink}prestashop>blocklink_3c2c4126af13baa9c6949bc7bcbb0664'] = '';
$_MODULE['<{blocklink}prestashop>blocklink_c07fa9efcc1618d6ef1a12d4f1107dea'] = '';
$_MODULE['<{blocklink}prestashop>blocklink_e83cb6a9bccb5b3fbed4bbeae17b2d12'] = '';


return $_MODULE;
