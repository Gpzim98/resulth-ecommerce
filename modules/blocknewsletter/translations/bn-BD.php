<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_9e31b08a00c1ed653bcaa517dee84714'] = 'নিউজ লেটার ব্লক';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_ba457fab18d697d978befb95e827eb91'] = 'নিউজলেটার সাবস্ক্রিপশন জন্য একটি ব্লক যুক্ত করুন';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_396c88991101b5ca362932952293d291'] = 'আপনি আপনার সমস্ত তথ্য মোছার বিষয়ে নিশ্চিত?';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_417d63b1effb110e438d4b4b9f0fbd95'] = 'ভাউচার কোডটি ভুল';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_c888438d14855d7d96a2724ee9c306bd'] = ' আপডেট সেটিংস হচ্ছে ';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_d37faa29432d65368723e141a02fb55c'] = 'CSV হিসাবে পাঠান';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_b718adec73e04ce3ec720dd11a06a308'] = 'আইডি';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_9f82518d468b9fee614fcc92f76bb163'] = 'দোকান';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_019ec3132cdf8ee0f2e2a75cf5d3e459'] = '';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_dff4bf10409100d989495c6d5486035e'] = '';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_04176f095283bc729f1e3926967e7034'] = '';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'ইমেইল';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_808c7457c768423c5651cbf676d9f6d7'] = '';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_ec59738d441f28011a81e62bdcea6200'] = '';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_3a1205098d0a13a9c41a8d538fd6a34a'] = 'নিউসলেটার রেজিসট্রেসন    ';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_0095a9fa74d1713e43e370a7d7846224'] = 'পাঠান';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_4351cfebe4b61d8aa5efa1d020710005'] = 'দেখুন';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_4182c8f19d40c7ca236a5f4f83faeb6b'] = '';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_e267e2be02cf3e29f4ba53b5d97cf78a'] = 'ইমেল ঠিকানা সটীক নয়';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_1c623b291d95f4f1b1d0c03d0dc0ffa1'] = 'এই ইমেইল ঠিকানা নিবন্ধীকৃত হয় নি.';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_3b1f17a6fd92e35bc744e986b8e7a61c'] = 'সদস্যতা মুক্ত করার সময় একটি ত্রুটি ঘটেছে';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_d4197f3e8e8b4b5d06b599bc45d683bb'] = 'Unsubscription সফল হয়েছে.';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_f6618fce0acbfca15e1f2b0991ddbcd0'] = 'এই ইমেইল ঠিকানা আগের নিবন্ধীকৃত';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_e172cb581e84f43a3bd8ee4e3b512197'] = 'বস্ক্রিপশন প্রক্রিয়ার সময় একটি ত্রুটি ঘটেছে ';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_ebc069b1b9a2c48edfa39e344103be1e'] = 'আপনার কাছে একটি যাচাইকরণ ইমেল পাঠানো হয়েছে. আপনার ইনবক্স চেক করুন.';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_77c576a354d5d6f5e2c1ba50167addf8'] = 'আপনি সফলভাবে এই নিউজলেটার সদস্যতা নিয়েছেন.';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_99c8b8e5e51bf8f00f1d66820684be9a'] = 'এই ইমেল ইতিমধ্যে নথিভুক্ত এবং / অথবা ভুল';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_4e1c51e233f1ed368c58db9ef09010ba'] = 'আমাদের নিউজলেটার সাবস্ক্রাইব করার জন্য আপনাকে ধন্যবাদ .';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_f4f70727dc34561dfde1a3c529b6205c'] = 'সেটিংস';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_54d2f1bab16b550e32395a7e6edb8de5'] = 'আপনি সাবস্ক্রিপশন এর পরে একটি যাচাইকরণ ইমেল পাঠাতে চান ?';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_93cba07454f06a4a960172bbd6e2a435'] = 'হ্যাঁ';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_bafd7322c6e97d25b6299b5d6fe8920b'] = 'না';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_b37f32d509cf5aabe806b16791588aa3'] = 'আপনি সাবস্ক্রিপশন এর পরে একটি যাচাইকরণ ইমেল পাঠাতে চান ?';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_506e58042922bff5bd753dc612e84f5b'] = 'ভাউচার কোড স্বাগত';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_1d612b943b8f4792bff603384a46e5f1'] = 'ডিফল্টরূপে নিষ্ক্রিয় করতে ফাঁকা ছেড়ে দিন.';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_c9cc8cce247e49bae79f15173ce97354'] = 'সংরক্ষণ করুন';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_b192ab83a19105bbf1e2d1fab548249a'] = '';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_c97b56b16c6fda0ef4e93f2135479647'] = '';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_dbb392a2dc9b38722e69f6032faea73e'] = '.csv ফাইল পাঠান';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_f6df4ad6dc4798f26d1f2460eef4f2e9'] = '';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_375afa60efcc1d48300bd339cb8154b0'] = '';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_e347582d22a4ba3c822de504b23d4704'] = '';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_13348442cc6a27032d2b4aa28b75a5d3'] = 'অনুসন্ধান করুন';
$_MODULE['<{blocknewsletter}prestashop>list_action_enable_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'সক্রিয়';
$_MODULE['<{blocknewsletter}prestashop>list_action_enable_b9f5c797ebbf55adccdd8539a65a0241'] = 'নিষ্ক্রিয়';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_ffb7e666a70151215b4c55c6268d7d72'] = 'নিউজ লেটার';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_416f61a2ce16586f8289d41117a2554e'] = 'আপনার ই মেইল';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_d8335f4a5d918134bd0bdc47cc487d74'] = 'আপনার ই-মেইল দিন ';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_a60852f204ed8028c1c58808b746d115'] = 'ঠিক আছে ';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_590b7aefc83952e3483c5168bf93e19f'] = 'নিউজলেটার : %1$s';


return $_MODULE;
