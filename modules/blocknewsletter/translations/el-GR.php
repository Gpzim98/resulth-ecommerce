<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_9e31b08a00c1ed653bcaa517dee84714'] = 'Μπλόκ Περιοδικού';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_ba457fab18d697d978befb95e827eb91'] = 'Προσθέτει ένα block για την εγγραφή στο newsletter';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_396c88991101b5ca362932952293d291'] = 'Είστε βέβαιοι ότι θέλετε να διαγράψετε όλες τις επαφές σας;';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_417d63b1effb110e438d4b4b9f0fbd95'] = 'Μη έγκυρος κωδικός κουπονιού';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_c888438d14855d7d96a2724ee9c306bd'] = 'Οι ρυθμίσεις ενημερώθηκαν';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_d37faa29432d65368723e141a02fb55c'] = 'Εξαγωγή ως CSV';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_9f82518d468b9fee614fcc92f76bb163'] = 'Κατάστημα';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_019ec3132cdf8ee0f2e2a75cf5d3e459'] = 'Φύλο';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_dff4bf10409100d989495c6d5486035e'] = 'Επώνυμο';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_04176f095283bc729f1e3926967e7034'] = 'Όνομα';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'e-mail';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_808c7457c768423c5651cbf676d9f6d7'] = 'Εγγραφήκατε';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_ec59738d441f28011a81e62bdcea6200'] = 'Εγγραφήκατε στις';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_3a1205098d0a13a9c41a8d538fd6a34a'] = 'Εγγραφές του Newsletter';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_0095a9fa74d1713e43e370a7d7846224'] = 'Εξαγωγή';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_4351cfebe4b61d8aa5efa1d020710005'] = 'Προβολή';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_4182c8f19d40c7ca236a5f4f83faeb6b'] = 'Κατάργηση εγγραφής';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_e267e2be02cf3e29f4ba53b5d97cf78a'] = 'Μη έγκυρη διεύθυνση e-mail';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_1c623b291d95f4f1b1d0c03d0dc0ffa1'] = 'Το e-mail δεν έχει καταχωρηθεί';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_3b1f17a6fd92e35bc744e986b8e7a61c'] = 'Σφάλμα κατά τη διάρκεια της διαγραφής';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_d4197f3e8e8b4b5d06b599bc45d683bb'] = 'Επιτυχής διαγραφή.';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_f6618fce0acbfca15e1f2b0991ddbcd0'] = 'Το E-mail έχει καταχωρηθεί ήδη ';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_e172cb581e84f43a3bd8ee4e3b512197'] = 'Σφάλμα κατά την εγγραφή';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_ebc069b1b9a2c48edfa39e344103be1e'] = 'Σας έχει σταλεί ένα μήνυμα επαλήθευσης. Παρακαλώ ελέγξτε το email σας.';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_77c576a354d5d6f5e2c1ba50167addf8'] = 'Επιτυχής εγγραφή';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_99c8b8e5e51bf8f00f1d66820684be9a'] = 'Το e-mail είναι ήδη καταχωρημένο ή δεν είναι έγκυρο';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_4e1c51e233f1ed368c58db9ef09010ba'] = 'Σας ευχαριστούμε για την εγγραφή σας στο newsletter μας.';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_f4f70727dc34561dfde1a3c529b6205c'] = 'Ρυθμίσεις';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_54d2f1bab16b550e32395a7e6edb8de5'] = 'Αποστολή e-mail επαλήθευσης μετά την εγγραφή;';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_93cba07454f06a4a960172bbd6e2a435'] = 'Ναι';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Όχι';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_b37f32d509cf5aabe806b16791588aa3'] = 'Αποστολή e-mail επιβεβαίωσης μετά την εγγραφή;';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_506e58042922bff5bd753dc612e84f5b'] = 'Καλώς ήρθατε κωδικός κουπονιού';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_1d612b943b8f4792bff603384a46e5f1'] = 'Αφήστε το κενό για απενεργοποίηση';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_c9cc8cce247e49bae79f15173ce97354'] = 'Αποθήκευση';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_b192ab83a19105bbf1e2d1fab548249a'] = 'Εξαγωγή Συνδρομητών Newsletter';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_c97b56b16c6fda0ef4e93f2135479647'] = 'Παράγει ένα αρχείο .CSV βασισμένο στα δεδομένα των συνδρομητών του BlockNewsletter. Μόνο οι συνδρομητές χωρίς λογαριασμό στο κατάστημα θα εξαχθούν.';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_dbb392a2dc9b38722e69f6032faea73e'] = 'Εξαγωγή αρχείου .CSV';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_f6df4ad6dc4798f26d1f2460eef4f2e9'] = 'Αναζήτηση για διευθύνσεις';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_375afa60efcc1d48300bd339cb8154b0'] = 'Διεύθυνση Email προς αναζήτηση';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_e347582d22a4ba3c822de504b23d4704'] = 'Παράδειγμα: contact@prestashop.com ή @prestashop.com';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_13348442cc6a27032d2b4aa28b75a5d3'] = 'Αναζήτηση';
$_MODULE['<{blocknewsletter}prestashop>list_action_enable_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Ενεργοποιημένο';
$_MODULE['<{blocknewsletter}prestashop>list_action_enable_b9f5c797ebbf55adccdd8539a65a0241'] = 'Απενεργοποιημένο';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_ffb7e666a70151215b4c55c6268d7d72'] = 'Ενημερωτικό δελτίο';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_416f61a2ce16586f8289d41117a2554e'] = 'το e-mail σας';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_d8335f4a5d918134bd0bdc47cc487d74'] = 'Εισάγετε τη διεύθυνση ηλεκτονικού ταχυδρομείου σας';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_a60852f204ed8028c1c58808b746d115'] = 'Εντάξει';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_590b7aefc83952e3483c5168bf93e19f'] = 'Newsletter : %1$s';


return $_MODULE;
