<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{cashondelivery}prestashop>cashondelivery_1f9497d3e8bac9b50151416f04119cec'] = 'Grynieji pristatymo metu';
$_MODULE['<{cashondelivery}prestashop>cashondelivery_7a3ef27eb0b1895ebf263ad7dd949fb6'] = 'Priimti apmokėjimą pristatant užsakymą';
$_MODULE['<{cashondelivery}prestashop>validation_644818852b4dd8cf9da73543e30f045a'] = '';
$_MODULE['<{cashondelivery}prestashop>validation_6ff063fbc860a79759a7369ac32cee22'] = 'Apsipirkimas';
$_MODULE['<{cashondelivery}prestashop>validation_d538c5b86e9a71455ba27412f4e9ab51'] = 'Grynieji pristatymo metu ';
$_MODULE['<{cashondelivery}prestashop>validation_f1d3b424cd68795ecaa552883759aceb'] = 'Užsakymo suvestinė';
$_MODULE['<{cashondelivery}prestashop>validation_ad2f6328c24caa7d25dd34bfc3e7e25a'] = '';
$_MODULE['<{cashondelivery}prestashop>validation_e2867a925cba382f1436d1834bb52a1c'] = 'Visa suma už jūsų užsakymą';
$_MODULE['<{cashondelivery}prestashop>validation_1f87346a16cf80c372065de3c54c86d9'] = '(su mok.)';
$_MODULE['<{cashondelivery}prestashop>validation_52f64bc0164b0e79deaeaaaa7e93f98f'] = '';
$_MODULE['<{cashondelivery}prestashop>validation_569fd05bdafa1712c4f6be5b153b8418'] = 'Kiti mokėjimo būdai';
$_MODULE['<{cashondelivery}prestashop>validation_46b9e3665f187c739c55983f757ccda0'] = 'Patvirtinu užsakymą';
$_MODULE['<{cashondelivery}prestashop>confirmation_88526efe38fd18179a127024aba8c1d7'] = 'Jūsų užsakymas parduotuvėje %s užbaigtas';
$_MODULE['<{cashondelivery}prestashop>confirmation_8861c5d3fa54b330d1f60ba50fcc4aab'] = 'Jus pasirinkote mokėjimą grynais atsiimant užsakymą.';
$_MODULE['<{cashondelivery}prestashop>confirmation_e6dc7945b557a1cd949bea92dd58963e'] = 'Jūsų užsakymas netrukus bus išsiųstas.';
$_MODULE['<{cashondelivery}prestashop>confirmation_0db71da7150c27142eef9d22b843b4a9'] = 'Jei turite klausimų galite susisiekti su ';
$_MODULE['<{cashondelivery}prestashop>confirmation_64430ad2835be8ad60c59e7d44e4b0b1'] = 'klientų skyriumi';
$_MODULE['<{cashondelivery}prestashop>payment_b7ada96a0da7ee7fb5371cca0b036d5c'] = 'Mokėti grynais atsiimant prekes';
$_MODULE['<{cashondelivery}prestashop>payment_536dc7424180872c8c2488ae0286fb53'] = 'Jus sumokėsite už prekes jas atsiimant';
$_MODULE['<{cashondelivery}prestashop>validation_ea9cf7e47ff33b2be14e6dd07cbcefc6'] = 'Pristatymas';
$_MODULE['<{cashondelivery}prestashop>confirmation_2e2117b7c81aa9ea6931641ea2c6499f'] = 'Jūsų užsakymas';
$_MODULE['<{cashondelivery}prestashop>confirmation_75fbf512d744977d62599cc3f0ae2bb4'] = 'yra užbaigtas.';


return $_MODULE;
