<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{cashondelivery}prestashop>cashondelivery_1f9497d3e8bac9b50151416f04119cec'] = 'Cash on delivery (COD)';
$_MODULE['<{cashondelivery}prestashop>cashondelivery_7a3ef27eb0b1895ebf263ad7dd949fb6'] = 'Menerima uang tunai atas pembayaran pengiriman';
$_MODULE['<{cashondelivery}prestashop>validation_644818852b4dd8cf9da73543e30f045a'] = 'Kembali ke halaman Checkout';
$_MODULE['<{cashondelivery}prestashop>validation_6ff063fbc860a79759a7369ac32cee22'] = 'Pemeriksaan';
$_MODULE['<{cashondelivery}prestashop>validation_d538c5b86e9a71455ba27412f4e9ab51'] = 'Cash on delivery (COD) Pembayaran';
$_MODULE['<{cashondelivery}prestashop>validation_f1d3b424cd68795ecaa552883759aceb'] = 'Ringkasan pembelian';
$_MODULE['<{cashondelivery}prestashop>validation_ad2f6328c24caa7d25dd34bfc3e7e25a'] = 'Anda telah memilih Cash on Delivery.';
$_MODULE['<{cashondelivery}prestashop>validation_e2867a925cba382f1436d1834bb52a1c'] = 'Jumlah total pembelian Anda';
$_MODULE['<{cashondelivery}prestashop>validation_1f87346a16cf80c372065de3c54c86d9'] = '(Termasuk pajak)';
$_MODULE['<{cashondelivery}prestashop>validation_52f64bc0164b0e79deaeaaaa7e93f98f'] = 'Konfirmasikan pembelian Anda dengan memilih tombol \'Konfirmasi pembelian\'.';
$_MODULE['<{cashondelivery}prestashop>validation_569fd05bdafa1712c4f6be5b153b8418'] = 'Metode pembayaran lainnya';
$_MODULE['<{cashondelivery}prestashop>validation_46b9e3665f187c739c55983f757ccda0'] = 'Konfirmasi pembelian';
$_MODULE['<{cashondelivery}prestashop>confirmation_88526efe38fd18179a127024aba8c1d7'] = 'Pembelian Anda pada %s telah selesai.';
$_MODULE['<{cashondelivery}prestashop>confirmation_8861c5d3fa54b330d1f60ba50fcc4aab'] = 'Anda telah memilih uang di metode pengiriman.';
$_MODULE['<{cashondelivery}prestashop>confirmation_e6dc7945b557a1cd949bea92dd58963e'] = 'Pesanan Anda akan dikirim segera.';
$_MODULE['<{cashondelivery}prestashop>confirmation_0db71da7150c27142eef9d22b843b4a9'] = 'Bila ada pertanyaan atau perlu  informasi lainnya, silahkan hubungi';
$_MODULE['<{cashondelivery}prestashop>confirmation_64430ad2835be8ad60c59e7d44e4b0b1'] = 'Layanan pelanggan';
$_MODULE['<{cashondelivery}prestashop>payment_b7ada96a0da7ee7fb5371cca0b036d5c'] = 'Membayar dengan cash on delivery (COD)';
$_MODULE['<{cashondelivery}prestashop>payment_536dc7424180872c8c2488ae0286fb53'] = 'Anda membayar untuk barang pada saat penyerahan';
$_MODULE['<{cashondelivery}prestashop>validation_ea9cf7e47ff33b2be14e6dd07cbcefc6'] = 'Pengiriman';
$_MODULE['<{cashondelivery}prestashop>confirmation_2e2117b7c81aa9ea6931641ea2c6499f'] = 'Anda order pada';
$_MODULE['<{cashondelivery}prestashop>confirmation_75fbf512d744977d62599cc3f0ae2bb4'] = 'selesai.';


return $_MODULE;
