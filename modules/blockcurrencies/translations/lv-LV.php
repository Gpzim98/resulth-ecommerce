<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockcurrencies}prestashop>blockcurrencies_f7a31ae8f776597d4282bd3b1013f08b'] = 'Valūtas Bloks';
$_MODULE['<{blockcurrencies}prestashop>blockcurrencies_80ed40ee905b534ee85ce49a54380107'] = 'Pievieno sadaļu, kas ļauj izvēlēties valūtu';
$_MODULE['<{blockcurrencies}prestashop>blockcurrencies_386c339d37e737a436499d423a77df0c'] = 'Valūta';


return $_MODULE;
