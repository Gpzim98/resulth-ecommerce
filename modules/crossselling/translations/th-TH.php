<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{crossselling}prestashop>crossselling_46532f43e161343763ff815e5208f7e9'] = '';
$_MODULE['<{crossselling}prestashop>crossselling_83bcd48a4b3938f7cd10c898ece01adf'] = '';
$_MODULE['<{crossselling}prestashop>crossselling_462390017ab0938911d2d4e964c0cab7'] = 'การตั้งค่าการปรับปรุงเรียบร้อยแล้ว';
$_MODULE['<{crossselling}prestashop>crossselling_f4f70727dc34561dfde1a3c529b6205c'] = 'ตั้งค่า';
$_MODULE['<{crossselling}prestashop>crossselling_b6bf131edd323320bac67303a3f4de8a'] = 'ราคาที่แสดงบนผลิตภัณฑ์';
$_MODULE['<{crossselling}prestashop>crossselling_70f9a895dc3273d34a7f6d14642708ec'] = 'แสดงราคาที่เกี่ยวกับผลิตภัณฑ์ในบล็อก';
$_MODULE['<{crossselling}prestashop>crossselling_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'เปิดใช้งาน';
$_MODULE['<{crossselling}prestashop>crossselling_b9f5c797ebbf55adccdd8539a65a0241'] = 'ปิดการใช้งาน';
$_MODULE['<{crossselling}prestashop>crossselling_19a799a6afd0aaf89bc7a4f7588bbf2c'] = '';
$_MODULE['<{crossselling}prestashop>crossselling_02128de6a3b085c72662973cd3448df2'] = '';
$_MODULE['<{crossselling}prestashop>crossselling_c9cc8cce247e49bae79f15173ce97354'] = 'บันทึก';
$_MODULE['<{crossselling}prestashop>crossselling_ef2b66b0b65479e08ff0cce29e19d006'] = 'ลูกค้าที่ซื้อสินค้านี้ได้ซื้อ :';
$_MODULE['<{crossselling}prestashop>crossselling_dd1f775e443ff3b9a89270713580a51b'] = 'ก่อนหน้า';
$_MODULE['<{crossselling}prestashop>crossselling_4351cfebe4b61d8aa5efa1d020710005'] = 'ดู';
$_MODULE['<{crossselling}prestashop>crossselling_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'ต่อไป';
$_MODULE['<{crossselling}prestashop>crossselling_0f169d3dc0db47a4074489a89cb034b5'] = '';


return $_MODULE;
