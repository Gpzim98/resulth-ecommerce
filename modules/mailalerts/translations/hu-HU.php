<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{mailalerts}prestashop>mailalerts-account_ae0e822b6fad0de61c231ef188997e92'] = 'Legalább egy terméknek lennie kell a figyelmeztetés törléséhez.';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_0d15d3afa8c174934ff0e43ce3b99bd3'] = 'Jelentkezz be a figyelmeztetések kezeléséhez.';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_a9839ad48cf107667f73bad1d651f2ca'] = 'Sablon nem található';
$_MODULE['<{mailalerts}prestashop>mailalerts_fd30254803e8db32521d3390131a44da'] = 'Értesítő levelek';
$_MODULE['<{mailalerts}prestashop>mailalerts_2d51f4a7ab8a12c4f35b507019523b8c'] = 'E-mail értesítők küldése a vásárlóknak és kereskedőknek.';
$_MODULE['<{mailalerts}prestashop>mailalerts_8fd3b84964bd6dfec8095f658d200b29'] = 'Biztosan törölni szeretnéd az összes vevő értesítést?';
$_MODULE['<{mailalerts}prestashop>mailalerts_c1ee76f076a5b97e3b4b0c0e5703246e'] = 'Beállítások frissítése sikertelen';
$_MODULE['<{mailalerts}prestashop>mailalerts_ce241f25e003bafeb9fce6857d8f027f'] = 'Adj meg egy (vagy több) emailcímet';
$_MODULE['<{mailalerts}prestashop>mailalerts_29aae9c646337554f4de7ae29050c39f'] = 'Érvénytelen e-mail cím:';
$_MODULE['<{mailalerts}prestashop>mailalerts_462390017ab0938911d2d4e964c0cab7'] = 'Beállítások sikeresen frissítve';
$_MODULE['<{mailalerts}prestashop>mailalerts_7cb9a154f101c674c945f88dad5c5e28'] = 'Nincs üzenet';
$_MODULE['<{mailalerts}prestashop>mailalerts_1d744a9ad1dac20645cfc4a36b77323b'] = 'kép';
$_MODULE['<{mailalerts}prestashop>mailalerts_9137796c15dd92e5553c3f29574d0968'] = 'Kupon kód:';
$_MODULE['<{mailalerts}prestashop>mailalerts_58a20987a1f4e45d508b4491614a2c57'] = 'Vevő értesítések';
$_MODULE['<{mailalerts}prestashop>mailalerts_808aaaa33716529eb7c9f658ff4a15b5'] = 'Termék elérhetőség';
$_MODULE['<{mailalerts}prestashop>mailalerts_bad4762226d82261d8c31fc120a03ed2'] = 'Lehetőséget ad a vevőnek, hogy értesítést kapjon egy termékről ha az újra készletről elérhető.';
$_MODULE['<{mailalerts}prestashop>mailalerts_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Bekapcsolt';
$_MODULE['<{mailalerts}prestashop>mailalerts_b9f5c797ebbf55adccdd8539a65a0241'] = 'Kikapcsolt';
$_MODULE['<{mailalerts}prestashop>mailalerts_c9cc8cce247e49bae79f15173ce97354'] = 'Mentés';
$_MODULE['<{mailalerts}prestashop>mailalerts_6f974bbda9064a9c0836370dbf5a6076'] = 'Kereskedői értesítések';
$_MODULE['<{mailalerts}prestashop>mailalerts_4c9120f1a5947445c0e9620254ceb30b'] = 'Új rendelés';
$_MODULE['<{mailalerts}prestashop>mailalerts_382ba298d6c80b03037629d326811919'] = 'Értesítés küldése rendelés leadásáról.';
$_MODULE['<{mailalerts}prestashop>mailalerts_b55197a49e8c4cd8c314bc2aa39d6feb'] = 'Nincs készleten';
$_MODULE['<{mailalerts}prestashop>mailalerts_c0146c441cd6d5dc9d6e0ea1464b345f'] = 'Értesítés küldése ha a termék készleten lévő mennyisége határérték alá esik.';
$_MODULE['<{mailalerts}prestashop>mailalerts_2a63f555989152ba866b43a1faacd680'] = 'Határérték';
$_MODULE['<{mailalerts}prestashop>mailalerts_5b38721cd2e6ea2e12a664fb6dca1990'] = 'Minimális készlet mennyiség, ami alatt értesítést küld a rendszer.';
$_MODULE['<{mailalerts}prestashop>mailalerts_135d106e835a4fa3b6fb5c4638a5d76b'] = 'Készlet figyelmeztetés';
$_MODULE['<{mailalerts}prestashop>mailalerts_218786c9273b95bc637e7adb710b4195'] = 'Értesítés küldése ha a termék készleten lévő mennyisége tűréshatáron kívül kerül.';
$_MODULE['<{mailalerts}prestashop>mailalerts_9841bdc50c4226cb6ec5db76494249e6'] = 'Lefedettség';
$_MODULE['<{mailalerts}prestashop>mailalerts_2a4ae220a78d0c4a4d4c2b7285753321'] = 'Raktárkészlet rendelkezésre állás, napokban. Ez a szám határozza meg adott termékre vonatkozóan a készlet elérhetőségét.';
$_MODULE['<{mailalerts}prestashop>mailalerts_4c3c81fd56b02829a5a1cf953396cd55'] = 'E-mail címek';
$_MODULE['<{mailalerts}prestashop>mailalerts_ee15e8240b16c6d48b217a1965b0ff22'] = 'Egy emailcím soronként (pl: sandor@pelda.hu).';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_36c94bd456cf8796723ad09eac258aef'] = 'Fiókom kezelése';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Saját fiók';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_4edfd10d0bb5f51e0fd2327df608b5a8'] = 'Figyelmeztetéseim';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_1063e38cb53d94d386f21227fcd84717'] = 'Eltávolítás';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_8bb23c2ae698681ebb650f43acb54dab'] = 'Még nincs email riasztás beállítva.';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_0b3db27bc15f682e92ff250ebb167d4b'] = 'Vissza a fiókodhoz';
$_MODULE['<{mailalerts}prestashop>my-account_4edfd10d0bb5f51e0fd2327df608b5a8'] = 'Figyelmeztetéseim';
$_MODULE['<{mailalerts}prestashop>product_67135a14d3ac4f1369633dd006d6efec'] = 'te@cimed.hu';
$_MODULE['<{mailalerts}prestashop>product_61172eb93737ebf095d3fa02119ce1df'] = 'Értesítés kérése regisztrálva';
$_MODULE['<{mailalerts}prestashop>product_bb51a155575b81f4a07f7a9bafdc3b01'] = 'Már beállítottál értesítést ehhez a termékhez';
$_MODULE['<{mailalerts}prestashop>product_900f8551b29793ecb604a545b2059cc1'] = 'Érvénytelen az e-mail címed';
$_MODULE['<{mailalerts}prestashop>product_546e02eaa9a986c83cc347e273269f2c'] = 'Értesíts engem, mikor elérhető';


return $_MODULE;
