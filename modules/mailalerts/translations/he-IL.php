<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{mailalerts}prestashop>mailalerts-account_ae0e822b6fad0de61c231ef188997e92'] = 'על מנת למחוק התראה יש לבחור מוצר.';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_0d15d3afa8c174934ff0e43ce3b99bd3'] = 'עליך להיות מחובר על מנת לנהל את ההתראות שלך.';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_a9839ad48cf107667f73bad1d651f2ca'] = 'לא נמצאה תבנית';
$_MODULE['<{mailalerts}prestashop>mailalerts_fd30254803e8db32521d3390131a44da'] = 'התראות אימייל';
$_MODULE['<{mailalerts}prestashop>mailalerts_2d51f4a7ab8a12c4f35b507019523b8c'] = 'שולח דואר אלקטרוני והודעות ללקוחות וסוחרים.';
$_MODULE['<{mailalerts}prestashop>mailalerts_8fd3b84964bd6dfec8095f658d200b29'] = 'האם אתה בטוח שאתה רוצה למחוק את כל הודעות ללקוח?';
$_MODULE['<{mailalerts}prestashop>mailalerts_c1ee76f076a5b97e3b4b0c0e5703246e'] = 'לא ניתן לעדכן הגדרות';
$_MODULE['<{mailalerts}prestashop>mailalerts_ce241f25e003bafeb9fce6857d8f027f'] = 'הזן כתובת אימייל אחת או יותר';
$_MODULE['<{mailalerts}prestashop>mailalerts_29aae9c646337554f4de7ae29050c39f'] = 'אימייל לא חוקי:';
$_MODULE['<{mailalerts}prestashop>mailalerts_462390017ab0938911d2d4e964c0cab7'] = 'ההגדרות עודכנו בהצלחה';
$_MODULE['<{mailalerts}prestashop>mailalerts_7cb9a154f101c674c945f88dad5c5e28'] = 'אין הודעה';
$_MODULE['<{mailalerts}prestashop>mailalerts_1d744a9ad1dac20645cfc4a36b77323b'] = 'תמונות';
$_MODULE['<{mailalerts}prestashop>mailalerts_9137796c15dd92e5553c3f29574d0968'] = 'קוד שובר:';
$_MODULE['<{mailalerts}prestashop>mailalerts_58a20987a1f4e45d508b4491614a2c57'] = 'הודעות ללקוחות';
$_MODULE['<{mailalerts}prestashop>mailalerts_808aaaa33716529eb7c9f658ff4a15b5'] = 'זמינות המוצר';
$_MODULE['<{mailalerts}prestashop>mailalerts_bad4762226d82261d8c31fc120a03ed2'] = 'מאפשר ללקוח לקבל התראה כאשר מוצר שאזל מהמלאי זמין מחדש.';
$_MODULE['<{mailalerts}prestashop>mailalerts_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'מופעל';
$_MODULE['<{mailalerts}prestashop>mailalerts_b9f5c797ebbf55adccdd8539a65a0241'] = 'מנוטרל';
$_MODULE['<{mailalerts}prestashop>mailalerts_c9cc8cce247e49bae79f15173ce97354'] = 'שמור';
$_MODULE['<{mailalerts}prestashop>mailalerts_6f974bbda9064a9c0836370dbf5a6076'] = 'הודעות לסוחר';
$_MODULE['<{mailalerts}prestashop>mailalerts_4c9120f1a5947445c0e9620254ceb30b'] = 'הזמנה חדשה';
$_MODULE['<{mailalerts}prestashop>mailalerts_382ba298d6c80b03037629d326811919'] = 'קבל הודעה כאשר הזמנה נוצרת.';
$_MODULE['<{mailalerts}prestashop>mailalerts_b55197a49e8c4cd8c314bc2aa39d6feb'] = 'לא במלאי';
$_MODULE['<{mailalerts}prestashop>mailalerts_c0146c441cd6d5dc9d6e0ea1464b345f'] = 'קבל התראה אם הכמות הזמינה של מוצר במלאי היא מתחת לסף הבא.';
$_MODULE['<{mailalerts}prestashop>mailalerts_2a63f555989152ba866b43a1faacd680'] = 'סף';
$_MODULE['<{mailalerts}prestashop>mailalerts_5b38721cd2e6ea2e12a664fb6dca1990'] = 'הכמות שעבורה המוצר יוגדר כאזל מהמלאי.';
$_MODULE['<{mailalerts}prestashop>mailalerts_135d106e835a4fa3b6fb5c4638a5d76b'] = 'התראת מלאי';
$_MODULE['<{mailalerts}prestashop>mailalerts_218786c9273b95bc637e7adb710b4195'] = '';
$_MODULE['<{mailalerts}prestashop>mailalerts_9841bdc50c4226cb6ec5db76494249e6'] = 'סקירה';
$_MODULE['<{mailalerts}prestashop>mailalerts_2a4ae220a78d0c4a4d4c2b7285753321'] = 'זמינות מלאי, בימים. כמו כן, זמינות המלאי של מוצר נתון תחושב על בסיס מספר זה.';
$_MODULE['<{mailalerts}prestashop>mailalerts_4c3c81fd56b02829a5a1cf953396cd55'] = 'כתובת אימייל';
$_MODULE['<{mailalerts}prestashop>mailalerts_ee15e8240b16c6d48b217a1965b0ff22'] = 'כתובת אימייל אחת בשורה (לדוגמא bob@example.com ).';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_36c94bd456cf8796723ad09eac258aef'] = 'נהל החשבון שלי';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'החשבון שלי';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_4edfd10d0bb5f51e0fd2327df608b5a8'] = 'ההתראות שלי';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_1063e38cb53d94d386f21227fcd84717'] = 'הסר';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_8bb23c2ae698681ebb650f43acb54dab'] = 'אין עדיין התראות דואר.';
$_MODULE['<{mailalerts}prestashop>mailalerts-account_0b3db27bc15f682e92ff250ebb167d4b'] = 'חזור לחשבונך';
$_MODULE['<{mailalerts}prestashop>my-account_4edfd10d0bb5f51e0fd2327df608b5a8'] = 'ההתראות שלי';
$_MODULE['<{mailalerts}prestashop>product_67135a14d3ac4f1369633dd006d6efec'] = 'your@email.co.il';
$_MODULE['<{mailalerts}prestashop>product_61172eb93737ebf095d3fa02119ce1df'] = 'בקשת התראה נרשמה';
$_MODULE['<{mailalerts}prestashop>product_bb51a155575b81f4a07f7a9bafdc3b01'] = 'יש לך כבר התראה למוצר זה';
$_MODULE['<{mailalerts}prestashop>product_900f8551b29793ecb604a545b2059cc1'] = 'כתובת האימייל לא תקינה';
$_MODULE['<{mailalerts}prestashop>product_546e02eaa9a986c83cc347e273269f2c'] = 'נא להודיע ​​לי כאשר המוצר זמין';


return $_MODULE;
