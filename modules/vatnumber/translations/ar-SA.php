<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{vatnumber}prestashop>vatnumber_b0b9a606a1c251323825ca45d40964dc'] = 'لم يتم تحديد الدوالة الافتراضية.';
$_MODULE['<{vatnumber}prestashop>vatnumber_cee549912e318726d2c4989bb507665f'] = 'رقم الضريبة الأوروبية على القيمة المضافة';
$_MODULE['<{vatnumber}prestashop>vatnumber_9fa70ad6139f2a83269df74eb6747816'] = '';
$_MODULE['<{vatnumber}prestashop>vatnumber_162b29cf61678af2aaac37f440265c28'] = 'تم تحديث الدولة.';
$_MODULE['<{vatnumber}prestashop>vatnumber_0ca51bcd22e4d7b56b6f1d8a01cefc1f'] = '';
$_MODULE['<{vatnumber}prestashop>vatnumber_467c214bb76759108ece49873eda44e4'] = '';
$_MODULE['<{vatnumber}prestashop>vatnumber_038b34b36ec9eaf5f96d11e17f208f1b'] = '-- اختر الدولة --';
$_MODULE['<{vatnumber}prestashop>vatnumber_f4f70727dc34561dfde1a3c529b6205c'] = 'الإعدادات';
$_MODULE['<{vatnumber}prestashop>vatnumber_fa01fd956e87307bce4c90a0de9b0437'] = 'دولة العملاء';
$_MODULE['<{vatnumber}prestashop>vatnumber_c0859b0a5241dff468da2a9a93c3284f'] = '';
$_MODULE['<{vatnumber}prestashop>vatnumber_da0e4b4c2ffe78456b5a1d67fd854389'] = '';
$_MODULE['<{vatnumber}prestashop>vatnumber_59e1a8522a3bfca342cd7110b1d67b24'] = '';
$_MODULE['<{vatnumber}prestashop>vatnumber_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'مفعل';
$_MODULE['<{vatnumber}prestashop>vatnumber_b9f5c797ebbf55adccdd8539a65a0241'] = 'غير مفعل';
$_MODULE['<{vatnumber}prestashop>vatnumber_c9cc8cce247e49bae79f15173ce97354'] = 'حفظ';


return $_MODULE;
