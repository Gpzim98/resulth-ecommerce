<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockadvertising}prestashop>blockadvertising_bedd646b07e65f588b06f275bd47be07'] = 'Блок реклами';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_dc23a0e1424eda56d0700f7ebe628c78'] = 'Додайте блок реклами до обраних секцій вашого магазину.';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_33476c93475bba83cdcaac18e09b95ec'] = '';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_070e16b4f77b90e802f789b5be583cfa'] = '';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_254f642527b45bc260048e30704edb39'] = 'Конфігурація';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_a15e4232c6c1fc1e67816dd517e0e966'] = '';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_351ec5e81711303a312e244ec952f8e9'] = '';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_4163f94824da4886254e88de13fbb863'] = '';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_24c28ef67324898298e45026d8efabaf'] = '';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_78315dd2b27ef8037115b9f66351c155'] = '';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_c9cc8cce247e49bae79f15173ce97354'] = 'Зберегти';


return $_MODULE;
