<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{statsregistrations}prestashop>statsregistrations_8b15fc6468c919d299f9a601b61b95fc'] = 'Klientų paskyros';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_f14056d6fef225c8aafd5a99d4c70fa8'] = '';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_247b3bdef50a59d5a83f23c4f1c8fa47'] = 'Lankytojai, kurie sustojo registracijos etape:';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_479c1246d97709e234574e1d2921994d'] = 'Lankytojų, kurie pateikė užsakymą iš karto po registracijos, skaičius:';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_a751e9cc4ed4c7585ecc0d97781cb48a'] = 'Viso pirkėjų paskyrų:';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_6602bbeb2956c035fb4cb5e844a4861b'] = 'Vedlys';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_fba0e64541196123bbf8e3737bf9287b'] = 'Sukurta pirkėjų paskyrų';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_76dcf557776c2b40d47b72ebcd9ac6b7'] = 'Sukurtų paskyrų skaičius pats savaime nėra svarbi informacija. Tačiau yra naudinga panagrinėti sukurtų paskyrų skaičių per laiko tarpą. Tai parodys ar reikalai eina teisinga linkme. Ar ne?';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_57a6f41a27c9baa5b402d30e97d4c1e8'] = 'Ką daryti su registracijų evoliucija?';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_271ef73d55b8e3cc30963ca9413d4a52'] = '';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_bcabec23c8f36cecde037bd35ca4c709'] = '';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_ded9c8756dc14fd26e3150c4718cd9d0'] = 'Tai yra sąvadas priežasčių, kurios galėjo įtakoti pirkėjų paskyrų kūrimą:';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_87c365f80449f43460a0567d3b24f29f'] = '';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_aa09be43df78c214e64ac3c3b255708e'] = '';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_8cb5605d77d1d2f9eab6191c0e027747'] = 'Išvaizda ir draugiškumas vartotojui interneto prekybos pasaulyje svarbus kaip niekada. Blogai parinkta ar sunkiai sekama grafinė išvaizda gali atgrasinti pirkėjus. Tai reiškia, kad savo parduotuvėje turėtumėte ieškoti balanso tarp grožio ir funkcionalumo.';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_998e4c5c80f27dec552e99dfed34889a'] = 'CSV eksportas';


return $_MODULE;
