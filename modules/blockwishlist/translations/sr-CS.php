<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockwishlist}prestashop>blockwishlist_2715a65604e1af3d6933b61704865daf'] = 'Blok lista želja';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_7244141a5de030c4c882556f4fd70a8b'] = 'Dodaje blok koji sadrži kupčev spisak želja';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_9ae79c1fccd231ac7fbbf3235dbf6326'] = 'Moj spisak želja';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_5ef2d617096ae7b47a83f3d4de9c84bd'] = 'Aktiviraj modul: nepravilan izbor';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_c888438d14855d7d96a2724ee9c306bd'] = 'Podešavanja su ažurirana';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_deb10517653c255364175796ace3553f'] = 'proizvod';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_694e8d1f2ee056f98ee488bdc4982d73'] = 'Količina';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_502996d9790340c5fd7b86a5b93b1c9f'] = 'Prioritet';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_655d20c1ca69519ca647684edbb2db35'] = 'Visok';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_87f8a6ab85c9ced3702b4ea641ad4bb5'] = 'Srednji';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_28d0edd045e05cf5af64e35ae0c4c6ef'] = 'Nizak';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_e0fd9b310aba555f96e76738ff192ac3'] = 'Spisak želja';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_862af8838fba380d3b30e63546a394e5'] = 'Nema spiska želja.';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_641254d77e7a473aa5910574f3f9453c'] = 'Spisak želja';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_88b589bbf6282a2e02f50ebe90aae6f1'] = 'Morate da budete prijavljeni da bi ste upravljali spiskom želja';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_56ee3495a32081ccb6d2376eab391bfa'] = 'Spisak';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_6bee4060e5e05246d4bcbb720736417c'] = 'Kupci';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_c515b215b9c6be251c924cc6d1324c41'] = 'Odaberi kupca';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_c440899c1d6f8c8271b9b1d171c7e665'] = 'Lista želja :';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_12246cb491c807e85279b8aed74ea3cf'] = 'Odaberi listu želja';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_be53a0541a6d36f6ecb879fa2c584b08'] = 'Slika';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_47ac923d219501859fb68fed8c8db77b'] = 'Kombinacije';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_81355310011c137fdd21cf9a1394ed6a'] = 'Lista proizvoda';
$_MODULE['<{blockwishlist}prestashop>buywishlistproduct_607e1d854783c8229998ac2b5b6923d3'] = 'Neispravan token';
$_MODULE['<{blockwishlist}prestashop>buywishlistproduct_b0ffc4925401f6f4edb038f5ca954937'] = 'Morate se prijaviti';
$_MODULE['<{blockwishlist}prestashop>cart_607e1d854783c8229998ac2b5b6923d3'] = 'Neispravan token';
$_MODULE['<{blockwishlist}prestashop>cart_a9839ad48cf107667f73bad1d651f2ca'] = 'Podloga nije pronađena';
$_MODULE['<{blockwishlist}prestashop>cart_16a23698e7cf5188ce1c07df74298076'] = 'Morate biti prijavljeni da biste upravljali spiskovima želja.';
$_MODULE['<{blockwishlist}prestashop>mywishlist_607e1d854783c8229998ac2b5b6923d3'] = 'Neispravan token';
$_MODULE['<{blockwishlist}prestashop>mywishlist_b30545c7b2d429352b9afdd85be810c7'] = 'Morate navesti ime.';
$_MODULE['<{blockwishlist}prestashop>mywishlist_b74c118d823d908d653cfbf1c877ae55'] = 'Ovo ime već koristi drugi spisak.';
$_MODULE['<{blockwishlist}prestashop>mywishlist_7098d49878bbd102b13038a748125e27'] = 'Nije moguće obrisati spisak želja';
$_MODULE['<{blockwishlist}prestashop>mywishlist_ce7510c007e56bccd6b16af6c40a03de'] = '';
$_MODULE['<{blockwishlist}prestashop>mywishlist_3c924eebbd7c3447336bbec3b325d3da'] = '';
$_MODULE['<{blockwishlist}prestashop>mywishlist_3540aa14bffcdfbbfc3aafbbcb028a1f'] = '';
$_MODULE['<{blockwishlist}prestashop>view_655d20c1ca69519ca647684edbb2db35'] = 'Visok';
$_MODULE['<{blockwishlist}prestashop>view_87f8a6ab85c9ced3702b4ea641ad4bb5'] = 'Srednji';
$_MODULE['<{blockwishlist}prestashop>view_28d0edd045e05cf5af64e35ae0c4c6ef'] = 'Nizak';
$_MODULE['<{blockwishlist}prestashop>managewishlist_a9839ad48cf107667f73bad1d651f2ca'] = 'Podloga nije pronađena';
$_MODULE['<{blockwishlist}prestashop>sendwishlist_8f4be21ec3cfbba15a349e9c5e888579'] = 'nevažeći token';
$_MODULE['<{blockwishlist}prestashop>sendwishlist_90d8a44a1fba13198035d86caeeb2d4d'] = 'nevažeća lista želja';
$_MODULE['<{blockwishlist}prestashop>sendwishlist_072df51ea0cb142b770d6209dab5a85b'] = 'Lista želja poslala grešku';
$_MODULE['<{blockwishlist}prestashop>blockwishlist-ajax_4a84e5921e203aede886d04fc41a414b'] = 'ukloni ovaj proizvod sa mog spiska želja';
$_MODULE['<{blockwishlist}prestashop>blockwishlist-ajax_f2a6c498fb90ee345d997f888fce3b18'] = 'Izbriši';
$_MODULE['<{blockwishlist}prestashop>blockwishlist-ajax_4b7d496eedb665d0b5f589f2f874e7cb'] = 'Detalji o proizvodu';
$_MODULE['<{blockwishlist}prestashop>blockwishlist-ajax_d037160cfb1fa5520563302d3a32630a'] = 'Morate napraviti spisak želja pre dodavanja proizvoda';
$_MODULE['<{blockwishlist}prestashop>blockwishlist-ajax_09dc02ecbb078868a3a86dded030076d'] = 'Nema proizvoda';
$_MODULE['<{blockwishlist}prestashop>blockwishlist-extra_2d96bb66d8541a89620d3c158ceef42b'] = '';
$_MODULE['<{blockwishlist}prestashop>blockwishlist-extra_ec211f7c20af43e742bf2570c3cb84f9'] = 'Dodaj';
$_MODULE['<{blockwishlist}prestashop>blockwishlist-extra_15b94c64c4d5a4f7172e5347a36b94fd'] = 'Dodaj na moj spisak želja';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_top_16a23698e7cf5188ce1c07df74298076'] = 'Morate biti prijavljeni da biste upravljali spiskovima želja.';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_top_ab086554d10bb35f820e7a704105abbf'] = 'Dodato na vašu listu želja.';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_7ec9cceb94985909c6994e95c31c1aa8'] = 'Moj spisak želja';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_4a84e5921e203aede886d04fc41a414b'] = 'ukloni ovaj proizvod sa mog spiska želja';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_f2a6c498fb90ee345d997f888fce3b18'] = 'Izbriši';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_4b7d496eedb665d0b5f589f2f874e7cb'] = 'Detalji o proizvodu';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_09dc02ecbb078868a3a86dded030076d'] = 'Nema proizvoda';
$_MODULE['<{blockwishlist}prestashop>my-account_7ec9cceb94985909c6994e95c31c1aa8'] = 'Moj spisak želja';
$_MODULE['<{blockwishlist}prestashop>managewishlist_f78674b9c6b19504756230c57f6aec38'] = '';
$_MODULE['<{blockwishlist}prestashop>managewishlist_0ac1aeb2429db494dd42ad2dc219ca7e'] = 'Sakrij proizvode';
$_MODULE['<{blockwishlist}prestashop>managewishlist_0de9d09a36e820f9da7e87ab3678dd12'] = 'Prikaži proizvode';
$_MODULE['<{blockwishlist}prestashop>managewishlist_e0977812a2d99e320fcaac92ff096b43'] = '';
$_MODULE['<{blockwishlist}prestashop>managewishlist_b1cf536563bc3b97ee404dab65db3a27'] = '';
$_MODULE['<{blockwishlist}prestashop>managewishlist_8725214cdd9f9af24e914b5da135793d'] = 'stalni link';
$_MODULE['<{blockwishlist}prestashop>managewishlist_30820a1bf6a285e45cda2beda3d7738d'] = 'Pošalji ovaj spisak želja';
$_MODULE['<{blockwishlist}prestashop>managewishlist_f2a6c498fb90ee345d997f888fce3b18'] = 'Izbriši';
$_MODULE['<{blockwishlist}prestashop>managewishlist_4b7d496eedb665d0b5f589f2f874e7cb'] = 'Detalji o proizvodu';
$_MODULE['<{blockwishlist}prestashop>managewishlist_694e8d1f2ee056f98ee488bdc4982d73'] = 'Količina';
$_MODULE['<{blockwishlist}prestashop>managewishlist_502996d9790340c5fd7b86a5b93b1c9f'] = 'Prioritet';
$_MODULE['<{blockwishlist}prestashop>managewishlist_655d20c1ca69519ca647684edbb2db35'] = 'Visok';
$_MODULE['<{blockwishlist}prestashop>managewishlist_87f8a6ab85c9ced3702b4ea641ad4bb5'] = 'Srednji';
$_MODULE['<{blockwishlist}prestashop>managewishlist_28d0edd045e05cf5af64e35ae0c4c6ef'] = 'Nizak';
$_MODULE['<{blockwishlist}prestashop>managewishlist_6bc362dbf494c61ea117fe3c71ca48a5'] = '';
$_MODULE['<{blockwishlist}prestashop>managewishlist_e68ee0c6758ab5b0eea4e105d694f5c4'] = '';
$_MODULE['<{blockwishlist}prestashop>managewishlist_c9cc8cce247e49bae79f15173ce97354'] = 'Sačuvaj';
$_MODULE['<{blockwishlist}prestashop>managewishlist_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'E-mail adresa';
$_MODULE['<{blockwishlist}prestashop>managewishlist_94966d90747b97d1f0f206c98a8b1ac3'] = 'Pošalji';
$_MODULE['<{blockwishlist}prestashop>managewishlist_19f823c6453c2b1ffd09cb715214813d'] = 'Obavezno polje';
$_MODULE['<{blockwishlist}prestashop>managewishlist_deb10517653c255364175796ace3553f'] = 'proizvod';
$_MODULE['<{blockwishlist}prestashop>managewishlist_3384622e86410fd01fa318fedc6a98ce'] = 'Nudi';
$_MODULE['<{blockwishlist}prestashop>managewishlist_44749712dbec183e983dcd78a7736c41'] = 'Datum';
$_MODULE['<{blockwishlist}prestashop>managewishlist_09dc02ecbb078868a3a86dded030076d'] = 'Nema proizvoda';
$_MODULE['<{blockwishlist}prestashop>mywishlist_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Moj nalog';
$_MODULE['<{blockwishlist}prestashop>mywishlist_7ec9cceb94985909c6994e95c31c1aa8'] = 'Moj spisak želja';
$_MODULE['<{blockwishlist}prestashop>mywishlist_06c335f27f292a096a9bf39e3a58e97b'] = 'Novi spisak želja';
$_MODULE['<{blockwishlist}prestashop>mywishlist_49ee3087348e8d44e1feda1917443987'] = 'Ime';
$_MODULE['<{blockwishlist}prestashop>mywishlist_c9cc8cce247e49bae79f15173ce97354'] = 'Sačuvaj';
$_MODULE['<{blockwishlist}prestashop>mywishlist_03ab340b3f99e03cff9e84314ead38c0'] = 'Količina';
$_MODULE['<{blockwishlist}prestashop>mywishlist_5e729042e30967c9d6f65c6eab73e2fe'] = 'Pregledano';
$_MODULE['<{blockwishlist}prestashop>mywishlist_0eceeb45861f9585dd7a97a3e36f85c6'] = 'Napravljeno';
$_MODULE['<{blockwishlist}prestashop>mywishlist_45284ef16392f85ff424b2ef36ab5948'] = 'Direktni link';
$_MODULE['<{blockwishlist}prestashop>mywishlist_7a1920d61156abc05a60135aefe8bc67'] = 'Podrazumevana vrednost';
$_MODULE['<{blockwishlist}prestashop>mywishlist_f2a6c498fb90ee345d997f888fce3b18'] = 'Izbriši';
$_MODULE['<{blockwishlist}prestashop>mywishlist_4351cfebe4b61d8aa5efa1d020710005'] = 'Pregled';
$_MODULE['<{blockwishlist}prestashop>mywishlist_d025259319054206be54336a00defe89'] = 'Da li zaista želite da obrišete spisak želja?';
$_MODULE['<{blockwishlist}prestashop>mywishlist_0b3db27bc15f682e92ff250ebb167d4b'] = 'Nazad na Vaš nalog';
$_MODULE['<{blockwishlist}prestashop>mywishlist_8cf04a9734132302f96da8e113e80ce5'] = 'Početak';
$_MODULE['<{blockwishlist}prestashop>view_641254d77e7a473aa5910574f3f9453c'] = 'Spisak želja';
$_MODULE['<{blockwishlist}prestashop>view_315356f4c2ed70df345ffc01021f7f56'] = 'Ostale liste želja od korisnika %1s %2s:';
$_MODULE['<{blockwishlist}prestashop>view_4b7d496eedb665d0b5f589f2f874e7cb'] = 'Detalji o proizvodu';
$_MODULE['<{blockwishlist}prestashop>view_694e8d1f2ee056f98ee488bdc4982d73'] = 'Količina';
$_MODULE['<{blockwishlist}prestashop>view_502996d9790340c5fd7b86a5b93b1c9f'] = 'Prioritet';
$_MODULE['<{blockwishlist}prestashop>view_4351cfebe4b61d8aa5efa1d020710005'] = 'Pregled';
$_MODULE['<{blockwishlist}prestashop>view_2d0f6b8300be19cf35e89e66f0677f95'] = 'Dodaj u korpu';
$_MODULE['<{blockwishlist}prestashop>blockwishlist_button_6a5373df703ab2827a4ba7facdfcf779'] = 'Dodaj na listu želja';
$_MODULE['<{blockwishlist}prestashop>blockwishlist-extra_33010ef6524c87c41380e2cc5e212f73'] = '';
$_MODULE['<{blockwishlist}prestashop>managewishlist_0bb3e067c0514f5ff2d5a8e45fc0f4be'] = 'Sakrij informacije o kupljenjim proizvodima';
$_MODULE['<{blockwishlist}prestashop>managewishlist_6fe88f5681da397d46fefe19b3020a6a'] = 'Prikaži informacije o kupljenim proizvodima';
$_MODULE['<{blockwishlist}prestashop>view_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Moj nalog';
$_MODULE['<{blockwishlist}prestashop>view_7ec9cceb94985909c6994e95c31c1aa8'] = 'Moj spisak želja';


return $_MODULE;
