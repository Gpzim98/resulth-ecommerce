<?php

global $_LANGMAIL;
$_LANGMAIL = array();

$_LANGMAIL['Hi {firstname} {lastname},'] = '{lastname} {firstname} 您好,';
$_LANGMAIL['Thank you for creating a customer account at {shop_name}.'] = '感謝您在 {shop_name} 創建賬戶。';
$_LANGMAIL['Your {shop_name} login details'] = '您登錄{shop_name}的詳情';
$_LANGMAIL['Here are your login details:'] = '以下是您的登錄信息：';
$_LANGMAIL['E-mail address:'] = '電郵地址：';
$_LANGMAIL['Password:'] = '密碼：';
$_LANGMAIL['Important Security Tips:'] = '重要的安全提示：';
$_LANGMAIL['Always keep your account details safe.'] = '始終保持你的帳戶詳細信息的安全。';
$_LANGMAIL['Never disclose your login details to anyone.'] = '永遠不要透露您的登錄信息給任何人。';
$_LANGMAIL['Change your password regularly.'] = '定期更改密碼。';
$_LANGMAIL['Should you suspect someone is using your account illegally, please notify us immediately.'] = '如果您懷疑有人違法使用您的帳戶，請立即通知我們。';
$_LANGMAIL['You can now place orders on our shop:'] = '您現在可以在我們的店鋪下單了：';
$_LANGMAIL['A new order has been generated on your behalf.'] = '您的新訂單已下單。';
$_LANGMAIL['Please go on <a href="{order_link}">{order_link}</a> to finalize the payment.'] = '請到<a href="{order_link}">{order_link}</a> 完成支付。';
$_LANGMAIL['Thank you for shopping with {shop_name}!'] = '多謝惠顧 {shop_name}!';
$_LANGMAIL['Order {order_name}'] = '訂單編號{order_name}';
$_LANGMAIL['Awaiting wire payment'] = '等待電匯付款';
$_LANGMAIL['Your order with the reference <span><strong>{order_name}</strong></span> has been placed successfully and will be <strong>shipped as soon as we receive your payment</strong>.'] = '編號為<span><strong>{order_name}</strong></span>的訂單已成功下置，我們將在<strong>收到付款後位馬上發貨</strong> 。';
$_LANGMAIL['You have selected to pay by wire transfer.'] = '你選擇以電匯付款。';
$_LANGMAIL['Here are the bank details for your transfer:'] = '請將電匯匯到以下銀行：';
$_LANGMAIL['Amount:'] = '金額：';
$_LANGMAIL['Account owner:'] = '户名：';
$_LANGMAIL['Account details:'] = '帳戶詳細資料 ：';
$_LANGMAIL['Bank address:'] = '銀行地址：';
$_LANGMAIL['You can review your order and download your invoice from the <a href="{history_url}">"Order history"</a> section of your customer account by clicking <a href="{my_account_url}">"My account"</a> on our shop.'] = '您可以查看您的訂單，並從這裡下載您的發票<a href="{history_url}">"訂單記錄"</a> 在您的客戶賬戶點擊<a href="{my_account_url}">"我的賬戶"</a>。';
$_LANGMAIL['If you have a guest account, you can follow your order via the <a href="{guest_tracking_url}">"Guest Tracking"</a> section on our shop.'] = '如果您是遊客賬戶，您可以在我們店舖的<a href="{guest_tracking_url}">"遊客追踪"</a> 頁面追踪您的訂單。';
$_LANGMAIL['Awaiting check payment'] = '等待支票付款';
$_LANGMAIL['You have selected to pay by check.'] = '你選擇了以支票支付。';
$_LANGMAIL['Here are the bank details for your check:'] = '以下是簽發支票時所需的銀行資料：';
$_LANGMAIL['Payable to the order of:'] = '支票抬頭：';
$_LANGMAIL['Please mail your check to:'] = '請郵寄支票到：';
$_LANGMAIL['Your message to {shop_name} Customer Service'] = '您給 {shop_name} 客服的留言';
$_LANGMAIL['Your message has been sent successfully.'] = '您的留言已成功發送。';
$_LANGMAIL['Message:'] = '訊息';
$_LANGMAIL['Order ID:'] = '訂單編號：';
$_LANGMAIL['Product:'] = '商品：';
$_LANGMAIL['Attached file:'] = '附件：';
$_LANGMAIL['We will answer as soon as possible.'] = '我們將盡快回覆。';
$_LANGMAIL['Message from a {shop_name} customer'] = '來自{shop_name} 客戶的留言';
$_LANGMAIL['Customer e-mail address:'] = '客戶電郵地址：';
$_LANGMAIL['Customer message:'] = '客戶留言：';
$_LANGMAIL['Credit slip created'] = '代金券已創建';
$_LANGMAIL['We have generated a credit slip in your name for order with the reference <span><strong>{order_name}</strong></span>.'] = '我們已經為您的訂單 (編號為<span><strong>{order_name}</strong></span>) 以您的名義發行了代金券。';
$_LANGMAIL['You can review this credit slip and download your invoice from the <a href="{history_url}">"My credit slips"</a> section of your account by clicking <a href="{my_account_url}">"My account"</a> on our shop.'] = '您可以閱覽此代金券，並在我們店鋪<a href="{history_url}">"我的代金券"</a> 下載您的發票，請直接點擊clicking <a href="{my_account_url}"> "我的賬戶"。';
$_LANGMAIL['Thank you for your order with the reference {order_name} from <strong>{shop_name}</strong>'] = '感謝您在<STRONG>{shop_name}</ STRONG>的訂單，訂單號為{ORDER_NAME}';
$_LANGMAIL['Product(s) now available for download'] = '商品現在可下載';
$_LANGMAIL['You have <span><strong>{nbProducts}</strong></span> product(s) now available for download using the following link(s):'] = '您有<SPAN><STRONG>{nbProducts}</ STRONG></ SPAN>個商品可以下載，請點擊以下鏈接：';
$_LANGMAIL['If you have a guest account, you can follow your order via the <a href="{guest_tracking_url}?id_order={order_name}">"Guest Tracking"</a> section on our shop.'] = '';
$_LANGMAIL['Your {shop_name} login information'] = '您的{shop_name}登錄信息';
$_LANGMAIL['Here is your personal login information for <span><strong>{shop_name}</strong></span>:'] = '這是您的<SPAN><STRONG>{shop_name}</ STRONG></ SPAN>私人登錄信息：';
$_LANGMAIL['First name:'] = '名：';
$_LANGMAIL['Last name:'] = '姓：';
$_LANGMAIL['<a href="{shop_url}">{shop_name}</a> powered by <a href="http://www.prestashop.com/">PrestaShop&trade;</a>'] = '';
$_LANGMAIL['Customer service - Forwarded discussion'] = '客服 - 轉發聊天';
$_LANGMAIL['<span><strong>{employee}</strong></span> wanted to forward this discussion to you.'] = '';
$_LANGMAIL['Discussion history:'] = '對話記錄：';
$_LANGMAIL['<span><strong>{employee}</strong></span> added <span><strong>"{comment}"</strong></span>'] = '';
$_LANGMAIL['Your customer account creation'] = '您的客戶賬戶創建';
$_LANGMAIL['Your guest account for <span><strong>{shop_name}</strong></span> has been transformed into a customer account.'] = '';
$_LANGMAIL['Please be careful when sharing these login details with others.'] = '';
$_LANGMAIL['You can access your customer account on our shop:'] = '您可以在我們的店舖裡使用客戶賬戶：';
$_LANGMAIL['Message from {shop_name}'] = '{shop_name}發出的消息';
$_LANGMAIL['In transit'] = '運輸中';
$_LANGMAIL['Your order with the reference <span><strong>{order_name}</strong></span> is currently in transit.'] = '您的訂單，編號為<SPAN><STRONG>{ORDER_NAME}</ STRONG></ SPAN>現在正在運輸中。';
$_LANGMAIL['You can track your package using the following link:'] = '您可以使用以下鏈接追踪您的包裹：';
$_LANGMAIL['{followup}'] = '{followup}';
$_LANGMAIL['You have received a new log alert'] = '您收到一個新 log 提醒';
$_LANGMAIL['<span><strong>Warning:</strong></span> you have received a new log alert in your Back Office.'] = '';
$_LANGMAIL['You can check for it in the <span><strong>"Tools" &gt; "Logs"</strong></span> section of your Back Office.'] = '';
$_LANGMAIL['Order canceled'] = '訂單已取消';
$_LANGMAIL['Your order with the reference <span><strong>{order_name}</strong></span> from <span><strong>{shop_name}</strong></span> has been canceled.'] = '您在<span><strong>{shop_name}</strong></span> 的訂單，編號為<span><strong>{order_name}</strong></span> 已被取消。';
$_LANGMAIL['Order changed'] = '訂單已更新';
$_LANGMAIL['Your order with the reference <span><strong>{order_name}</strong></span> from <span><strong>{shop_name}</strong></span> has been changed by the merchant.'] = '您在<span><strong>{shop_name}</strong></span> 的訂單，編號為<span><strong>{order_name}</strong></span> 已被更改。';
$_LANGMAIL['Order details'] = '訂單詳情';
$_LANGMAIL['Order:'] = '訂單：';
$_LANGMAIL['Placed on'] = '下單：';
$_LANGMAIL['Payment:'] = '付款：';
$_LANGMAIL['Reference'] = '代號';
$_LANGMAIL['Product'] = '產品';
$_LANGMAIL['Unit price'] = '單價';
$_LANGMAIL['Quantity'] = '數量';
$_LANGMAIL['Total price'] = '總價';
$_LANGMAIL['Products'] = '產品';
$_LANGMAIL['Discounts'] = '折扣';
$_LANGMAIL['Gift-wrapping'] = '禮品包裝';
$_LANGMAIL['Shipping'] = '配送';
$_LANGMAIL['Total Tax paid'] = '總稅已付';
$_LANGMAIL['Total paid'] = '全部已付款';
$_LANGMAIL['Carrier:'] = '承運商：';
$_LANGMAIL['Delivery address'] = '送貨地址';
$_LANGMAIL['Billing address'] = '帳單地址';
$_LANGMAIL['Message from a customer'] = '客戶留言';
$_LANGMAIL['You have received a new message regarding order with the reference'] = '您收到了一條留言關於訂單';
$_LANGMAIL['Customer:'] = '客戶：';
$_LANGMAIL['You have received a new message from <span><strong>{shop_name}</strong></span> regarding order with the reference <span><strong>{order_name}</strong></span>.'] = '您收到了一條由<span><strong>{shop_name}</strong></span> 發來的留言，關於訂單<span><strong>{order_name}</strong></span> 。';
$_LANGMAIL['Return #{id_order_return} - update'] = '退貨 #{id_order_return} - 更新';
$_LANGMAIL['We have updated the progress on your return #{id_order_return}, the new status is:'] = '您的退貨進度已更新，新狀態為：';
$_LANGMAIL['Thanks for your order with the reference {order_name} from {shop_name}.'] = '感謝您在{shop_name} 下的訂單{order_name}。';
$_LANGMAIL['Item(s) out of stock'] = '件商品缺貨';
$_LANGMAIL['Unfortunately, one or more items are currently out of stock. This may cause a slight delay in your delivery. Please accept our apologies and rest assured that we are working hard to rectify this.'] = '對不起，您的訂單內一或多個產品目前缺貨。這可能會導致訂單發貨輕微延遲。請接受我們的道歉。我們會盡快解決這個問題。';
$_LANGMAIL['Your new {shop_name} login details'] = '你{shop_name}登入的的詳細資訊';
$_LANGMAIL['Password reset request for {shop_name}'] = '{shop_name}密碼重置請求';
$_LANGMAIL['You have requested to reset your <span><strong>{shop_name}</strong></span> login details.'] = '您申請重置您的<span><strong>{shop_name}</strong></span> 登​​錄信息。';
$_LANGMAIL['Please note that this will change your current password.'] = '請注意，這將改變目前的密碼。';
$_LANGMAIL['To confirm this action, please use the following link:'] = '如確定這樣做，請點擊以下鏈接：';
$_LANGMAIL['Payment processing error'] = '支付處理錯誤';
$_LANGMAIL['There is a problem with your payment for <strong><span>{shop_name}</span></strong> order with the reference <strong><span>{order_name}</span></strong>. Please contact us at your earliest convenience.'] = '您在<strong><span>{shop_name}</span></strong> 的訂單<strong><span>{order_name}</span></strong> 支付出現了錯誤。請盡快與我們聯絡。';
$_LANGMAIL['We cannot ship your order until we receive your payment.'] = '我們在收到付款前不能發貨。';
$_LANGMAIL['Payment processed'] = '付款已處理';
$_LANGMAIL['Your payment for order with the reference <strong><span>{order_name}</span></strong> was successfully processed.'] = '您的訂單編號為<strong><span>{order_name}</span></strong> 已經成功支付。';
$_LANGMAIL['Processing'] = '處理中...';
$_LANGMAIL['We are currently processing your <strong><span>{shop_name}</span></strong> order with the reference <strong><span>{order_name}</span></strong>.'] = '我們正在處理您在 <strong><span>{shop_name}</span></strong> 的訂單 <strong><span>{order_name}</span></strong>。';
$_LANGMAIL['Refund processed'] = '退款已處理';
$_LANGMAIL['We have processed your <strong><span>{shop_name}</span></strong> refund for order with the reference <strong><span>{order_name}</span></strong>.'] = '我們以處理您在<strong><span>{shop_name}</span></strong> 的訂單<strong><span>{order_name}</span></strong> 退貨申請。';
$_LANGMAIL['Please do not reply directly to this email, we will not receive it.'] = '';
$_LANGMAIL['In order to reply, please use the following link: <a href="{link}">{link}</a>'] = '';
$_LANGMAIL['Your order has been shipped'] = '';
$_LANGMAIL['Shipped'] = '已發貨';
$_LANGMAIL['Your order with the reference <span><strong>{order_name}</strong></span> has been shipped.'] = '您的訂單<span><strong>{order_name}</strong></span> 已經發貨。';
$_LANGMAIL['Hello'] = '您好';
$_LANGMAIL['This is a <strong>test e-mail</strong> from your shop.<br /><br /> If you can read this, the test was successful!'] = '';
$_LANGMAIL['This is to inform you about the creation of a voucher.'] = '';
$_LANGMAIL['Here is the code of your voucher:'] = '您的優惠券的代碼：';
$_LANGMAIL['Simply copy/paste this code during the payment process for your next order.'] = '';
$_LANGMAIL['Voucher created'] = '折扣券已創建';
$_LANGMAIL['A voucher has been created in your name as a result of your order with the reference <span><strong>{order_name}</strong></span>.'] = '';
$_LANGMAIL['<span><strong>Voucher code: {voucher_num}</strong></span> in the amount of <span><strong>{voucher_amount}</strong></span>'] = '';
$_LANGMAIL['Hi,'] = '您好，';
$_LANGMAIL['Thank you for subscribing to our newsletter.'] = '感謝您訂閱我們的電子報';
$_LANGMAIL['Thank you for subscribing to our newsletter, please confirm your request by clicking the link below :'] = '';
$_LANGMAIL['Newsletter subscription'] = '訂閱電子通訊';
$_LANGMAIL['Regarding your newsletter subscription, we are pleased to offer you the following voucher:'] = '因為您訂閱了電子通訊，我們很高興回饋您以下折扣券：';
$_LANGMAIL['<span><strong>{shop_name}</strong></span> invites you to send this link to your friends, so they can see your wishlist:'] = '<span><strong>{shop_name}</strong></span> 邀請您發送此鏈接給您的朋友，讓他們可以看到您的願望清單：';
$_LANGMAIL['<span><strong>{firstname} {lastname}</strong></span> indicated you may want to see his/her wishlist:'] = '<span><strong>{firstname} {lastname}</strong></span> 表示您可能希望看到他/她的願望清單：';
$_LANGMAIL['Your cart at {shop_name}'] = '您在 {shop_name} 的購物車';
$_LANGMAIL['We noticed that during your last visit on {shop_name}, you did not complete the order you had started.'] = '我們注意到您上次到訪{shop_name}期間，沒有完成訂單。';
$_LANGMAIL['Your cart has been saved, you can resume your order by visiting our shop:'] = '您的購物車已保存，您可以通過訪問我們的商店恢復您的訂單：';
$_LANGMAIL['As an incentive, we can give you a discount of {amount}% off your next order! This offer is valid for <span><strong>{days}</strong></span> days, so do not waste a moment!'] = '作為鼓勵，我們可以給您的下一個訂單{amount}％的折扣！此優惠有效期為<span><strong>{days}</strong></span> 天，所以不要浪費！';
$_LANGMAIL['Here is your coupon:'] = '這是您的優惠券：';
$_LANGMAIL['Enter this code in your shopping cart to get your discount.'] = '在您的購物車中輸入驗證碼去領取您的折扣。 ';
$_LANGMAIL['Thank you for your order at {shop_name}.'] = '感謝您在 {shop_name} 的訂單。';
$_LANGMAIL['As our way of saying thanks, we want to give you a discount of <span><strong>{amount}</strong></span>% off your next order! This offer is valid for <span><strong>{days}</strong></span> days, so do not waste a moment!'] = '為了答謝您，我們可以給您的下一個訂單<span><strong>{amount}</strong></span>％的折扣！此優惠有效期為<span><strong>{days}</strong></span> 天，所以不要浪費！';
$_LANGMAIL['Thanks for your trust.'] = '感謝您的信任。';
$_LANGMAIL['You are one of our best customers and as such we want to thank you for your continued patronage.'] = '';
$_LANGMAIL['As appreciation for your loyalty, we want to give you a discount of <span><strong>{amount}</strong></span>% valid on your next order! This offer is valid for <span><strong>{days}</strong></span> days, so do not waste a moment!'] = '';
$_LANGMAIL['You are one of our best customers, however you have not placed an order in {days_threshold} days.'] = '';
$_LANGMAIL['We wish to thank you for the trust you have placed in us and want to give you a discount of {amount}% valid on your next order! This offer is valid for <span><strong>{days}</strong></span> days, so do not waste a moment!'] = '我們想感謝您給予我們的信任，在我們店鋪下訂單，在此想藉這個機會我們給您的下一個訂單{amount}％的折扣！此優惠有效期為<span><strong>{days}</strong></span>天，所以不要浪費！';
$_LANGMAIL['{product} is now available.'] = '{product} 已經有貨了。';
$_LANGMAIL['This item is once again in-stock.'] = '這件商品又有貨了。';
$_LANGMAIL['You can access the product page by clicking on the link:'] = '點擊鏈接以進入商品頁面：';
$_LANGMAIL['You can order it right now from our online shop.'] = '您現在可以從我們的網店訂購。';
$_LANGMAIL['Congratulations!'] = '恭喜！';
$_LANGMAIL['A new order was placed on {shop_name} by the following customer: {firstname} {lastname} ({email})'] = '';
$_LANGMAIL['{product} is almost out of stock.'] = '{product} 幾乎沒有存貨了。';
$_LANGMAIL['The stock cover is now less than the specified minimum of:'] = '';
$_LANGMAIL['Current stock cover:'] = '供貨情況：';
$_LANGMAIL['{product} is nearly out of stock.'] = '{product} 就快缺貨了。';
$_LANGMAIL['The remaining stock is now less than the specified minimum of'] = '';
$_LANGMAIL['Remaining stock:'] = '所剩存貨：';
$_LANGMAIL['You are advised to open the product&#039;s admin Product Page in order to replenish your inventory.'] = '';
$_LANGMAIL['Your referred friend <span><strong>{sponsored_firstname} {sponsored_lastname}</strong></span> has placed his or her first order on <a href="{shop_url}">{shop_name}</a>!'] = '您推薦的朋友<span><strong>{sponsored_firstname} {sponsored_lastname}</strong></span> 剛剛在<a href="{shop_url}">{shop_name}</a> 下了她／他的第一個訂單！';
$_LANGMAIL['We are pleased to offer you a voucher worth <span><strong>{discount_display} (voucher # {discount_name})</strong></span> that you can use on your next order.'] = '我們很高興為您提供折扣券，價值<span><strong>{discount_display} (voucher # {discount_name})</strong></span>。您可以在下次下單的時候使用。';
$_LANGMAIL['Best regards,'] = '祝好。';
$_LANGMAIL['join us!'] = '歡迎加入！';
$_LANGMAIL['Your friend <span><strong>{firstname} {lastname}</strong></span> wants to refer you on <a href="{shop_url}">{shop_name}</a>!'] = '您的朋友<span><strong>{firstname} {lastname}</strong></span> 想向您推薦<a href="{shop_url}">{shop_name}</a>!';
$_LANGMAIL['We are pleased to offer you a voucher worth <span><strong>{discount}</strong></span> that you can use on your next order.'] = '我們很高興為您提供折扣券，價值<span><strong>{discount}</strong></span>。您可以在下次下單的時候使用。';
$_LANGMAIL['Get referred and earn a discount voucher of <span><strong>{discount}!</strong></span>'] = '推薦給好友，贏得一張<span><strong>{discount}!</strong></span> 折扣券';
$_LANGMAIL['It&#039;s very easy to sign up. Just click here!'] = '註冊非常簡單。只需點擊這裡！';
$_LANGMAIL['When signing up, don&#039;t forget to provide the e-mail address of your referring friend:'] = '在註冊時，不要忘記把推薦給您的朋友的郵件地址填上：';
$_LANGMAIL['Referral Program'] = '推廣計劃';
$_LANGMAIL['We have created a voucher in your name for referring a friend.'] = '我們已在您名下創建了一張折扣券給您的推薦好友。';
$_LANGMAIL[', with an amount of'] = '，金额為';
$_LANGMAIL['Hi {name},'] = '{name} 您好,';
$_LANGMAIL['{customer} has sent you a link to a product that (s)he thinks may interest you.'] = '{customer} 向您發送了一個他（她）認為您可能感興趣的商品鏈接。';
$_LANGMAIL['Click here to view this item:'] = '點擊這裡查看資料：';


return $_LANGMAIL;
