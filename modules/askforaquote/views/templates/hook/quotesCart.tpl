{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- MODULE Quotes cart -->
<script type="text/javascript">
    var quotesCartEmpty  = '{l s="Your quotes cart is empty" mod='askforaquote'}';
</script>

{if $active_overlay == 0}
    <div class="clearfix col-sm-3 quotesOuterBox block">
        <div class="row quotes_cart">
            <a href="{$quotesCart|escape:'html':'UTF-8'}" rel="nofollow" id="quotes-cart-link">
                <b>{l s='Quotes' mod='askforaquote'}</b>
                <span class="ajax_quote_quantity{if $quoteTotalProducts == 0} unvisible{/if}">{$quoteTotalProducts|intval}</span>
                <span class="ajax_quote_product_txt{if $quoteTotalProducts != 1} unvisible{/if}">{l s='Product' mod='askforaquote'}</span>
                <span class="ajax_quote_product_txt_s{if $quoteTotalProducts < 2} unvisible{/if}">{l s='Products' mod='askforaquote'}</span>
                <span class="ajax_quote_no_product{if $quoteTotalProducts > 0} unvisible{/if}">{l s='(empty)' mod='askforaquote'}</span>
            </a>
            <div class="col-sm-12 quotes_cart_block exclusive" id="box-body" style="display:none;">
                <div class="block_content">
                    <div class="row product-list" id="product-list">
                        {if $quoteTotalProducts > 0}
                            <dl class="products" id="quotes-products">
                                {foreach $products as $key=>$product}
                                    {if is_numeric($key)}
                                        <dt class="item">
                                            <a class="cart-images" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.title|escape:'html':'UTF-8'}">
                                                <img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'cart_default')|escape:'html':'UTF-8'}" alt="{$product.title|escape:'html':'UTF-8'}">
                                            </a>
                                        <div class="cart-info">
                                            <div class="product-name">
                                                <span class="quantity-formated"><span class="quantity">{$product.quantity|intval}</span>&nbsp;x&nbsp;</span><a class="cart_block_product_name" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.title|escape:'html':'UTF-8'}">{$product.title|truncate:20:'...'|escape:'html':'UTF-8'}</a>
                                            </div>
                                            <div class="product-attr">
                                                <small>{$product.combinations|escape:'html':'UTF-8'}</small>
                                            </div>
											{if $MAIN_PRICE}
                                            <span class="price">
                                                {$product.unit_price|escape:'html':'UTF-8'}
                                            </span>
											{/if}
                                            <div class="remove-wrap">
                                                <hr/>
                                                <a href="javascript:void(0);" rel="{$product.id|intval}_{$product.id_attribute|intval}" class="remove-quote">{l s="Remove" mod='askforaquote'}</a>
                                            </div>
                                        </div>
                                        </dt>
                                    {/if}
                                {/foreach}
                            </dl>
							{if $MAIN_PRICE}
                            <div class="quotes-cart-prices">
                                <div class="row">
                                    <span class="col-xs-12 col-lg-6 text-center">{l s="Total:" mod='askforaquote'}</span>
                                    <span class="col-xs-12 col-lg-6 text-center">{$cart_total.total|escape:'html':'UTF-8'}</span>

                                </div>
                            </div>
							{/if}
                        {else}
                            <div class="alert">
                                {l s="No products to quote" mod='askforaquote'}
                            </div>
                        {/if}
                    </div>
                    <p class="cart-buttons">
                            {if $logged && $enable_submit && !$terms_page}
                                <a id="button_order_cart" class="btn btn-default button button-small submit_quote" href="javascript:void(0);" title="{l s='Submit quote' mod='askforaquote'}" rel="nofollow">
                                <span>
                                    {l s='Submit now' mod='askforaquote'}<i class="icon-chevron-right right"></i>
                                </span>
                                </a>
                            {/if}
                        <a id="button_order_cart" class="btn btn-default button button-small" href="{$quotesCart|escape:'html':'UTF-8'}" title="{l s='Submit quote' mod='askforaquote'}" rel="nofollow">
                            <span>
                                {l s='View list' mod='askforaquote'}<i class="icon-chevron-right right"></i>
                            </span>
                        </a>

                    </p>
                </div>
            </div>
        </div>
    </div>


{elseif $active_overlay == 1}
	<div id="quotes_layer_cart">
		<div class="clearfix">
            <h2><i class="icon-ok-circle"></i> {l s='Product successfully added to your quotes cart' mod='askforaquote'}</h2>
			<div class="quotes_layer_cart_product col-xs-12 col-md-6">
				<span class="cross" title="{l s='Close window' mod='askforaquote'}"></span>
                {if $popup_product}
                    <div class="product-image-container layer_cart_img">
                        <img class="layer_cart_img img-responsive" src="{$link->getImageLink($popup_product.link_rewrite, $popup_product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$popup_product.name|escape:'html':'UTF-8'}" title="{$popup_product.name|escape:'html':'UTF-8'}">
                    </div>
                    <div class="layer_cart_product_info">
                        <span class="product-name">{$popup_product.name|escape:'html':'UTF-8'}</span>
                        <div>
                            <strong class="dark">{l s='Quantity' mod='askforaquote'}</strong>
                            <span id="layer_cart_product_quantity">{$popup_product.quantity|intval}</span>
                        </div>
						{if $MAIN_PRICE}
                        <div>
                            <strong class="dark">{l s='Total' mod='askforaquote'}:</strong>
                            <span id="layer_cart_product_price">{$popup_product.price_total|escape:'html':'UTF-8'}</span>
                        </div>
						{/if}
                    </div>
                {/if}
			</div>
			<div class="quotes_layer_cart_cart col-xs-12 col-md-6">
				<br/>
				<hr/>
				<div class="button-container">
					<span class="continue btn btn-default button exclusive-medium" title="{l s='Continue shopping' mod='askforaquote'}">
						<span>
							<i class="icon-chevron-left left"></i>{l s='Continue shopping' mod='askforaquote'}
						</span>
					</span>
					{if $enablePopSubmit && $logged && !$terms_page}
						<a id="button_order_cart" class="btn btn-default button button-medium submit_quote" href="javascript:void(0);" title="{l s='Submit quote' mod='askforaquote'}" rel="nofollow">
							<span>
								{l s='Submit now' mod='askforaquote'}<i class="icon-chevron-right right"></i>
							</span>
						</a>
					{else}
						<a class="btn btn-default button button-medium"	href="{$link->getModuleLink('askforaquote','QuotesCart')|escape:'html':'UTF-8'}" title="{l s='Proceed to checkout' mod='askforaquote'}" rel="nofollow">
							<span>
								{l s='Proceed to checkout' mod='askforaquote'}<i class="icon-chevron-right right"></i>
							</span>
						</a>
					{/if}
				</div>
				<hr/>
			</div>
		</div>
		<div class="crossseling"></div>
	</div> <!-- #layer_cart -->
	<div class="quotes_layer_cart_overlay"></div>

{/if}
<!-- /MODULE Quotes cart -->
{strip}
    {addJsDef quotesCart=$actionAddQuotes}
    {addJsDef catalogMode=$PS_CATALOG_MODE|intval}
    {addJsDef terms_page=$terms_page|intval}
    {addJsDef messagingEnabled=$MESSAGING_ENABLED|intval}
{/strip}
