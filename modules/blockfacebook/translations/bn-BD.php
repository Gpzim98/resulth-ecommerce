<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockfacebook}prestashop>blockfacebook_43d541d80b37ddb75cde3906b1ded452'] = '';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_e2887a32ddafab9926516d8cb29aab76'] = '';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'কনফিগারেশন আপডেট হয়েছে ';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_f4f70727dc34561dfde1a3c529b6205c'] = 'সেটিংস';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_c98cf18081245e342d7929c117066f0b'] = 'ফেইসবুক লিঙ্ক (সম্পূর্ণ লিঙ্ক প্রয়োজন) ';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_c9cc8cce247e49bae79f15173ce97354'] = 'সংরক্ষণ করুন';
$_MODULE['<{blockfacebook}prestashop>blockfacebook_374fe11018588d3d27e630b2dffb7909'] = '';
$_MODULE['<{blockfacebook}prestashop>preview_31fde7b05ac8952dacf4af8a704074ec'] = 'দেখুন';
$_MODULE['<{blockfacebook}prestashop>preview_374fe11018588d3d27e630b2dffb7909'] = '';


return $_MODULE;
