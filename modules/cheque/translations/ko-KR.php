<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{cheque}prestashop>cheque_7b4cc4f79be9aae43efd53b4ae5cba4d'] = '';
$_MODULE['<{cheque}prestashop>cheque_14e41f4cfd99b10766cc15676d8cda66'] = '';
$_MODULE['<{cheque}prestashop>cheque_e09484ba6c16bc20236b63cc0d87ee95'] = '고객님의 신상정보를 삭제 하시겠습니까?';
$_MODULE['<{cheque}prestashop>cheque_32776feb26ff6f9648054e796aa0e423'] = '';
$_MODULE['<{cheque}prestashop>cheque_a02758d758e8bec77a33d7f392eb3f8a'] = '이 모듈은 비통화로 이루어져 있습니다.';
$_MODULE['<{cheque}prestashop>cheque_81c6c3ba23ca2657a8eedc561f865ddb'] = '';
$_MODULE['<{cheque}prestashop>cheque_00a369029140cfd18857425d49b472f8'] = '';
$_MODULE['<{cheque}prestashop>cheque_c888438d14855d7d96a2724ee9c306bd'] = '설정 업데이트';
$_MODULE['<{cheque}prestashop>cheque_a60468657881aa431a0a5fccc76258e2'] = '';
$_MODULE['<{cheque}prestashop>cheque_5dd532f0a63d89c5af0243b74732f63c'] = '연락처 세부 사항';
$_MODULE['<{cheque}prestashop>cheque_4b2f62e281e9a6829c6df0e87d34233a'] = '';
$_MODULE['<{cheque}prestashop>cheque_dd7bf230fde8d4836917806aff6a6b27'] = '주소';
$_MODULE['<{cheque}prestashop>cheque_0fe62049ad5246bc188ec1bae347269e'] = '';
$_MODULE['<{cheque}prestashop>cheque_c9cc8cce247e49bae79f15173ce97354'] = '저장';
$_MODULE['<{cheque}prestashop>validation_e2b7dec8fa4b498156dfee6e4c84b156'] = '이 결제방법은 사용할 수 없습니다';
$_MODULE['<{cheque}prestashop>payment_execution_644818852b4dd8cf9da73543e30f045a'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_6ff063fbc860a79759a7369ac32cee22'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_8520b283b0884394b13b80d5689628b3'] = '수표 지물';
$_MODULE['<{cheque}prestashop>payment_execution_f1d3b424cd68795ecaa552883759aceb'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_879f6b8877752685a966564d072f498f'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_060bf2d587991d8f090a1309b285291c'] = '수표';
$_MODULE['<{cheque}prestashop>payment_execution_76ca011e4772bfcce26aecd42c598510'] = '고객님은 수표로 지불을 선택했습니다.';
$_MODULE['<{cheque}prestashop>payment_execution_c884ed19483d45970c5bf23a681e2dd2'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_3b3b41f131194e747489ef93e778ed0d'] = '주문 총액 입니다';
$_MODULE['<{cheque}prestashop>payment_execution_1f87346a16cf80c372065de3c54c86d9'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_7b1c6e78d93817f61f2b1bbc2108a803'] = '여러가지 통화를 수표로 인정합니다.';
$_MODULE['<{cheque}prestashop>payment_execution_a7a08622ee5c8019b57354b99b7693b2'] = '결제 화폐를 선택해 주십시오. :';
$_MODULE['<{cheque}prestashop>payment_execution_f73ad0f08052884ff465749bf48b55ce'] = '위 주문 총 금액을 결제할 수 있는 통화:';
$_MODULE['<{cheque}prestashop>payment_execution_7135ff14c7931e1c8e9d33aff3dfc7f7'] = '다음 페이지에는 수표 소유자와 주소지 정보를 명시 합니다';
$_MODULE['<{cheque}prestashop>payment_execution_52f64bc0164b0e79deaeaaaa7e93f98f'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_46b9e3665f187c739c55983f757ccda0'] = '주문확인';
$_MODULE['<{cheque}prestashop>payment_execution_569fd05bdafa1712c4f6be5b153b8418'] = '이전화면';
$_MODULE['<{cheque}prestashop>infos_14e41f4cfd99b10766cc15676d8cda66'] = '';
$_MODULE['<{cheque}prestashop>infos_e444fe40d43bccfad255cf62ddc8d18f'] = '';
$_MODULE['<{cheque}prestashop>infos_8c88bbf5712292b26e2a6bbeb0a7b5c4'] = '';
$_MODULE['<{cheque}prestashop>payment_return_88526efe38fd18179a127024aba8c1d7'] = '';
$_MODULE['<{cheque}prestashop>payment_return_61da27a5dd1f8ced46c77b0feaa9e159'] = '과 함께 저희에게 보내 주시시오';
$_MODULE['<{cheque}prestashop>payment_return_621455d95c5de701e05900a98aaa9c66'] = '';
$_MODULE['<{cheque}prestashop>payment_return_9b8f932b1412d130ece5045ecafd1b42'] = '순서에 따른 지불 만기';
$_MODULE['<{cheque}prestashop>payment_return_9a94f1d749a3de5d299674d6c685e416'] = '전자메일';
$_MODULE['<{cheque}prestashop>payment_return_e1c54fdba2544646684f41ace03b5fda'] = '';
$_MODULE['<{cheque}prestashop>payment_return_4761b03b53bc2b3bd948bb7443a26f31'] = '';
$_MODULE['<{cheque}prestashop>payment_return_610abe74e72f00210e3dcb91a0a3f717'] = '다음 정보로 고객님께 전자 메일을 보내 드렸습니다';
$_MODULE['<{cheque}prestashop>payment_return_ffd2478830ca2f519f7fe7ee259d4b96'] = '고객님이 지불 하시는 대로 고객님의 주문 상품을 발송 합니다';
$_MODULE['<{cheque}prestashop>payment_return_0db71da7150c27142eef9d22b843b4a9'] = '배송, 결제, 상품에 대한 궁금하신 점이 있으시면 언제든지 문의 주시기 바랍니다.';
$_MODULE['<{cheque}prestashop>payment_return_decce112a9e64363c997b04aa71b7cb8'] = '고객지원';
$_MODULE['<{cheque}prestashop>payment_return_9bdf695c5a30784327137011da6ef568'] = '고객님의 주문에문제가 있어 통지를 헀습니다. 만약 고객님이 이 통지가 잘못된거라 생각하신다면, 저의에게 연락을 주십시오';
$_MODULE['<{cheque}prestashop>payment_4b80fae2153218ed763bdadc418e8589'] = '수표로 지불';
$_MODULE['<{cheque}prestashop>payment_4e1fb9f4b46556d64db55d50629ee301'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_0881a11f7af33bc1b43e437391129d66'] = '주문확인을 눌러 페이팔 결제창으로 이동합니다.';
$_MODULE['<{cheque}prestashop>payment_f05fd8637f8a6281466a507fcb56baec'] = '수표로 지불';


return $_MODULE;
