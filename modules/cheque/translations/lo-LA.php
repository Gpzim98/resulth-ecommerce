<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{cheque}prestashop>cheque_7b4cc4f79be9aae43efd53b4ae5cba4d'] = '';
$_MODULE['<{cheque}prestashop>cheque_14e41f4cfd99b10766cc15676d8cda66'] = '';
$_MODULE['<{cheque}prestashop>cheque_e09484ba6c16bc20236b63cc0d87ee95'] = '';
$_MODULE['<{cheque}prestashop>cheque_32776feb26ff6f9648054e796aa0e423'] = '';
$_MODULE['<{cheque}prestashop>cheque_a02758d758e8bec77a33d7f392eb3f8a'] = 'ບໍ່ມີສະກຸນເງິນຖືກຕັ້ງໄວ້ສຳລັບໂມດູນນີ້';
$_MODULE['<{cheque}prestashop>cheque_81c6c3ba23ca2657a8eedc561f865ddb'] = '';
$_MODULE['<{cheque}prestashop>cheque_00a369029140cfd18857425d49b472f8'] = '';
$_MODULE['<{cheque}prestashop>cheque_c888438d14855d7d96a2724ee9c306bd'] = 'ອັບເດດການຕັ້ງຄ່າ';
$_MODULE['<{cheque}prestashop>cheque_a60468657881aa431a0a5fccc76258e2'] = '';
$_MODULE['<{cheque}prestashop>cheque_5dd532f0a63d89c5af0243b74732f63c'] = 'ລາຍລະອຽດການຕິດຕໍ່';
$_MODULE['<{cheque}prestashop>cheque_4b2f62e281e9a6829c6df0e87d34233a'] = '';
$_MODULE['<{cheque}prestashop>cheque_dd7bf230fde8d4836917806aff6a6b27'] = 'ທີ່ຢູ່';
$_MODULE['<{cheque}prestashop>cheque_0fe62049ad5246bc188ec1bae347269e'] = '';
$_MODULE['<{cheque}prestashop>cheque_c9cc8cce247e49bae79f15173ce97354'] = 'ບັນທຶກ';
$_MODULE['<{cheque}prestashop>validation_e2b7dec8fa4b498156dfee6e4c84b156'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_644818852b4dd8cf9da73543e30f045a'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_6ff063fbc860a79759a7369ac32cee22'] = 'ຄິດໄລ່ເງິນ';
$_MODULE['<{cheque}prestashop>payment_execution_8520b283b0884394b13b80d5689628b3'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_f1d3b424cd68795ecaa552883759aceb'] = 'ສະຫຼຸບຄຳສັ່ງຊື້';
$_MODULE['<{cheque}prestashop>payment_execution_879f6b8877752685a966564d072f498f'] = 'ກະຕ່າຂອງເຈົ້າເປົ່າຫວ່າງ';
$_MODULE['<{cheque}prestashop>payment_execution_060bf2d587991d8f090a1309b285291c'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_76ca011e4772bfcce26aecd42c598510'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_c884ed19483d45970c5bf23a681e2dd2'] = 'ນີ້ແມ່ນສະຫຼຸບໂດຍຫຍໍ້ຂອງຄຳສ່ັງຊື້ຂອງເຈົ້າ:';
$_MODULE['<{cheque}prestashop>payment_execution_3b3b41f131194e747489ef93e778ed0d'] = 'ຈຳນວນລວມຂອງຄຳສັ່່ງຊື້ເຈົ້າແມ່ນ';
$_MODULE['<{cheque}prestashop>payment_execution_1f87346a16cf80c372065de3c54c86d9'] = '( ລວມຄ່າພາສີ )';
$_MODULE['<{cheque}prestashop>payment_execution_7b1c6e78d93817f61f2b1bbc2108a803'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_a7a08622ee5c8019b57354b99b7693b2'] = 'ເລືອກອັນໜຶ່ງຢູ່ຂ້າງລຸ່ມ:';
$_MODULE['<{cheque}prestashop>payment_execution_f73ad0f08052884ff465749bf48b55ce'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_7135ff14c7931e1c8e9d33aff3dfc7f7'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_52f64bc0164b0e79deaeaaaa7e93f98f'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_46b9e3665f187c739c55983f757ccda0'] = 'ຂ້ອຍຢືນຢັນຄຳສັ່ງຊື້ຂອງຂ້ອຍ';
$_MODULE['<{cheque}prestashop>payment_execution_569fd05bdafa1712c4f6be5b153b8418'] = 'ວິທີການຈ່າຍເງິນແບບອື່ນໆ.';
$_MODULE['<{cheque}prestashop>infos_14e41f4cfd99b10766cc15676d8cda66'] = '';
$_MODULE['<{cheque}prestashop>infos_e444fe40d43bccfad255cf62ddc8d18f'] = '';
$_MODULE['<{cheque}prestashop>infos_8c88bbf5712292b26e2a6bbeb0a7b5c4'] = '';
$_MODULE['<{cheque}prestashop>payment_return_88526efe38fd18179a127024aba8c1d7'] = '';
$_MODULE['<{cheque}prestashop>payment_return_61da27a5dd1f8ced46c77b0feaa9e159'] = '';
$_MODULE['<{cheque}prestashop>payment_return_621455d95c5de701e05900a98aaa9c66'] = '';
$_MODULE['<{cheque}prestashop>payment_return_9b8f932b1412d130ece5045ecafd1b42'] = '';
$_MODULE['<{cheque}prestashop>payment_return_9a94f1d749a3de5d299674d6c685e416'] = '';
$_MODULE['<{cheque}prestashop>payment_return_e1c54fdba2544646684f41ace03b5fda'] = '';
$_MODULE['<{cheque}prestashop>payment_return_4761b03b53bc2b3bd948bb7443a26f31'] = '';
$_MODULE['<{cheque}prestashop>payment_return_610abe74e72f00210e3dcb91a0a3f717'] = 'ອີເມວໄດ້ສົ່ງໄປຫາເຈົ້າພ້ອມກັບຂໍ້ມູນນີ້.';
$_MODULE['<{cheque}prestashop>payment_return_ffd2478830ca2f519f7fe7ee259d4b96'] = '';
$_MODULE['<{cheque}prestashop>payment_return_0db71da7150c27142eef9d22b843b4a9'] = 'ສຳລັບຄຳຖາມໃດໆ ຫຼື ຂໍ້ມູນເພີ່ມເຕີມ, ກະລຸນາຕິດຕໍ່ຫາພວກເຮົາ';
$_MODULE['<{cheque}prestashop>payment_return_decce112a9e64363c997b04aa71b7cb8'] = 'ໝ່ວຍງານສະໜັບສະໜຸນຊ່ວຍເຫຼືອລູກຄ້າ';
$_MODULE['<{cheque}prestashop>payment_return_9bdf695c5a30784327137011da6ef568'] = 'ພວກເຮົາຂໍແຈ້ງບັນຫາກັບຄຳສັ່ງຊື້ຂອງເຈົ້າ. ຖ້າເຈົ້າຄິດວ່ານີ້ແມ່ນຂໍ້ຜິດພາດ, ເຈົ້າສາມາດຕິດຕໍ່ພວກເຮົາ';
$_MODULE['<{cheque}prestashop>payment_4b80fae2153218ed763bdadc418e8589'] = '';
$_MODULE['<{cheque}prestashop>payment_4e1fb9f4b46556d64db55d50629ee301'] = '';
$_MODULE['<{cheque}prestashop>payment_execution_0881a11f7af33bc1b43e437391129d66'] = 'ກະລຸນາຢັ້ງຢືນຄຳສັ່ງຊື້ຂອງເຈົ້າໂດຍການກົດ \'ຂ້ອຍຂໍຢັ້ງຢືນຄຳສັ່ງຊື້ຂອງຂ້ອຍ\'';
$_MODULE['<{cheque}prestashop>payment_f05fd8637f8a6281466a507fcb56baec'] = '';


return $_MODULE;
