<?php
include(dirname(__FILE__).'/../../config/config.inc.php');

if (isset($_GET['validate_cpf'])) {
	$ret = Validate::isCPF($_GET['validate_cpf']);
	if ($ret !== true) {
		echo json_encode(array('valid' => false, 'erro' => $ret));
	} else {
		echo json_encode(array('valid' => true));
	}


} else if (isset($_GET['validate_cnpj'])) {
	$ret = Validate::isCNPJ($_GET['validate_cnpj']);
	if ($ret !== true) {
		echo json_encode(array('valid' => false, 'erro' => $ret));
	} else {
		echo json_encode(array('valid' => true));
	}

} else {
	echo json_encode(array('valid' => false, 'erro' => 'Não foi iformado o tipo de validação.'));
}
