
/** Processa os campos al carragar a página */
jQuery().ready(function() {
    $("#cpf").unmask().mask("999.999.999-99");
    $("#siret").unmask().mask("99.999.999/9999-99");
    getSpanErro("#ape").html("(Inscrição Estadual, CFDF ou 'ISENTO')");

    $("#cpf").live("blur", function(){
        validaCpfCnpj($(this), "cpf");
    });
    $("#siret").live("blur", function(){
        validaCpfCnpj($(this), "cnpj");
    });

    $("#ape").live("blur", function(){
        var valor = $.trim($(this).val());
        if (valor.toUpperCase() == "ISENTO") {
          valor = valor.toUpperCase();
        } else {
          valor = valor.replace(/[^\d\.]/g, '');
        }

        $(this).val(valor);
    });
});

function getSpanErro(campo){
  var $name = campo.replace(/[^a-z0-9]/gi, "") + "_error";
  var $span = $(campo).siblings("small." + $name);

  /** Se não econtrou a span, então cria */
  if ($span.length == 0) {
    $span = $("<small />");
    $span.addClass($name);

    $(campo).after($span);
  }
  return $span;
}

function validaCpfCnpj(campo, tipo){
    var campoId = "#" + campo.attr("id");
    getSpanErro(campoId).css("color","orange");
    getSpanErro(campoId).html("Aguarde, validando...");

    var retorno = undefined;
    $.ajax({
        url : "../modules/atsmodule/validate.php?validate_" + tipo + "=" + $.trim(campo.val()),
        context : document.body,
        async : false,
        dataType : "JSON"
    }).done(function(result){
        retorno = result;
    });

    if (retorno != undefined && retorno.valid) {
        getSpanErro(campoId).css("color","green");
        getSpanErro(campoId).html(tipo.toUpperCase() + " válido");
    } else {
        getSpanErro(campoId).css("color","red");
        getSpanErro(campoId).html(retorno != undefined && retorno.erro != undefined ?
            retorno.erro : tipo.toUpperCase() + " inválido.");
    }
    return retorno;
}
