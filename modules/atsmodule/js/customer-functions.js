/**
 * TODO: Esse é um script genérico, cuidado com implementações especificas

 * Inputs, isso permite que seja configurado externamente
 */
var CUSTOMER_PARAM = $.extend({
    NOME_INPUT          : "input#firstname",
    SOBRENOME_INPUT     : "input#lastname",
    RG_INPUT            : "input#rg",
    CPF_INPUT           : "input#cpf",
    CPF_ALERT           : "span#alertcpf",
    CNPJ_INPUT          : "input#siret",
    CNPJ_ALERT          : "span#alertsiret",
    IE_INPUT            : "input#ape",
    TIPO_RADIO          : "input[type|='radio'][name|='tipo_pessoa']",
    ADDR_CEP_INPUT      : "input#postcode",
    ADDR_CEP_ALERT      : "span#alertcep",
    ADDR_RUA_INPUT      : "input#address1",
    ADDR_NUMERO_INPUT   : "input#numero",
    ADDR_BAIRRO_INPUT   : "input#address2",
    ADDR_CIDADE_INPUT   : "input#city",
    ADDR_ESTADO_SELECT  : "select#id_state",
    B2B_DIV_CLASS       : "div.b2b_information",
    BUTTON_SUBMIT       : "#submitAccount"
}, CUSTOMER_PARAM);

jQuery().ready(function() {
    if ($(CUSTOMER_PARAM.CPF_INPUT).length > 0) {
        $(CUSTOMER_PARAM.CPF_INPUT).unmask().mask("999.999.999-99");
        $(CUSTOMER_PARAM.CPF_ALERT).html("");

        $(CUSTOMER_PARAM.CPF_INPUT).live("blur", function(){
            validaCpfCnpj($(this).val(), "cpf", CUSTOMER_PARAM.CPF_ALERT);
        });
    }

    if ($(CUSTOMER_PARAM.CNPJ_INPUT).length > 0) {
        $(CUSTOMER_PARAM.CNPJ_INPUT).unmask().mask("99.999.999/9999-99");
        $(CUSTOMER_PARAM.CNPJ_ALERT).html("");

        $(CUSTOMER_PARAM.CNPJ_INPUT).live("blur", function(){
            validaCpfCnpj($(this).val(), "cnpj", CUSTOMER_PARAM.CNPJ_ALERT);
        });
    }

    if ($(CUSTOMER_PARAM.IE_INPUT).length > 0) {
        $(CUSTOMER_PARAM.IE_INPUT).live("blur", function(){
            var valor = $.trim($(this).val());
            if (valor.toUpperCase() == "ISENTO") {
              valor = valor.toUpperCase();
            } else {
              valor = valor.replace(/[^\d\.]/g, '');
            }

            $(this).val(valor);
        });
    }

    if ($(CUSTOMER_PARAM.TIPO_RADIO).length > 0) {
        $(CUSTOMER_PARAM.TIPO_RADIO).live("change", function(){
            processaB2b($(this).val());
        })

        processaB2b($(CUSTOMER_PARAM.TIPO_RADIO + ":checked").val());
    } else {
        processaB2b();
    }

    if ($(CUSTOMER_PARAM.ADDR_CEP_INPUT).length > 0) {
        $(CUSTOMER_PARAM.ADDR_CEP_INPUT).live("blur", function(){
            getAddressByCep();
        });
    }
});

function processaB2b (tipo) {
    var add = false;
    if (tipo === "juridica") {
        $(CUSTOMER_PARAM.B2B_DIV_CLASS).removeClass("hide");
        add = true;
    } else {
        $(CUSTOMER_PARAM.B2B_DIV_CLASS).addClass("hide");
    }

    addRemetenteLabel(CUSTOMER_PARAM.CPF_INPUT, "CPF", add);
    addRemetenteLabel(CUSTOMER_PARAM.RG_INPUT, "RG", add);
    addRemetenteLabel(CUSTOMER_PARAM.NOME_INPUT, "Nome", add);
    addRemetenteLabel(CUSTOMER_PARAM.SOBRENOME_INPUT, "Sobrenome", add);
}

function addRemetenteLabel(campo, labelOriginal, add){
    var label = $(campo).prev("label");
    var oldText = labelOriginal + (!add ? ' do representante' : '');
    var newText = labelOriginal + (add ? ' do representante' : '');
    var fullText = label.html();

    if (typeof(fullText) == "string") {
        fullText = fullText.replace(new RegExp(oldText, 'i'), newText);
        label.html(fullText);
    }
}

function validaCpfCnpj(valor, campo, spanAlerta){
    $(spanAlerta).css("color","orange");
    $(spanAlerta).html("Aguarde, validando...");

    valor = valor.replace(/[^0-9]/g, "");

    var retorno = undefined;
    $.ajax({
        url : "modules/atsmodule/validate.php?validate_" + campo + "=" + $.trim(valor),
        context : document.body,
        async : false,
        dataType : "JSON"
    }).done(function(result){
        retorno = result;
    });

    if (retorno !== undefined && retorno.valid) {
        $(spanAlerta).css("color","green");
        $(spanAlerta).html(campo.toUpperCase() + " válido");
        $(CUSTOMER_PARAM.BUTTON_SUBMIT + ':disabled').removeAttr('disabled');
    } else {
        $(spanAlerta).css("color","red");
        $(spanAlerta).html(retorno != undefined && retorno.erro != undefined ?
            retorno.erro : campo.toUpperCase() + " inválido.");
    }
    return retorno;
}

/**
 * Busca o endereço pelo cep, essa função só deve ser usada no address.tpl
 * Função extraida e modificado do módulo "CPF Module"
 */
function getAddressByCep() {
    if($.trim($(CUSTOMER_PARAM.ADDR_CEP_INPUT).val()) != "") {
        $(CUSTOMER_PARAM.ADDR_CEP_ALERT).css("color","orange");
        $(CUSTOMER_PARAM.ADDR_CEP_ALERT).html("Aguarde, validando...");

        var cep = $(CUSTOMER_PARAM.ADDR_CEP_INPUT).val().replace(/[^0-9]/g, "");
        var url = "http://cep.republicavirtual.com.br/web_cep.php?"
            + "formato=json&cep=" + cep;

        $.ajax({
            url: url,
            dataType: "json",
            success: function(json){
                if (!json || !json.resultado) {
                    $(CUSTOMER_PARAM.ADDR_CEP_ALERT).css("color","red");
                    $(CUSTOMER_PARAM.ADDR_CEP_ALERT).html("CEP não encontrado."
                        + "Verifique, por favor.");
                } else {

                    $(CUSTOMER_PARAM.ADDR_CEP_ALERT).css("color","green");
                    $(CUSTOMER_PARAM.ADDR_CEP_ALERT).html("");
                    $(CUSTOMER_PARAM.ADDR_RUA_INPUT).val(unescape(json.tipo_logradouro)
                                    + "  " + unescape(json.logradouro));
                    $(CUSTOMER_PARAM.ADDR_BAIRRO_INPUT).val(unescape(json.bairro));
                    $(CUSTOMER_PARAM.ADDR_CIDADE_INPUT).val(unescape(json.cidade));

                    if(json.uf == "AC")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("313");
                    if(json.uf == "AL")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("314");
                    if(json.uf == "AP")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("315");
                    if(json.uf == "AM")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("316");
                    if(json.uf == "BA")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("317");
                    if(json.uf == "CE")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("318");
                    if(json.uf == "ES")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("319");
                    if(json.uf == "GO")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("320");
                    if(json.uf == "MA")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("321");
                    if(json.uf == "MT")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("322");
                    if(json.uf == "MS")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("323");
                    if(json.uf == "MG")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("324");
                    if(json.uf == "PA")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("325");
                    if(json.uf == "PB")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("326");
                    if(json.uf == "PR")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("327");
                    if(json.uf == "PE")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("328");
                    if(json.uf == "PI")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("329");
                    if(json.uf == "RJ")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("330");
                    if(json.uf == "RN")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("331");
                    if(json.uf == "RS")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("332");
                    if(json.uf == "RO")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("333");
                    if(json.uf == "RR")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("334");
                    if(json.uf == "SC")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("335");
                    if(json.uf == "SP")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("336");
                    if(json.uf == "SE")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("337");
                    if(json.uf == "TO")$(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).val("338");
                    $(CUSTOMER_PARAM.ADDR_ESTADO_SELECT).click(); // Força atualizar

                    $(CUSTOMER_PARAM.ADDR_NUMERO_INPUT).focus();
                }
            },
            error: function(){
                $(CUSTOMER_PARAM.ADDR_CEP_ALERT).css("color","red");
                $(CUSTOMER_PARAM.ADDR_CEP_ALERT).html("Falha ao buscar CEP.");
            }
        });
   }
}
