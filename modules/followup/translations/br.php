<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{followup}prestashop>followup_9c34e90380dac7b56fdd19192a99d531'] = 'Acompanhamento do cliente';
$_MODULE['<{followup}prestashop>followup_57476355fcd04050bff196ae9aa4673c'] = 'Entre em contato diariamente com os seus clientes através de e-mails personalizados.';
$_MODULE['<{followup}prestashop>followup_f71a41841c80c2ef0ec02a6ad5449c65'] = 'Tem certeza de que deseja apagar todas as configurações e seus registros?';
$_MODULE['<{followup}prestashop>followup_e316b4398212d473f7f53c7728fe1c37'] = 'Configurações atualizadas';
$_MODULE['<{followup}prestashop>followup_003b06dcfe67596f17e3de26e6a436c8'] = 'Ocorreu um erro durante a atualização das configurações';
$_MODULE['<{followup}prestashop>followup_edf9feeab43a7623f6afc152d1ede515'] = 'Desconto para seu carrinho de compras cancelado';
$_MODULE['<{followup}prestashop>followup_a053fc9952a7dfc79282eba56ab8ad3a'] = 'Obrigado pelo pedido.';
$_MODULE['<{followup}prestashop>followup_7fcd592dd47028c7c1f2d0ce9168a303'] = 'Você é um dos nossos melhores clientes!';
$_MODULE['<{followup}prestashop>followup_c2ab23672d4bb31c7664bf8e854a10f7'] = 'Estamos com saudades!';
$_MODULE['<{followup}prestashop>followup_4d469abf7834b49015e753c9578a1934'] = 'Defina as configurações e insira a seguinte URL em seu CRON, ou acesse-a manualmente todos os dias:';
$_MODULE['<{followup}prestashop>followup_c4c95c36570d5a8834be5e88e2f0f6b2'] = 'Informações';
$_MODULE['<{followup}prestashop>followup_87b1e57b5e8cd700618b6ae4938a4023'] = 'Quatro tipos de alertas de e-mail disponíveis para manter contato com seus clientes!';
$_MODULE['<{followup}prestashop>followup_b547c073d144a57761d1d00d0b9d9f27'] = 'Carrinhos cancelados';
$_MODULE['<{followup}prestashop>followup_ea9dc62a0ddf4640f019f04a22ab9835'] = 'Para cada carrinho cancelado (sem um pedido), gerar um cupom de desconto e enviá-lo ao cliente.';
$_MODULE['<{followup}prestashop>followup_2faec1f9f8cc7f8f40d521c4dd574f49'] = 'Ativar';
$_MODULE['<{followup}prestashop>followup_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Ativado';
$_MODULE['<{followup}prestashop>followup_b9f5c797ebbf55adccdd8539a65a0241'] = 'Desativado';
$_MODULE['<{followup}prestashop>followup_b30690be173bcd4a83df0cd68f89a385'] = 'Valor do Desconto';
$_MODULE['<{followup}prestashop>followup_2018945d252b94aeb99b0909f288717c'] = 'Validade do desconto';
$_MODULE['<{followup}prestashop>followup_225e75c29d32392d311f5dc94c792384'] = 'dia(s)';
$_MODULE['<{followup}prestashop>followup_f120254f109d626d73ddddeb9cda26e5'] = 'Próximo processo enviará: %d e-mails';
$_MODULE['<{followup}prestashop>followup_c9cc8cce247e49bae79f15173ce97354'] = 'Salvar';
$_MODULE['<{followup}prestashop>followup_a8b8dbd070a92fb8b17baab71d8d633f'] = 'Refazer pedido';
$_MODULE['<{followup}prestashop>followup_895858cf10b8a1750a42875cb9c69092'] = 'Para cada pedido validado, gerar um cupom de desconto e enviá-lo ao cliente.';
$_MODULE['<{followup}prestashop>followup_8b83489bd116cb60e2f348e9c63cd7f6'] = 'Melhores clientes';
$_MODULE['<{followup}prestashop>followup_a46ad892f7f00e051cc90050ff2e1ddf'] = 'Para cada cliente aumentando o limite, geram um cupom de desconto e enviá-lo ao cliente.';
$_MODULE['<{followup}prestashop>followup_2a63f555989152ba866b43a1faacd680'] = 'Limite';
$_MODULE['<{followup}prestashop>followup_7d75b7b0f976b3091f490864c6ffbf97'] = 'Clientes ruins';
$_MODULE['<{followup}prestashop>followup_e552313de5ebebdafa8bfa0dcc703e19'] = 'Para cada cliente que já tenha realizado pelo menos um pedido e sem nenhum pedido em um determinado período, gerar um cupom de desconto e enviá-lo ao cliente.';
$_MODULE['<{followup}prestashop>followup_d82843c16839bfb05827d1912d89c917'] = 'A partir de x dias';
$_MODULE['<{followup}prestashop>followup_0db377921f4ce762c62526131097968f'] = 'Geral';
$_MODULE['<{followup}prestashop>followup_1d52a8fe6cab6930fad9d814a1b0ca4c'] = 'Excluir descontos vencidos em cada lançamento para limpar o banco de dados';
$_MODULE['<{followup}prestashop>stats_c33e404a441c6ba9648f88af3c68a1ca'] = 'Estatísticas';
$_MODULE['<{followup}prestashop>stats_1deaabda093f617a57f732c07e635fac'] = 'Estatísticas dos últimos 30 dias:';
$_MODULE['<{followup}prestashop>stats_f23b0eec6c25c49afa4b29c57e671728'] = 'E = Número de emails enviados';
$_MODULE['<{followup}prestashop>stats_fde0b9a66bffdd6ee0886613e8031d9a'] = 'U = Número de descontos utilizados (apenas pedidos válidos)';
$_MODULE['<{followup}prestashop>stats_cceb59e78d00c10436ff5e777dd5d895'] = '% = Taxa de conversão';
$_MODULE['<{followup}prestashop>stats_44749712dbec183e983dcd78a7736c41'] = 'Data';
$_MODULE['<{followup}prestashop>stats_b547c073d144a57761d1d00d0b9d9f27'] = 'Carrinhos cancelados';
$_MODULE['<{followup}prestashop>stats_a8b8dbd070a92fb8b17baab71d8d633f'] = 'Refazer pedido';
$_MODULE['<{followup}prestashop>stats_5e53dfa887dfb989f0bc3e9fb9b34a2d'] = 'Melhor custo';
$_MODULE['<{followup}prestashop>stats_66ff4e89c767ab0a4e5ddc0251a101bc'] = 'Custo ruim.';
$_MODULE['<{followup}prestashop>stats_5dbc98dcc983a70728bd082d1a47546e'] = 'S';
$_MODULE['<{followup}prestashop>stats_4c614360da93c0a041b22e537de151eb'] = 'U';
$_MODULE['<{followup}prestashop>stats_85b6769250887ba6c16099596c75164d'] = 'Não há estatísticas no momento.';


return $_MODULE;
