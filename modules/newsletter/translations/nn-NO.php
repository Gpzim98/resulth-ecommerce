<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{newsletter}prestashop>newsletter_ffb7e666a70151215b4c55c6268d7d72'] = 'Nyheitsbrev';
$_MODULE['<{newsletter}prestashop>newsletter_804a924e464fd21ed92f820224c4091d'] = 'Genererer ei .CSV-fil for massesending av e-post';
$_MODULE['<{newsletter}prestashop>newsletter_c3987e4cac14a8456515f0d200da04ee'] = 'Alle land';
$_MODULE['<{newsletter}prestashop>newsletter_fa01fd956e87307bce4c90a0de9b0437'] = 'Landet kunden kjem frå';
$_MODULE['<{newsletter}prestashop>newsletter_c0859b0a5241dff468da2a9a93c3284f'] = 'Betene eit filter for landet kunden kjem frå.';
$_MODULE['<{newsletter}prestashop>newsletter_2198f293f5e1e95dddeff819fbca0975'] = 'Påmeldt nyheitsbrevmottakarar';
$_MODULE['<{newsletter}prestashop>newsletter_99006a61d48499231e1be92241cf772a'] = 'Filter for påmeldt nyheitsbrevmottakarar.';
$_MODULE['<{newsletter}prestashop>newsletter_7e3a51a56ddd2846e21c33f05e0aea6f'] = 'Alle kunder';
$_MODULE['<{newsletter}prestashop>newsletter_39f7a3e2b56e9bfd753ba6325533a127'] = 'Abonnentar';
$_MODULE['<{newsletter}prestashop>newsletter_011d8c5d94f729f013963d856cd78745'] = 'Ikkje-abonnentar';
$_MODULE['<{newsletter}prestashop>newsletter_793ee192a9124cd6f529460eef17d3e5'] = 'Selvpåmeldte';
$_MODULE['<{newsletter}prestashop>newsletter_7b15c043d66fecc45c8752592aa38e38'] = 'Filtrer selvpåmeldte.';
$_MODULE['<{newsletter}prestashop>newsletter_82e5e0bc0f9c776c98253d569c111c0f'] = 'Ingen kunder funne med filter!';
$_MODULE['<{newsletter}prestashop>newsletter_644ecc2486a059ca16b001a77909bf40'] = 'CSV-filen er eksportert. %d kunder funnet.';
$_MODULE['<{newsletter}prestashop>newsletter_48e3d5f66961b621c78f709afcd7d437'] = 'Last ned fila';
$_MODULE['<{newsletter}prestashop>newsletter_faa1115fbab933e7178ee43ce1590977'] = 'HUSK: Opnar du CSV-fila i Excel må du velje å dekode med UTF-8.';
$_MODULE['<{newsletter}prestashop>newsletter_81573e0ea79138f02fd2cee94786d7e9'] = 'Feil: kan ikkje skrive';
$_MODULE['<{newsletter}prestashop>newsletter_73059f9530a1a37563150df4dea4bb70'] = 'Alle påmeldte';
$_MODULE['<{newsletter}prestashop>newsletter_2eb05fb68ae560f1f25f46422dcfe9c2'] = 'Påmeldte med konto';
$_MODULE['<{newsletter}prestashop>newsletter_f821fd15db9c1d946a449837c291fc68'] = 'Påmeldte uten konto';
$_MODULE['<{newsletter}prestashop>newsletter_4713ef5f2d6fc1e8f088c850e696a04b'] = 'Eksporter kunder';
$_MODULE['<{newsletter}prestashop>newsletter_dbb392a2dc9b38722e69f6032faea73e'] = 'Eksporter .CSV-fil';


return $_MODULE;
