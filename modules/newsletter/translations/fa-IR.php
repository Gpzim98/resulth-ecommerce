<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{newsletter}prestashop>newsletter_ffb7e666a70151215b4c55c6268d7d72'] = 'خبرنامه';
$_MODULE['<{newsletter}prestashop>newsletter_804a924e464fd21ed92f820224c4091d'] = 'تولید یک فایل CSV برای ارسال انبوه ایمیل';
$_MODULE['<{newsletter}prestashop>newsletter_c3987e4cac14a8456515f0d200da04ee'] = 'همه کشورها';
$_MODULE['<{newsletter}prestashop>newsletter_fa01fd956e87307bce4c90a0de9b0437'] = 'کشور مشتری';
$_MODULE['<{newsletter}prestashop>newsletter_c0859b0a5241dff468da2a9a93c3284f'] = 'اعمال فیلتر بر روی کشور مشتری.';
$_MODULE['<{newsletter}prestashop>newsletter_2198f293f5e1e95dddeff819fbca0975'] = 'مشترکین خبرنامه';
$_MODULE['<{newsletter}prestashop>newsletter_99006a61d48499231e1be92241cf772a'] = 'فیلتر کردن مشترک های خبرنامه.';
$_MODULE['<{newsletter}prestashop>newsletter_7e3a51a56ddd2846e21c33f05e0aea6f'] = 'تمامی مشتریان';
$_MODULE['<{newsletter}prestashop>newsletter_39f7a3e2b56e9bfd753ba6325533a127'] = 'مشترکین';
$_MODULE['<{newsletter}prestashop>newsletter_011d8c5d94f729f013963d856cd78745'] = 'غیر مشترکین';
$_MODULE['<{newsletter}prestashop>newsletter_793ee192a9124cd6f529460eef17d3e5'] = 'مشترک های انتخاب شده';
$_MODULE['<{newsletter}prestashop>newsletter_7b15c043d66fecc45c8752592aa38e38'] = 'مشترک های انتخاب شده فیلتر شده.';
$_MODULE['<{newsletter}prestashop>newsletter_82e5e0bc0f9c776c98253d569c111c0f'] = 'هیچ مشتری با این فیلترها پیدا نشد!';
$_MODULE['<{newsletter}prestashop>newsletter_644ecc2486a059ca16b001a77909bf40'] = '';
$_MODULE['<{newsletter}prestashop>newsletter_48e3d5f66961b621c78f709afcd7d437'] = 'دانلود فایل';
$_MODULE['<{newsletter}prestashop>newsletter_faa1115fbab933e7178ee43ce1590977'] = 'هشدار:  اگر این فایل csv. را با Excel باز می کنید، فراموش نکنید که از انکودینگ  UTF-8 استفاده کنید در غیر این صورت کاراکترهای نامشخصی خواهید دید.';
$_MODULE['<{newsletter}prestashop>newsletter_81573e0ea79138f02fd2cee94786d7e9'] = 'خطا: عدم توانایی نوشتن بر روی';
$_MODULE['<{newsletter}prestashop>newsletter_73059f9530a1a37563150df4dea4bb70'] = '';
$_MODULE['<{newsletter}prestashop>newsletter_2eb05fb68ae560f1f25f46422dcfe9c2'] = '';
$_MODULE['<{newsletter}prestashop>newsletter_f821fd15db9c1d946a449837c291fc68'] = '';
$_MODULE['<{newsletter}prestashop>newsletter_4713ef5f2d6fc1e8f088c850e696a04b'] = 'صدور مشترکن';
$_MODULE['<{newsletter}prestashop>newsletter_dbb392a2dc9b38722e69f6032faea73e'] = 'صدور فایل csv';


return $_MODULE;
