<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{newsletter}prestashop>newsletter_ffb7e666a70151215b4c55c6268d7d72'] = 'ידיעון';
$_MODULE['<{newsletter}prestashop>newsletter_804a924e464fd21ed92f820224c4091d'] = 'מייצר. קובץ CSV עבור דיוור המוני';
$_MODULE['<{newsletter}prestashop>newsletter_c3987e4cac14a8456515f0d200da04ee'] = 'כל הארצות';
$_MODULE['<{newsletter}prestashop>newsletter_fa01fd956e87307bce4c90a0de9b0437'] = 'ארץ המוצא של הלקוח';
$_MODULE['<{newsletter}prestashop>newsletter_c0859b0a5241dff468da2a9a93c3284f'] = 'הפעל מסנן על ארץ הלקוח.';
$_MODULE['<{newsletter}prestashop>newsletter_2198f293f5e1e95dddeff819fbca0975'] = 'רשימת מנויי הידיעון';
$_MODULE['<{newsletter}prestashop>newsletter_99006a61d48499231e1be92241cf772a'] = 'סנן מנויי ידיעון.';
$_MODULE['<{newsletter}prestashop>newsletter_7e3a51a56ddd2846e21c33f05e0aea6f'] = 'כל הלקוחות';
$_MODULE['<{newsletter}prestashop>newsletter_39f7a3e2b56e9bfd753ba6325533a127'] = 'מנויים';
$_MODULE['<{newsletter}prestashop>newsletter_011d8c5d94f729f013963d856cd78745'] = 'שאינם מנויים';
$_MODULE['<{newsletter}prestashop>newsletter_793ee192a9124cd6f529460eef17d3e5'] = 'מנויים שנרשמו לקבל';
$_MODULE['<{newsletter}prestashop>newsletter_7b15c043d66fecc45c8752592aa38e38'] = 'סנן מנויים שנרשמו לקבל.';
$_MODULE['<{newsletter}prestashop>newsletter_82e5e0bc0f9c776c98253d569c111c0f'] = 'לא נמצאו לקוחות עם מסננים אלו!';
$_MODULE['<{newsletter}prestashop>newsletter_644ecc2486a059ca16b001a77909bf40'] = 'קובץ ה- CSV יוצא בהצלחה: נמצאו %d לקוחות.';
$_MODULE['<{newsletter}prestashop>newsletter_48e3d5f66961b621c78f709afcd7d437'] = 'הורד את הקובץ';
$_MODULE['<{newsletter}prestashop>newsletter_faa1115fbab933e7178ee43ce1590977'] = 'אזהרה:. אם תפתח קובץ CSV זה עם Excel, זכור לבחור בקידוד UTF-8 , או אתה עשוי לראות תווים מוזרים.';
$_MODULE['<{newsletter}prestashop>newsletter_81573e0ea79138f02fd2cee94786d7e9'] = 'שגיאה: לא יכול לכתוב';
$_MODULE['<{newsletter}prestashop>newsletter_73059f9530a1a37563150df4dea4bb70'] = 'כל המנויים';
$_MODULE['<{newsletter}prestashop>newsletter_2eb05fb68ae560f1f25f46422dcfe9c2'] = 'מנויים בעלי חשבון משתמש';
$_MODULE['<{newsletter}prestashop>newsletter_f821fd15db9c1d946a449837c291fc68'] = 'מנויים שאינם בעלי חשבון משתמש';
$_MODULE['<{newsletter}prestashop>newsletter_4713ef5f2d6fc1e8f088c850e696a04b'] = 'ייצוא לקוחות';
$_MODULE['<{newsletter}prestashop>newsletter_dbb392a2dc9b38722e69f6032faea73e'] = 'ייצוא. קובץ CSV';


return $_MODULE;
