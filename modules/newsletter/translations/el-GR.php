<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{newsletter}prestashop>newsletter_ffb7e666a70151215b4c55c6268d7d72'] = 'Ενημερωτικό δελτίο';
$_MODULE['<{newsletter}prestashop>newsletter_804a924e464fd21ed92f820224c4091d'] = 'Παράγει ένα αρχείο .CSV για μαζική ταχυδρόμηση';
$_MODULE['<{newsletter}prestashop>newsletter_c3987e4cac14a8456515f0d200da04ee'] = 'Όλες οι χώρες';
$_MODULE['<{newsletter}prestashop>newsletter_fa01fd956e87307bce4c90a0de9b0437'] = 'Χώρα πελάτη';
$_MODULE['<{newsletter}prestashop>newsletter_c0859b0a5241dff468da2a9a93c3284f'] = 'Λειτουργία ενός αρχείου στη χώρα των πελατών.';
$_MODULE['<{newsletter}prestashop>newsletter_2198f293f5e1e95dddeff819fbca0975'] = 'Συνδρομητές στο newsletter';
$_MODULE['<{newsletter}prestashop>newsletter_99006a61d48499231e1be92241cf772a'] = 'Φιλτράρισμα συνδρομητών στο newsletter.';
$_MODULE['<{newsletter}prestashop>newsletter_7e3a51a56ddd2846e21c33f05e0aea6f'] = 'Όλοι οι πελάτες';
$_MODULE['<{newsletter}prestashop>newsletter_39f7a3e2b56e9bfd753ba6325533a127'] = 'Συνδρομητές';
$_MODULE['<{newsletter}prestashop>newsletter_011d8c5d94f729f013963d856cd78745'] = 'Μη συνδρομητές';
$_MODULE['<{newsletter}prestashop>newsletter_793ee192a9124cd6f529460eef17d3e5'] = 'Συνδρομητές με συγκατάθεση';
$_MODULE['<{newsletter}prestashop>newsletter_7b15c043d66fecc45c8752592aa38e38'] = 'Φιλτρό με τους συνδρομητές με συγκατάθεση.';
$_MODULE['<{newsletter}prestashop>newsletter_82e5e0bc0f9c776c98253d569c111c0f'] = 'Δε βρέθηκε κανένας πελάτης με αυτά τα φίλτρα!';
$_MODULE['<{newsletter}prestashop>newsletter_644ecc2486a059ca16b001a77909bf40'] = 'Το αρχείο .CSV έχει εξαχθεί επιτυχώς: βρέθηκαν %d πελάτες.';
$_MODULE['<{newsletter}prestashop>newsletter_48e3d5f66961b621c78f709afcd7d437'] = 'Λήψη αρχείου';
$_MODULE['<{newsletter}prestashop>newsletter_faa1115fbab933e7178ee43ce1590977'] = 'ΠΡΟΕΙΔΟΠΟΊΗΣΗ: Αν αυτό το αρχείο .csv ανοιχτεί με το Excel, θυμηθείτε να επιλέξετε την κωδικοποίηση UTF-8 διαφορετικά ίσως δείτε περίεργους χαρακτήρες.';
$_MODULE['<{newsletter}prestashop>newsletter_81573e0ea79138f02fd2cee94786d7e9'] = 'Σφάλμα: αδυναμία εγγραφής';
$_MODULE['<{newsletter}prestashop>newsletter_73059f9530a1a37563150df4dea4bb70'] = 'Όλοι οι συνδρομητές';
$_MODULE['<{newsletter}prestashop>newsletter_2eb05fb68ae560f1f25f46422dcfe9c2'] = 'Συνδρομητές με λογαριασμό';
$_MODULE['<{newsletter}prestashop>newsletter_f821fd15db9c1d946a449837c291fc68'] = 'Συνδρομητές χωρίς λογαριασμό';
$_MODULE['<{newsletter}prestashop>newsletter_4713ef5f2d6fc1e8f088c850e696a04b'] = 'Εξαγωγή πελατών';
$_MODULE['<{newsletter}prestashop>newsletter_dbb392a2dc9b38722e69f6032faea73e'] = 'Εξαγωγή αρχείου .CSV';


return $_MODULE;
