<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{newsletter}prestashop>newsletter_ffb7e666a70151215b4c55c6268d7d72'] = 'Uudiskiri';
$_MODULE['<{newsletter}prestashop>newsletter_804a924e464fd21ed92f820224c4091d'] = 'Genereerib .CSV faili meiliandmetega';
$_MODULE['<{newsletter}prestashop>newsletter_c3987e4cac14a8456515f0d200da04ee'] = 'Kõik riigid';
$_MODULE['<{newsletter}prestashop>newsletter_fa01fd956e87307bce4c90a0de9b0437'] = 'Kliendi riik';
$_MODULE['<{newsletter}prestashop>newsletter_c0859b0a5241dff468da2a9a93c3284f'] = 'Filtreerib klientide riikide järgi';
$_MODULE['<{newsletter}prestashop>newsletter_2198f293f5e1e95dddeff819fbca0975'] = 'Uudiskirja tellinud';
$_MODULE['<{newsletter}prestashop>newsletter_99006a61d48499231e1be92241cf772a'] = 'Filtreeri uudiskirja tellijad.';
$_MODULE['<{newsletter}prestashop>newsletter_7e3a51a56ddd2846e21c33f05e0aea6f'] = 'Kõik kliendid';
$_MODULE['<{newsletter}prestashop>newsletter_39f7a3e2b56e9bfd753ba6325533a127'] = 'Tellijad';
$_MODULE['<{newsletter}prestashop>newsletter_011d8c5d94f729f013963d856cd78745'] = 'Mitte tellijad';
$_MODULE['<{newsletter}prestashop>newsletter_793ee192a9124cd6f529460eef17d3e5'] = 'Reklaampakkumiste tellijad';
$_MODULE['<{newsletter}prestashop>newsletter_7b15c043d66fecc45c8752592aa38e38'] = 'Filtreeri reklaampakkumiste tellijate järgi.';
$_MODULE['<{newsletter}prestashop>newsletter_82e5e0bc0f9c776c98253d569c111c0f'] = 'Ühtegi klienti ei leitud antud filtritega!';
$_MODULE['<{newsletter}prestashop>newsletter_644ecc2486a059ca16b001a77909bf40'] = '';
$_MODULE['<{newsletter}prestashop>newsletter_48e3d5f66961b621c78f709afcd7d437'] = 'Lae fail alla';
$_MODULE['<{newsletter}prestashop>newsletter_faa1115fbab933e7178ee43ce1590977'] = 'HOIATUS: Kui avate selle .csv faili Exceliga, siis valige kindlasti kodeeringuks UTF-8 või muidu võite näha väga kahtlaseid tähemärke.';
$_MODULE['<{newsletter}prestashop>newsletter_81573e0ea79138f02fd2cee94786d7e9'] = 'Viga: ei saa kirjutada';
$_MODULE['<{newsletter}prestashop>newsletter_73059f9530a1a37563150df4dea4bb70'] = '';
$_MODULE['<{newsletter}prestashop>newsletter_2eb05fb68ae560f1f25f46422dcfe9c2'] = '';
$_MODULE['<{newsletter}prestashop>newsletter_f821fd15db9c1d946a449837c291fc68'] = '';
$_MODULE['<{newsletter}prestashop>newsletter_4713ef5f2d6fc1e8f088c850e696a04b'] = 'Ekspordi klient';
$_MODULE['<{newsletter}prestashop>newsletter_dbb392a2dc9b38722e69f6032faea73e'] = 'Ekspordi .CSV fail';


return $_MODULE;
