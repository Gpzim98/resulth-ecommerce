<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{carriercompare}prestashop>carriercompare_c65b3289df9c2c54a493dcd271d24df0'] = 'Verzendkostenberekening';
$_MODULE['<{carriercompare}prestashop>carriercompare_369f88ec14d06a2166f5fcd4e7c919f1'] = 'Module om transport mogelijkheden te vergelijken voordat men gaat afrekenen';
$_MODULE['<{carriercompare}prestashop>carriercompare_f4f70727dc34561dfde1a3c529b6205c'] = 'Instellingen';
$_MODULE['<{carriercompare}prestashop>carriercompare_57d1936edb715b45f971985f444bf98b'] = 'Hoe de verzenderslijst updaten?';
$_MODULE['<{carriercompare}prestashop>carriercompare_3a7c4c163e83b13d64278a5d4dfb71b3'] = 'Deze bepaalt wanneer de lijst met verzenders wordt geupdate.';
$_MODULE['<{carriercompare}prestashop>carriercompare_faf5148e1ec87fc7d6acc89a81168831'] = 'automatisch met elke verandering van veld';
$_MODULE['<{carriercompare}prestashop>carriercompare_c41e98315a4764541547a1a16786732d'] = 'Wanneer de klant klikt op de \'raam verzendingskost" knop';
$_MODULE['<{carriercompare}prestashop>carriercompare_c9cc8cce247e49bae79f15173ce97354'] = 'Opslaan';
$_MODULE['<{carriercompare}prestashop>carriercompare_2833e625c13a70c7b84ad98c97976c9d'] = 'Verversen van de pagina en de winkelwagen...';
$_MODULE['<{carriercompare}prestashop>carriercompare_b49f15ff6f8530a76ed954676abbc4d6'] = 'Opzoeken beschikbare staten...';
$_MODULE['<{carriercompare}prestashop>carriercompare_ed22df3e47c667a95dd43e59f2f38522'] = 'Informatie ophalen...';
$_MODULE['<{carriercompare}prestashop>carriercompare_e7a6ca4e744870d455a57b644f696457'] = 'Gratis!';
$_MODULE['<{carriercompare}prestashop>carriercompare_533bf2149f935df1f934e5ee55f68d20'] = 'Bereken uw verzendkosten & btw';
$_MODULE['<{carriercompare}prestashop>carriercompare_59716c97497eb9694541f7c3d37b1a4d'] = 'Land';
$_MODULE['<{carriercompare}prestashop>carriercompare_46a2a41cc6e552044816a2d04634545d'] = 'Provincie';
$_MODULE['<{carriercompare}prestashop>carriercompare_5e178542b85fb18ca3c459e9a95f4f2e'] = 'Postcode';
$_MODULE['<{carriercompare}prestashop>carriercompare_9207f4078f515ecc8256bae33fa1f497'] = 'Benodigd voor bepaalde vervoerders';
$_MODULE['<{carriercompare}prestashop>carriercompare_914419aa32f04011357d3b604a86d7eb'] = 'Vervoerder';
$_MODULE['<{carriercompare}prestashop>carriercompare_a82be0f551b8708bc08eb33cd9ded0cf'] = 'Informatie';
$_MODULE['<{carriercompare}prestashop>carriercompare_3601146c4e948c32b6424d2c0a7f0118'] = 'Prijs';
$_MODULE['<{carriercompare}prestashop>carriercompare_568ee69520d3299f2f0ea64d501b6f50'] = 'Voor deze selectie is geen vervoerder beschikbaar';
$_MODULE['<{carriercompare}prestashop>carriercompare_1de0f9fcbe30b056483fe964aed55adc'] = 'Winkelwagen bijwerken';
$_MODULE['<{carriercompare}prestashop>carriercompare_a305b69f91e8c50117cc540114b9ba06'] = 'Raam verzendingskost';
$_MODULE['<{carriercompare}prestashop>configuration_a2d3779b2b18f09740a46050388e08f0'] = 'Er is een fout opgetreden tijdens de validatie van het formulier.';
$_MODULE['<{carriercompare}prestashop>configuration_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'Configuratie bijgewerkt';
$_MODULE['<{carriercompare}prestashop>configuration_99e09df7d3344d2d04153f297163affa'] = 'Deze module is alleen beschikbaar bij het standaard bestelproces omdat op de Een Pagina Afrekenen het vervoerdersoverzicht al beschikbaar is.';
$_MODULE['<{carriercompare}prestashop>configuration_6408d076fa6417e7bc8ddc3cdf9a0644'] = 'Globale configuratie';
$_MODULE['<{carriercompare}prestashop>configuration_4c1f76824b0d7333652d5f64a3e07ef5'] = 'Altijd';
$_MODULE['<{carriercompare}prestashop>configuration_31efb362f3723b55bfa28f3551a672f7'] = 'Vereist alle informatie';
$_MODULE['<{carriercompare}prestashop>configuration_9c7cc22fb61d22cf2560f172ea85ab7b'] = 'Manier waarop de informatie voor een klant wordt ververst';
$_MODULE['<{carriercompare}prestashop>configuration_a4d3b161ce1309df1c4e25df28694b7b'] = 'Toevoegen';
$_MODULE['<{carriercompare}prestashop>carriercompare_c6e2a9b2d1979c244d18c7e9fbf9a86f'] = 'Postcode';
$_MODULE['<{carriercompare}prestashop>carriercompare_653e78154f5596fc293bf9db65110bbd'] = 'Vervoerderlijst bijwerken';
$_MODULE['<{carriercompare}prestashop>configuration_152c9745ad3e3720a15b058df5d16116'] = 'Er is een fout opgetreden tijdens de validatie van het formulier.';
$_MODULE['<{carriercompare}prestashop>configuration_242c88eb5c5a76cab7c30fef4cd89471'] = 'Deze module is alleen beschikbaar bij het standaard bestelproces omdat op de Een Pagina Afrekenen het vervoerdersoverzicht al beschikbaar is.';


return $_MODULE;
