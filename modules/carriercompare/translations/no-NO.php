<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{carriercompare}prestashop>carriercompare_c65b3289df9c2c54a493dcd271d24df0'] = 'Fraktestimat';
$_MODULE['<{carriercompare}prestashop>carriercompare_369f88ec14d06a2166f5fcd4e7c919f1'] = 'Sammenlign speditørmuligheter før du går til kassen.';
$_MODULE['<{carriercompare}prestashop>carriercompare_f4f70727dc34561dfde1a3c529b6205c'] = 'Innstillinger';
$_MODULE['<{carriercompare}prestashop>carriercompare_57d1936edb715b45f971985f444bf98b'] = 'Hvordan oppdatere leverandørlisten?';
$_MODULE['<{carriercompare}prestashop>carriercompare_3a7c4c163e83b13d64278a5d4dfb71b3'] = 'Dette avgjør når listen over leverandører blir oppdatert.';
$_MODULE['<{carriercompare}prestashop>carriercompare_faf5148e1ec87fc7d6acc89a81168831'] = 'Automatisk med hver feltendring';
$_MODULE['<{carriercompare}prestashop>carriercompare_c41e98315a4764541547a1a16786732d'] = 'Når kunden klikker på "Estimert fraktkostnad"-knappen';
$_MODULE['<{carriercompare}prestashop>carriercompare_c9cc8cce247e49bae79f15173ce97354'] = 'Lagre';
$_MODULE['<{carriercompare}prestashop>carriercompare_2833e625c13a70c7b84ad98c97976c9d'] = 'Oppdaterer siden og handlekurven..';
$_MODULE['<{carriercompare}prestashop>carriercompare_b49f15ff6f8530a76ed954676abbc4d6'] = 'Kontrollerer tilgjenglige fylker..';
$_MODULE['<{carriercompare}prestashop>carriercompare_ed22df3e47c667a95dd43e59f2f38522'] = 'Henter informasjon..';
$_MODULE['<{carriercompare}prestashop>carriercompare_e7a6ca4e744870d455a57b644f696457'] = 'Gratis!';
$_MODULE['<{carriercompare}prestashop>carriercompare_533bf2149f935df1f934e5ee55f68d20'] = 'Beregn fraktkostnader og MVA.';
$_MODULE['<{carriercompare}prestashop>carriercompare_59716c97497eb9694541f7c3d37b1a4d'] = 'Land';
$_MODULE['<{carriercompare}prestashop>carriercompare_46a2a41cc6e552044816a2d04634545d'] = 'Stat/Fylke';
$_MODULE['<{carriercompare}prestashop>carriercompare_5e178542b85fb18ca3c459e9a95f4f2e'] = 'Postnummer';
$_MODULE['<{carriercompare}prestashop>carriercompare_9207f4078f515ecc8256bae33fa1f497'] = 'Påkrevet for noen transportører.';
$_MODULE['<{carriercompare}prestashop>carriercompare_914419aa32f04011357d3b604a86d7eb'] = 'Transportør';
$_MODULE['<{carriercompare}prestashop>carriercompare_a82be0f551b8708bc08eb33cd9ded0cf'] = 'Informasjon';
$_MODULE['<{carriercompare}prestashop>carriercompare_3601146c4e948c32b6424d2c0a7f0118'] = 'Pris';
$_MODULE['<{carriercompare}prestashop>carriercompare_568ee69520d3299f2f0ea64d501b6f50'] = 'Ingen transportør er tilgjenglig for dette utvalget.';
$_MODULE['<{carriercompare}prestashop>carriercompare_1de0f9fcbe30b056483fe964aed55adc'] = 'Oppdater handlevogn';
$_MODULE['<{carriercompare}prestashop>carriercompare_a305b69f91e8c50117cc540114b9ba06'] = 'Estimert fraktkostnad';
$_MODULE['<{carriercompare}prestashop>configuration_a2d3779b2b18f09740a46050388e08f0'] = 'En feil oppstod under verifiseringen av skjemaet.';
$_MODULE['<{carriercompare}prestashop>configuration_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'Konfigurasjon oppdatert';
$_MODULE['<{carriercompare}prestashop>configuration_99e09df7d3344d2d04153f297163affa'] = 'Denne modulen er kun tilgjenglig under standard fem-trinn-utsjekk prosessen. Transportørlisten har allerede blitt definert for en-side utsjekk.';
$_MODULE['<{carriercompare}prestashop>configuration_6408d076fa6417e7bc8ddc3cdf9a0644'] = 'Global konfigurasjon';
$_MODULE['<{carriercompare}prestashop>configuration_4c1f76824b0d7333652d5f64a3e07ef5'] = 'Når som helst';
$_MODULE['<{carriercompare}prestashop>configuration_31efb362f3723b55bfa28f3551a672f7'] = 'Påkrevet informasjon er ikke lagt inn.';
$_MODULE['<{carriercompare}prestashop>configuration_9c7cc22fb61d22cf2560f172ea85ab7b'] = 'Hvordan ønsker du å oppdatere informasjon for en kunde?';
$_MODULE['<{carriercompare}prestashop>configuration_a4d3b161ce1309df1c4e25df28694b7b'] = 'Legg til';
$_MODULE['<{carriercompare}prestashop>carriercompare_c6e2a9b2d1979c244d18c7e9fbf9a86f'] = 'Zip/postkode';
$_MODULE['<{carriercompare}prestashop>carriercompare_653e78154f5596fc293bf9db65110bbd'] = 'Oppdater transportørliste';
$_MODULE['<{carriercompare}prestashop>configuration_152c9745ad3e3720a15b058df5d16116'] = 'En feil oppstod under verifisering av skjemaet.';
$_MODULE['<{carriercompare}prestashop>configuration_242c88eb5c5a76cab7c30fef4cd89471'] = 'Denne modulen er kun tilgjenglig under standard fem trinns utsjekking. Transportørlisten er allerede definert for en-sides utsjekking.';


return $_MODULE;
