<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{carriercompare}prestashop>carriercompare_c65b3289df9c2c54a493dcd271d24df0'] = 'শিপিং সম্পর্কিত  অনুমান';
$_MODULE['<{carriercompare}prestashop>carriercompare_369f88ec14d06a2166f5fcd4e7c919f1'] = 'বিদায় এর আগে ক্যারিয়ার পছন্দ/তুলনা করুন ';
$_MODULE['<{carriercompare}prestashop>carriercompare_f4f70727dc34561dfde1a3c529b6205c'] = 'সেটিংস';
$_MODULE['<{carriercompare}prestashop>carriercompare_57d1936edb715b45f971985f444bf98b'] = 'কিভাবে বাহক লিস্ট রিফ্রেশ করতে হয় ?';
$_MODULE['<{carriercompare}prestashop>carriercompare_3a7c4c163e83b13d64278a5d4dfb71b3'] = 'কখন ক্রেতাকে দেখানো বাহক লিস্ট আপডেট করা হবে এটি তা নির্ধারণ করে ';
$_MODULE['<{carriercompare}prestashop>carriercompare_faf5148e1ec87fc7d6acc89a81168831'] = 'প্রতিটি ফিল্ড পরিবর্তন এর সাথে স্বয়ংক্রিয় ভাবে ';
$_MODULE['<{carriercompare}prestashop>carriercompare_c41e98315a4764541547a1a16786732d'] = 'যখন ক্রেতা "আনুমানিক পরিবহন খরচ" বাটনে ক্লিক করবে ';
$_MODULE['<{carriercompare}prestashop>carriercompare_c9cc8cce247e49bae79f15173ce97354'] = 'সংরক্ষণ করুন';
$_MODULE['<{carriercompare}prestashop>carriercompare_2833e625c13a70c7b84ad98c97976c9d'] = 'পৃষ্ঠা রিফ্রেশ হচ্ছে  এবং আপনার কার্ট আপডেট হচ্ছে ...';
$_MODULE['<{carriercompare}prestashop>carriercompare_b49f15ff6f8530a76ed954676abbc4d6'] = 'সটীক এস্তাট  চেক করা হচ্ছে ...';
$_MODULE['<{carriercompare}prestashop>carriercompare_ed22df3e47c667a95dd43e59f2f38522'] = 'তথ্য পুনরুদ্ধার করা হচ্ছে ...';
$_MODULE['<{carriercompare}prestashop>carriercompare_e7a6ca4e744870d455a57b644f696457'] = 'বিনামূল্যে!';
$_MODULE['<{carriercompare}prestashop>carriercompare_533bf2149f935df1f934e5ee55f68d20'] = 'শিপিং ও করের খরচ অনুমান করা হচ্ছে';
$_MODULE['<{carriercompare}prestashop>carriercompare_59716c97497eb9694541f7c3d37b1a4d'] = 'দেশ';
$_MODULE['<{carriercompare}prestashop>carriercompare_46a2a41cc6e552044816a2d04634545d'] = 'স্টেট';
$_MODULE['<{carriercompare}prestashop>carriercompare_5e178542b85fb18ca3c459e9a95f4f2e'] = 'জিপ / পোস্টাল কোড';
$_MODULE['<{carriercompare}prestashop>carriercompare_9207f4078f515ecc8256bae33fa1f497'] = 'নির্দিষ্ট বাহক জন্য প্রয়োজন ';
$_MODULE['<{carriercompare}prestashop>carriercompare_914419aa32f04011357d3b604a86d7eb'] = 'বাহক';
$_MODULE['<{carriercompare}prestashop>carriercompare_a82be0f551b8708bc08eb33cd9ded0cf'] = 'তথ্য';
$_MODULE['<{carriercompare}prestashop>carriercompare_3601146c4e948c32b6424d2c0a7f0118'] = 'দাম';
$_MODULE['<{carriercompare}prestashop>carriercompare_568ee69520d3299f2f0ea64d501b6f50'] = 'কোন ক্যারিয়ার এই অর্ডার এর জন্য পাওয়া যায় নি.';
$_MODULE['<{carriercompare}prestashop>carriercompare_1de0f9fcbe30b056483fe964aed55adc'] = 'কার্ট আপডেট করুন';
$_MODULE['<{carriercompare}prestashop>carriercompare_a305b69f91e8c50117cc540114b9ba06'] = 'আনুমানিক পরিবহন খরচ ';
$_MODULE['<{carriercompare}prestashop>configuration_a2d3779b2b18f09740a46050388e08f0'] = 'ফর্ম যাচাইকরণ এর সময় একটি ত্রুটি ঘটেছে.';
$_MODULE['<{carriercompare}prestashop>configuration_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'কনফিগারেশন আপডেট হয়েছে ';
$_MODULE['<{carriercompare}prestashop>configuration_99e09df7d3344d2d04153f297163affa'] = 'এই মডিউলের সাহায্যে প্রমিত পাঁচ ধাপ চেকআউট প্রক্রিয়ার সম্পন্ন করা যাবে,  ক্যারিয়ার তালিকাটি ইতিমধ্যেই এক পাতা চেকআউট জন্য সংজ্ঞায়িত করা হয়েছে.';
$_MODULE['<{carriercompare}prestashop>configuration_6408d076fa6417e7bc8ddc3cdf9a0644'] = 'গ্লোবাল কনফিগারেশন';
$_MODULE['<{carriercompare}prestashop>configuration_4c1f76824b0d7333652d5f64a3e07ef5'] = 'যে কোনো সময়';
$_MODULE['<{carriercompare}prestashop>configuration_31efb362f3723b55bfa28f3551a672f7'] = 'প্রয়োজনীয় তথ্য সেট করা হয়েছে ';
$_MODULE['<{carriercompare}prestashop>configuration_9c7cc22fb61d22cf2560f172ea85ab7b'] = 'আপনি কি এই গ্রাহকের জন্য তথ্য রিফ্রেশ করতে চান?';
$_MODULE['<{carriercompare}prestashop>configuration_a4d3b161ce1309df1c4e25df28694b7b'] = 'জমা দিন';
$_MODULE['<{carriercompare}prestashop>carriercompare_c6e2a9b2d1979c244d18c7e9fbf9a86f'] = 'জিপ/পোস্টাল কোড ';
$_MODULE['<{carriercompare}prestashop>carriercompare_653e78154f5596fc293bf9db65110bbd'] = 'ক্যারিয়ার তালিকা ';
$_MODULE['<{carriercompare}prestashop>configuration_152c9745ad3e3720a15b058df5d16116'] = '';
$_MODULE['<{carriercompare}prestashop>configuration_242c88eb5c5a76cab7c30fef4cd89471'] = '';


return $_MODULE;
