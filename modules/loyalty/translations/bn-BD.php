<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{loyalty}prestashop>loyalty_30146a132c2aa28808a8411ed74c12ed'] = 'গ্রাহক বিশ্বস্থতা ও পুরস্কার';
$_MODULE['<{loyalty}prestashop>loyalty_f8763c754ba455aa6e8ddf0e62911eb7'] = 'আপনার গ্রাহকদের আনুগত্য প্রোগ্রাম প্রদান করুন';
$_MODULE['<{loyalty}prestashop>loyalty_f0338d5a7bbd642cc188ca69c8a97b12'] = 'আপনি কি সকল আনুগত্য পয়েন্ট এবং গ্রাহক ইতিহাস মুছে দেওয়ার বিষয়ে নিশ্চিত?';
$_MODULE['<{loyalty}prestashop>loyalty_7307b68f93443d5863f1d3943c546b20'] = 'বিশ্বস্ততা পুরস্কার';
$_MODULE['<{loyalty}prestashop>loyalty_e81b2826b5aebd9c92fb5d090f0cdc9d'] = 'আপনি ভাউচার এর ক্রিয়ার জন্য অন্তত একটি বিভাগ চয়ন করতে হবে';
$_MODULE['<{loyalty}prestashop>loyalty_f38f5974cdc23279ffe6d203641a8bdf'] = 'সেটিংস আপডেট হয়েছে ';
$_MODULE['<{loyalty}prestashop>loyalty_8b80d4b6307990874b832cc15a92e5a3'] = 'মিসিং পরামিতি ';
$_MODULE['<{loyalty}prestashop>loyalty_a82e0d057f443115e807bd6ca595fc8c'] = 'ভুল অর্ডার বিষয়';
$_MODULE['<{loyalty}prestashop>loyalty_94c69408d25102ba7ddcf3533b56c407'] = 'সয়ভুল গ্রাহক বিষয়';
$_MODULE['<{loyalty}prestashop>loyalty_94c2a3734a95577d173f702aa67a4788'] = 'বিশ্বস্ততা পয়েন্ট (%d)';
$_MODULE['<{loyalty}prestashop>loyalty_309cd9f5437d1bb06a7fdab1811afe1a'] = ' এই গ্রাহক এর কোন পয়েন্ট নেই';
$_MODULE['<{loyalty}prestashop>loyalty_a240fa27925a635b08dc28c9e4f9216d'] = 'অর্ডার';
$_MODULE['<{loyalty}prestashop>loyalty_44749712dbec183e983dcd78a7736c41'] = 'তারিখ';
$_MODULE['<{loyalty}prestashop>loyalty_aa7f22f84f7be784055a3e7e7d22c519'] = 'মোট(সরবরাহ মূল্য বেতিত)';
$_MODULE['<{loyalty}prestashop>loyalty_75dd5f1160a3f02b6fae89c54361a1b3'] = 'পয়েন্ট';
$_MODULE['<{loyalty}prestashop>loyalty_1026e44f047fb9da36a62c0a8846baac'] = 'পয়েন্ট এর পরিমান';
$_MODULE['<{loyalty}prestashop>loyalty_98ab04462a3ea83fe76f4163efe755da'] = '#%d';
$_MODULE['<{loyalty}prestashop>loyalty_66c2c90ea9f6f4a12854195085781d7f'] = 'মোট প্রাপ্ত পয়েন্ট';
$_MODULE['<{loyalty}prestashop>loyalty_928666bdf20510dfa5c58393b77f1798'] = 'ভাউচার মূল্য:';
$_MODULE['<{loyalty}prestashop>loyalty_a9be824aae4f2381a27b7c699b1e041e'] = 'অপেক্ষারত গ্রহণযোগ্যতা';
$_MODULE['<{loyalty}prestashop>loyalty_78945de8de090e90045d299651a68a9b'] = 'প্রাপ্তিসিাধ্য';
$_MODULE['<{loyalty}prestashop>loyalty_a149e85a44aeec9140e92733d9ed694e'] = 'বাতিল';
$_MODULE['<{loyalty}prestashop>loyalty_4cb08bf5ad3d3c7b010dde725a078b28'] = 'ইতিমধ্যে রূপান্তরিত';
$_MODULE['<{loyalty}prestashop>loyalty_df05c2db84dacb19b599b489bf3963db'] = 'মূল্যছাড় পাওয়া যাচ্ছে না';
$_MODULE['<{loyalty}prestashop>loyalty_01371a1d58e9234c0b9dbc08cf54fa8b'] = 'মূল্যছাড় এ পাওয়া যাচ্ছে না';
$_MODULE['<{loyalty}prestashop>loyalty_f4f70727dc34561dfde1a3c529b6205c'] = 'সেটিংস';
$_MODULE['<{loyalty}prestashop>loyalty_8334a158298fbcf163f4dcb4a387d150'] = 'অনুপাত';
$_MODULE['<{loyalty}prestashop>loyalty_ea92f82fb14a544ac9d38c3e6c8dc03b'] = '= 1 পুরস্কার পয়েন্ট.';
$_MODULE['<{loyalty}prestashop>loyalty_cd53f34f7289c573ba717b7767919a96'] = '1 পয়েন্ট =';
$_MODULE['<{loyalty}prestashop>loyalty_bd43f22cbd337b68c1ee876323e1241d'] = 'ডিসকাউন্ট এর জন্য.';
$_MODULE['<{loyalty}prestashop>loyalty_1063340c25c87a309d79f5c049246a0f'] = 'পয়েন্টের কার্যকারিতা সময়';
$_MODULE['<{loyalty}prestashop>loyalty_44fdec47036f482b68b748f9d786801b'] = 'দিন';
$_MODULE['<{loyalty}prestashop>loyalty_98cf9475009d3c6e795ffac5d391cec4'] = 'ভাউচার বিবরণ';
$_MODULE['<{loyalty}prestashop>loyalty_dd98e4d652530674f61201056fdbe9b4'] = 'ভাউচার ব্যবহার করা যাবে এমন নূন্যতম পরিমাণ';
$_MODULE['<{loyalty}prestashop>loyalty_af720a2679a80545d064cf0350a7fc38'] = 'ভাউচারের উপর ট্যাক্স প্রয়োগ করুন';
$_MODULE['<{loyalty}prestashop>loyalty_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'সক্রিয়';
$_MODULE['<{loyalty}prestashop>loyalty_b9f5c797ebbf55adccdd8539a65a0241'] = 'নিষ্ক্রিয়';
$_MODULE['<{loyalty}prestashop>loyalty_ade45d72ab6ba1ab576d8b9deb0c2438'] = 'যে সমস্থ অর্ডার এ পয়েন্ট পুরস্কার প্রদান করা হয়';
$_MODULE['<{loyalty}prestashop>loyalty_9611a682e61c503c32e2dc58fdbc8ddf'] = 'যে সমস্থ অর্ডার এ পয়েন্ট বাতিল করা হয়';
$_MODULE['<{loyalty}prestashop>loyalty_51ab56dd5b46c7b5c8fdf22651ae0db6'] = 'বাট্টাকৃত পণ্য এর জন্য পয়েন্ট দিন';
$_MODULE['<{loyalty}prestashop>loyalty_e32f75d18c920282f6d8770d19d43d91'] = '';
$_MODULE['<{loyalty}prestashop>loyalty_9516494d859b2819c76023f8ce906795'] = '';
$_MODULE['<{loyalty}prestashop>loyalty_91b442d385b54e1418d81adc34871053'] = 'নির্বাচিত';
$_MODULE['<{loyalty}prestashop>loyalty_b56c3bda503a8dc4be356edb0cc31793'] = 'সমস্ত কলাপস করুন';
$_MODULE['<{loyalty}prestashop>loyalty_5ffd7a335dd836b3373f5ec570a58bdc'] = 'সকল এক্সপান্ড  করুন';
$_MODULE['<{loyalty}prestashop>loyalty_5e9df908eafa83cb51c0a3720e8348c7'] = 'সব চেক করুন';
$_MODULE['<{loyalty}prestashop>loyalty_9747d23c8cc358c5ef78c51e59cd6817'] = 'সব টিক চিহ্ন তুলে দিন';
$_MODULE['<{loyalty}prestashop>loyalty_c9cc8cce247e49bae79f15173ce97354'] = 'সংরক্ষণ করুন';
$_MODULE['<{loyalty}prestashop>loyalty_30e793698766edbaaf84a74d4c377f72'] = 'বিশ্বস্ততা পয়েন্ট এর অগ্রগতি';
$_MODULE['<{loyalty}prestashop>loyalty_4f2a91e15af2631ff9424564b8a45fb2'] = 'প্রাথমিক';
$_MODULE['<{loyalty}prestashop>loyalty_453e6aa38d87b28ccae545967c53004f'] = 'অপ্রাপ্য ';
$_MODULE['<{loyalty}prestashop>loyalty_6366c60fc5b4f4fce0e3dd146494a4f4'] = 'রূপান্তরিত';
$_MODULE['<{loyalty}prestashop>loyalty_13148717f8faa9037f37d28971dfc219'] = 'ভ্যালিডেশন';
$_MODULE['<{loyalty}prestashop>loyalty_36c94bd456cf8796723ad09eac258aef'] = 'আমার অ্যাকাউন্ট বাবস্থাপনা করুন';
$_MODULE['<{loyalty}prestashop>loyalty_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'আমার অ্যাকাউন্ট';
$_MODULE['<{loyalty}prestashop>loyalty_c540093e64d84440025b2d8201f04336'] = 'আমার বিশ্বস্ততা পয়েন্ট';
$_MODULE['<{loyalty}prestashop>loyalty_01abfc750a0c942167651c40d088531d'] = '#';
$_MODULE['<{loyalty}prestashop>loyalty_5acc2ceeb883ba07cef2d02ea382f242'] = 'আপনি কোন আদেশ স্থাপন করেননি.';
$_MODULE['<{loyalty}prestashop>loyalty_dd1f775e443ff3b9a89270713580a51b'] = 'আগেরটি';
$_MODULE['<{loyalty}prestashop>loyalty_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'পরবর্তী ';
$_MODULE['<{loyalty}prestashop>loyalty_e0aa021e21dddbd6d8cecec71e9cf564'] = 'ঠিক আছে';
$_MODULE['<{loyalty}prestashop>loyalty_6c583afb157e33bfb5b7c3d4114c4dd5'] = 'আইটেম';
$_MODULE['<{loyalty}prestashop>loyalty_c48105520852bbd0fa692e4c9fd61628'] = 'এখান থেকে প্রাপ্ত ভাউচার নিম্নের ক্ষেত্র সমুহে যবহার করা যেতে পারেঃ';
$_MODULE['<{loyalty}prestashop>loyalty_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'সমস্ত';
$_MODULE['<{loyalty}prestashop>loyalty_5b7d558a20e8bcb6d9355a012becb1eb'] = 'আপনি আপনার পয়েন্টকে ভাউচারএ রুপান্তর করার ব্যাপারে নিশ্চিত ?';
$_MODULE['<{loyalty}prestashop>loyalty_4db04271e368fe3d4e1aa7332a18fa9d'] = 'আমার পয়েন্টকে ভাউচারএ রুপান্তর করুন';
$_MODULE['<{loyalty}prestashop>loyalty_b39cba8836db01a04888aef6ba386420'] = 'বিশ্বস্ততা পয়েন্ট থেকে আমার ভাউচার';
$_MODULE['<{loyalty}prestashop>loyalty_0eceeb45861f9585dd7a97a3e36f85c6'] = 'তৈরি হয়েছে';
$_MODULE['<{loyalty}prestashop>loyalty_689202409e48743b914713f96d93947c'] = 'মূল্য';
$_MODULE['<{loyalty}prestashop>loyalty_ca0dbad92a874b2f69b549293387925e'] = 'কোড';
$_MODULE['<{loyalty}prestashop>loyalty_eb902cf204f3e4dfffeb56d92a9b5c26'] = 'বৈধ ফর্ম';
$_MODULE['<{loyalty}prestashop>loyalty_b2844b8e17ecaaeae68d018fe9418af0'] = 'যে তারিখ পর্যন্ত বৈধ';
$_MODULE['<{loyalty}prestashop>loyalty_ec53a8c4f07baed5d8825072c89799be'] = 'অবস্থা';
$_MODULE['<{loyalty}prestashop>loyalty_3ec365dd533ddb7ef3d1c111186ce872'] = 'বিবরণ';
$_MODULE['<{loyalty}prestashop>loyalty_29aa46cc3d2677c7e0f216910df600ff'] = 'ফ্রি শিপিং';
$_MODULE['<{loyalty}prestashop>loyalty_2c8bb57a0b1dff255f3d6684a9fddda3'] = '';
$_MODULE['<{loyalty}prestashop>loyalty_24b9e0a0faa7aa28dcadeb4cf7f860fd'] = '';
$_MODULE['<{loyalty}prestashop>loyalty_2af3bf4c82c5b33875d532820a959799'] = 'এই নিম্নলিখিত আদেশ দ্বারা উত্পন্ন করুন';
$_MODULE['<{loyalty}prestashop>loyalty_18fb6221fe0d9895c2e9ba08283f00e9'] = 'অর্ডার #%d';
$_MODULE['<{loyalty}prestashop>loyalty_4c8d2e0395ae7d21e374dcbeb1cbeaaa'] = '%d';
$_MODULE['<{loyalty}prestashop>loyalty_1f67ea7a0b26e9eacc70523bde28df0c'] = 'আরও...';
$_MODULE['<{loyalty}prestashop>loyalty_a16cf3ec5200cc519f4fe48e34b1df83'] = 'এই ভাউচার ব্যবহার করার জন্য ন্যূনতম ক্রয় আদেশের পরিমাণ:';
$_MODULE['<{loyalty}prestashop>loyalty_8e69341aca5dbf9f55c2e75a2ed5df3c'] = 'এখন ও কোন ভাউচার নেই';
$_MODULE['<{loyalty}prestashop>loyalty_00d56a5e37c19c59d521530fc8e7f337'] = 'এখন ও কোন পুরস্কার পয়েন্ট নেই';
$_MODULE['<{loyalty}prestashop>loyalty_0b3db27bc15f682e92ff250ebb167d4b'] = 'আপনার অ্যাকাউন্টে ফিরে যান';
$_MODULE['<{loyalty}prestashop>loyalty_8cf04a9734132302f96da8e113e80ce5'] = 'হোম /বাসা';
$_MODULE['<{loyalty}prestashop>my-account_c540093e64d84440025b2d8201f04336'] = 'আমার বিশ্বস্ততা পয়েন্ট';
$_MODULE['<{loyalty}prestashop>product_054a9c66cc92b7f1bfcacee3b7c7ad54'] = 'এই পণ্যের জন্য কোনো পুরস্কার পয়েন্ট নেই কারন ইতিমধ্যেই ডিসকাউন্ট আছে';
$_MODULE['<{loyalty}prestashop>product_e94d481804904a48c1a8093e7a069570'] = 'পণ্যটির জন্য কোন পুরস্কার পয়েন্ট নেই';
$_MODULE['<{loyalty}prestashop>product_08ef6b34ab8e7039ef0ee69378f0ac0b'] = 'এই পণ্য ক্রয় করে আপনি পর্যন্ত সংগ্রহ করতে পারবেন';
$_MODULE['<{loyalty}prestashop>product_2996152bb442bf98c80c515c6055de5f'] = 'বিশ্বস্ততা পয়েন্ট';
$_MODULE['<{loyalty}prestashop>product_b40d5c523ee75453134b1449dd9cd13a'] = 'বিশ্বস্ততা পয়েন্ট';
$_MODULE['<{loyalty}prestashop>product_b9cb3a85529dd593c14c838e22976cff'] = 'আপনার মোট হবে';
$_MODULE['<{loyalty}prestashop>product_0aab81de5c4c87021772015efc184d67'] = 'পয়েন্ট';
$_MODULE['<{loyalty}prestashop>product_78ee54aa8f813885fe2fe20d232518b9'] = 'পয়েন্ট';
$_MODULE['<{loyalty}prestashop>product_443c3e03e194c2a4cdb107808b051615'] = 'একটি ভাউচার এ রূপান্তরিত করা যেতে পারে';
$_MODULE['<{loyalty}prestashop>shopping-cart_ea2c0ea1a08add3a75273e7f32f05f7a'] = 'বিশ্বস্ততা';
$_MODULE['<{loyalty}prestashop>shopping-cart_562b7108857d8394e83861d0529cc7dd'] = 'এই শপিং কার্ট চেক আউট করে আপনি সর্বচচ্চ সংগ্রহ করতে পারবেন';
$_MODULE['<{loyalty}prestashop>shopping-cart_1340ddee36660d8e1d5f6918dee0ba4e'] = '%d বিশ্বস্ততা পয়েন্ট';
$_MODULE['<{loyalty}prestashop>shopping-cart_e7e421ccebbe3fafffe0f67dbd595d5b'] = '%d বিশ্বস্ততা পয়েন্ট';
$_MODULE['<{loyalty}prestashop>shopping-cart_443c3e03e194c2a4cdb107808b051615'] = 'একটি ভাউচার এ রূপান্তরিত করা যেতে পারে';
$_MODULE['<{loyalty}prestashop>shopping-cart_2d354f4df80bbebf309f184c3953dc1d'] = 'তাত্ক্ষনিক চেক আউট অর্ডার এর জন্য উপস্থিত নেই';
$_MODULE['<{loyalty}prestashop>shopping-cart_8cec799df06a3f5a026b31fcd95e0172'] = 'কিছু আনুগত্য পয়েন্ট সংগ্রহএর জন্য আপনার শপিং কার্টএ কিছু পণ্য যুক্ত করুন';
$_MODULE['<{loyalty}prestashop>loyalty_cec73b5ce095a59305ad92a0d47495cb'] = 'ব্যবহার এর জন্য';
$_MODULE['<{loyalty}prestashop>loyalty_019d1ca7d50cc54b995f60d456435e87'] = 'বাবহারিত';


return $_MODULE;
