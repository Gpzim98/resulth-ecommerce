<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{statslive}prestashop>statslive_fa55230e9791f2b71322869318a5f00f'] = 'Pengunjung online';
$_MODULE['<{statslive}prestashop>statslive_abf306fd198ab007d480ed610a6690fb'] = 'Letakkan senarai pelanggan dan pelawat yang sedang dalam talian di papan pemuka.';
$_MODULE['<{statslive}prestashop>statslive_85f955e33756b8f40ce35e5b277de5bc'] = '';
$_MODULE['<{statslive}prestashop>statslive_f5ee3b50dba1fb98f1342a584e46cd30'] = '';
$_MODULE['<{statslive}prestashop>statslive_66c4c5112f455a19afde47829df363fa'] = 'JUmlah:';
$_MODULE['<{statslive}prestashop>statslive_d37c2bf1bd3143847fca087b354f920e'] = '';
$_MODULE['<{statslive}prestashop>statslive_49ee3087348e8d44e1feda1917443987'] = 'Nama';
$_MODULE['<{statslive}prestashop>statslive_13aa8652e950bb7c4b9b213e6d8d0dc5'] = 'Lancar laman';
$_MODULE['<{statslive}prestashop>statslive_9dd3bc54879dc9425d44de81f3d7dfdc'] = '';
$_MODULE['<{statslive}prestashop>statslive_08448d43d8e4b39ad5126d4b06e2f3cc'] = '';
$_MODULE['<{statslive}prestashop>statslive_cf6377279146be659952cea754c558b1'] = '';
$_MODULE['<{statslive}prestashop>statslive_f8cf0a1a6b03d2b01602992ea273134c'] = '';
$_MODULE['<{statslive}prestashop>statslive_a12a3079e14ced46e69ba52b8a90b21a'] = 'IP';
$_MODULE['<{statslive}prestashop>statslive_bc5188ca43d423ba3730e4c030609d6e'] = '';
$_MODULE['<{statslive}prestashop>statslive_b6f05e5ddde1ec63d992d61144452dfa'] = 'Perujuk';
$_MODULE['<{statslive}prestashop>statslive_ec0fc0100c4fc1ce4eea230c3dc10360'] = 'TIdak ditakrif';
$_MODULE['<{statslive}prestashop>statslive_6adf97f83acf6453d4a6a4b1070f3754'] = 'tak satupun';
$_MODULE['<{statslive}prestashop>statslive_a55533db46597bee3cd16899c007257e'] = 'Tidak ada pengunjung online.';
$_MODULE['<{statslive}prestashop>statslive_24efa7ee4511563b16144f39706d594f'] = 'Notis';
$_MODULE['<{statslive}prestashop>statslive_e5900cd9ae26ca607f7cd497f114b9f9'] = 'IP Penyelenggaraan dikecualikan dari pelawat yang datang.';
$_MODULE['<{statslive}prestashop>statslive_05b564d49dbd9049f0df80a45cfe7d1c'] = 'Tambah atau hapuskan alamat IP.';


return $_MODULE;
