<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsequipment}prestashop>statsequipment_247270d410e2b9de01814b82111becda'] = 'Navegadores e sistemas operacionais';
$_MODULE['<{statsequipment}prestashop>statsequipment_2876718a648dea03aaafd4b5a63b1efe'] = 'Adiciona uma aba contendo gráficos sobre o navegador e sistema operacional utilizado pelos clientes ao Painel de Estatísticas.';
$_MODULE['<{statsequipment}prestashop>statsequipment_6602bbeb2956c035fb4cb5e844a4861b'] = 'Guia';
$_MODULE['<{statsequipment}prestashop>statsequipment_998e4c5c80f27dec552e99dfed34889a'] = 'Exportar CSV';
$_MODULE['<{statsequipment}prestashop>statsequipment_bb38096ab39160dc20d44f3ea6b44507'] = 'Plugins';
