<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_c9a6962d020e53e6dd4651f272bfabe2'] = 'Deluxe Pago en Tienda';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_a9df443c850cf34d7a362bdeb7a56c49'] = 'Pago en Tienda';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_506157b172320dca2e326f43ba388a55'] = 'Puedes aceptar pagos en tu tienda física.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_efc226b17e0532afff43be870bff0de7'] = 'Los cambios han sido guardados';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_32e9a0e1e3a971625b22a6f0bb6a189a'] = 'No se insertó la categoría';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_6c58031e7824c3cd6febc2f2107b0652'] = 'Configuración actualizada.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_805112206c8fbbc14e5c9de5a4ad0ef0'] = 'No se insertó el producto.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_292e0bcc4e1d5c6b1d604646cc33d3b3'] = 'No se insertó el fabricante';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_6e4b6ac3903df1275224e801496626a0'] = 'No se insertó el proveedor';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_4457d440870ad6d42bab9082d9bf9b61'] = 'Fijo';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_37be07209f53a5d636d5c904ca9ae64c'] = 'Porcentaje';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_79735062b996130c6cf396c4df968aca'] = 'Fijo+Porcentaje';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_254f642527b45bc260048e30704edb39'] = 'Configuración';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_a1fa27779242b4902f7ae3bdd5c6d508'] = 'Tipo';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_b2f40690858b404ed10e62bdf422c704'] = 'Cantidad';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_bb98358f7aa362c47f58654c42ac1280'] = 'Cantidad fija a añadir al coste del pedido.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_cc47e3746b8bc16cd392e5c77b2d8a21'] = 'Porcentaje a añadir al coste del pedido.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_3b004a0ef57c5118565bdc42433ecabc'] = 'Comisión minima';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_8d9e44bdc375016f07d71ad8cbb7e04d'] = 'Comisión mínima a añadir al coste del pedido.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_209e1cf195878f91470df4aa41405219'] = 'Comisión máxima';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_8cae844ba4b0d6b25b1215ad2b804bdb'] = 'Comisión máxima a añadir al coste del pedido.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_9f2abc95b499cbc04329d528f6dd8b88'] = 'Cantidad fija a añadir al coste del pedido.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_3e29c4c40ec32d3596135da0b823430e'] = 'Permitir todos los métodos de pago';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_7a4fff13e6d3890fecbc4001e5ef051d'] = 'Pedido mínimo:';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_8fb0b4e8f3998cd32c7943df217641e4'] = 'Pedido mínimo para habilitar este método de pago (0 para deshabilitar).';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_d20808273c94c1c7dde936dd30e8b295'] = 'Pedido máximo:';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_42b5b0f6b90fd18ad23b73beccb48b71'] = 'Pedido máximo para habilitar este método de pago (0 para deshabilitar).';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_ca6f69caa556b8d3d7e2c128c90004c1'] = 'Cantidad mínima para comisión gratuita:';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_29509bbb343946dcfb06ff15409f0b8e'] = 'Importe pedido desde el cual la comisión es gratuita (0 para deshabilitar).';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_538b0040eddb7fb0873d4f0d2e102082'] = 'Permitir Pago en Tienda sólo si este transportista esta seleccionado:';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_2429d1214a9b3ed058cf8a69a0e59cef'] = 'Seleccione los transportistas para el método de pago en tienda habilitado.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_1a01bf6bd2e574742b398651992ff03a'] = 'Gestione sus restricciones por categoria, producto, proveedor o fabricante';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_e4d6f977f5ff18d2d94ae8bf10eff660'] = 'Administre sus restricciones';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_46093614b53f59cb71cb3ac8cd36483c'] = 'Restricción por categoría';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_3adbdb3ac060038aa0e6e6c138ef9873'] = 'Categoría';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_0b0e00bd9dd6829f89ec09645c934ac5'] = 'Restringir categoría';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_f4931b54ca664d34756fdc953db3cadf'] = 'Restricción por producto';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_deb10517653c255364175796ace3553f'] = 'Producto';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_feac3f603e322cc6feae07d3b913f7d9'] = 'Restringir por fabricante';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Fabricante';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_cac385b5742daa42a756cd6293f6fc7e'] = 'Restringir por proveedor';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_ec136b444eede3bc85639fac0dd06229'] = 'Proveedor';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_35fdbcbd1520e80343a131d9d472fcb1'] = 'Restringir proveedor';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_33e3cb8ab97a6c63e92492dc27da2918'] = 'Restringir por categoría';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_b481d1059720265c0835830d5bdc54b5'] = 'Este módulo permite a tus clientes elegir el pago en tu tienda física.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_3d4ce2f59b9a98a4104e20da815e686a'] = 'La categoría no se pudo eliminar.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_181062cf9a92a6649168dbd7cd8466c5'] = 'Los cambios han sido eliminados';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_40dee352f41ea8483fcff599183c6504'] = 'El producto no se pudo eliminar.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_c9d0119ec901f48327cfaa05c1e8b300'] = 'El fabricante no se pudo eliminar.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_06566649505024a8a793dcf746c00244'] = 'El proveedor no se pudo eliminar.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_d60b15553541d83bf7a4f5fc1810c5ac'] = 'Comisión reembolso incluída';
$_MODULE['<{deluxestorepayment}prestashop>validation_e2b7dec8fa4b498156dfee6e4c84b156'] = 'Este método de pago no esá disponible';
$_MODULE['<{deluxestorepayment}prestashop>validation_eb7297e94e2cb86e90a6ead067666717'] = 'Comisión';
$_MODULE['<{deluxestorepayment}prestashop>validation_c2fb74ea9be3c74eb2e86d86672f87cd'] = 'Comisión pago en tienda';
$_MODULE['<{deluxestorepayment}prestashop>validation_b24ce0cd392a5b0b8dedc66c25213594'] = 'Gratis';
$_MODULE['<{deluxestorepayment}prestashop>validation_e03d0c6baeb72118bb480787536b316c'] = 'Efectivo en pago en tienda com comisión';
$_MODULE['<{deluxestorepayment}prestashop>validation_f1d3b424cd68795ecaa552883759aceb'] = 'Detalle del pedido';
$_MODULE['<{deluxestorepayment}prestashop>validation_572da7ef1411f2a12409e752f3eb2f7a'] = 'Su carrito está vacío.';
$_MODULE['<{deluxestorepayment}prestashop>validation_03cc9a9a5999a3c0eba7ab534ce6e614'] = 'Efectivo en pago en tienda con comisión';
$_MODULE['<{deluxestorepayment}prestashop>validation_f9d49e6a39bdc491898bead184e36105'] = 'Has elegido el método de pago en tienda';
$_MODULE['<{deluxestorepayment}prestashop>validation_965e8ec03f7b6aeeb4bd1a95f3587ea2'] = 'Detalle de su pedido (impuestos incluídos)';
$_MODULE['<{deluxestorepayment}prestashop>validation_2ca3deb5cd68fa9119b285804fab572f'] = 'Pedido:';
$_MODULE['<{deluxestorepayment}prestashop>validation_591e94b932145579f74fa5dcd191bf98'] = 'Transporte:';
$_MODULE['<{deluxestorepayment}prestashop>validation_978811b06868760a2f0bddde785615f3'] = 'Efectivo pago en tienda con comisión';
$_MODULE['<{deluxestorepayment}prestashop>validation_a0177d57ceb462f5e42a98b1ed0e26b6'] = 'Efectivo pago en tienda con comisión (libre de';
$_MODULE['<{deluxestorepayment}prestashop>validation_61482a397a95aa3b77f0f184cc556079'] = 'de compra):';
$_MODULE['<{deluxestorepayment}prestashop>validation_0269dd261581c98f23f62235bfe868d3'] = 'La comisión de pago en tienda será añadida al coste del transporte';
$_MODULE['<{deluxestorepayment}prestashop>validation_66c4c5112f455a19afde47829df363fa'] = 'Total:';
$_MODULE['<{deluxestorepayment}prestashop>validation_7060a20aa356eeca216a2e0b580331fc'] = 'Por favor, confirme su pedido haciendo click en \'Confirmar mi pedido\'';
$_MODULE['<{deluxestorepayment}prestashop>validation_52bab1c3ac8aff9159be10acf1d5e55e'] = 'Confirmar mi pedido';
$_MODULE['<{deluxestorepayment}prestashop>validation_569fd05bdafa1712c4f6be5b153b8418'] = 'Otros métodos de pago';
$_MODULE['<{deluxestorepayment}prestashop>payment_a9df443c850cf34d7a362bdeb7a56c49'] = 'Pago en tienda';
$_MODULE['<{deluxestorepayment}prestashop>payment_45670e214c88963bd379248632b19316'] = 'Pago en Tienda';
$_MODULE['<{deluxestorepayment}prestashop>payment_0624cba615efba023ef0d25f2bc14819'] = '(Comisión)';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_2e2117b7c81aa9ea6931641ea2c6499f'] = 'Su pedido en';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_75fbf512d744977d62599cc3f0ae2bb4'] = 'está completado.';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_b2f65d5942124f7dcec73e1cd527edea'] = 'El pedido lo pagarás en nuestra tienda.';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_3be457684cc05cc5e2d72cb209381d3e'] = 'Total pago pendiente:';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_49849739f555740f5e7443d67e3486a6'] = 'Para cualquier consulta o información, por favor contacte con nosotros.';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_576e30e62b4ca426d0401412102fc9df'] = 'Atención al cliente.';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_300225ee958b6350abc51805dab83c24'] = 'Continuar comprando';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_0222ac7dbb5716cabc1f213fef92f654'] = 'Hay un problema con su pedido. Si usted piensa que es un error, contacte con nosotros.';
