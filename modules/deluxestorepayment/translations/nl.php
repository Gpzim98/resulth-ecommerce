<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_c9a6962d020e53e6dd4651f272bfabe2'] = 'Deluxe Bezahlung im Ladengeschäft';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_a9df443c850cf34d7a362bdeb7a56c49'] = 'Bezahlung im Ladengeschäft';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_506157b172320dca2e326f43ba388a55'] = 'Sie können Zahlungen in Ihrem Ladengeschäft entgegennehmen';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_cb5cd2de68100b8f01550a566228ec33'] = 'Die Kommission für die Bezahlung im Ladengeschäft wurde angewendet';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_c9cc8cce247e49bae79f15173ce97354'] = 'Opslaan';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_efc226b17e0532afff43be870bff0de7'] = 'Die Änderungen wurden gespeichert';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_32e9a0e1e3a971625b22a6f0bb6a189a'] = 'Die Kategorie wurde nicht eingefügt';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_6c58031e7824c3cd6febc2f2107b0652'] = 'Die Konfiguration wurde aktualisiert';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_805112206c8fbbc14e5c9de5a4ad0ef0'] = 'Es wurde kein Produkt eingefügt';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_292e0bcc4e1d5c6b1d604646cc33d3b3'] = 'Es wurde kein Hersteller eingefügt';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_6e4b6ac3903df1275224e801496626a0'] = 'Es wurde kein Anbieter eingefügt';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_4457d440870ad6d42bab9082d9bf9b61'] = 'Vast';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_37be07209f53a5d636d5c904ca9ae64c'] = 'Percentage';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_79735062b996130c6cf396c4df968aca'] = 'Vast+Percentage';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_254f642527b45bc260048e30704edb39'] = 'Configuratie';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_a1fa27779242b4902f7ae3bdd5c6d508'] = 'Type';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_b2f40690858b404ed10e62bdf422c704'] = 'Hoeveelheid';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_bb98358f7aa362c47f58654c42ac1280'] = 'Vast bedrag bovenop de prijs van de bestelling';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_cc47e3746b8bc16cd392e5c77b2d8a21'] = 'Percentage bovenop de prijs van de bestelling';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_3b004a0ef57c5118565bdc42433ecabc'] = 'Minimale commissie';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_8d9e44bdc375016f07d71ad8cbb7e04d'] = 'Minimale commissie bovenop de prijs van de bestelling';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_209e1cf195878f91470df4aa41405219'] = 'Maximale commissie';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_8cae844ba4b0d6b25b1215ad2b804bdb'] = 'Maximale commissie bovenop de prijs van de bestelling';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_9f2abc95b499cbc04329d528f6dd8b88'] = 'Vast bedrag bovenop de prijs van de bestelling';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_7a4fff13e6d3890fecbc4001e5ef051d'] = 'Minimale bestelling';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_8fb0b4e8f3998cd32c7943df217641e4'] = 'Minimale bestelling om deze betalingswijze mogelijk te maken (0 om uit te schakelen)';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_d20808273c94c1c7dde936dd30e8b295'] = 'Maximale bestelling';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_42b5b0f6b90fd18ad23b73beccb48b71'] = 'Maximale bestelling om deze betalingswijze mogelijk te maken (0 om uit te schakelen)';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_ca6f69caa556b8d3d7e2c128c90004c1'] = 'Minimaal bedrag voor gratis commissie';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_29509bbb343946dcfb06ff15409f0b8e'] = 'Opgevraagd bedrag vanaf waar de commissie gratis is (0 om uit te schakelen)';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_2429d1214a9b3ed058cf8a69a0e59cef'] = 'Wählen Sie die Lieferanten für die Zahlungsmethode entsprechende “Bezahlung im Ladengeschäft”.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_1a01bf6bd2e574742b398651992ff03a'] = 'Beheer uw beperkingen per categorie, product, leverancier of fabrikant';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_e4d6f977f5ff18d2d94ae8bf10eff660'] = 'Beheer uw beperkingen';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_46093614b53f59cb71cb3ac8cd36483c'] = 'Beperking per categorie';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_3adbdb3ac060038aa0e6e6c138ef9873'] = 'Categorie';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_0b0e00bd9dd6829f89ec09645c934ac5'] = 'Categorie beperken';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_f4931b54ca664d34756fdc953db3cadf'] = 'Beperking per product';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_deb10517653c255364175796ace3553f'] = 'Product';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_feac3f603e322cc6feae07d3b913f7d9'] = 'Beperken per fabrikant';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Fabrikant';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_cac385b5742daa42a756cd6293f6fc7e'] = 'Beperken per leverancier';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_ec136b444eede3bc85639fac0dd06229'] = 'Leverancier';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_35fdbcbd1520e80343a131d9d472fcb1'] = 'Leverancier beperken';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_33e3cb8ab97a6c63e92492dc27da2918'] = 'Beperken per categorie';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_b481d1059720265c0835830d5bdc54b5'] = 'Dieses Modul erlaubt es Ihren Kunden, die Zahlungsmethode in Ihrem Ladengeschäft auszuwählen';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_3d4ce2f59b9a98a4104e20da815e686a'] = 'Die Kategorie konnte nicht gelöscht werden.';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_181062cf9a92a6649168dbd7cd8466c5'] = 'De wijzigingen zijn verwijderd';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_40dee352f41ea8483fcff599183c6504'] = 'Das Produkt konnte nicht gelöscht werden';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_c9d0119ec901f48327cfaa05c1e8b300'] = 'Der Hersteller konnte nicht gelöscht werden';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_06566649505024a8a793dcf746c00244'] = 'Der Anbieter konnte nicht gelöscht werden';
$_MODULE['<{deluxestorepayment}prestashop>deluxestorepayment_d60b15553541d83bf7a4f5fc1810c5ac'] = 'Zahlungskommission eingeschlossen.';
$_MODULE['<{deluxestorepayment}prestashop>validation_e2b7dec8fa4b498156dfee6e4c84b156'] = 'Deze betalingswijze is niet beschikbaar';
$_MODULE['<{deluxestorepayment}prestashop>validation_eb7297e94e2cb86e90a6ead067666717'] = 'Kommission';
$_MODULE['<{deluxestorepayment}prestashop>validation_c2fb74ea9be3c74eb2e86d86672f87cd'] = 'Kommission für Bezahlung im Ladengeschäft';
$_MODULE['<{deluxestorepayment}prestashop>validation_b24ce0cd392a5b0b8dedc66c25213594'] = 'Gratis';
$_MODULE['<{deluxestorepayment}prestashop>validation_e03d0c6baeb72118bb480787536b316c'] = 'Zu zahlender Betrag im Ladengeschäft mit Kommission';
$_MODULE['<{deluxestorepayment}prestashop>validation_f1d3b424cd68795ecaa552883759aceb'] = 'Details van de bestelling';
$_MODULE['<{deluxestorepayment}prestashop>validation_572da7ef1411f2a12409e752f3eb2f7a'] = 'Uw winkelmandje is leeg';
$_MODULE['<{deluxestorepayment}prestashop>validation_03cc9a9a5999a3c0eba7ab534ce6e614'] = 'Zu zahlender Betrag im Ladengeschäft mit Kommission';
$_MODULE['<{deluxestorepayment}prestashop>validation_f9d49e6a39bdc491898bead184e36105'] = 'Sie haben die Methode “Bezahlung bei Abholung” gewählt';
$_MODULE['<{deluxestorepayment}prestashop>validation_965e8ec03f7b6aeeb4bd1a95f3587ea2'] = 'Einzelheiten zu Ihrer Bestellung (inkl. Steuern)';
$_MODULE['<{deluxestorepayment}prestashop>validation_2ca3deb5cd68fa9119b285804fab572f'] = 'Bestelling';
$_MODULE['<{deluxestorepayment}prestashop>validation_591e94b932145579f74fa5dcd191bf98'] = 'Verzending';
$_MODULE['<{deluxestorepayment}prestashop>validation_978811b06868760a2f0bddde785615f3'] = 'Zu zahlender Betrag im Ladengeschäft mit Kommission';
$_MODULE['<{deluxestorepayment}prestashop>validation_a0177d57ceb462f5e42a98b1ed0e26b6'] = 'Zu zahlender Betrag im Ladengeschäft mit Kommission (frei von';
$_MODULE['<{deluxestorepayment}prestashop>validation_61482a397a95aa3b77f0f184cc556079'] = 'des Kaufs)';
$_MODULE['<{deluxestorepayment}prestashop>validation_0269dd261581c98f23f62235bfe868d3'] = 'Die Kommission für die Bezahlung bei Abholung wird den Transportkosten hinzugefügt';
$_MODULE['<{deluxestorepayment}prestashop>validation_66c4c5112f455a19afde47829df363fa'] = 'Totaal';
$_MODULE['<{deluxestorepayment}prestashop>validation_7060a20aa356eeca216a2e0b580331fc'] = 'Gelieve uw bestelling te bevestigen door op \"Bevestig mijn bestelling\" te klikken';
$_MODULE['<{deluxestorepayment}prestashop>validation_52bab1c3ac8aff9159be10acf1d5e55e'] = 'Bevestig mijn bestelling';
$_MODULE['<{deluxestorepayment}prestashop>validation_569fd05bdafa1712c4f6be5b153b8418'] = 'Andere betalingswijzen';
$_MODULE['<{deluxestorepayment}prestashop>payment_a9df443c850cf34d7a362bdeb7a56c49'] = 'Bezahlung im Ladengeschäft.';
$_MODULE['<{deluxestorepayment}prestashop>payment_45670e214c88963bd379248632b19316'] = 'Bezahlung im Ladengeschäft';
$_MODULE['<{deluxestorepayment}prestashop>payment_0624cba615efba023ef0d25f2bc14819'] = '(Kommission)';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_2e2117b7c81aa9ea6931641ea2c6499f'] = 'Ihre Bestellung in';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_75fbf512d744977d62599cc3f0ae2bb4'] = 'ist abgeschlossen';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_b2f65d5942124f7dcec73e1cd527edea'] = 'Die Bestellung bezahlen Sie in unserem Ladengeschäft';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_3be457684cc05cc5e2d72cb209381d3e'] = 'Ausstehender Betrag:';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_49849739f555740f5e7443d67e3486a6'] = 'Voor vragen of aanvullende informatie gelieve contact met ons op te nemen';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_576e30e62b4ca426d0401412102fc9df'] = 'Klantenservice';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_300225ee958b6350abc51805dab83c24'] = 'Verder winkelen';
$_MODULE['<{deluxestorepayment}prestashop>payment_return_0222ac7dbb5716cabc1f213fef92f654'] = 'Er heeft zich een probleem voorgedaan met uw bestelling. Als u denkt dat het om een fout gaat, gelieve contact met ons op te nemen';
