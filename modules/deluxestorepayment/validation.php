<?php
/**
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2014 Innova Deluxe SL

* @license   INNOVADELUXE
*/

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include(dirname(__FILE__).'/deluxestorepayment.php');

$context = Context::getContext();
$cart = $context->cart;

$storePayment = new DeluxeStorePayment();

if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$storePayment->active)
	Tools::redirect('index.php?controller=order&step=1');

$authorized = false;
foreach (Module::getPaymentModules() as $module)
	if ($module['name'] == 'deluxestorepayment')
	{
		$authorized = true;
		break;
	}
if (!$authorized)
	die($storePayment->l('This payment method is not available.', 'validation'));

$customer = new Customer((int)$cart->id_customer);

if (!Validate::isLoadedObject($customer))
	Tools::redirect('index.php?controller=order&step=1');

$currency = $context->currency;
$total = (float)($cart->getOrderTotal(true, Cart::BOTH));

$storePayment->validateOrder($cart->id, Configuration::get('PS_OS_STOREPAYMENT'), $total, $storePayment->displayName, NULL, array(), (int)$currency->id, false, $customer->secure_key);

$order = new Order($storePayment->currentOrder);
Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$storePayment->id.'&id_order='.$storePayment->currentOrder.'&key='.$customer->secure_key);
