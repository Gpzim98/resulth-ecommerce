<?php
/**
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2014 Innova Deluxe SL

* @license   INNOVADELUXE
*/

//SSL Management
$useSSL = true;

require('../../config/config.inc.php');
Tools::displayFileAsDeprecated();

$controller = new FrontController();
$controller->init();

Tools::redirect(Context::getContext()->link->getModuleLink('deluxestorepayment', 'payment'));