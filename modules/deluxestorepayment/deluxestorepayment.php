<?php
/**
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * @author    Innova Deluxe SL
 * @copyright 2015 Innova Deluxe SL

 * @license   INNOVADELUXE
 */

if (!defined('_PS_VERSION_'))
	exit;

class DeluxeStorePayment extends PaymentModule
{
	private $admin_img ='deluxestorepayment_admin.gif';
	private $ico = '';
	public $es15;
	
	public function __construct()
	{
		$this->name = 'deluxestorepayment';
		$this->tab = 'payments_gateways';
		$this->version = '1.6';
		$this->module_key ='98ad3fee20e1c470d972c6a034f3e412';
		$this->author = 'Innova Deluxe';
		$this->controllers = array('payment', 'validation');
		$this->es15 = version_compare(_PS_VERSION_, '1.6.0.0', '<');
		
		$this->bootstrap = true;
		parent::__construct();	

		$this->displayName = $this->l('Deluxe Store Payment');
		$this->pmethod = $this->l('Store payment');
		$this->description = $this->l('You can accept payments in your store');
	}

	public function install()
	{
		// install sql
		$table_def = Db::getInstance()->Execute('
		CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->name.'_categories` (
			`id_forbidden_cat` int(10) unsigned NOT NULL auto_increment,
			`id_shop` int(10) unsigned NOT NULL,
            `id_categoria` int(10) unsigned NOT NULL,
			PRIMARY KEY (`id_forbidden_cat`)
		) DEFAULT CHARSET=utf8');
        
       $table_def &= Db::getInstance()->Execute('
		CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->name.'_products` (
			`id_forbidden_prod` int(10) unsigned NOT NULL auto_increment,
            `id_shop` int(10) unsigned NOT NULL,
			`id_product` int(10) unsigned NOT NULL,
			PRIMARY KEY (`id_forbidden_prod`)
		) DEFAULT CHARSET=utf8');
        
        $table_def &= Db::getInstance()->Execute('
		CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->name.'_manufacturers` (
			`id_forbidden_manuf` int(10) unsigned NOT NULL auto_increment,
            `id_shop` int(10) unsigned NOT NULL,
			`id_manuf` int(10) unsigned NOT NULL,
			PRIMARY KEY (`id_forbidden_manuf`)
		) DEFAULT CHARSET=utf8');
        
        $table_def &= Db::getInstance()->Execute('
		CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->name.'_suppliers` (
			`id_forbidden_supp` int(10) unsigned NOT NULL auto_increment,
            `id_shop` int(10) unsigned NOT NULL,
			`id_supp` int(10) unsigned NOT NULL,
			PRIMARY KEY (`id_forbidden_supp`)
		) DEFAULT CHARSET=utf8');
        
        if (!$table_def){ parent::uninstall(); return false;} 
       

        // new order state
		$languages = Language::getLanguages(false);
		$orderState = new OrderState();
		$orderState->name = array();
		$status_lang = $this->switchStatusLang($this->context->language->iso_code);
		foreach ($languages as $language)
		{
			$orderState->name[$language['id_lang']] = $status_lang;
		}
		$orderState->send_email = 0;
		$orderState->color = '#46C5F0';
		$orderState->module_name = 'deluxestorepayment';
		$orderState->hidden = false;
		$orderState->delivery = false;
		$orderState->logable = 0;
		$orderState->invoice = false;
			
		if (!$orderState->add())
		{
			Db::getInstance()->Execute('DROP TABLE `' . _DB_PREFIX_ . $this->name.'_categories`');
			Db::getInstance()->Execute('DROP TABLE `' . _DB_PREFIX_ . $this->name.'_products`');
			Db::getInstance()->Execute('DROP TABLE `' . _DB_PREFIX_ . $this->name.'_manufacturers`');
			Db::getInstance()->Execute('DROP TABLE `' . _DB_PREFIX_ . $this->name.'_suppliers`');
			return false;
		}	
		Configuration::updateValue('PS_OS_STOREPAYMENT', (int)$orderState->id);
		
		// copy logo 
		$img_new_name = $orderState->id.'.gif';
		if(_PS_VERSION_ > '1.5.3')
		{
			if (!Tools::copy( _PS_MODULE_DIR_.$this->name.'/img/'.$this->admin_img,  _PS_MODULE_DIR_.$this->name.'/img/'.$img_new_name)) return false;
			if (!Tools::copy(_PS_MODULE_DIR_.$this->name.'/img/'.$img_new_name, _PS_IMG_DIR_.'os/'.$img_new_name)) return false;
		}else{
			copy( _PS_MODULE_DIR_.$this->name.'/img/'.$this->admin_img,  _PS_MODULE_DIR_.$this->name.'/img/'.$img_new_name);
			copy(_PS_MODULE_DIR_.$this->name.'/img/'.$img_new_name, _PS_IMG_DIR_.'os/'.$img_new_name);
		}
		$this->ico = $img_new_name;
		
		Configuration::updateValue(Tools::strtoupper($this->name).'_TAX', '0');
		Configuration::updateValue(Tools::strtoupper($this->name), '0');
		Configuration::updateValue(Tools::strtoupper($this->name).'_FREE', '0');
		Configuration::updateValue(Tools::strtoupper($this->name).'_TYPE', '0');
		Configuration::updateValue(Tools::strtoupper($this->name).'_MIN', '0');
		Configuration::updateValue(Tools::strtoupper($this->name).'_MAX', '0');
		Configuration::updateValue(Tools::strtoupper($this->name).'_MIN_AMOUNT', '0');
		Configuration::updateValue(Tools::strtoupper($this->name).'_MAX_AMOUNT', '0');
		Configuration::updateValue(Tools::strtoupper($this->name).'_CARRIERS', '0');
        Configuration::updateValue(Tools::strtoupper($this->name).'_FREE_CARRIER', '0');
        
        return parent::install() && 
			$this->registerHook('displayPayment') &&
			$this->registerHook('displayPaymentReturn') &&
			$this->registerHook('displayPDFInvoice');
	}

	public function uninstall()
	{

		$idState = Configuration::get('PS_OS_STOREPAYMENT');
		$delete_ico = $idState.'.gif';
		if(_PS_VERSION_ > '1.5.3')
		{
			Tools::deleteFile(_PS_IMG_DIR_.'os/'.$delete_ico);
		}else{
			unlink(_PS_IMG_DIR_.'os/'.$delete_ico);
		}
		
		Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'order_state` SET `deleted` = 1 WHERE `id_order_state` = '.(int)$idState);
		if (!Db::getInstance()->Execute('DROP TABLE `' . _DB_PREFIX_ . $this->name.'_categories`')) return false;
		if (!Db::getInstance()->Execute('DROP TABLE `' . _DB_PREFIX_ . $this->name.'_products`')) return false;
		if (!Db::getInstance()->Execute('DROP TABLE `' . _DB_PREFIX_ . $this->name.'_manufacturers`')) return false;
		if (!Db::getInstance()->Execute('DROP TABLE `' . _DB_PREFIX_ . $this->name.'_suppliers`')) return false;
		
		Configuration::deleteByName('PS_OS_STOREPAYMENT');
		Configuration::deleteByName(Tools::strtoupper($this->name).'_TAX');
		Configuration::deleteByName(Tools::strtoupper($this->name));
		Configuration::deleteByName(Tools::strtoupper($this->name).'_FREE');
		Configuration::deleteByName(Tools::strtoupper($this->name).'_TYPE');
		Configuration::deleteByName(Tools::strtoupper($this->name).'_MIN');
		Configuration::deleteByName(Tools::strtoupper($this->name).'_MAX');
		Configuration::deleteByName(Tools::strtoupper($this->name).'_MIN_AMOUNT');
		Configuration::deleteByName(Tools::strtoupper($this->name).'_MAX_AMOUNT');
		Configuration::deleteByName(Tools::strtoupper($this->name).'_CARRIERS');
		Configuration::deleteByName('CONF_DELUXESTOREPAYMENT_FIXED');
		Configuration::deleteByName('CONF_DELUXESTOREPAYMENT_VAR');
		Configuration::deleteByName('CONF_DELUXESTOREPAYMENT_FIXED_FOREIGN');
		Configuration::deleteByName('CONF_DELUXESTOREPAYMENT_VAR_FOREIGN');
		
		return parent::uninstall();
	}
    
	public function hookDisplayPayment($params)
	{
		if ($this->active)
		{
		$show = true;
		$current_cart = $params['cart'];
		// Controlar si en el carro hay algun producto de las categorias prohibidas.
		$products = $current_cart->getProducts();
		
		foreach($products as $product)
		{
			$forbidden_categories = Db::getInstance()->ExecuteS('
				SELECT `id_categoria` FROM  `'._DB_PREFIX_.$this->name.'_categories`
				WHERE id_categoria = '.$product['id_category_default'].'
			');
						
				if ($forbidden_categories) $show = false;
			
			$forbidden_products = Db::getInstance()->ExecuteS('
				SELECT `id_product` FROM  `'._DB_PREFIX_.$this->name.'_products`
				WHERE id_product = '.$product['id_product'].'
			');
			
				if ($forbidden_products) $show = false;
            
			$forbidden_manufacturers = Db::getInstance()->ExecuteS('
				SELECT `id_manuf` FROM  `'._DB_PREFIX_.$this->name.'_manufacturers`
				WHERE id_manuf = '.$product['id_manufacturer'].'
			');
            
				if ($forbidden_manufacturers) $show = false;
            
			$forbidden_suppliers = Db::getInstance()->ExecuteS('
				SELECT `id_supp` FROM  `'._DB_PREFIX_.$this->name.'_suppliers`
				WHERE id_supp = '.$product['id_supplier'].'
			');
            
				if ($forbidden_suppliers) $show = false;
		}
        
		$minimum_amount = Configuration::get(Tools::strtoupper($this->name).'_MIN_AMOUNT');
		$maximum_amount = Configuration::get(Tools::strtoupper($this->name).'_MAX_AMOUNT');
		$fee = (float)Tools::ps_round((float)$this->getFeeCost($params['cart']), 2);
		$cartcost = $params['cart']->getOrderTotal(true, 3);
		$total = $fee + $cartcost;
		$id_carriers_selected_array = explode(';', Configuration::get(Tools::strtoupper($this->name).'_CARRIERS'));
		$this->smarty->assign(array(
			'es15' =>$this->es15,
			'ps_version' => _PS_VERSION_,
            'allow_payments' => Configuration::get(Tools::strtoupper($this->name).'_FREE_CARRIER'),
			'show' => $show,
			'this_path' => $this->_path,
			'cartcost' => number_format((float)$cartcost, 2, '.', ''),
			'fee' => number_format((float)$fee, 2, '.', ''),
			'minimum_amount' => number_format((float)$minimum_amount, 2, '.', ''),
			'maximum_amount' => number_format((float)$maximum_amount, 2, '.', ''),
			'total' => number_format((float)$total, 2, '.', ''),
			'carriers_array' => $id_carriers_selected_array,
			'carrier_selected' => $params['cart']->id_carrier,
			'this_path_ssl' => (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8').__PS_BASE_URI__.'modules/'.$this->name.'/'
		));
		return $this->display(__FILE__,'payment.tpl');
		}
	}
	
	public function hookDisplayPaymentReturn($params)
	{
		if ($this->active)
		{
				$state = $params['objOrder']->getCurrentState();
				
				if ($state == Configuration::get('PS_OS_STOREPAYMENT') || $state == Configuration::get('PS_OS_OUTOFSTOCK'))
				{
					$this->smarty->assign(array(
						'total' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
						'success' => true,
						'id_order' => $params['objOrder']->id,
						'es15' => $this->es15,
					));
				}
				else{
					$this->smarty->assign(array(
						'status'=> false,
						'success' => false,
					));
				}
				return $this->display(__FILE__, 'payment_return.tpl');
		}    
	}
	
	
	public function getContent()
	{
		$output = $this->innovaTitle();
		$output .= $this->postProcess() . $this->renderForm();
		return $output;
	}
    
	public function postProcess()
	{	
		
		$foo = Tools::getValue('deluxecod_in');
		if (Tools::isSubmit('submitSettings') && (Tools::getValue('submitSettings') == 1 ||
			Tools::getValue('submitSettings')) == $this->l('Save') && empty($foo))
		{
			if (Tools::isEmpty(Tools::getValue('fee_tax')))
				$_POST['fee_tax'] = '0';
			if (Tools::isEmpty(Tools::getValue('fee')))
				$_POST['fee'] = '0';
			if (Tools::isEmpty(Tools::getValue('free_fee')))
				$_POST['free_fee'] = '0';
			if (Tools::isEmpty(Tools::getValue('feetype')))
				$_POST['feetype'] = '0';
			if (Tools::isEmpty(Tools::getValue('feemin')))
				$_POST['feemin'] = '0';
			if (Tools::isEmpty(Tools::getValue('feemax')))
				$_POST['feemax'] = '0';
			if (Tools::isEmpty(Tools::getValue('minimum_amount')))
				$_POST['minimum_amount'] = '0';
			if (Tools::isEmpty(Tools::getValue('maximum_amount')))
				$_POST['maximum_amount'] = '0';
			if (Tools::isEmpty(Tools::getValue('id_carriers')))
				$_POST['id_carriers'] = '0';
			
			if(Tools::getValue('id_carrier'))
			{
				$carriers = Tools::getValue('id_carrier');
				$all_carriers = implode($carriers,';');
			}else{
				$all_carriers = '0';
			}
			
            $selected_carrier[0] = 0;
            if (Tools::getValue('select_free_carrier')) $selected_carrier = (Tools::getValue('select_free_carrier'));
                        
			Configuration::updateValue(Tools::strtoupper($this->name).'_TAX', (float)str_replace(',','.',Tools::getValue('fee_tax')));
			Configuration::updateValue(Tools::strtoupper($this->name), (float)str_replace(',','.',Tools::getValue('fee')));
			Configuration::updateValue(Tools::strtoupper($this->name).'_FREE', (float)str_replace(',','.',Tools::getValue('free_fee')));
			Configuration::updateValue(Tools::strtoupper($this->name).'_TYPE', (float)str_replace(',','.',Tools::getValue('feetype')));
			Configuration::updateValue(Tools::strtoupper($this->name).'_MIN', (float)str_replace(',','.',Tools::getValue('feemin')));
			Configuration::updateValue(Tools::strtoupper($this->name).'_MAX', (float)str_replace(',','.',Tools::getValue('feemax')));
			Configuration::updateValue(Tools::strtoupper($this->name).'_MIN_AMOUNT', (float)str_replace(',','.',Tools::getValue('minimum_amount')));
			Configuration::updateValue(Tools::strtoupper($this->name).'_MAX_AMOUNT', (float)str_replace(',','.',Tools::getValue('maximum_amount')));
			Configuration::updateValue(Tools::strtoupper($this->name).'_CARRIERS', trim($all_carriers, ';'));
            Configuration::updateValue(Tools::strtoupper($this->name).'_FREE_CARRIER', $selected_carrier[0]);
			return $this->displayConfirmation($this->l('The settings have been updated.'));
		}
		
		if (Tools::isSubmit('submitRestrictCategory'))
		{
		        if (!Db::getInstance()->Execute('
			    INSERT INTO `'._DB_PREFIX_.$this->name.'_categories`
			    VALUES ("", "'.(int)$this->context->shop->id.'", "'.(int)Tools::getValue('deluxecategory').'")
					')
			) return $this->displayError ($this->l('The category have been not inserted'));
			
				return $this->displayConfirmation($this->l('Configuration updated.'));
		}
		
		if (Tools::isSubmit('submitRestrictProduct'))
		{
			if (!Db::getInstance()->Execute('
				INSERT INTO `'._DB_PREFIX_.$this->name.'_products`
				VALUES ("", "'.(int)$this->context->shop->id.'", "'.(int)Tools::getValue('deluxeproduct').'")
		   		')
			) return $this->displayError ($this->l('The product have been not inserted'));
			return $this->displayConfirmation($this->l('Configuration updated.'));
		}
		
		if (Tools::isSubmit('submitRestrictManufacture'))
		{
			if (!Db::getInstance()->Execute('
				INSERT INTO `'._DB_PREFIX_.$this->name.'_manufacturers`
				VALUES ("", "'.(int)$this->context->shop->id.'", "'.(int)Tools::getValue('deluxemanuf').'")
		   		')
			) return $this->displayError ($this->l('The manufacturer have been not inserted'));
			return $this->displayConfirmation($this->l('Configuration updated.'));
		}
		
		if (Tools::isSubmit('submitRestrictSupplier'))
		{
			if (!Db::getInstance()->Execute('
				INSERT INTO `'._DB_PREFIX_.$this->name.'_suppliers`
				VALUES ("", "'.(int)$this->context->shop->id.'", "'.(int)Tools::getValue('deluxesupp').'")
		   		')
			) return $this->displayError ($this->l('The supplier have been not inserted'));
			return $this->displayConfirmation($this->l('Configuration updated.'));
		}
		
		if (Tools::isSubmit("delete". _DB_PREFIX_.$this->name."_categories"))
			$this->deleteCodFeeCategory(Tools::getValue('id_configuration'));
		if (Tools::isSubmit("delete". _DB_PREFIX_.$this->name."_products"))
			$this->deleteCodFeeProduct(Tools::getValue('id_configuration'));
		if (Tools::isSubmit("delete". _DB_PREFIX_.$this->name."_manufacturers"))
			$this->deleteCodFeeManufacturer(Tools::getValue('id_configuration'));
		if (Tools::isSubmit("delete". _DB_PREFIX_.$this->name."_suppliers"))
			$this->deleteCodFeeSupplier(Tools::getValue('id_configuration'));
		return '';
	}

	public function renderForm()
	{
		$comision_type_op = array(
			array(
				'id_option' => '0',                 
				'name' => $this->l('Fixed')           
			),
			array(
				'id_option' => '1',
				'name' => $this->l('Percentage')     
			),
			array(
				'id_option' => '2',
				'name' => $this->l('Fixed+Percentage')     
			)
		);
		
		$js = '';
		if(!$this->es15)
		{
			$js= '$(\'input[name=submitSettings]\').attr(\'value\', \'0\');$(\'#module_form\').submit();';
		}
		else if($this->es15)
		{
			$js='if ($(\'#customdeluxebutt\').length == 0){$(\'#module_form\').append(\'<input id=customdeluxebutt type=hidden name=deluxecod_in value=99>\');} ;
					$(\'#customdeluxebutt\').attr(\'value\', \'99\');
					$(\'#module_form\').submit();
			';
		}
		
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Configuration'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'select',
						'label' => $this->l('Type'),
						'name' => 'feetype',
						'desc' => $this->l('Type'),
						'onchange' => $js,
						'options' => array(
							'query' => $comision_type_op,
							'id' => 'id_option',
							'name' => 'name',
							'value' => 'id_option'
						)
					),
				)
			),
		);
		
		if (Tools::getValue('feetype') == 0 || Configuration::get(Tools::strtoupper($this->name).'_TYPE') == 0)
		{
			$fields_form['form']['input'][1] = array(
				'type' => 'text',
				'label' => $this->l('Amount'),
				'name' => 'fee',
				'desc' => $this->l('Fixed amount to add to the cost of the order.'),
				'type' => 'text',			
			);
		}
		
		if (Tools::getValue('feetype') == 1 || Configuration::get(Tools::strtoupper($this->name).'_TYPE') == 1)
		{
			$fields_form['form']['input'][1] = array(
				'type' => 'text',
				'label' => $this->l('Percentage'),
				'name' => 'fee_tax',
				'desc' => $this->l('Percentage to add to the cost of the order.'),
				'type' => 'text',			
			);
			$fields_form['form']['input'][] = array(
				'type' => 'text',
				'label' => $this->l('Minimum Fee'),
				'name' => 'feemin',
				'desc' => $this->l('Minimum fee to add to the cost of the order.'),
				'type' => 'text',			
			);
			$fields_form['form']['input'][] = array(
				'type' => 'text',
				'label' => $this->l('Maximum Fee'),
				'name' => 'feemax',
				'desc' => $this->l('Maximum fee to add to the cost of the order.'),
				'type' => 'text',			
			);
		}
		
		if (Tools::getValue('feetype') == 2 || Configuration::get(Tools::strtoupper($this->name).'_TYPE') == 2)
		{
			$fields_form['form']['input'][1] = array(
				'type' => 'text',
				'label' => $this->l('Amount'),
				'name' => 'fee',
				'desc' => $this->l('Fixed amount to add to the cost of the order..'),
				'type' => 'text',			
			);
			$fields_form['form']['input'][] = array(
				'type' => 'text',
				'label' => $this->l('Percentage'),
				'name' => 'fee_tax',
				'desc' => $this->l('Percentage to add to the cost of the order.'),
				'type' => 'text',			
			);
			$fields_form['form']['input'][] = array(
				'type' => 'text',
				'label' => $this->l('Minimum Fee'),
				'name' => 'feemin',
				'desc' => $this->l('Minimum fee to add to the cost of the order.'),
				'type' => 'text',			
			);
			$fields_form['form']['input'][] = array(
				'type' => 'text',
				'label' => $this->l('Maximum Fee'),
				'name' => 'feemax',
				'desc' => $this->l('Maximum fee to add to the cost of the order.'),
				'type' => 'text',			
			);
            $fields_form['form']['input'][] = array(
				'type' => 'text',
				'label' => $this->l('Maximum Fee'),
				'name' => 'feemax',
				'desc' => $this->l('Maximum fee to add to the cost of the order.'),
				'type' => 'text',			
			);
		}
        
		// set carriers 
		$carrier_options = array();
		foreach (Carrier::getCarriers($this->context->language->id, true, false, false, null, Carrier::ALL_CARRIERS) as $carrier)
		{
			$carrier_options[] = array(
					"id" => $carrier['id_carrier'],
					"name" => $carrier['name'],
					"val" => $carrier['id_carrier']
			);
		}
        
        $hide_carrier_options = $carrier_options;
        array_unshift($hide_carrier_options, array("id"=>0, "name"=>"Allow all payment methods", "val"=>0));
        		
    	$fields_form['form']['input'][] = array(
			'type' => 'text',			
			'label' => $this->l('Minimum amount:'),
			'name' => 'minimum_amount',
			'desc' => $this->l('Minimum amount to be available this payment method (0 to disable).')
		);
		$fields_form['form']['input'][] = array(
			'type' => 'text',
			'label' => $this->l('Maximum amount:'),
			'name' => 'maximum_amount',
			'desc' => $this->l('Maximum amount to be available this payment method (0 to disable).')				
		
		);
		
		$fields_form['form']['input'][] = array(
			'type' => 'text',
			'label' => $this->l('Minimum amount for free fee:'),
			'name' => 'free_fee',
			'desc' => $this->l('Amount from which the fee is free (0 to disable).')				
		
		);
        
        $fields_form['form']['input'][] = array(
			'type' => 'select',
			'label' => $this->l('Only allow Store Payments if this carrier is selected: '),
			'name' => 'select_free_carrier',
			'desc' => $this->l(''),
			'options' => array(
				  'query' => $hide_carrier_options,
				  'id' => 'id',
				  'name' => 'name'
			  ),
			'class' =>'deluxecodfee_carrier');
		
		$fields_form['form']['input'][] = array(
			'type' => 'checkbox',
			'label' => $this->l('Select the carriers with the method of payment in store enabled'),
			'name' => 'id_carrier[]',
			'desc' => $this->l(''),
			'values' => array(
				  'query' => $carrier_options,
				  'id' => 'id',
				  'name' => 'name'
			  ),
			'class' =>'deluxecodfee_carrier'
		);
		
		$fields_form['form']['input'][] = array(
			'type' => 'hidden',
			'id' =>'id_carriers',
			'name' => 'id_carriers'
		
		);
		
		$fields_form['form']['submit'] = array(
			'title' => $this->l('Save'),
			'class' => 'btn btn-default'				
		
		);
		
		$languages = Language::getLanguages(false);
		foreach ($languages as $k => $language) $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->languages = $languages;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitSettings';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		
		return $helper->generateForm(array($fields_form)) . $this->showConfiguration() . $this->renderRestrictCategory() . $this->renderRestrictProduct() .
		$this->renderRestrictManufacture() . $this->renderRestrictSupplier() . $this->renderListCategories() . $this->renderListProducts()
		. $this->renderListManufacturers() . $this->renderListSuppliers();
	}
	
	public function showConfiguration()
	{
		$fields_form = array(
			'form' => array(
					'legend' => array(
					'title' => $this->l('Manage your restrictions by category, product, supplier or manufacturers'),
					'icon' => 'icon-cogs'
				),
			'submit' => array(
				'title' => $this->l('Manage your restrictions'),
				'class' => 'btn btn-default',
				'icon' => 'process-icon-refresh')
			),
		);
		
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'showConfig';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'id_language' => $this->context->language->id
		);
		
		return $helper->generateForm(array($fields_form));
	}
	
public function renderRestrictCategory()
	{
		$categories = Db::getInstance()->ExecuteS('
		    SELECT DISTINCT c.`id_category`, cl.`name`
		    FROM `'._DB_PREFIX_.'category` c
		    LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category`)
		    WHERE cl.`id_lang` = '.(int)($this->context->language->id).'
		    AND c.`id_category` NOT IN (SELECT dl.`id_categoria` FROM  `'._DB_PREFIX_.$this->name.'_categories` dl WHERE dl.`id_shop` = "'.$this->context->shop->id.'")
		');
		
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Restriction by category'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'select',
						'label' => $this->l('Category'),
						'name' => 'deluxecategory',
						'desc' => $this->l(''),
						'options' => array(
							'query' => $categories,
								'id' => 'id_category',
								'name' => 'name'
						)
					)
				),
			'submit' => array(
				'title' => $this->l('Category restriction'),
				'class' => 'btn btn-default')
			),
		);
		
		$languages = Language::getLanguages(false);
		foreach ($languages as $k => $language) $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->languages = $languages;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitRestrictCategory';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		
		if (Tools::isSubmit('showConfig')){
			return $helper->generateForm(array($fields_form));
		}
	}
	
public function renderRestrictProduct()
	{
		$products = Db::getInstance()->ExecuteS('
		    SELECT DISTINCT c.`id_product`, cl.`name`
		    FROM `'._DB_PREFIX_.'product` c
		    LEFT JOIN `'._DB_PREFIX_.'product_lang` cl ON (c.`id_product` = cl.`id_product`)
		    WHERE cl.`id_lang` = '.(int)($this->context->language->id).'
		    AND c.`id_product` NOT IN (SELECT dl.`id_product` FROM  `'._DB_PREFIX_.$this->name.'_products` dl WHERE dl.`id_shop` = "'.$this->context->shop->id.'")
		');
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Restrict by product'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'select',
						'label' => $this->l('Product'),
						'name' => 'deluxeproduct',
						'desc' => $this->l(''),
						'options' => array(
							'query' => $products,
							'id' => 'id_product',
							'name' => 'name'
						),
					)
				),
			'submit' => array(
				'title' => $this->l('Restrict by product'),
				'class' => 'btn btn-default')
			),
		);
		
		$languages = Language::getLanguages(false);
		foreach ($languages as $k => $language) $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->languages = $languages;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitRestrictProduct';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		if (Tools::isSubmit('showConfig')){
			return $helper->generateForm(array($fields_form));
		}
	}
	
	public function renderRestrictManufacture()
	{
		$manufacturers = Db::getInstance()->ExecuteS('
		    SELECT DISTINCT c.`id_manufacturer`, c.`name`
		    FROM `'._DB_PREFIX_.'manufacturer` c
		    WHERE c.`id_manufacturer` NOT IN (SELECT dl.`id_manuf` FROM  `'._DB_PREFIX_.$this->name.'_manufacturers` dl WHERE dl.`id_shop` = "'.$this->context->shop->id.'")
		');
		
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Restrict by manufacturer'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'select',
						'label' => $this->l('Manufacturer'),
						'name' => 'deluxemanuf',
						'desc' => $this->l(''),
						'options' => array(
							'query' =>  $manufacturers,
							'id' => 'id_manufacturer',
							'name' => 'name'
						),
					)
				),
			'submit' => array(
				'title' => $this->l('Restrict by manufacturer'),
				'class' => 'btn btn-default')
			),
		);
		
		$languages = Language::getLanguages(false);
		foreach ($languages as $k => $language) $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->languages = $languages;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitRestrictManufacture';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		if (Tools::isSubmit('showConfig'))
		{
			return $helper->generateForm(array($fields_form));
		}
	}
	
	public function renderRestrictSupplier()
	{
		 $suppliers = Db::getInstance()->ExecuteS('
		    SELECT DISTINCT c.`id_supplier`, c.`name`
		    FROM `'._DB_PREFIX_.'supplier` c
		    WHERE c.`id_supplier` NOT IN (SELECT dl.`id_supp` FROM  `'._DB_PREFIX_.$this->name.'_suppliers` dl WHERE dl.`id_shop` = "'.$this->context->shop->id.'")
		');
		
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Restrict by supplier'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'select',
						'label' => $this->l('Supplier'),
						'name' => 'deluxesupp',
						'desc' => $this->l(''),
						'options' => array(
							'query' =>  $suppliers,
							'id' => 'id_supplier',
							'name' => 'name'
						),
					)
				),
			'submit' => array(
				'title' => $this->l('Restrict supplier'),
				'class' => 'btn btn-default')
			),
		);
		
		$languages = Language::getLanguages(false);
		foreach ($languages as $k => $language) $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->languages = $languages;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitRestrictSupplier';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		if (Tools::isSubmit('showConfig'))
		{
			return $helper->generateForm(array($fields_form));
		}
	}
	
public function renderListCategories()
	{
		
		$selectAllConfiguration = $this->getForbiddenCategories();
		$select = array();
		foreach($selectAllConfiguration as $key => $value)
		{
			$select[$key]['id_configuration'] = $value['id_forbidden_cat'];
			$c = new Category((int)$value['id_categoria'], $this->context->language->id);
			$select[$key]['name'] = $c->name;//$value['name'];
			
		}
		
		$fields_list= array(
			'id_configuration' => array('title' => "Id", 'align' => 'left','width' => 30),
			'name' => array('title' => $this->l('Category'), 'type' => '', 'align' => 'left')
			
		);
		    $helper = new HelperList();
		    $helper->shopLinkType = '';
		    $helper->simple_header = true;
		    // Actions to be displayed in the "Actions" column
		    $helper->actions = array('delete');
		    $helper->identifier = 'id_configuration';
		    $helper->show_toolbar = true;
		    $helper->title = $this->l('Restrict by category');
		    $helper->table = _DB_PREFIX_.$this->name."_categories";
		    $helper->token = Tools::getAdminTokenLite('AdminModules');
		    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		    return $helper->generateList($select, $fields_list);
	}
	
	public function renderListProducts()
	{
		
		$selectAllConfiguration = $this->getForbiddenProducts();
		$select = array();
		foreach($selectAllConfiguration as $key => $value){
			$select[$key]['id_configuration'] = $value['id_forbidden_prod'];
			$select[$key]['name'] = Product::getProductName((int)$value['id_product'], null, $this->context->language->id);
			
		}
		
		$fields_list= array(
			'id_configuration' => array('title' => "Id", 'align' => 'left','width' => 30),
			'name' => array('title' => $this->l('Product'), 'type' => '', 'align' => 'left')
			
		);
		    $helper = new HelperList();
		    $helper->shopLinkType = '';
		    $helper->simple_header = true;
		    // Actions to be displayed in the "Actions" column
		    $helper->actions = array('delete');
		    $helper->identifier = 'id_configuration';
		    $helper->show_toolbar = true;
		    $helper->title = $this->l('Restrict by product');
		    $helper->table = _DB_PREFIX_.$this->name."_products";
		    $helper->token = Tools::getAdminTokenLite('AdminModules');
		    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		 
		return $helper->generateList($select, $fields_list);
	
	}
	
	public function renderListManufacturers()
	{
		
		$selectAllConfiguration = $this->getForbiddenManufacturers();
		$select = array();
		foreach($selectAllConfiguration as $key => $value)
		{
			$select[$key]['id_configuration'] = $value['id_forbidden_manuf'];
			$select[$key]['name'] = Manufacturer::getNameById((int)$value['id_manuf']);
		}
		
		$fields_list= array(
			'id_configuration' => array('title' => "Id", 'align' => 'left','width' => 30),
			'name' => array('title' => $this->l('Manufacturer'), 'type' => '', 'align' => 'left')
			
		);
		    $helper = new HelperList();
		    $helper->shopLinkType = '';
		    $helper->simple_header = true;
		    // Actions to be displayed in the "Actions" column
		    $helper->actions = array('delete');
		    $helper->identifier = 'id_configuration';
		    $helper->show_toolbar = true;
		    $helper->title = $this->l('Restrict by manufacturer');
		    $helper->table = _DB_PREFIX_.$this->name."_manufacturers";
		    $helper->token = Tools::getAdminTokenLite('AdminModules');
		    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		    return $helper->generateList($select, $fields_list);
	}
	
	public function renderListSuppliers()
	{
		
		$selectAllConfiguration = $this->getForbiddenSuppliers();
		$select = array();
		foreach($selectAllConfiguration as $key => $value)
		{
			$select[$key]['id_configuration'] = $value['id_forbidden_supp'];
			$select[$key]['name'] = Supplier::getNameById((int)$value['id_supp']);
		}
		
		$fields_list= array(
			'id_configuration' => array('title' => "Id", 'align' => 'left','width' => 30),
			'name' => array('title' => $this->l('Supplier'), 'type' => '', 'align' => 'left')
			
		);
		    $helper = new HelperList();
		    $helper->shopLinkType = '';
		    $helper->simple_header = true;
		    // Actions to be displayed in the "Actions" column
		    $helper->actions = array('delete');
		    $helper->identifier = 'id_configuration';
		    $helper->show_toolbar = true;
		    $helper->title = $this->l('Restrict by supplier');
		    $helper->table = _DB_PREFIX_.$this->name."_suppliers";
		    $helper->token = Tools::getAdminTokenLite('AdminModules');
		    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		    return $helper->generateList($select, $fields_list);
	}
	
	public function getConfigFieldsValues()
	{
		$id_carriers = explode(";", Configuration::get(Tools::strtoupper($this->name).'_CARRIERS'));
		$return_carriers = array();
		foreach ($id_carriers as $carrier)
		{
			$return_carriers['id_carrier[]_'.$carrier] =  'on';
		}
		$return = array(
			'feetype' => Tools::getValue('feetype', Configuration::get(Tools::strtoupper($this->name).'_TYPE')),
			'fee' => Tools::getValue('fee', Configuration::get(Tools::strtoupper($this->name))),
			'minimum_amount' => Tools::getValue('minimum_amount',Configuration::get(Tools::strtoupper($this->name).'_MIN_AMOUNT')),
			'maximum_amount' => Tools::getValue('maximum_amount', Configuration::get(Tools::strtoupper($this->name).'_MAX_AMOUNT')),
			'free_fee' => Tools::getValue('free_fee', Configuration::get(Tools::strtoupper($this->name).'_FREE')),
			'id_carriers' => Tools::getValue('id_carriers', Configuration::get(Tools::strtoupper($this->name).'_CARRIERS')),
			'deluxecategory' => Tools::getValue('deluxecategory'),
			'deluxeproduct' => Tools::getValue('deluxeproduct'),
			'deluxemanuf' => Tools::getValue('deluxemanuf'),
			'deluxesupp' => Tools::getValue('deluxesupp'),
			'fee_tax' => Tools::getValue('fee_tax', Configuration::get(Tools::strtoupper($this->name).'_TAX')),
			'feemin' =>Tools::getValue('feemin', Configuration::get(Tools::strtoupper($this->name).'_MIN')),
			'feemax' => Tools::getValue('feemax', Configuration::get(Tools::strtoupper($this->name).'_MAX')),
			'showRestrictHid' => Tools::getValue('showRestrictHid'),
            'select_free_carrier' => Tools::getValue('select_free_carrier', Configuration::get(Tools::strtoupper($this->name).'_FREE_CARRIER')),
		);
		
		return array_merge($return,$return_carriers);
	}
    
	// Custom --------->
	
	public function innovaTitle()
	{
		return "
		<div class='panel heading'>
			<h4>
				<p>".$this->l('This module allows to your customer the  payments in your store.')."</p>
				<img src='" . $this->_path . "/logo.png'>
				&nbsp;&nbsp;INNOVADELUXE&nbsp;&nbsp;-&nbsp;&nbsp;" . $this->displayName . "&nbsp;&nbsp;-&nbsp;&nbsp;
			</h4>
		</div>
		";
	}
	

	public function getFeeCost($cart)
	{
		$conv_rate = (float)$this->context->currency->conversion_rate;
		
		if(Configuration::get(Tools::strtoupper($this->name).'_TYPE') == 0)
		{
			$free_fee = (float)Tools::ps_round((float)Configuration::get(Tools::strtoupper($this->name).'_FREE') * (float)$conv_rate, 2);
			$cartvalue = (float)$cart->getOrderTotal(true, 3);
			if (($free_fee<$cartvalue)&($free_fee!=0))
			{
				return (float)0;
			}
			else
			{
				return (float)Tools::ps_round((float)Configuration::get(Tools::strtoupper($this->name)) * (float)$conv_rate, 2);
			}
		}
		else if(Configuration::get(Tools::strtoupper($this->name).'_TYPE') == 1)
		{
			$minimalfee = (float)Tools::ps_round((float)Configuration::get(Tools::strtoupper($this->name).'_MIN') * (float)$conv_rate, 2);
			$maximalfee = (float)Tools::ps_round((float)Configuration::get(Tools::strtoupper($this->name).'_MAX') * (float)$conv_rate, 2);
			$free_fee = (float)Tools::ps_round((float)Configuration::get(Tools::strtoupper($this->name).'_FREE') * (float)$conv_rate, 2);
			$cartvalue = (float)$cart->getOrderTotal(true, 3);
			$percent = (float)Configuration::get(Tools::strtoupper($this->name).'_TAX');
			$percent = $percent / 100;
			$fee = $cartvalue * $percent;
			
			if(($fee<$minimalfee)&($minimalfee!=0))
			{
				$fee=$minimalfee;
			}
			else if (($fee>$maximalfee)&($maximalfee!=0))
			{
				$fee=$maximalfee;
			}
			if (($free_fee<$cartvalue)&($free_fee!=0))
			{
				$fee=0;
			}
			return (float)$fee;
		}
		else if(Configuration::get(Tools::strtoupper($this->name).'_TYPE') == 2)
		{
			$minimalfee = (float)Tools::ps_round((float)Configuration::get(Tools::strtoupper($this->name).'_MIN') * (float)$conv_rate, 2);
			$maximalfee = (float)Tools::ps_round((float)Configuration::get(Tools::strtoupper($this->name).'_MAX') * (float)$conv_rate, 2);
			$free_fee = (float)Tools::ps_round((float)Configuration::get(Tools::strtoupper($this->name).'_FREE') * (float)$conv_rate, 2);
			$cartvalue = (float)$cart->getOrderTotal(true, 3);
			$percent = (float)Configuration::get(Tools::strtoupper($this->name).'_TAX');
			$percent = $percent / 100;
			$fee_tax = (float)Tools::ps_round((float)Configuration::get(Tools::strtoupper($this->name)) * (float)$conv_rate, 2);
			$fee = ($cartvalue * $percent) + $fee_tax;
			
			if(($fee<$minimalfee)&($minimalfee!=0))
			{
				$fee=$minimalfee;
			}
			else if (($fee>$maximalfee)&($maximalfee!=0))
			{
				$fee=$maximalfee;
			}
			if (($free_fee<$cartvalue)&($free_fee!=0))
			{
				$fee=0;
			}
			return (float)$fee;
		}
	}
	
	public function _checkCurrency($cart)
	{
		$currency_order = new Currency((int)$cart->id_currency);
		$currencies_module = $this->getCurrency();
		$currency_default = Configuration::get('PS_CURRENCY_DEFAULT');
		
		if (is_array($currencies_module))
			foreach ($currencies_module AS $currency_module)
				if ($currency_order->id == $currency_module['id_currency'])
					return true;
		return false;
	}
	
	public function getForbiddenCategories()
	{
		return Db::getInstance()->ExecuteS('
			SELECT * FROM '. _DB_PREFIX_.$this->name.'_categories 				   
		');
	}
	public function getForbiddenProducts()
	{
		return Db::getInstance()->ExecuteS('
			SELECT * FROM '. _DB_PREFIX_.$this->name.'_products 				   
		');
	}
	
	public function getForbiddenManufacturers()
	{
		return Db::getInstance()->ExecuteS('
			SELECT * FROM '. _DB_PREFIX_.$this->name.'_manufacturers 				   
		');
	}
	
	public function getForbiddenSuppliers()
	{
		return Db::getInstance()->ExecuteS('
			SELECT * FROM '. _DB_PREFIX_.$this->name.'_suppliers 				   
		');
	}
	public function deleteCodFeeCategory($id_config)
	{
		if (!Db::getInstance()->Execute('
                    DELETE FROM `'._DB_PREFIX_.$this->name.'_categories`
                    WHERE id_forbidden_cat = "'.(int)$id_config.'" AND id_shop = '.(int)$this->context->shop->id)
                )
		{
			return $this->displayError ($this->l('The category have been not deleted'));
		}
		else{
			return $this->displayConfirmation($this->l('The settings have been deleted.'));
		}
	}
	
	public function deleteCodFeeProduct($id_config)
	{
		if (!Db::getInstance()->Execute('
                    DELETE FROM `'._DB_PREFIX_.$this->name.'_products`
                    WHERE id_forbidden_prod = "'.(int)$id_config.'" AND id_shop = '.(int)$this->context->shop->id)
                )
		{
			return $this->displayError ($this->l('The product have been not deleted'));
		}
		else{
			return $this->displayConfirmation($this->l('The settings have been deleted.'));
		}
	}
	
	public function deleteCodFeeManufacturer($id_config)
	{
		if (!Db::getInstance()->Execute('
                    DELETE FROM `'._DB_PREFIX_.$this->name.'_manufacturers`
                    WHERE id_forbidden_manuf = "'.(int)$id_config.'" AND id_shop = '.(int)$this->context->shop->id)
                )
		{
			return $this->displayError ($this->l('The manufacturer have been not deleted'));
		}
		else{
			return $this->displayConfirmation($this->l('The settings have been deleted.'));
		}
	}
	
	public function deleteCodFeeSupplier($id_config)
	{
		if (!Db::getInstance()->Execute('
                    DELETE FROM `'._DB_PREFIX_.$this->name.'_suppliers`
                    WHERE id_forbidden_supp = "'.(int)$id_config.'" AND id_shop = '.(int)$this->context->shop->id)
                )
		{
			return $this->displayError ($this->l('The supplier have been not deleted'));
		}
		else{
			return $this->displayConfirmation($this->l('The settings have been deleted.'));
		}
	}
	
	public function validateOrderStorePayment($id_cart, $id_order_state, $amount_paid, $deluxecodfees, $payment_method = 'Unknown',
			$message = null, $extra_vars = array(), $currency_special = null, $dont_touch_amount = false,
			$secure_key = false, Shop $shop = null)
	{
		$this->context->cart = new Cart($id_cart);
		$this->context->customer = new Customer($this->context->cart->id_customer);
		$this->context->language = new Language($this->context->cart->id_lang);
		$this->context->shop = ($shop ? $shop : new Shop($this->context->cart->id_shop));
		$id_currency = $currency_special ? (int)$currency_special : (int)$this->context->cart->id_currency;
		$this->context->currency = new Currency($id_currency, null, $this->context->shop->id);
		if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery')
			$context_country = $this->context->country;
	
		$order_status = new OrderState((int)$id_order_state, (int)$this->context->language->id);
		if (!Validate::isLoadedObject($order_status))
			throw new PrestaShopException('Can\'t load Order state status');
	
		if (!$this->active)
			die(Tools::displayError());
		// Does order already exists ?
		if (Validate::isLoadedObject($this->context->cart) && $this->context->cart->OrderExists() == false)
		{
			if ($secure_key !== false && $secure_key != $this->context->cart->secure_key)
				die(Tools::displayError());
	
			// For each package, generate an order
			$delivery_option_list = $this->context->cart->getDeliveryOptionList();
			$package_list = $this->context->cart->getPackageList();
			$cart_delivery_option = $this->context->cart->getDeliveryOption();
	
			// If some delivery options are not defined, or not valid, use the first valid option
			foreach ($delivery_option_list as $id_address => $package)
			if (!isset($cart_delivery_option[$id_address]) || !array_key_exists($cart_delivery_option[$id_address], $package))
			foreach ($package as $key => $val)
			{
				$cart_delivery_option[$id_address] = $key;
				break;
			}
	
			$order_list = array();
			$order_detail_list = array();
			$reference = Order::generateReference();
			$this->currentOrderReference = $reference;
	
			$order_creation_failed = false;
			$cart_total_paid = (float)Tools::ps_round((float)$this->context->cart->getOrderTotal(true, Cart::BOTH)+$deluxecodfees, 2);
	
			if ($this->context->cart->orderExists())
			{
				$error = Tools::displayError('An order has already been placed using this cart.');
				Logger::addLog($error, 4, '0000001', 'Cart', (int)($this->context->cart->id));
				die($error);
			}
	
			foreach ($cart_delivery_option as $id_address => $key_carriers)
			foreach ($delivery_option_list[$id_address][$key_carriers]['carrier_list'] as $id_carrier => $data)
			foreach ($data['package_list'] as $id_package)
			{
				// Rewrite the id_warehouse
				$package_list[$id_address][$id_package]['id_warehouse'] = (int)$this->context->cart->getPackageIdWarehouse($package_list[$id_address][$id_package], (int)$id_carrier);
				$package_list[$id_address][$id_package]['id_carrier'] = $id_carrier;
			}
			// Make sure CarRule caches are empty
			CartRule::cleanCache();
	
			foreach ($package_list as $id_address => $packageByAddress)
			foreach ($packageByAddress as $id_package => $package)
			{
				$order = new Order();
				$order->product_list = $package['product_list'];
					
				if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery')
				{
					$address = new Address($id_address);
					$this->context->country = new Country($address->id_country, $this->context->cart->id_lang);
				}
					
				$carrier = null;
				if (!$this->context->cart->isVirtualCart() && isset($package['id_carrier']))
				{
					$carrier = new Carrier($package['id_carrier'], $this->context->cart->id_lang);
					$order->id_carrier = (int)$carrier->id;
					$id_carrier = (int)$carrier->id;
				}
				else
				{
					$order->id_carrier = 0;
					$id_carrier = 0;
				}
					
				$order->id_customer = (int)$this->context->cart->id_customer;
				$order->id_address_invoice = (int)$this->context->cart->id_address_invoice;
				$order->id_address_delivery = (int)$id_address;
				$order->id_currency = $this->context->currency->id;
				$order->id_lang = (int)$this->context->cart->id_lang;
				$order->id_cart = (int)$this->context->cart->id;
				$order->reference = $reference;
				$order->id_shop = (int)$this->context->shop->id;
				$order->id_shop_group = (int)$this->context->shop->id_shop_group;
	
				$order->secure_key = ($secure_key ? pSQL($secure_key) : pSQL($this->context->customer->secure_key));
				$order->payment = $payment_method;
				if (isset($this->name))
					$order->module = $this->name;
				$order->recyclable = $this->context->cart->recyclable;
				$order->gift = (int)$this->context->cart->gift;
				$order->gift_message = $this->context->cart->gift_message;
				$order->conversion_rate = $this->context->currency->conversion_rate;
				$amount_paid = !$dont_touch_amount ? Tools::ps_round((float)$amount_paid, 2) : $amount_paid;
				$order->total_paid_real = 0;
					
				$order->total_products = (float)$this->context->cart->getOrderTotal(false, Cart::ONLY_PRODUCTS, $order->product_list, $id_carrier);
				$order->total_products_wt = (float)$this->context->cart->getOrderTotal(true, Cart::ONLY_PRODUCTS, $order->product_list, $id_carrier);
	
				$order->total_discounts_tax_excl = (float)abs($this->context->cart->getOrderTotal(false, Cart::ONLY_DISCOUNTS, $order->product_list, $id_carrier));
				$order->total_discounts_tax_incl = (float)abs($this->context->cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS, $order->product_list, $id_carrier));
				$order->total_discounts = $order->total_discounts_tax_incl;
	
				$deluxecodfees_wt = 0;
	
				if (!is_null($carrier) && Validate::isLoadedObject($carrier))
				{
					$order->carrier_tax_rate = $carrier->getTaxesRate(new Address($this->context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')}));
					$deluxecodfees_wt = $deluxecodfees / (1 + (($order->carrier_tax_rate)/100));
				}
	
				$order->total_shipping_tax_excl = (float)Tools::ps_round((float)($this->context->cart->getPackageShippingCost((int)$id_carrier, false, null, $order->product_list) + $deluxecodfees_wt), 4);
				$order->total_shipping_tax_incl = (float)Tools::ps_round((float)($this->context->cart->getPackageShippingCost((int)$id_carrier, true, null, $order->product_list) + $deluxecodfees), 4);
				$order->total_shipping = $order->total_shipping_tax_incl;
					
				$order->total_wrapping_tax_excl = (float)abs($this->context->cart->getOrderTotal(false, Cart::ONLY_WRAPPING, $order->product_list, $id_carrier));
				$order->total_wrapping_tax_incl = (float)abs($this->context->cart->getOrderTotal(true, Cart::ONLY_WRAPPING, $order->product_list, $id_carrier));
				$order->total_wrapping = $order->total_wrapping_tax_incl;
	
				$order->total_paid_tax_excl = (float)Tools::ps_round((float)$this->context->cart->getOrderTotal(false, Cart::BOTH, $order->product_list, $id_carrier) + $deluxecodfees_wt, 2);
				$order->total_paid_tax_incl = (float)Tools::ps_round((float)$this->context->cart->getOrderTotal(true, Cart::BOTH, $order->product_list, $id_carrier) + $deluxecodfees, 2);
				$order->total_paid = $order->total_paid_tax_incl;
	
				$order->invoice_date = '0000-00-00 00:00:00';
				$order->delivery_date = '0000-00-00 00:00:00';
	
				$order->deluxecodfees = $deluxecodfees;
	
				// Creating order
				$result = $order->add();
	
				if (!$result)
					throw new PrestaShopException('Can\'t save Order');
	
				// Amount paid by customer is not the right one -> Status = payment error
				// We don't use the following condition to avoid the float precision issues : http://www.php.net/manual/en/language.types.float.php
				// if ($order->total_paid != $order->total_paid_real)
				// We use number_format in order to compare two string
				if ($order_status->logable && number_format($cart_total_paid, 2) != number_format($amount_paid, 2))
					$id_order_state = Configuration::get('PS_OS_ERROR');
	
				$order_list[] = $order;
	
				// Insert new Order detail list using cart for the current order
				$order_detail = new OrderDetail(null, null, $this->context);
				$order_detail->createList($order, $this->context->cart, $id_order_state, $order->product_list, 0, true, $package_list[$id_address][$id_package]['id_warehouse']);
				$order_detail_list[] = $order_detail;
	
				// Adding an entry in order_carrier table
				if (!is_null($carrier))
				{
					$order_carrier = new OrderCarrier();
					$order_carrier->id_order = (int)$order->id;
					$order_carrier->id_carrier = (int)$id_carrier;
					$order_carrier->weight = (float)$order->getTotalWeight();
					$order_carrier->shipping_cost_tax_excl = (float)$order->total_shipping_tax_excl;
					$order_carrier->shipping_cost_tax_incl = (float)$order->total_shipping_tax_incl;
					$order_carrier->add();
				}
			}
	
			// The country can only change if the address used for the calculation is the delivery address, and if multi-shipping is activated
			if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery')
				$this->context->country = $context_country;
	
			// Register Payment only if the order status validate the order
			if ($order_status->logable)
			{
				// $order is the last order loop in the foreach
				// The method addOrderPayment of the class Order make a create a paymentOrder
				//     linked to the order reference and not to the order id
				if (isset($extra_vars['transaction_id']))
					$transaction_id = $extra_vars['transaction_id'];
				else
					$transaction_id = null;
	
				if (!$order->addOrderPayment($amount_paid, null, $transaction_id))
					throw new PrestaShopException('Can\'t save Order Payment');
			}
	
			// Next !
			$only_one_gift = false;
			$cart_rule_used = array();
			$products = $this->context->cart->getProducts();
			$cart_rules = $this->context->cart->getCartRules();
	
			// Make sure CarRule caches are empty
			CartRule::cleanCache();
	
			foreach ($order_detail_list as $key => $order_detail)
			{
				$order = $order_list[$key];
				if (!$order_creation_failed & isset($order->id))
				{
					if (!$secure_key)
						$message .= '<br />'.Tools::displayError('Warning: the secure key is empty, check your payment account before validation');
					// Optional message to attach to this order
					if (isset($message) & !empty($message))
					{
						$msg = new Message();
						$message = strip_tags($message, '<br>');
						if (Validate::isCleanHtml($message))
						{
							$msg->message = $message;
							$msg->id_order = (int)($order->id);
							$msg->private = 1;
							$msg->add();
						}
					}
	
					// Insert new Order detail list using cart for the current order
					//$orderDetail = new OrderDetail(null, null, $this->context);
					//$orderDetail->createList($order, $this->context->cart, $id_order_state);
	
					// Construct order detail table for the email
					$products_list = '';
					$virtual_product = true;
					foreach ($products as $key => $product)
					{
						$price = Product::getPriceStatic((int)$product['id_product'], false, ($product['id_product_attribute'] ? (int)$product['id_product_attribute'] : null), 6, null, false, true, $product['cart_quantity'], false, (int)$order->id_customer, (int)$order->id_cart, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
						$price_wt = Product::getPriceStatic((int)$product['id_product'], true, ($product['id_product_attribute'] ? (int)$product['id_product_attribute'] : null), 2, null, false, true, $product['cart_quantity'], false, (int)$order->id_customer, (int)$order->id_cart, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
	
						$customization_quantity = 0;
						if (isset($customized_datas[$product['id_product']][$product['id_product_attribute']]))
						{
							$customization_text = '';
							foreach ($customized_datas[$product['id_product']][$product['id_product_attribute']] as $customization)
							{
								if (isset($customization['datas'][Product::CUSTOMIZE_TEXTFIELD]))
								foreach ($customization['datas'][Product::CUSTOMIZE_TEXTFIELD] as $text)
									$customization_text .= $text['name'].': '.$text['value'].'<br />';
	
								if (isset($customization['datas'][Product::CUSTOMIZE_FILE]))
									$customization_text .= sprintf(Tools::displayError('%d image(s)'), count($customization['datas'][Product::CUSTOMIZE_FILE])).'<br />';
	
								$customization_text .= '---<br />';
							}
	
							$customization_text = rtrim($customization_text, '---<br />');
	
							$customization_quantity = (int)$product['customizationQuantityTotal'];
							$products_list .=
							'<tr style="background-color: '.($key % 2 ? '#DDE2E6' : '#EBECEE').';">
								<td style="padding: 0.6em 0.4em;width: 15%;">'.$product['reference'].'</td>
								<td style="padding: 0.6em 0.4em;width: 30%;"><strong>'.$product['name'].(isset($product['attributes']) ? ' - '.$product['attributes'] : '').' - '.Tools::displayError('Customized').(!empty($customization_text) ? ' - '.$customization_text : '').'</strong></td>
								<td style="padding: 0.6em 0.4em; width: 20%;">'.Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ?  Tools::ps_round($price, 2) : $price_wt, $this->context->currency, false).'</td>
								<td style="padding: 0.6em 0.4em; width: 15%;">'.$customization_quantity.'</td>
								<td style="padding: 0.6em 0.4em; width: 20%;">'.Tools::displayPrice($customization_quantity * (Product::getTaxCalculationMethod() == PS_TAX_EXC ? Tools::ps_round($price, 2) : $price_wt), $this->context->currency, false).'</td>
							</tr>';
						}
	
						if (!$customization_quantity || (int)$product['cart_quantity'] > $customization_quantity)
							$products_list .=
							'<tr style="background-color: '.($key % 2 ? '#DDE2E6' : '#EBECEE').';">
								<td style="padding: 0.6em 0.4em;width: 15%;">'.$product['reference'].'</td>
								<td style="padding: 0.6em 0.4em;width: 30%;"><strong>'.$product['name'].(isset($product['attributes']) ? ' - '.$product['attributes'] : '').'</strong></td>
								<td style="padding: 0.6em 0.4em; width: 20%;">'.Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ? Tools::ps_round($price, 2) : $price_wt, $this->context->currency, false).'</td>
								<td style="padding: 0.6em 0.4em; width: 15%;">'.((int)$product['cart_quantity'] - $customization_quantity).'</td>
								<td style="padding: 0.6em 0.4em; width: 20%;">'.Tools::displayPrice(((int)$product['cart_quantity'] - $customization_quantity) * (Product::getTaxCalculationMethod() == PS_TAX_EXC ? Tools::ps_round($price, 2) : $price_wt), $this->context->currency, false).'</td>
							</tr>';
	
						// Check if is not a virutal product for the displaying of shipping
						if (!$product['is_virtual'])
							$virtual_product &= false;
	
					} // end foreach ($products)
	
					$cart_rules_list = '';
					foreach ($cart_rules as $cart_rule)
					{
						$package = array('id_carrier' => $order->id_carrier, 'id_address' => $order->id_address_delivery, 'products' => $order->product_list);
						$values = array(
								'tax_incl' => $cart_rule['obj']->getContextualValue(true, $this->context, CartRule::FILTER_ACTION_ALL, $package),
								'tax_excl' => $cart_rule['obj']->getContextualValue(false, $this->context, CartRule::FILTER_ACTION_ALL, $package)
						);
	
						// If the reduction is not applicable to this order, then continue with the next one
						if (!$values['tax_excl'])
							continue;
	
						$order->addCartRule($cart_rule['obj']->id, $cart_rule['obj']->name, $values);
	
						/* IF
						 ** - This is not multi-shipping
						** - The value of the voucher is greater than the total of the order
						** - Partial use is allowed
						** - This is an "amount" reduction, not a reduction in % or a gift
						** THEN
						** The voucher is cloned with a new value corresponding to the remainder
						*/
						if (count($order_list) == 1 && $values['tax_incl'] > $order->total_products_wt && $cart_rule['obj']->partial_use == 1 && $cart_rule['obj']->reduction_amount > 0)
						{
							// Create a new voucher from the original
							$voucher = new CartRule($cart_rule['obj']->id); // We need to instantiate the CartRule without lang parameter to allow saving it
							unset($voucher->id);
	
							// Set a new voucher code
							$voucher->code = empty($voucher->code) ? Tools::substr(md5($order->id.'-'.$order->id_customer.'-'.$cart_rule['obj']->id), 0, 16) : $voucher->code.'-2';
							if (preg_match('/\-([0-9]{1,2})\-([0-9]{1,2})$/', $voucher->code, $matches) && $matches[1] == $matches[2])
								$voucher->code = preg_replace('/'.$matches[0].'$/', '-'.((int)($matches[1]) + 1), $voucher->code);
	
							// Set the new voucher value
							if ($voucher->reduction_tax)
								$voucher->reduction_amount = $values['tax_incl'] - $order->total_products_wt;
							else
								$voucher->reduction_amount = $values['tax_excl'] - $order->total_products;
	
							$voucher->id_customer = $order->id_customer;
							$voucher->quantity = 1;
							if ($voucher->add())
							{
								// If the voucher has conditions, they are now copied to the new voucher
								CartRule::copyConditions($cart_rule['obj']->id, $voucher->id);
	
								$params = array(
										'{voucher_amount}' => Tools::displayPrice($voucher->reduction_amount, $this->context->currency, false),
										'{voucher_num}' => $voucher->code,
										'{firstname}' => $this->context->customer->firstname,
										'{lastname}' => $this->context->customer->lastname,
										'{id_order}' => $order->reference,
										'{order_name}' => $order->getUniqReference()
								);
								Mail::Send(
								(int)$order->id_lang,
								'voucher',
								sprintf(Mail::l('New voucher regarding your order %s', (int)$order->id_lang), $order->reference),
								$params,
								$this->context->customer->email,
								$this->context->customer->firstname.' '.$this->context->customer->lastname,
								null, null, null, null, _PS_MAIL_DIR_, false, (int)$order->id_shop
								);
							}
						}
	
						if ($id_order_state != Configuration::get('PS_OS_ERROR') && $id_order_state != Configuration::get('PS_OS_CANCELED') && !in_array($cart_rule['obj']->id, $cart_rule_used))
						{
							$cart_rule_used[] = $cart_rule['obj']->id;
	
							// Create a new instance of Cart Rule without id_lang, in order to update its quantity
							$cart_rule_to_update = new CartRule($cart_rule['obj']->id);
							$cart_rule_to_update->quantity = max(0, $cart_rule_to_update->quantity - 1);
							$cart_rule_to_update->update();
						}
	
						$cart_rules_list .= '
						<tr>
							<td colspan="4" style="padding:0.6em 0.4em;text-align:right">'.Tools::displayError('Voucher name:').' '.$cart_rule['obj']->name.'</td>
							<td style="padding:0.6em 0.4em;text-align:right">'.($values['tax_incl'] != 0.00 ? '-' : '').Tools::displayPrice($values['tax_incl'], $this->context->currency, false).'</td>
						</tr>';
					}
	
					// Specify order id for message
					$old_message = Message::getMessageByCartId((int)$this->context->cart->id);
					if ($old_message)
					{
						$update_message = new Message((int)$old_message['id_message']);
						$update_message->id_order = (int)$order->id;
						$update_message->update();
	
						// Add this message in the customer thread
						$customer_thread = new CustomerThread();
						$customer_thread->id_contact = 0;
						$customer_thread->id_customer = (int)$order->id_customer;
						$customer_thread->id_shop = (int)$this->context->shop->id;
						$customer_thread->id_order = (int)$order->id;
						$customer_thread->id_lang = (int)$this->context->language->id;
						$customer_thread->email = $this->context->customer->email;
						$customer_thread->status = 'open';
						$customer_thread->token = Tools::passwdGen(12);
						$customer_thread->add();
	
						$customer_message = new CustomerMessage();
						$customer_message->id_customer_thread = $customer_thread->id;
						$customer_message->id_employee = 0;
						$customer_message->message = htmlentities($update_message->message, ENT_COMPAT, 'UTF-8');
						$customer_message->private = 0;
	
						if (!$customer_message->add())
							$this->errors[] = Tools::displayError('An error occurred while saving message');
					}
	
					// Hook validate order
					Hook::exec('actionValidateOrder', array(
					'cart' => $this->context->cart,
					'order' => $order,
					'customer' => $this->context->customer,
					'currency' => $this->context->currency,
					'orderStatus' => $order_status
					));
	
					foreach ($this->context->cart->getProducts() as $product)
					if ($order_status->logable)
						ProductSale::addProductSale((int)$product['id_product'], (int)$product['cart_quantity']);
	
					if (Configuration::get('PS_STOCK_MANAGEMENT') && $order_detail->getStockState())
					{
						$history = new OrderHistory();
						$history->id_order = (int)$order->id;
						$history->changeIdOrderState(Configuration::get('PS_OS_OUTOFSTOCK'), $order, true);
						$history->addWithemail();
					}
	
					// Set order state in order history ONLY even if the "out of stock" status has not been yet reached
					// So you migth have two order states
					$new_history = new OrderHistory();
					$new_history->id_order = (int)$order->id;
					$new_history->changeIdOrderState((int)$id_order_state, $order, true);
					$new_history->addWithemail(true, $extra_vars);
	
					unset($order_detail);
	
					// Order is reloaded because the status just changed
					$order = new Order($order->id);
	
					// Send an e-mail to customer (one order = one email)
					if ($id_order_state != Configuration::get('PS_OS_ERROR') && $id_order_state != Configuration::get('PS_OS_CANCELED') && $this->context->customer->id)
					{
						$invoice = new Address($order->id_address_invoice);
						$delivery = new Address($order->id_address_delivery);
						$delivery_state = $delivery->id_state ? new State($delivery->id_state) : false;
						$invoice_state = $invoice->id_state ? new State($invoice->id_state) : false;
	
						$data = array(
								'{firstname}' => $this->context->customer->firstname,
								'{lastname}' => $this->context->customer->lastname,
								'{email}' => $this->context->customer->email,
								'{delivery_block_txt}' => $this->_getFormatedAddress($delivery, "\n"),
								'{invoice_block_txt}' => $this->_getFormatedAddress($invoice, "\n"),
								'{delivery_block_html}' => $this->_getFormatedAddress($delivery, '<br />', array(
										'firstname'	=> '<span style="font-weight:bold;">%s</span>',
										'lastname'	=> '<span style="font-weight:bold;">%s</span>'
								)),
								'{invoice_block_html}' => $this->_getFormatedAddress($invoice, '<br />', array(
										'firstname'	=> '<span style="font-weight:bold;">%s</span>',
										'lastname'	=> '<span style="font-weight:bold;">%s</span>'
								)),
								'{delivery_company}' => $delivery->company,
								'{delivery_firstname}' => $delivery->firstname,
								'{delivery_lastname}' => $delivery->lastname,
								'{delivery_address1}' => $delivery->address1,
								'{delivery_address2}' => $delivery->address2,
								'{delivery_city}' => $delivery->city,
								'{delivery_postal_code}' => $delivery->postcode,
								'{delivery_country}' => $delivery->country,
								'{delivery_state}' => $delivery->id_state ? $delivery_state->name : '',
								'{delivery_phone}' => ($delivery->phone) ? $delivery->phone : $delivery->phone_mobile,
								'{delivery_other}' => $delivery->other,
								'{invoice_company}' => $invoice->company,
								'{invoice_vat_number}' => $invoice->vat_number,
								'{invoice_firstname}' => $invoice->firstname,
								'{invoice_lastname}' => $invoice->lastname,
								'{invoice_address2}' => $invoice->address2,
								'{invoice_address1}' => $invoice->address1,
								'{invoice_city}' => $invoice->city,
								'{invoice_postal_code}' => $invoice->postcode,
								'{invoice_country}' => $invoice->country,
								'{invoice_state}' => $invoice->id_state ? $invoice_state->name : '',
								'{invoice_phone}' => ($invoice->phone) ? $invoice->phone : $invoice->phone_mobile,
								'{invoice_other}' => $invoice->other,
								'{order_name}' => $order->getUniqReference(),
								'{date}' => Tools::displayDate(date('Y-m-d H:i:s'), (int)$order->id_lang, 1),
								'{carrier}' => $virtual_product ? Tools::displayError('No carrier') : $carrier->name,
								'{payment}' => Tools::substr($order->payment, 0, 32),
								'{products}' => $this->formatProductAndVoucherForEmail($products_list),
								'{discounts}' => $this->formatProductAndVoucherForEmail($cart_rules_list),
								'{total_paid}' => Tools::displayPrice($order->total_paid, $this->context->currency, false),
								'{total_products}' => Tools::displayPrice($order->total_paid - $order->total_shipping - $order->total_wrapping + $order->total_discounts, $this->context->currency, false),
								'{total_discounts}' => Tools::displayPrice($order->total_discounts, $this->context->currency, false),
								'{total_shipping}' => Tools::displayPrice($order->total_shipping, $this->context->currency, false).'<br />'.$this->l('COD fee included'),
								'{total_wrapping}' => Tools::displayPrice($order->total_wrapping, $this->context->currency, false));
	
						if (is_array($extra_vars))
							$data = array_merge($data, $extra_vars);
	
						// Join PDF invoice
						if ((int)Configuration::get('PS_INVOICE') && $order_status->invoice && $order->invoice_number)
						{
							$pdf = new PDF($order->getInvoicesCollection(), PDF::TEMPLATE_INVOICE, $this->context->smarty);
							$file_attachement['content'] = $pdf->render(false);
							$file_attachement['name'] = Configuration::get('PS_INVOICE_PREFIX', (int)$order->id_lang).sprintf('%06d', $order->invoice_number).'.pdf';
							$file_attachement['mime'] = 'application/pdf';
						}
						else
							$file_attachement = null;
	
						if (Validate::isEmail($this->context->customer->email))
							Mail::Send(
									(int)$order->id_lang,
									'order_conf',
									Mail::l('Order confirmation', (int)$order->id_lang),
									$data,
									$this->context->customer->email,
									$this->context->customer->firstname.' '.$this->context->customer->lastname,
									null,
									null,
									$file_attachement,
									null, _PS_MAIL_DIR_, false, (int)$order->id_shop
							);
					}
	
					// updates stock in shops
					if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT'))
					{
						$product_list = $order->getProducts();
						foreach ($product_list as $product)
						{
							// if the available quantities depends on the physical stock
							if (StockAvailable::dependsOnStock($product['product_id']))
							{
								// synchronizes
								StockAvailable::synchronize($product['product_id'], $order->id_shop);
							}
						}
					}
				}
				else
				{
					$error = Tools::displayError('Order creation failed');
					Logger::addLog($error, 4, '0000002', 'Cart', (int)($order->id_cart));
					die($error);
				}
			} // End foreach $order_detail_list
			// Use the last order as currentOrder
			$this->currentOrder = (int)$order->id;
			return true;
		}
		else
		{
			$error = Tools::displayError('Cart cannot be loaded or an order has already been placed using this cart');
			Logger::addLog($error, 4, '0000001', 'Cart', (int)($this->context->cart->id));
			die($error);
		}
	}
	
	public function validateOrderStorePaymentOld($id_cart, $id_order_state, $amount_paid, $deluxecodfees, $payment_method = 'Unknown',
			$message = null, $extra_vars = array(), $currency_special = null, $dont_touch_amount = false,
			$secure_key = false, Shop $shop = null)
	{
		$this->context->cart = new Cart($id_cart);
		$this->context->customer = new Customer($this->context->cart->id_customer);
		$this->context->language = new Language($this->context->cart->id_lang);
		$this->context->shop = ($shop ? $shop : new Shop($this->context->cart->id_shop));
		$id_currency = $currency_special ? (int)$currency_special : (int)$this->context->cart->id_currency;
		$this->context->currency = new Currency($id_currency, null, $this->context->shop->id);
		if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery')
			$context_country = $this->context->country;
	
		$order_status = new OrderState((int)$id_order_state, (int)$this->context->language->id);
		if (!Validate::isLoadedObject($order_status))
			throw new PrestaShopException('Can\'t load Order state status');
	
		if (!$this->active)
			die(Tools::displayError());
		// Does order already exists ?
		if (Validate::isLoadedObject($this->context->cart) && $this->context->cart->OrderExists() == false)
		{
			if ($secure_key !== false && $secure_key != $this->context->cart->secure_key)
				die(Tools::displayError());
	
			// For each package, generate an order
			$delivery_option_list = $this->context->cart->getDeliveryOptionList();
			$package_list = $this->context->cart->getPackageList();
			$cart_delivery_option = $this->context->cart->getDeliveryOption();
	
			// If some delivery options are not defined, or not valid, use the first valid option
			foreach ($delivery_option_list as $id_address => $package)
			if (!isset($cart_delivery_option[$id_address]) || !array_key_exists($cart_delivery_option[$id_address], $package))
			foreach ($package as $key => $val)
			{
				$cart_delivery_option[$id_address] = $key;
				break;
			}
	
			$order_list = array();
			$order_detail_list = array();
			$reference = Order::generateReference();
			$this->currentOrderReference = $reference;
	
			$order_creation_failed = false;
			$cart_total_paid = (float)Tools::ps_round((float)$this->context->cart->getOrderTotal(true, Cart::BOTH)+$deluxecodfees, 2);
	
			if ($this->context->cart->orderExists())
			{
				$error = Tools::displayError('An order has already been placed using this cart.');
				Logger::addLog($error, 4, '0000001', 'Cart', (int)($this->context->cart->id));
				die($error);
			}
	
			foreach ($cart_delivery_option as $id_address => $key_carriers)
			foreach ($delivery_option_list[$id_address][$key_carriers]['carrier_list'] as $id_carrier => $data)
			foreach ($data['package_list'] as $id_package)
				$package_list[$id_address][$id_package]['id_carrier'] = $id_carrier;
	
			// Make sure CarRule caches are empty
			CartRule::cleanCache();
	
			foreach ($package_list as $id_address => $packageByAddress)
			foreach ($packageByAddress as $id_package => $package)
			{
				$order = new Order();
				$order->product_list = $package['product_list'];
					
				if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery')
				{
					$address = new Address($id_address);
					$this->context->country = new Country($address->id_country, $this->context->cart->id_lang);
				}
					
				$carrier = null;
				if (!$this->context->cart->isVirtualCart() && isset($package['id_carrier']))
				{
					$carrier = new Carrier($package['id_carrier'], $this->context->cart->id_lang);
					$order->id_carrier = (int)$carrier->id;
					$id_carrier = (int)$carrier->id;
				}
				else
				{
					$order->id_carrier = 0;
					$id_carrier = 0;
				}
					
				$order->id_customer = (int)$this->context->cart->id_customer;
				$order->id_address_invoice = (int)$this->context->cart->id_address_invoice;
				$order->id_address_delivery = (int)$id_address;
				$order->id_currency = $this->context->currency->id;
				$order->id_lang = (int)$this->context->cart->id_lang;
				$order->id_cart = (int)$this->context->cart->id;
				$order->reference = $reference;
				$order->id_shop = (int)$this->context->shop->id;
				$order->id_shop_group = (int)$this->context->shop->id_shop_group;
	
				$order->secure_key = ($secure_key ? pSQL($secure_key) : pSQL($this->context->customer->secure_key));
				$order->payment = $payment_method;
				if (isset($this->name))
					$order->module = $this->name;
				$order->recyclable = $this->context->cart->recyclable;
				$order->gift = (int)$this->context->cart->gift;
				$order->gift_message = $this->context->cart->gift_message;
				$order->conversion_rate = $this->context->currency->conversion_rate;
				$amount_paid = !$dont_touch_amount ? Tools::ps_round((float)$amount_paid, 2) : $amount_paid;
				$order->total_paid_real = 0;
					
				$order->total_products = (float)$this->context->cart->getOrderTotal(false, Cart::ONLY_PRODUCTS, $order->product_list, $id_carrier);
				$order->total_products_wt = (float)$this->context->cart->getOrderTotal(true, Cart::ONLY_PRODUCTS, $order->product_list, $id_carrier);
	
				$order->total_discounts_tax_excl = (float)abs($this->context->cart->getOrderTotal(false, Cart::ONLY_DISCOUNTS, $order->product_list, $id_carrier));
				$order->total_discounts_tax_incl = (float)abs($this->context->cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS, $order->product_list, $id_carrier));
				$order->total_discounts = $order->total_discounts_tax_incl;
	
				$deluxecodfees_wt = 0;
	
				if (!is_null($carrier) && Validate::isLoadedObject($carrier))
				{
					$order->carrier_tax_rate = $carrier->getTaxesRate(new Address($this->context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')}));
					$deluxecodfees_wt = $deluxecodfees / (1 + (($order->carrier_tax_rate)/100));
				}
	
				$order->total_shipping_tax_excl = (float)Tools::ps_round(($this->context->cart->getPackageShippingCost((int)$id_carrier, false, null, $order->product_list) + $deluxecodfees_wt), 2);
				$order->total_shipping_tax_incl = (float)Tools::ps_round(($this->context->cart->getPackageShippingCost((int)$id_carrier, true, null, $order->product_list) + $deluxecodfees), 2);
				$order->total_shipping = $order->total_shipping_tax_incl;
	
				$order->total_wrapping_tax_excl = (float)abs($this->context->cart->getOrderTotal(false, Cart::ONLY_WRAPPING, $order->product_list, $id_carrier));
				$order->total_wrapping_tax_incl = (float)abs($this->context->cart->getOrderTotal(true, Cart::ONLY_WRAPPING, $order->product_list, $id_carrier));
				$order->total_wrapping = $order->total_wrapping_tax_incl;
	
				$order->total_paid_tax_excl = (float)Tools::ps_round((float)$this->context->cart->getOrderTotal(false, Cart::BOTH, $order->product_list, $id_carrier) + $deluxecodfees_wt, 2);
				$order->total_paid_tax_incl = (float)Tools::ps_round((float)$this->context->cart->getOrderTotal(true, Cart::BOTH, $order->product_list, $id_carrier) + $deluxecodfees, 2);
				$order->total_paid = $order->total_paid_tax_incl;
	
				$order->invoice_date = '0000-00-00 00:00:00';
				$order->delivery_date = '0000-00-00 00:00:00';
	
				$order->deluxecodfees = $deluxecodfees;
	
				// Creating order
				$result = $order->add();
	
				if (!$result)
					throw new PrestaShopException('Can\'t save Order');
	
				// Amount paid by customer is not the right one -> Status = payment error
				// We don't use the following condition to avoid the float precision issues : http://www.php.net/manual/en/language.types.float.php
				// if ($order->total_paid != $order->total_paid_real)
				// We use number_format in order to compare two string
				if ($order_status->logable && number_format($cart_total_paid, 2) != number_format($amount_paid, 2))
					$id_order_state = Configuration::get('PS_OS_ERROR');
	
				$order_list[] = $order;
	
				// Insert new Order detail list using cart for the current order
				$order_detail = new OrderDetail(null, null, $this->context);
				$order_detail->createList($order, $this->context->cart, $id_order_state, $order->product_list, 0, true, $package_list[$id_address][$id_package]['id_warehouse']);
				$order_detail_list[] = $order_detail;
	
				// Adding an entry in order_carrier table
				if (!is_null($carrier))
				{
					$order_carrier = new OrderCarrier();
					$order_carrier->id_order = (int)$order->id;
					$order_carrier->id_carrier = (int)$id_carrier;
					$order_carrier->weight = (float)$order->getTotalWeight();
					$order_carrier->shipping_cost_tax_excl = (float)$order->total_shipping_tax_excl;
					$order_carrier->shipping_cost_tax_incl = (float)$order->total_shipping_tax_incl;
					$order_carrier->add();
				}
			}
	
			// The country can only change if the address used for the calculation is the delivery address, and if multi-shipping is activated
			if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery')
				$this->context->country = $context_country;
	
			// Register Payment only if the order status validate the order
			if ($order_status->logable)
			{
				// $order is the last order loop in the foreach
				// The method addOrderPayment of the class Order make a create a paymentOrder
				//     linked to the order reference and not to the order id
				if (!$order->addOrderPayment($amount_paid))
					throw new PrestaShopException('Can\'t save Order Payment');
			}
	
			// Next !
			$only_one_gift = false;
			$cart_rule_used = array();
			$products = $this->context->cart->getProducts();
			$cart_rules = $this->context->cart->getCartRules();
	
			// Make sure CarRule caches are empty
			CartRule::cleanCache();
	
			foreach ($order_detail_list as $key => $order_detail)
			{
				$order = $order_list[$key];
				if (!$order_creation_failed & isset($order->id))
				{
					if (!$secure_key)
						$message .= '<br />'.Tools::displayError('Warning: the secure key is empty, check your payment account before validation');
					// Optional message to attach to this order
					if (isset($message) & !empty($message))
					{
						$msg = new Message();
						$message = strip_tags($message, '<br>');
						if (Validate::isCleanHtml($message))
						{
							$msg->message = $message;
							$msg->id_order = (int)($order->id);
							$msg->private = 1;
							$msg->add();
						}
					}
	
					// Insert new Order detail list using cart for the current order
					//$orderDetail = new OrderDetail(null, null, $this->context);
					//$orderDetail->createList($order, $this->context->cart, $id_order_state);
	
					// Construct order detail table for the email
					$products_list = '';
					$virtual_product = true;
					foreach ($products as $key => $product)
					{
						$price = Product::getPriceStatic((int)$product['id_product'], false, ($product['id_product_attribute'] ? (int)$product['id_product_attribute'] : null), 6, null, false, true, $product['cart_quantity'], false, (int)$order->id_customer, (int)$order->id_cart, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
						$price_wt = Product::getPriceStatic((int)$product['id_product'], true, ($product['id_product_attribute'] ? (int)$product['id_product_attribute'] : null), 2, null, false, true, $product['cart_quantity'], false, (int)$order->id_customer, (int)$order->id_cart, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
	
						$customization_quantity = 0;
						if (isset($customized_datas[$product['id_product']][$product['id_product_attribute']]))
						{
							$customization_text = '';
							foreach ($customized_datas[$product['id_product']][$product['id_product_attribute']] as $customization)
							{
								if (isset($customization['datas'][Product::CUSTOMIZE_TEXTFIELD]))
								foreach ($customization['datas'][Product::CUSTOMIZE_TEXTFIELD] as $text)
									$customization_text .= $text['name'].': '.$text['value'].'<br />';
	
								if (isset($customization['datas'][Product::CUSTOMIZE_FILE]))
									$customization_text .= sprintf(Tools::displayError('%d image(s)'), count($customization['datas'][Product::CUSTOMIZE_FILE])).'<br />';
	
								$customization_text .= '---<br />';
							}
	
							$customization_text = rtrim($customization_text, '---<br />');
	
							$customization_quantity = (int)$product['customizationQuantityTotal'];
							$products_list .=
							'<tr style="background-color: '.($key % 2 ? '#DDE2E6' : '#EBECEE').';">
								<td style="padding: 0.6em 0.4em;">'.$product['reference'].'</td>
								<td style="padding: 0.6em 0.4em;"><strong>'.$product['name'].(isset($product['attributes']) ? ' - '.$product['attributes'] : '').' - '.Tools::displayError('Customized').(!empty($customization_text) ? ' - '.$customization_text : '').'</strong></td>
								<td style="padding: 0.6em 0.4em; text-align: right;">'.Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ?  Tools::ps_round($price, 2) : $price_wt, $this->context->currency, false).'</td>
								<td style="padding: 0.6em 0.4em; text-align: center;">'.$customization_quantity.'</td>
								<td style="padding: 0.6em 0.4em; text-align: right;">'.Tools::displayPrice($customization_quantity * (Product::getTaxCalculationMethod() == PS_TAX_EXC ? Tools::ps_round($price, 2) : $price_wt), $this->context->currency, false).'</td>
							</tr>';
						}
	
						if (!$customization_quantity || (int)$product['cart_quantity'] > $customization_quantity)
							$products_list .=
							'<tr style="background-color: '.($key % 2 ? '#DDE2E6' : '#EBECEE').';">
								<td style="padding: 0.6em 0.4em;">'.$product['reference'].'</td>
								<td style="padding: 0.6em 0.4em;"><strong>'.$product['name'].(isset($product['attributes']) ? ' - '.$product['attributes'] : '').'</strong></td>
								<td style="padding: 0.6em 0.4em; text-align: right;">'.Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ? Tools::ps_round($price, 2) : $price_wt, $this->context->currency, false).'</td>
								<td style="padding: 0.6em 0.4em; text-align: center;">'.((int)$product['cart_quantity'] - $customization_quantity).'</td>
								<td style="padding: 0.6em 0.4em; text-align: right;">'.Tools::displayPrice(((int)$product['cart_quantity'] - $customization_quantity) * (Product::getTaxCalculationMethod() == PS_TAX_EXC ? Tools::ps_round($price, 2) : $price_wt), $this->context->currency, false).'</td>
							</tr>';
	
						// Check if is not a virutal product for the displaying of shipping
						if (!$product['is_virtual'])
							$virtual_product &= false;
	
					} // end foreach ($products)
	
					$cart_rules_list = '';
					foreach ($cart_rules as $cart_rule)
					{
						$package = array('id_carrier' => $order->id_carrier, 'id_address' => $order->id_address_delivery, 'products' => $order->product_list);
						$values = array(
								'tax_incl' => $cart_rule['obj']->getContextualValue(true, $this->context, CartRule::FILTER_ACTION_ALL, $package),
								'tax_excl' => $cart_rule['obj']->getContextualValue(false, $this->context, CartRule::FILTER_ACTION_ALL, $package)
						);
	
						// If the reduction is not applicable to this order, then continue with the next one
						if (!$values['tax_excl'])
							continue;
	
						$order->addCartRule($cart_rule['obj']->id, $cart_rule['obj']->name, $values);
	
						/* IF
						 ** - This is not multi-shipping
						** - The value of the voucher is greater than the total of the order
						** - Partial use is allowed
						** - This is an "amount" reduction, not a reduction in % or a gift
						** THEN
						** The voucher is cloned with a new value corresponding to the remainder
						*/
						if (count($order_list) == 1 && $values['tax_incl'] > $order->total_products_wt && $cart_rule['obj']->partial_use == 1 && $cart_rule['obj']->reduction_amount > 0)
						{
							// Create a new voucher from the original
							$voucher = new CartRule($cart_rule['obj']->id); // We need to instantiate the CartRule without lang parameter to allow saving it
							unset($voucher->id);
	
							// Set a new voucher code
							$voucher->code = empty($voucher->code) ? Tools::substr(md5($order->id.'-'.$order->id_customer.'-'.$cart_rule['obj']->id), 0, 16) : $voucher->code.'-2';
							if (preg_match('/\-([0-9]{1,2})\-([0-9]{1,2})$/', $voucher->code, $matches) && $matches[1] == $matches[2])
								$voucher->code = preg_replace('/'.$matches[0].'$/', '-'.((int)($matches[1]) + 1), $voucher->code);
	
							// Set the new voucher value
							if ($voucher->reduction_tax)
								$voucher->reduction_amount = $values['tax_incl'] - $order->total_products_wt;
							else
								$voucher->reduction_amount = $values['tax_excl'] - $order->total_products;
	
							$voucher->id_customer = $order->id_customer;
							$voucher->quantity = 1;
							if ($voucher->add())
							{
								// If the voucher has conditions, they are now copied to the new voucher
								CartRule::copyConditions($cart_rule['obj']->id, $voucher->id);
	
								$params = array(
										'{voucher_amount}' => Tools::displayPrice($voucher->reduction_amount, $this->context->currency, false),
										'{voucher_num}' => $voucher->code,
										'{firstname}' => $this->context->customer->firstname,
										'{lastname}' => $this->context->customer->lastname,
										'{id_order}' => $order->reference,
										'{order_name}' => $order->getUniqReference()
								);
								Mail::Send(
								(int)$order->id_lang,
								'voucher',
								sprintf(Mail::l('New voucher regarding your order %s', (int)$order->id_lang), $order->reference),
								$params,
								$this->context->customer->email,
								$this->context->customer->firstname.' '.$this->context->customer->lastname,
								null, null, null, null, _PS_MAIL_DIR_, false, (int)$order->id_shop
								);
							}
						}
	
						if ($id_order_state != Configuration::get('PS_OS_ERROR') && $id_order_state != Configuration::get('PS_OS_CANCELED') && !in_array($cart_rule['obj']->id, $cart_rule_used))
						{
							$cart_rule_used[] = $cart_rule['obj']->id;
	
							// Create a new instance of Cart Rule without id_lang, in order to update its quantity
							$cart_rule_to_update = new CartRule($cart_rule['obj']->id);
							$cart_rule_to_update->quantity = max(0, $cart_rule_to_update->quantity - 1);
							$cart_rule_to_update->update();
						}
	
						$cart_rules_list .= '
						<tr style="background-color:#EBECEE;">
							<td colspan="4" style="padding:0.6em 0.4em;text-align:right">'.Tools::displayError('Voucher name:').' '.$cart_rule['obj']->name.'</td>
							<td style="padding:0.6em 0.4em;text-align:right">'.($values['tax_incl'] != 0.00 ? '-' : '').Tools::displayPrice($values['tax_incl'], $this->context->currency, false).'</td>
						</tr>';
					}
	
					// Specify order id for message
					$old_message = Message::getMessageByCartId((int)$this->context->cart->id);
					if ($old_message)
					{
						$message = new Message((int)$old_message['id_message']);
						$message->id_order = (int)$order->id;
						$message->update();
	
						// Add this message in the customer thread
						$customer_thread = new CustomerThread();
						$customer_thread->id_contact = 0;
						$customer_thread->id_customer = (int)$order->id_customer;
						$customer_thread->id_shop = (int)$this->context->shop->id;
						$customer_thread->id_order = (int)$order->id;
						$customer_thread->id_lang = (int)$this->context->language->id;
						$customer_thread->email = $this->context->customer->email;
						$customer_thread->status = 'open';
						$customer_thread->token = Tools::passwdGen(12);
						$customer_thread->add();
	
						$customer_message = new CustomerMessage();
						$customer_message->id_customer_thread = $customer_thread->id;
						$customer_message->id_employee = 0;
						$customer_message->message = htmlentities($message->message, ENT_COMPAT, 'UTF-8');
						$customer_message->private = 0;
	
						if (!$customer_message->add())
							$this->errors[] = Tools::displayError('An error occurred while saving message');
					}
	
					// Hook validate order
					Hook::exec('actionValidateOrder', array(
					'cart' => $this->context->cart,
					'order' => $order,
					'customer' => $this->context->customer,
					'currency' => $this->context->currency,
					'orderStatus' => $order_status
					));
	
					foreach ($this->context->cart->getProducts() as $product)
					if ($order_status->logable)
						ProductSale::addProductSale((int)$product['id_product'], (int)$product['cart_quantity']);
	
					if (Configuration::get('PS_STOCK_MANAGEMENT') && $order_detail->getStockState())
					{
						$history = new OrderHistory();
						$history->id_order = (int)$order->id;
						$history->changeIdOrderState(Configuration::get('PS_OS_OUTOFSTOCK'), $order, true);
						$history->addWithemail();
					}
	
					// Set order state in order history ONLY even if the "out of stock" status has not been yet reached
					// So you migth have two order states
					$new_history = new OrderHistory();
					$new_history->id_order = (int)$order->id;
					$new_history->changeIdOrderState((int)$id_order_state, $order, true);
					$new_history->addWithemail(true, $extra_vars);
	
					unset($order_detail);
	
					// Order is reloaded because the status just changed
					$order = new Order($order->id);
	
					// Send an e-mail to customer (one order = one email)
					if ($id_order_state != Configuration::get('PS_OS_ERROR') && $id_order_state != Configuration::get('PS_OS_CANCELED') && $this->context->customer->id)
					{
						$invoice = new Address($order->id_address_invoice);
						$delivery = new Address($order->id_address_delivery);
						$delivery_state = $delivery->id_state ? new State($delivery->id_state) : false;
						$invoice_state = $invoice->id_state ? new State($invoice->id_state) : false;
	
						$data = array(
								'{firstname}' => $this->context->customer->firstname,
								'{lastname}' => $this->context->customer->lastname,
								'{email}' => $this->context->customer->email,
								'{delivery_block_txt}' => $this->_getFormatedAddress($delivery, "\n"),
								'{invoice_block_txt}' => $this->_getFormatedAddress($invoice, "\n"),
								'{delivery_block_html}' => $this->_getFormatedAddress($delivery, '<br />', array(
										'firstname'	=> '<span style="color:#DB3484; font-weight:bold;">%s</span>',
										'lastname'	=> '<span style="color:#DB3484; font-weight:bold;">%s</span>'
								)),
								'{invoice_block_html}' => $this->_getFormatedAddress($invoice, '<br />', array(
										'firstname'	=> '<span style="color:#DB3484; font-weight:bold;">%s</span>',
										'lastname'	=> '<span style="color:#DB3484; font-weight:bold;">%s</span>'
								)),
								'{delivery_company}' => $delivery->company,
								'{delivery_firstname}' => $delivery->firstname,
								'{delivery_lastname}' => $delivery->lastname,
								'{delivery_address1}' => $delivery->address1,
								'{delivery_address2}' => $delivery->address2,
								'{delivery_city}' => $delivery->city,
								'{delivery_postal_code}' => $delivery->postcode,
								'{delivery_country}' => $delivery->country,
								'{delivery_state}' => $delivery->id_state ? $delivery_state->name : '',
								'{delivery_phone}' => ($delivery->phone) ? $delivery->phone : $delivery->phone_mobile,
								'{delivery_other}' => $delivery->other,
								'{invoice_company}' => $invoice->company,
								'{invoice_vat_number}' => $invoice->vat_number,
								'{invoice_firstname}' => $invoice->firstname,
								'{invoice_lastname}' => $invoice->lastname,
								'{invoice_address2}' => $invoice->address2,
								'{invoice_address1}' => $invoice->address1,
								'{invoice_city}' => $invoice->city,
								'{invoice_postal_code}' => $invoice->postcode,
								'{invoice_country}' => $invoice->country,
								'{invoice_state}' => $invoice->id_state ? $invoice_state->name : '',
								'{invoice_phone}' => ($invoice->phone) ? $invoice->phone : $invoice->phone_mobile,
								'{invoice_other}' => $invoice->other,
								'{order_name}' => $order->getUniqReference(),
								'{date}' => Tools::displayDate(date('Y-m-d H:i:s'), (int)$order->id_lang, 1),
								'{carrier}' => $virtual_product ? Tools::displayError('No carrier') : $carrier->name,
								'{payment}' => Tools::substr($order->payment, 0, 45),
								'{products}' => $this->formatProductAndVoucherForEmail($products_list),
								'{discounts}' => $this->formatProductAndVoucherForEmail($cart_rules_list),
								'{total_paid}' => Tools::displayPrice($order->total_paid, $this->context->currency, false),
								'{total_products}' => Tools::displayPrice($order->total_paid - $order->total_shipping - $order->total_wrapping + $order->total_discounts, $this->context->currency, false),
								'{total_discounts}' => Tools::displayPrice($order->total_discounts, $this->context->currency, false),
								'{total_shipping}' => Tools::displayPrice($order->total_shipping, $this->context->currency, false).'<br />'.$this->l('COD fee included'),
								'{total_wrapping}' => Tools::displayPrice($order->total_wrapping, $this->context->currency, false));
	
						if (is_array($extra_vars))
							$data = array_merge($data, $extra_vars);
	
						// Join PDF invoice
						if ((int)Configuration::get('PS_INVOICE') && $order_status->invoice && $order->invoice_number)
						{
							$pdf = new PDF($order->getInvoicesCollection(), PDF::TEMPLATE_INVOICE, $this->context->smarty);
							$file_attachement['content'] = $pdf->render(false);
							$file_attachement['name'] = Configuration::get('PS_INVOICE_PREFIX', (int)$order->id_lang).sprintf('%06d', $order->invoice_number).'.pdf';
							$file_attachement['mime'] = 'application/pdf';
						}
						else
							$file_attachement = null;
	
						if (Validate::isEmail($this->context->customer->email))
							Mail::Send(
									(int)$order->id_lang,
									'order_conf',
									Mail::l('Order confirmation', (int)$order->id_lang),
									$data,
									$this->context->customer->email,
									$this->context->customer->firstname.' '.$this->context->customer->lastname,
									null,
									null,
									$file_attachement,
									null, _PS_MAIL_DIR_, false, (int)$order->id_shop
							);
					}
	
					// updates stock in shops
					if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT'))
					{
						$product_list = $order->getProducts();
						foreach ($product_list as $product)
						{
							// if the available quantities depends on the physical stock
							if (StockAvailable::dependsOnStock($product['product_id']))
							{
								// synchronizes
								StockAvailable::synchronize($product['product_id'], $order->id_shop);
							}
						}
					}
				}
				else
				{
					$error = Tools::displayError('Order creation failed');
					Logger::addLog($error, 4, '0000002', 'Cart', (int)($order->id_cart));
					die($error);
				}
			} // End foreach $order_detail_list
			// Use the last order as currentOrder
			$this->currentOrder = (int)$order->id;
			return true;
		}
		else
		{
			$error = Tools::displayError('Cart cannot be loaded or an order has already been placed using this cart');
			Logger::addLog($error, 4, '0000001', 'Cart', (int)($this->context->cart->id));
			die($error);
		}
	}
	
	public function switchStatusLang($iso_lang)
	{
		switch ($iso_lang)
		{
			case 'en': return 'Awaiting store payment';
			case 'es': return 'En espera de pago en tienda';
			case 'fr': return 'Paiement en boutique';
			case 'it': return 'Pagamento in negozio';
			case 'de': return 'Bezahlung im Ladengeschaft.'; 
			case 'nl': return 'Bezahlung im Ladengeschaft.';
			case 'pl': return 'Platnosc w sklepie';
			case 'pt': return 'Pagament la Loja';
			default: return 'Awaiting store payment';
		}
	}
}