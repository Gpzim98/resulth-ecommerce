{*
* 2007-2014 Innova Deluxe
*
*  @author      Innova Deluxe
*  @copyright   Innova Deluxe
*  @version     1.0
*}

{literal}
<script type="text/javascript">
$(window).load(function() {
	$.ajax({async:false});
	var shipping_cost_text = {/literal}{$total_shipping_cart}{literal};
	
	if(shipping_cost_text!="")
	{{/literal}{if $es15}{literal}
		var ship = "{/literal}{convertPriceWithCurrency price=$total_shipping_cart currency=$currency}{literal}";
		$("#total_shipping").html(ship);
		$("#cart_block_total").html("{/literal}{convertPriceWithCurrency price=$total currency=$currency}{literal}");
		$('#cart-prices').prepend('<span>{/literal}{l s='Fee' mod='deluxestorepayment'}{literal}</span><span id=cart_block_shipping_cost class=price ajax_cart_shipping_cost>{/literal}{convertPriceWithCurrency price=$fee currency=$currency}{literal}</span>');
	{/literal}{else}{literal}
		var ship = "{/literal}{convertPriceWithCurrency price=$total_shipping_cart currency=$currency}{literal}";
		$("#total_shipping").html(ship);
		$("input[name=total_shipping_cost]").val(ship);
		$("#cart_block_total").html("{/literal}{convertPriceWithCurrency price=$total currency=$currency}{literal}");
		$('#cart-prices').prepend('<span id=cart_block_shipping_cost class=price ajax_cart_shipping_cost>{/literal}{convertPriceWithCurrency price=$fee currency=$currency}{literal}</span><span>{/literal}{l s='Store payment fee' mod='deluxestorepayment'}{literal}</span><br />');
	{/literal}{/if}{literal}
	}else{
		$("#total_shipping").html("{/literal}{l s='Free' mod='deluxestorepayment'}{literal}");
	}
});
function loading()
{
	$('*').css('cursor', 'wait');
	document.getElementById('loading').style.display = "block";
}
</script>
{/literal}
{capture name=path}{l s='Cash on payment store with fee' mod='deluxestorepayment'}{/capture}
<h2>{l s='Order summary' mod='deluxestorepayment'}</h2>

{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

{if $nbProducts <= 0}
	<p class="warning">{l s='Your cart is empty.' mod='deluxestorepayment'}</p>
{else}
{assign var='carrier_selected' value='0'}
<h3>{l s='Cash on payment store with fee payment' mod='deluxestorepayment'}</h3>
<form action="{$link->getModuleLink('deluxestorepayment', 'validation', [], true)|escape:'html'}" method="post">
<p>
	<img src="{$modules_dir|escape:'quotes'}deluxestorepayment/img/deluxestorepayment.gif" alt="{l s='Cash on payment store with fee payment' mod='deluxestorepayment'}" style="float:left; margin: 0px 10px 5px 0px;" />
	<br />
	{l s='You have chosen the cash on payment store method.' mod='deluxestorepayment'}
	<br /><br /><br /><br />
	<table id="cart_summary" class="std">
		<thead>
			<tr>
				<th class="cart_product first_item" colspan="7">{l s='Details of your order (taxes included)' mod='deluxestorepayment'}</th>
			</tr>
		</thead>
		<tfoot>
			<tr class="cart_total_price">
				<td colspan="6">{l s='Order:' mod='deluxestorepayment'}</td>
				<td colspan="1" class="price" id="total_product">{convertPriceWithCurrency price=$cartwithoutshipping currency=$currency}</td>
			</tr>
			<tr class="cart_total_delivery">
				<td colspan="6">{l s='Shipping cost:' mod='deluxestorepayment'}</td>
				<td colspan="1" class="price" id="total_shipping">
					<img src="{$modules_dir|escape:'quotes'}deluxestorepayment/img/loading.gif" alt="" />
				</td>
			</tr>
			<tr class="cart_total_tax">
				{if $fee!=0}
					<td colspan="6">{l s='Cash payment in store fee:' mod='deluxestorepayment'}</td>
					<td colspan="1" class="price" id="total_tax">{convertPriceWithCurrency price=$fee currency=$currency}</td>
					<input type="hidden" value="{$fee}" name="total_tax_fee">
				{else}
					<td colspan="6">{l s='Cash payment in store fee (free from' mod='deluxestorepayment'} {convertPriceWithCurrency price=$free_fee currency=$currency} {l s='of purchase):' mod='deluxestorepayment'}</td>
					<td colspan="1" class="price" id="total_tax">{convertPriceWithCurrency price=$fee currency=$currency}</td>
				{/if}
			</tr>
			<tr class="cart_total_price">
				<td colspan="6" id="cart_voucher" class="cart_voucher">
					{if $fee!=0}
						{l s='*Store payment fee will be added to the cost of transport.' mod='deluxestorepayment'}
					{/if}
				</td>
				<td colspan="1" class="price total_price_container" id="total_price_container">
					<p>{l s='Total:' mod='deluxestorepayment'}</p>
					<span id="total_price">{convertPriceWithCurrency price=$total currency=$currency}</span>
				</td>
			</tr>
		</tfoot>
	</table>
</p>
<b>{l s='Please confirm your order by clicking \'Confirm my order\'' mod='deluxestorepayment'}.</b>
<p id="loading" style="text-align:center;display:none;"><br /><img src="{$base_dir|escape:'quotes'}img/loadingAnimation.gif" width="208" height="13" />

{if $es15}
<p class="cart_navigation submit">
	<input type="submit" style="padding: 1%" name="paymentSubmit" value="{l s='Confirm my order' mod='deluxestorepayment'}" class="exclusive_large" onclick="loading();" />
	<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html'}" class="button_large">
		{l s='Other payment methods' mod='deluxestorepayment'}
	</a>
</p>
{/if}

{if !$es15}
<p class="cart_navigation">
	<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html'}" class="button-exclusive btn btn-default">
		<i class="icon-chevron-left"></i>
		{l s='Other payment methods' mod='deluxestorepayment'}
	</a>
	<input type="submit" style="padding: 1%" name="paymentSubmit" value="{l s='Confirm my order' mod='deluxestorepayment'}" class="button btn btn-default standard-checkout button-medium" onclick="loading();" />
</p>
{/if}

</form>
{/if}