{*
* 2007-2014 Innova Deluxe
*
*  @author      Innova Deluxe
*  @copyright   Innova Deluxe
*  @version     1.0
*}

{if $success == true}
		{if $es15}
			<div>
			<p>{l s='Your order on' mod='deluxestorepayment'} <span class="bold">{$shop_name|escape:'htmlall'}</span> {l s='is complete.' mod='deluxestorepayment'}
				<br /><br />
				{l s='The order you will pay in the store.' mod='deluxestorepayment'}
				<br /><br />- {l s='Total payment pending:' mod='deluxestorepayment'} <span class="price">{convertPriceWithCurrency price=$total currency=$currency}</span>
				<br /><br />{l s='For any questions or for further information, please contact us.' mod='deluxestorepayment'} <a style="text-decoration: underline" href="{$base_dir}index.php?controller=contact">{l s='Customer support.' mod='deluxestorepayment'}</a>
			</p>
			</div>
			<p class='cart_navigation submit'>
				<a href="{$base_dir_ssl|escape:'quotes'}" class="button btn btn-default button-medium">
					<span>
						{l s='Continue shopping' mod='deluxestorepayment'}
					<i class="icon-chevron-right right"></i>
					</span>
					
				</a>
			</p>
		{/if}
	
		{if !$es15}
			<div>
			<p>{l s='Your order on' mod='deluxestorepayment'} <span class="bold">{$shop_name|escape:'htmlall'}</span> {l s='is complete.' mod='deluxestorepayment'}
				<br /><br />
				{l s='The order you will pay in the store.' mod='deluxestorepayment'}
				<br /><br />- {l s='Total payment pending:' mod='deluxestorepayment'} <span class="price">{convertPriceWithCurrency price=$total currency=$currency}</span>
				<br /><br />{l s='For any questions or for further information, please contact us.' mod='deluxestorepayment'} <a style="text-decoration: underline" href="{$base_dir}index.php?controller=contact">{l s='Customer support.' mod='deluxestorepayment'}</a>
			</p>
			</div>
			<p class='cart_navigation'>
				<a href="{$base_dir_ssl|escape:'quotes'}" class="button btn btn-default button-medium">
					<span>
						{l s='Continue shopping' mod='deluxestorepayment'}
					<i class="icon-chevron-right right"></i>
					</span>
					
				</a>
			</p>
		{/if}
{else}
	<p class="cart_navigation">
		{l s='We noticed a problem with your order. If you think this is an error, you can contact us.' mod='deluxestorepayment'}	
		<a href="{$base_dir|escape:'quotes'}index.php?controller=contact" class="button btn btn-default button-medium">
			<span>
			{l s='Customer support.' mod='deluxestorepayment'}
			<i class="icon-chevron-right right"></i>
			</span>
		</a>
	</p>
{/if}
