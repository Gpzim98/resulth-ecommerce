{*
* 2007-2014 Innova Deluxe
*
*  @author      Innova Deluxe
*  @copyright   Innova Deluxe
*  @version     1.2
*}

{if $show}
	{literal}
	<script type="text/javascript">
		$(document).ready(function() {
			$('.cart_quantity_button').click(function() {
				updateCarrierSelectionAndGift();
			});
			$('.cart_quantity_delete').click(function() {
				updateCarrierSelectionAndGift();
			});
			$('#cgv').click(function() {
				updateCarrierSelectionAndGift();
			});
		});
	{/literal}
	</script>
	
	{assign var='carrier_ok' value='nok'}
	
	{foreach $carriers_array as $carrier_array}
		{if $carrier_selected == $carrier_array}
			{$carrier_ok = 'ok'}
			{break}
		{/if}
	{/foreach}
    {if $carrier_ok == 'ok'}
		{if ($maximum_amount > 0 && $cartcost < $maximum_amount) || ($maximum_amount == 0)}
			{if $cartcost > $minimum_amount}
                {if $allow_payments == $carrier_selected}
                
                    {literal}
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('.payment_module').empty();
                                $('#store_payment_innova').addClass("payment_module");
                            });
                        </script>
                    {/literal}
                    
                    <div class="row">
                        <div class="col-xs-12 {if $ps_version < '1.6.0.11'}col-md-6{/if}">
                            <p id="store_payment_innova">
                                {if $es15}<img src="{$this_path|escape:'quotes'}img/deluxestorepayment.gif" alt="{l s='Store payment' mod='deluxestorepayment'}" />{/if}
                                <a  class="cash" href="{$link->getModuleLink('deluxestorepayment', 'payment')|escape:'html'}" title="{l s='Store payment' mod='deluxestorepayment'}">
                                    {l s='Payment Store:' mod='deluxestorepayment'} {convertPriceWithCurrency price=$cartcost currency=$currency}
                                    
                                    {if $fee > 0}
                                    + {convertPriceWithCurrency price=$fee currency=$currency} {l s='(Fee)' mod='deluxestorepayment'} 
                                    = {convertPriceWithCurrency price=$total currency=$currency}
                                    {/if}
                                </a>
                            </p>
                        </div>
                    </div>    
                {else}
                    <div class="row">
                        <div class="col-xs-12 {if $ps_version < '1.6.0.11'}col-md-6{/if}">
                            <p class="payment_module">
                                {if $es15}<img src="{$this_path|escape:'quotes'}img/deluxestorepayment.gif" alt="{l s='Store payment' mod='deluxestorepayment'}" />{/if}
                                <a  class="cash" href="{$link->getModuleLink('deluxestorepayment', 'payment')|escape:'html'}" title="{l s='Store payment' mod='deluxestorepayment'}">
                                    {l s='Payment Store:' mod='deluxestorepayment'} {convertPriceWithCurrency price=$cartcost currency=$currency}
                                    {if $fee > 0}
                                    + {convertPriceWithCurrency price=$fee currency=$currency} {l s='(Fee)' mod='deluxestorepayment'} 
                                    = {convertPriceWithCurrency price=$total currency=$currency}
                                    {/if}
                                </a>
                            </p>
                        </div>
                    </div>    
                {/if}
                
			{/if}
		{/if}
	{/if}
{/if}