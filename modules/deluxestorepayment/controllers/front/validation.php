<?php
/**
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2014 Innova Deluxe SL

* @license   INNOVADELUXE
*/

class DeluxeStorePaymentValidationModuleFrontController extends ModuleFrontController
{
	public function postProcess()
	{
		$cart = $this->context->cart;
		$total_tax_fee = number_format((float)Tools::getValue('total_tax_fee'),2);
		if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active)
			Tools::redirect('index.php?controller=order&step=1');

		$authorized = false;
		foreach (Module::getPaymentModules() as $module)
			if ($module['name'] == 'deluxestorepayment')
			{
				$authorized = true;
				break;
			}
		if (!$authorized)
			die($this->module->l('This payment method is not available.', 'validation'));

		$customer = new Customer($cart->id_customer);
		if (!Validate::isLoadedObject($customer))
			Tools::redirect('index.php?controller=order&step=1');

		$currency = $this->context->currency;
	
		$cart->additional_shipping_cost = $total_tax_fee;
		$cartcost = $cart->getOrderTotal(true, 3);
		$total = $total_tax_fee + $cartcost;
		$deluxeStorePay = new DeluxeStorePayment();
		if(_PS_VERSION_>='1.5.3')
		{
			$deluxeStorePay->validateOrderStorePayment($cart->id, Configuration::get('PS_OS_STOREPAYMENT'), $total, $total_tax_fee, $this->module->displayName, NULL, NULL, (int)$currency->id, false, $customer->secure_key, $this->context->shop);
			Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$customer->secure_key);
		}
		else
		{
			$deluxeStorePay->validateOrderStorePaymentOld($cart->id, Configuration::get('PS_OS_STOREPAYMENT'), $total, $total_tax_fee, $this->module->displayName, NULL, NULL, (int)$currency->id, false, $customer->secure_key, $this->context->shop);
			Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$customer->secure_key);
				
		}
	}
}
