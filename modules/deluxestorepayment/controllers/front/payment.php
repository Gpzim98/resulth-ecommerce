<?php
/**
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* @author    Innova Deluxe SL
* @copyright 2014 Innova Deluxe SL

* @license   INNOVADELUXE
*/

class DeluxeStorePaymentPaymentModuleFrontController extends ModuleFrontController
{
	public $ssl = true;
	public $display_column_left = false;

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		$this->display_column_left = false;
		parent::initContent();
		$cart = $this->context->cart;
		$cashOnPayment = new DeluxeStorePayment();
		$fee = (float)Tools::ps_round((float)$cashOnPayment->getFeeCost($cart), 2);
		$cartcost = $cart->getOrderTotal(true, 3);
		$cartwithoutshipping = $cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING);
		$total = $fee + $cartcost;
		$cart->additional_shipping_cost = $fee;
		$carriers = explode(';', Configuration::get('DELUXESTOREPAYMENT_CARRIERS'));
		$cart = $this->context->cart;
		$conv_rate = (float)$this->context->currency->conversion_rate;
		
		if (!$this->module->_checkCurrency($cart))
			Tools::redirect('index.php?controller=order');

		$this->context->smarty->assign(array(
			'nbProducts' => $cart->nbProducts(),
			'cartcost' => number_format((float)$cartcost, 2, '.', ''),
			'cartwithoutshipping' => number_format((float)$cartwithoutshipping, 2, '.', ''),
			'fee' => number_format((float)$fee, 2, '.', ''),
			'free_fee' => (float)Tools::ps_round((float)Configuration::get('DELUXESTOREPAYMENT_FREE') * (float)$conv_rate, 2),
			'currency' => new Currency((int)$cart->id_currency),
			'total' => number_format((float)$total, 2, '.', ''),
			'carrier' => $cart->id_carrier,
			'carriers' => $carriers,
			'total_shipping_cart' => $cart->getTotalShippingCost(),
			'ps_version' => Tools::substr(_PS_VERSION_,0,-4),
			'es15' => version_compare(_PS_VERSION_, '1.6.0.0', '<'),
			'this_path_ssl' => (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8').__PS_BASE_URI__.'modules/deluxecodfees/'
		));

		$this->setTemplate('validation.tpl');
	}
}
