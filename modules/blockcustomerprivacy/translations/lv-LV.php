<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_e0de5a06213f21c55ca3283c009e0907'] = 'Pircēja datu drošības bloks';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_f192f208f0bc97af4c5213eee3e78793'] = 'Pievieno bloku, kas attēlo infromāciju par pircēju datiem un tās drošību.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_d71315851e7e67cbacf5101c5c4ab83d'] = 'Personīgā informācija ko norādat tiek izmantota, lai apkopotu aptaujas, apstrādātu pasūtījumus.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_d847e75da5489bbc525b6f6548d7f50a'] = 'Jūs varat labot vai izdzēst personīgo informāciju no "Mans Konts" lapas.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'Konfigurācija atjaunota';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_fb32badede7c8613fddb8502d847c18b'] = 'Lūdzu piekrīti par klienta datu aizsardzību, atzīmējot izvēles aili zemāk.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_f4f70727dc34561dfde1a3c529b6205c'] = 'Uzstādījumi';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_aafe06caae02aee29775402368a6d22c'] = 'Ziņa par klientu datu aizsardzību:';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_03e1a999dcdb904300ee1b1e767c83c9'] = 'Ziņa, kas tiks parādīta ,kad izveidos kontu.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_b51d73fb490ad1245fa9b87042bbbbb7'] = '';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_c9cc8cce247e49bae79f15173ce97354'] = 'Saglabāt';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_fb0440f9ca32a8b49eded51b09e70821'] = 'Klientu datu privātums';


return $_MODULE;
