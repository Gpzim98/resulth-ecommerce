<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_e0de5a06213f21c55ca3283c009e0907'] = 'Πληροφορίες εμπιστευτικότητας δεδομένων χρήστη';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_f192f208f0bc97af4c5213eee3e78793'] = 'Προσθέτει ένα ταμπλό που εμφανίζει ένα μήνυμα σχετικά με τα προσωπικά δεδομένα των πελατών.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_d71315851e7e67cbacf5101c5c4ab83d'] = 'Τα προσωπικά δεδομένα που παρέχετε χρησιμοποιούνται για την απάντηση ερωτήσεων, την διαδικασία παραγγελειών ή για να επιτραπεί η πρόσβαση σε συγκεκριμένες πληροφορίες.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_d847e75da5489bbc525b6f6548d7f50a'] = 'Έχετε το δικαίωμα να τροποποιήσετε και να διαγράψετε όλα τις προσωπικές πληροφορίες που βρίσκονται στη σελίδα "Ο Λογαριασμός μου".';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'Η διαμόρφωση ενημερώθηκε';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_fb32badede7c8613fddb8502d847c18b'] = 'Παρακαλούμε εάν συμφωνείτε με την πολιτική προστασίας δεδομένων των πελατών, επιλέγοντας το πλαίσιο επιλογής παρακάτω.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_f4f70727dc34561dfde1a3c529b6205c'] = 'Ρυθμίσεις';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_aafe06caae02aee29775402368a6d22c'] = 'Μήνυμα προσωπικών δεδομένών του πελάτη σας:';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_03e1a999dcdb904300ee1b1e767c83c9'] = 'Μήνυμα που θα εμφανίζεται στη φόρμα δημιουργίας λογαριασμού.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_b51d73fb490ad1245fa9b87042bbbbb7'] = 'Συμβουλή: Εάν το μήνυμα προσωπικών δεδομένων πελάτη είναι πολύ μεγάλο για να γραφτεί κατ\' ευθείαν στη φόρμα, μπορείτε να προσθέσετε ένα σύνδεσμο σε μια από τις σελίδες σας. Αυτό μπορεί να δημιουργηθεί εύκολα μέσω της σελίδας "CMS" στο μενού "Προτιμήσεις".';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_c9cc8cce247e49bae79f15173ce97354'] = 'Αποθήκευση';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_fb0440f9ca32a8b49eded51b09e70821'] = 'Προστασία προσωπικών δεδομένων πελατών';


return $_MODULE;
