<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_e0de5a06213f21c55ca3283c009e0907'] = 'Blok: Ochrana osobných údajov';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_f192f208f0bc97af4c5213eee3e78793'] = 'Pridá blok zobrazujúci správu o ochrane osobných údajov zákazníka.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_d71315851e7e67cbacf5101c5c4ab83d'] = 'Osobné údaje, ktoré ste poskytli slúžia na zodpovedanie otázok, spracovanie objednávky, alebo umožňujú prístup k špecifickým informáciám.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_d847e75da5489bbc525b6f6548d7f50a'] = 'Máte právo upravovať a zmazať všetky osobné informácie nájdené na stránke "Môj účet".';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_20015706a8cbd457cbb6ea3e7d5dc9b3'] = '';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_fb32badede7c8613fddb8502d847c18b'] = 'Prosím, prejavte súhlas so spracovaním osobných osobných údajov v súlade s pravidlami ich ochrany zaškrtnutím políčka nižšie.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_f4f70727dc34561dfde1a3c529b6205c'] = 'Nastavenia';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_aafe06caae02aee29775402368a6d22c'] = 'Správa o bezpečnosti osobných údajov zákazníka:';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_03e1a999dcdb904300ee1b1e767c83c9'] = 'Správa, ktorá bude zobrazená vo formulári pri vytváraní účtu';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_b51d73fb490ad1245fa9b87042bbbbb7'] = '';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_c9cc8cce247e49bae79f15173ce97354'] = 'Uložiť';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_fb0440f9ca32a8b49eded51b09e70821'] = 'Ochrana osobných údajov';


return $_MODULE;
