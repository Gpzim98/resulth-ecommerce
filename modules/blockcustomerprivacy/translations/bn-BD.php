<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_e0de5a06213f21c55ca3283c009e0907'] = 'গ্রাহক তথ্য গোপনীয়তা ব্লক';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_f192f208f0bc97af4c5213eee3e78793'] = 'গ্রাহকের গোপনীয়তা তথ্য সম্পর্কে একটি বার্তা প্রদর্শন করা ব্লক যুক্ত করুন.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_d71315851e7e67cbacf5101c5c4ab83d'] = 'আপনার প্রদান করা ব্যক্তিগত তথ্য অনুসন্ধান প্রক্রিয়া,তথ্য প্রদান করার বা অর্ডার উত্তর বা নির্দিষ্ট তথ্য এর জন্য জন্য ব্যবহার করা হয়';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_d847e75da5489bbc525b6f6548d7f50a'] = 'আপনার " আমার অ্যাকাউন্ট " পৃষ্ঠার সকল ব্যক্তিগত তথ্য পরিবর্তন করার এবং মুছে দেওয়ার অধিকার আছে.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'কনফিগারেশন আপডেট হয়েছে ';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_fb32badede7c8613fddb8502d847c18b'] = 'আপনি যদি  কাস্টমার ডেটা গোপনীয় বার্তার শর্তাবলীর সাথে সম্মত হন ,তাহলে নীচের চেকবক্স ক্লিক করুন';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_f4f70727dc34561dfde1a3c529b6205c'] = 'সেটিংস';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_aafe06caae02aee29775402368a6d22c'] = 'গ্রাহক তথ্য গোপনীয়তা বার্তা:';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_03e1a999dcdb904300ee1b1e767c83c9'] = 'গ্রাহকের তথ্য গোপনীয়তা  বার্তা অ্যাকাউন্ট তৈরির ফরমে  প্রদর্শন করা হবে.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_b51d73fb490ad1245fa9b87042bbbbb7'] = '';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_c9cc8cce247e49bae79f15173ce97354'] = 'সংরক্ষণ করুন';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_fb0440f9ca32a8b49eded51b09e70821'] = 'গ্রাহক তথ্য গোপনীয়তা';


return $_MODULE;
