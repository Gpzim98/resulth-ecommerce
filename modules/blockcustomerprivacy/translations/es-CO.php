<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_e0de5a06213f21c55ca3283c009e0907'] = 'Bloque de privacidad de los datos del cliente';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_f192f208f0bc97af4c5213eee3e78793'] = 'Agrega un bloque que muestra un mensaje sobre la privacidad de los datos del cliente.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_d71315851e7e67cbacf5101c5c4ab83d'] = 'Los datos personales que usted proporciona se utilizan para responder a las consultas, procesar pedidos o permitir el acceso a información específica.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_d847e75da5489bbc525b6f6548d7f50a'] = 'Usted tiene el derecho de modificar y eliminar toda la información personal que se encuentra en la página "Mi Cuenta".';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'Configuración actualizada';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_fb32badede7c8613fddb8502d847c18b'] = 'Si usted acepta los términos del mensaje sobre la Privacidad de los Datos de los Clientes, por favor haga clic en la casilla de verificación a continuación.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_f4f70727dc34561dfde1a3c529b6205c'] = 'Ajustes';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_aafe06caae02aee29775402368a6d22c'] = 'Mensaje sobre la privacidad de los datos de los clientes:';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_03e1a999dcdb904300ee1b1e767c83c9'] = 'El mensaje sobre la privacidad de datos del cliente se mostrará en el formulario de creación de la cuenta.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_b51d73fb490ad1245fa9b87042bbbbb7'] = 'Consejo: Si el mensaje de la privacidad del cliente es demasiado largo para ser escrito directamente en el formulario, puede añadir un enlace a una de sus páginas. Esto puede crearse fácilmente través de la página "CMS" en el menú "Preferencias".';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_fb0440f9ca32a8b49eded51b09e70821'] = 'Privacidad de los datos de los clientes';


return $_MODULE;
