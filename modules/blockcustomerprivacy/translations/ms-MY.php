<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_e0de5a06213f21c55ca3283c009e0907'] = 'Blok privasi data pelanggan';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_f192f208f0bc97af4c5213eee3e78793'] = 'Letak satu blok yang memaparkan mesej tentang data privasi pelanggan.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_d71315851e7e67cbacf5101c5c4ab83d'] = 'Data peribadi yang anda beri hanya digunakan untuk menjawab pertanyaan, memproses tempahan atau membenarkan akses ke maklumat terperinci.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_d847e75da5489bbc525b6f6548d7f50a'] = 'Anda berhak mengubah dan memadam segala maklumat peribadi yang boleh dijumpai di halaman "Akaun Saya".';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'Tatarajah yang baru';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_fb32badede7c8613fddb8502d847c18b'] = 'Jika anda setuju dengan terma dan syarat di dalam Privasi Data Pelanggan, sila klik kotak di bawah.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_f4f70727dc34561dfde1a3c529b6205c'] = 'Tetapan';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_aafe06caae02aee29775402368a6d22c'] = 'Mesej privasi data pelanggan:';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_03e1a999dcdb904300ee1b1e767c83c9'] = 'Mesej privasi data pelanggan akan dipaparkan di borang penciptaan akaun.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_b51d73fb490ad1245fa9b87042bbbbb7'] = '';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_c9cc8cce247e49bae79f15173ce97354'] = 'Simpan';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_fb0440f9ca32a8b49eded51b09e70821'] = 'Privasi data pelanggan';


return $_MODULE;
