<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{productscategory}prestashop>productscategory_8a4f5a66d0fcc9d13614516db6e3d47a'] = 'Producten in dezelfde categorie';
$_MODULE['<{productscategory}prestashop>productscategory_1d269d7f013c3d9d891a146f4379eb02'] = 'Voegt een blok toe aan de productpagina die producten van dezelfde categorie toont.';
$_MODULE['<{productscategory}prestashop>productscategory_8dd2f915acf4ec98006d11c9a4b0945b'] = 'Instellingen zijn met succes bijgewerkt';
$_MODULE['<{productscategory}prestashop>productscategory_f4f70727dc34561dfde1a3c529b6205c'] = 'Instellingen';
$_MODULE['<{productscategory}prestashop>productscategory_e06ba84b50810a88438ae0537405f65a'] = 'Toon de product prijzen';
$_MODULE['<{productscategory}prestashop>productscategory_1d986024f548d57b1d743ec7ea9b09d9'] = 'Toon de prijzen van de producten die getoond worden in het blok.';
$_MODULE['<{productscategory}prestashop>productscategory_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Ingeschakeld';
$_MODULE['<{productscategory}prestashop>productscategory_b9f5c797ebbf55adccdd8539a65a0241'] = 'Uitgeschakeld';
$_MODULE['<{productscategory}prestashop>productscategory_c9cc8cce247e49bae79f15173ce97354'] = 'Opslaan';
$_MODULE['<{productscategory}prestashop>productscategory_1f910bcf84a92cb7c71fa3d926c8a525'] = 'Andere producten in dezelfde categorie';
$_MODULE['<{productscategory}prestashop>productscategory_dd1f775e443ff3b9a89270713580a51b'] = 'Vorige';
$_MODULE['<{productscategory}prestashop>productscategory_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'Volgende';
$_MODULE['<{productscategory}prestashop>productscategory_4aae87211f77aada2c87907121576cfe'] = 'Andere producten in dezelfde categorie:';


return $_MODULE;
