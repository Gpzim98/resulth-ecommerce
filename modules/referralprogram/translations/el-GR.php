<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{referralprogram}prestashop>referralprogram_40604a195d001353d697b0fd26f5d8fe'] = 'Όλοι οι χορηγοί και οι φίλοι θα διαγραφούν. Είστε σίγουρος ότι θέλετε να απεγκαταστήσετε αυτή τη μονάδα;';
$_MODULE['<{referralprogram}prestashop>referralprogram_83090230d3c11aa76851030eba008a71'] = 'Πρόγραμμα προτεινόμενων πελατών';
$_MODULE['<{referralprogram}prestashop>referralprogram_46a3a666d8823b972af8018a5242a3ac'] = 'Ενσωματώστε ένα σύστημα προτεινόμενων στο κατάστημα σας.';
$_MODULE['<{referralprogram}prestashop>referralprogram_d59ce2e96c0c2362a0a4269e0d874e32'] = 'Παρακαλώ ορίστε ένα μέγεθος για κουπόνια για το πρόγραμμα σύστασης.';
$_MODULE['<{referralprogram}prestashop>referralprogram_284033830e0eaf55340844305bf34fdd'] = 'Αμοιβή προτεινόμενων';
$_MODULE['<{referralprogram}prestashop>referralprogram_3c5db6c207f700f4f209b43bed966e96'] = 'Αδυναμία διαγραφής αυτού του αρχείου:';
$_MODULE['<{referralprogram}prestashop>referralprogram_6c58031e7824c3cd6febc2f2107b0652'] = 'Οι ρυθμίσεις ενημερώθηκαν.';
$_MODULE['<{referralprogram}prestashop>referralprogram_167579019fe14b4efed431a82985d9ad'] = 'Ποσότητα παραγγελίας είναι απαραίτητη/μη έγκυρη.';
$_MODULE['<{referralprogram}prestashop>referralprogram_ceb0ce9b627fb9a962543f3271da29b1'] = 'Η τιμή έκπτωσης δεν είναι έγκυρη.';
$_MODULE['<{referralprogram}prestashop>referralprogram_9611f10b809aa2daf5c0eb88153901d4'] = 'Εκπτωτική αξία για το νόμισμα #%d είναι άδεια.';
$_MODULE['<{referralprogram}prestashop>referralprogram_ee328e3466040a3e52db5d764b7b7e0c'] = 'Εκπτωτική αξία για το νόμισμα #%d είναι λάθος.';
$_MODULE['<{referralprogram}prestashop>referralprogram_addae886b20a06e2954461706c90cd7d'] = 'Ο τύπος έκπτωσης απαιτείται/δεν είναι έγκυρος.';
$_MODULE['<{referralprogram}prestashop>referralprogram_ac3357de2dec8d74d89dd378962ec621'] = 'Νούμερο φίλων που χρειάζεται/εσφαλμένο.';
$_MODULE['<{referralprogram}prestashop>referralprogram_18d0d79eb088abb4357e3666eec35660'] = 'Εκπτωτικό ποσοστό χρειάζεται/λανθασμένο.';
$_MODULE['<{referralprogram}prestashop>referralprogram_3d31dd445991f35b1ee6491eec7ac71c'] = 'Αδυναμία εγγραφής στο αρχείο xml.';
$_MODULE['<{referralprogram}prestashop>referralprogram_96191c1f54bb6311624210333ef797eb'] = 'Αδυναμία κλεισίματος του αρχείου xml.';
$_MODULE['<{referralprogram}prestashop>referralprogram_21cc1fccae3b04bb8cd2719cc5269e1e'] = 'Αδυναμία ενημέρωσης του αρχείου xml. Παρακαλώ ελέγξτε τα δικαιώματα εγγραφής του αρχείου xml.';
$_MODULE['<{referralprogram}prestashop>referralprogram_94c69408d25102ba7ddcf3533b56c407'] = 'Λανθασμένο αντικείμενο πελάτη.';
$_MODULE['<{referralprogram}prestashop>referralprogram_a82e0d057f443115e807bd6ca595fc8c'] = 'Λανθασμένο αντικείμενο παραγγελίας.';
$_MODULE['<{referralprogram}prestashop>referralprogram_8b80d4b6307990874b832cc15a92e5a3'] = 'Λείπουν παράμετροι';
$_MODULE['<{referralprogram}prestashop>referralprogram_f4f70727dc34561dfde1a3c529b6205c'] = 'Ρυθμίσεις';
$_MODULE['<{referralprogram}prestashop>referralprogram_b99212fd7697557380441442dc4f965e'] = 'Ελάχιστος αριθμός από παραγγελίες ένας πελάτης πρέπει να βάλει για να γίνει χορηγός';
$_MODULE['<{referralprogram}prestashop>referralprogram_624f983eb0713e10faf35ff3889e8d68'] = 'Νούμερο φίλων στη φόρμα πρόσκλησης στο πρόγραμμα προτεινόμενων (ενότητα λογαριασμό πελάτη, πρόγραμμα προτεινόμενων):';
$_MODULE['<{referralprogram}prestashop>referralprogram_6e043d6d6f5a7b9d42a81f9f8bf4956c'] = 'Τύπος κουπονιού :';
$_MODULE['<{referralprogram}prestashop>referralprogram_46da7ad7d01e209241016d308f9fafce'] = 'Κουπόνι που προσφέρει ένα ποσοστό';
$_MODULE['<{referralprogram}prestashop>referralprogram_97839379a0f447599405341b852e2e24'] = 'Κουπόνι που προσφέρει μια καθορισμένη ποσότητα (κατά νόμισμα)';
$_MODULE['<{referralprogram}prestashop>referralprogram_37be07209f53a5d636d5c904ca9ae64c'] = 'Ποσοστό';
$_MODULE['<{referralprogram}prestashop>referralprogram_edf7f0a17b8a8f1732f12856fcbc8a6b'] = 'Ποσότητα κουπονιών';
$_MODULE['<{referralprogram}prestashop>referralprogram_c2fac6cb3fc5ec77cb3957e148d18b0d'] = 'Φόρος Κουπονιού';
$_MODULE['<{referralprogram}prestashop>referralprogram_befcac0f9644a7abee43e69f49252ac4'] = 'χωρίς ΦΠΑ';
$_MODULE['<{referralprogram}prestashop>referralprogram_f4a0d7cb0cd45214c8ca5912c970de13'] = 'με ΦΠΑ';
$_MODULE['<{referralprogram}prestashop>referralprogram_cf6964e57fc5ed2000af41b39cc71f10'] = 'Περιγραφή κουπονιού';
$_MODULE['<{referralprogram}prestashop>referralprogram_c9cc8cce247e49bae79f15173ce97354'] = 'Αποθήκευση';
$_MODULE['<{referralprogram}prestashop>referralprogram_6b719c160f9b08dad4760bcc4b52ed48'] = 'Όροι του παραπεμπτικού προγράμματος';
$_MODULE['<{referralprogram}prestashop>referralprogram_9dffbf69ffba8bc38bc4e01abf4b1675'] = 'Κείμενο';
$_MODULE['<{referralprogram}prestashop>form_386c339d37e737a436499d423a77df0c'] = 'Nόμισμα';
$_MODULE['<{referralprogram}prestashop>form_edf7f0a17b8a8f1732f12856fcbc8a6b'] = 'Ποσότητα κουπονιών';
$_MODULE['<{referralprogram}prestashop>program_d3d2e617335f08df83599665eef8a418'] = 'Κλείσιμο';
$_MODULE['<{referralprogram}prestashop>program_7a81aa9275331bb0f5e6adb5e8650a03'] = 'ή το κουμπί Esc';
$_MODULE['<{referralprogram}prestashop>program_36c94bd456cf8796723ad09eac258aef'] = 'Διαχείρηση του λογαριασμού μου';
$_MODULE['<{referralprogram}prestashop>program_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Ο λογαριασμός μου';
$_MODULE['<{referralprogram}prestashop>program_6540b502e953f4c05abeb8f234cd50bf'] = 'Πρόγραμμα Συστάσεων';
$_MODULE['<{referralprogram}prestashop>program_6b31baf25848e7a6563ecc3946626c80'] = 'Παραπεμπτικό πρόγραμμα';
$_MODULE['<{referralprogram}prestashop>program_dbc65cd60abde51277c2881ce915a225'] = 'Απαιτείτε να συμφωνήσετε στους όρους του πρόγραμμα προτεινόμενων!';
$_MODULE['<{referralprogram}prestashop>program_83fc792f687bc45d75ac35c84c721a26'] = 'Τουλάχιστον ένα email είναι λανθασμένο!';
$_MODULE['<{referralprogram}prestashop>program_1019072b9e450c8652590cda2db45e49'] = 'Τουλάχιστον ένα όνομα ή επίθετο είναι λανθασμένο!';
$_MODULE['<{referralprogram}prestashop>program_ff2d2e45b90b4426c3bb14bd56b95a2d'] = 'Κάποιος με αυτό το email έχει ήδη προταθεί!';
$_MODULE['<{referralprogram}prestashop>program_3f8a4c7f93945fea0d951fa402ee4272'] = 'Παρακαλώ τσεκάρετε τουλάχιστον ένα κουτάκι';
$_MODULE['<{referralprogram}prestashop>program_dcc99d8715486f570db3ec5ee469a828'] = 'Δεν μπορώ να προσθέσω φίλους στην βάση δεδομένων';
$_MODULE['<{referralprogram}prestashop>program_f6cb78f0afcf7c3a06048a7a5855d6a1'] = 'E-mail στάλθηκαν στους φίλους σας!';
$_MODULE['<{referralprogram}prestashop>program_79cd362fc64832faa0a2079f1142aa12'] = 'Ένα email στάλθηκε στο/η φίλο/η σας!';
$_MODULE['<{referralprogram}prestashop>program_2b90ca4a7b1c83e0a3bb65899725cd65'] = 'Email υπενθύμισης στάλθηκαν στους φίλους σας!';
$_MODULE['<{referralprogram}prestashop>program_819e52b3c6ca4db131dcfea19188a0c3'] = 'Ένα email υπενθύμισης στάλθηκε στο/η φίλο/η σας!';
$_MODULE['<{referralprogram}prestashop>program_46ee2fe8845962d24bf5178a26e109f3'] = 'Χορήγηση των φίλων μου';
$_MODULE['<{referralprogram}prestashop>program_7e9b0e998138fefac8749975c737ac27'] = 'Λίστα των φίλων που εκκρεμούν';
$_MODULE['<{referralprogram}prestashop>program_c56567bc42584de1a7ac430039b3a87e'] = 'Εκκρεμείς φίλοι';
$_MODULE['<{referralprogram}prestashop>program_b9ebe5bbe91ed6e7e23285fb6c595ab4'] = 'Λίστα φίλων που χορήγησα';
$_MODULE['<{referralprogram}prestashop>program_58c7f2542ab2e2c3e4e39e851ea0f225'] = 'Φίλοι που χορήγησαν';
$_MODULE['<{referralprogram}prestashop>program_a44fa3fcad7f0f3ef4b6d77b74ca66f2'] = 'Λάβετε μια έκπτωση των %1$s για εσάς και τους φίλους σας προτείνοντας αυτή την Ιστοσελίδα.';
$_MODULE['<{referralprogram}prestashop>program_8d3ae82bfa996855cdf841dd9e15a7e3'] = 'Είναι εύκολο και γρήγορο. Απλά συμπληρώστε το όνομα, το επώνυμο και την ηλεκτρονική διεύθυνση(σεις) του φίλου(ων) σας στα παρακάτω πεδία.';
$_MODULE['<{referralprogram}prestashop>program_c92182a305ac874bae5e54d75bbd9327'] = 'Όταν ένας από αυτούς κάνει τουλάχιστον %d παραγγελίες';
$_MODULE['<{referralprogram}prestashop>program_6e8dedb6b5dcad23e24d07c10aff29d8'] = 'Όταν ένας από αυτούς κάνει τουλάχιστον %d παραγγελία';
$_MODULE['<{referralprogram}prestashop>program_8d122aed0fe2c5076e4941bfbd53fe04'] = 'αυτός ή αυτή θα λάβει ένα %1$s κουπόνι και θα λάβετε το δικό σας κουπόνι αξίας %1$s.';
$_MODULE['<{referralprogram}prestashop>program_8d3f5eff9c40ee315d452392bed5309b'] = 'Επώνυμο';
$_MODULE['<{referralprogram}prestashop>program_20db0bfeecd8fe60533206a2b5e9891a'] = 'Όνομα';
$_MODULE['<{referralprogram}prestashop>program_1e884e3078d9978e216a027ecd57fb34'] = 'e-mail';
$_MODULE['<{referralprogram}prestashop>program_9386de858384e7f790a28beecdb986dd'] = 'Σημαντικό: Οι ηλεκτρονικές διευθύνσεις των φίλων σας θα χρησιμοποιηθούν μόνο στο παραπεμπτικό πρόγραμμα. Δεν χρησιμοποιηθούν ποτέ για άλλους σκοπούς.';
$_MODULE['<{referralprogram}prestashop>program_605eef3cad421619ce034ab48415190f'] = 'Συμφωνώ με τους όρους της υπηρεσίας και θα τους τηρώ άνευ όρων.';
$_MODULE['<{referralprogram}prestashop>program_6b719c160f9b08dad4760bcc4b52ed48'] = 'Όροι του παραπεμπτικού προγράμματος';
$_MODULE['<{referralprogram}prestashop>program_868ca5fe643791c23b47c75fb833c9b8'] = 'Διαβάστε τους όρους.';
$_MODULE['<{referralprogram}prestashop>program_31fde7b05ac8952dacf4af8a704074ec'] = 'Προεπισκόπηση';
$_MODULE['<{referralprogram}prestashop>program_8e8dc296c6bf3876468aa028974bfebe'] = 'Εmail-πρόσκληση';
$_MODULE['<{referralprogram}prestashop>program_a86073a0c3b0bebf11bd807caf8e505a'] = 'το προεπιλεγμένο email';
$_MODULE['<{referralprogram}prestashop>program_7532696b81dfc0b94a37e876677152c5'] = 'που θα σταλθεί στον φίλο(ους) σας.';
$_MODULE['<{referralprogram}prestashop>program_ad3d06d03d94223fa652babc913de686'] = 'Επικύρωση';
$_MODULE['<{referralprogram}prestashop>program_59352cd5314a67c0fb10c964831920f3'] = 'Για να γίνετε χορηγός, χρειάζεται να έχετε ολοκληρώσει τουλάχιστον';
$_MODULE['<{referralprogram}prestashop>program_12c500ed0b7879105fb46af0f246be87'] = 'Παραγγελίες';
$_MODULE['<{referralprogram}prestashop>program_70a17ffa722a3985b86d30b034ad06d7'] = 'παραγγελία';
$_MODULE['<{referralprogram}prestashop>program_ec7342814444c667ab93181b30b28e38'] = 'Αυτοί οι φίλοι δεν κάνανε καμία παραγγελία σε αυτό το site από τότε που τους πρότεινες, αλλά μπορείς να δοκιμάσεις πάλι! Για να το κάνεις, επέλεξε τα κουτάκια που αντιστοιχούν στον/στους φίλο/ους που θέλεις να υπενθυμίσεις, και μετά κάνε κλικ στο κουμπί "Υπενθύμισε τον/τους φίλο/ους"';
$_MODULE['<{referralprogram}prestashop>program_3e717a04ff77cd5fa068d8ad9d3facc8'] = 'Τελευταία πρόσκληση';
$_MODULE['<{referralprogram}prestashop>program_9c9d4ed270f02c72124702edb192ff19'] = 'Υπενθύμιση φίλου(ων) μου';
$_MODULE['<{referralprogram}prestashop>program_161133b6c2d0f520d9221ec8180c16e2'] = 'Δεν έχεις εκκρεμείς προσκλήσεις.';
$_MODULE['<{referralprogram}prestashop>program_26ef5cd4989dd9b7a6e81a22312b06fd'] = 'Δεν έχεις προτείνει φίλους ακόμα.';
$_MODULE['<{referralprogram}prestashop>program_193f3d8bbaceba40499cab1a3545e9e8'] = 'Εδώ είναι οι χορηγούμενη φίλοι που αποδέχθηκαν την πρόσκλησή σας:';
$_MODULE['<{referralprogram}prestashop>program_3c648ba41cfb45f13b083a9cbbacdfdf'] = 'Ημερομηνία επιγραφής';
$_MODULE['<{referralprogram}prestashop>program_8d4e5c2bc4c3cf67d2b59b263a707cb6'] = 'Κανένας χορηγημένος φίλος δεν αποδέχθηκε την πρόσκληση σας ακόμα.';
$_MODULE['<{referralprogram}prestashop>program_0b3db27bc15f682e92ff250ebb167d4b'] = 'Επιστροφή στο λογαριασμό σας';
$_MODULE['<{referralprogram}prestashop>program_8cf04a9734132302f96da8e113e80ce5'] = 'Αρχική';
$_MODULE['<{referralprogram}prestashop>rules_01705c0177ebf5fbcbf4e882bc454405'] = 'Κανόνες παραπεμπτικών προγραμμάτων';
$_MODULE['<{referralprogram}prestashop>authentication_6b31baf25848e7a6563ecc3946626c80'] = 'Παραπεμπτικό πρόγραμμα';
$_MODULE['<{referralprogram}prestashop>authentication_8fdb2298a0db461ac64e71192a562ca1'] = 'Ηλεκτρονική διεύθυνση email του χορηγού σας';
$_MODULE['<{referralprogram}prestashop>hook_customers_16_6b31baf25848e7a6563ecc3946626c80'] = 'Παραπεμπτικό πρόγραμμα';
$_MODULE['<{referralprogram}prestashop>hook_customers_16_7790d51a3d62c85aae65464dee12ee8b'] = 'Πελάτης Προτασσόμενος:';
$_MODULE['<{referralprogram}prestashop>hook_customers_16_f964f762284ede747ed9f6428a5469b8'] = 'Κανείς δεν έχει προτείνει αυτόν τον πελάτη.';
$_MODULE['<{referralprogram}prestashop>hook_customers_16_53d0d7aba39ee971f7f179e6e1092708'] = 'Προταθείς πελάτες:';
$_MODULE['<{referralprogram}prestashop>hook_customers_16_58dc82d3e2e17ced0225064a9b496ee9'] = 'Προταθείς πελάτης:';
$_MODULE['<{referralprogram}prestashop>hook_customers_16_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{referralprogram}prestashop>hook_customers_16_49ee3087348e8d44e1feda1917443987'] = 'Όνομα';
$_MODULE['<{referralprogram}prestashop>hook_customers_16_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'e-mail';
$_MODULE['<{referralprogram}prestashop>hook_customers_16_22ffd0379431f3b615eb8292f6c31d12'] = 'Εγγραφή ημερομηνία';
$_MODULE['<{referralprogram}prestashop>hook_customers_16_325c8bbd07033a39d25b5c4457f79861'] = 'Πελάτες προτάθηκαν από τον φίλο';
$_MODULE['<{referralprogram}prestashop>hook_customers_16_fc6e0920b914b164802d44220e6163f3'] = 'Παραγγελίες που υποβλήθηκαν';
$_MODULE['<{referralprogram}prestashop>hook_customers_16_970ad4e4787cc75cd63dbf8d5c757513'] = 'Λογαριασμός πελάτη δημιουργήθηκε';
$_MODULE['<{referralprogram}prestashop>hook_customers_6b31baf25848e7a6563ecc3946626c80'] = 'Παραπεμπτικό πρόγραμμα';
$_MODULE['<{referralprogram}prestashop>hook_customers_7790d51a3d62c85aae65464dee12ee8b'] = 'Πελάτης Προτασσόμενος:';
$_MODULE['<{referralprogram}prestashop>hook_customers_f964f762284ede747ed9f6428a5469b8'] = 'Κανείς δεν έχει προτείνει αυτόν τον πελάτη.';
$_MODULE['<{referralprogram}prestashop>hook_customers_53d0d7aba39ee971f7f179e6e1092708'] = 'Προταθείς πελάτες:';
$_MODULE['<{referralprogram}prestashop>hook_customers_58dc82d3e2e17ced0225064a9b496ee9'] = 'Προταθείς πελάτης:';
$_MODULE['<{referralprogram}prestashop>hook_customers_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{referralprogram}prestashop>hook_customers_49ee3087348e8d44e1feda1917443987'] = 'Όνομα';
$_MODULE['<{referralprogram}prestashop>hook_customers_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'e-mail';
$_MODULE['<{referralprogram}prestashop>hook_customers_22ffd0379431f3b615eb8292f6c31d12'] = 'Εγγραφή ημερομηνία';
$_MODULE['<{referralprogram}prestashop>hook_customers_325c8bbd07033a39d25b5c4457f79861'] = 'Πελάτες προτάθηκαν από τον φίλο';
$_MODULE['<{referralprogram}prestashop>hook_customers_fc6e0920b914b164802d44220e6163f3'] = 'Παραγγελίες που υποβλήθηκαν';
$_MODULE['<{referralprogram}prestashop>hook_customers_970ad4e4787cc75cd63dbf8d5c757513'] = 'Λογαριασμός πελάτη δημιουργήθηκε';
$_MODULE['<{referralprogram}prestashop>my-account_6b31baf25848e7a6563ecc3946626c80'] = 'Παραπεμπτικό πρόγραμμα';
$_MODULE['<{referralprogram}prestashop>order-confirmation_f2ef523efa8d23f8afc29e195592fc58'] = 'Χάρη στην παραγγελία σας, ο χορηγός σας %1$s %2$s θα λάβει ένα κουπόνι αξίας %3$d κάτω όταν επιβεβαιωθεί αυτή η παραγγελία.';
$_MODULE['<{referralprogram}prestashop>shopping-cart_6b31baf25848e7a6563ecc3946626c80'] = 'Παραπεμπτικό πρόγραμμα';
$_MODULE['<{referralprogram}prestashop>shopping-cart_b76b807810393d9fce7f154d82aef1d1'] = 'Κερδίσατε ένα κουπόνι αξίας %s από τις χορηγίες σας!';
$_MODULE['<{referralprogram}prestashop>shopping-cart_9a5b602be8d9b2d4b8c3f22911fba01d'] = 'Εισάγετε τον κωδικό του κουπονιού %s για να λάβετε έκπτωση σε αυτή την παραγγελία.';
$_MODULE['<{referralprogram}prestashop>shopping-cart_106527986549f3ec8da1ae5a7abde467'] = 'Εμφάνιση του προγράμματος προτεινόμενων.';
$_MODULE['<{referralprogram}prestashop>program_666149e67589bd65ed1e787932cd74f5'] = 'Όταν ένας από αυτούς κάνει τουλάχιστον %d παραγγελίες ';
$_MODULE['<{referralprogram}prestashop>program_def1ac1f353beef52f7e86a30cae01c4'] = 'Όταν ένας από αυτούς κάνει τουλάχιστον %d παραγγελία ';


return $_MODULE;
