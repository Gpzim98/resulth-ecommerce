<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockbanner}prestashop>blockbanner_4b92fcfe6f0ec26909935aa960b7b81f'] = 'பதாகை தொகுதி';
$_MODULE['<{blockbanner}prestashop>blockbanner_9ec3d0f07db2d25a37ec4b7a21c788f8'] = '';
$_MODULE['<{blockbanner}prestashop>blockbanner_df7859ac16e724c9b1fba0a364503d72'] = 'இந்த கோப்பை பதிவேற்றும்  முயற்சியில் ஒரு பிழை ஏற்பட்டுள்ளது.';
$_MODULE['<{blockbanner}prestashop>blockbanner_efc226b17e0532afff43be870bff0de7'] = 'இந்த அமைப்புகள் புதுப்பிக்கப்பட்டுள்ளன.';
$_MODULE['<{blockbanner}prestashop>blockbanner_f4f70727dc34561dfde1a3c529b6205c'] = 'அமைப்புகள்';
$_MODULE['<{blockbanner}prestashop>blockbanner_9edcdbdff24876b0dac92f97397ae497'] = '';
$_MODULE['<{blockbanner}prestashop>blockbanner_e90797453e35e4017b82e54e2b216290'] = '';
$_MODULE['<{blockbanner}prestashop>blockbanner_46fae48f998058600248a16100acfb7e'] = '';
$_MODULE['<{blockbanner}prestashop>blockbanner_084fa1da897dfe3717efa184616ff91c'] = '';
$_MODULE['<{blockbanner}prestashop>blockbanner_ff09729bee8a82c374f6b61e14a4af76'] = '';
$_MODULE['<{blockbanner}prestashop>blockbanner_112f6f9a1026d85f440e5ca68d8e2ec5'] = 'தயவு கூர்ந்து இந்த பதாகைக்கு ஒரு குறுகிய ஆனால் அர்த்தமுடன் விளக்கத்தை உள்ளிடவும்.';
$_MODULE['<{blockbanner}prestashop>blockbanner_c9cc8cce247e49bae79f15173ce97354'] = 'சேமி';
$_MODULE['<{blockbanner}prestashop>form_92fbf0e5d97b8afd7e73126b52bdc4bb'] = 'ஒரு கோப்பை தேர்வு செய்யவும்';


return $_MODULE;
