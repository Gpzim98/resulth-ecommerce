<?php
/**
 * 2007-2013 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2014 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */


require '/../../includes/include.php';
class CieloNotificationOrderPrestashop
{

    
    public function postProcess($post)
    {
        try {
           // Resgata último pedido feito da SESSION
            $ultimoPedido = $post->count();    
            $ultimoPedido -= 1;
            $Pedido = new Pedido();
            $Pedido->FromString($post->offsetGet($ultimoPedido));
            // Consulta situação da transação
            $objResposta = $Pedido->RequisicaoConsulta();
            // Atualiza status
            $Pedido->status = $objResposta->status;
            // Atualiza Pedido da SESSION
            $StrPedido = $Pedido->ToString();
            $post->offsetSet($ultimoPedido, $StrPedido);
            //status do pedido            
            $status = $Pedido->getStatus();
            //id do status do pedido
            $idStatus = $this->getStatusCode($status);
            //id do pedido
            $idOrder = $this->getIdOrderByTId($Pedido->tid);
            //referência do pedido
            $reference = $this->getReferenceByTId($Pedido->tid);
            //atualiza tabela da cielo
            $this->createOrderCielo($reference,$Pedido->dadosPedidoValor,$Pedido->tid);
            //atualiza status do pedido
            $this->createAddOrderHistory($idOrder,$idStatus);

        } catch (Exception $e) {
            
        }

    }

    private function createOrderCielo($reference, $total, $tid, $auth){

        $sql = "SELECT `id` FROM `" . _DB_PREFIX_ . "cielo_order` WHERE `reference` = '".$reference."'";
        $cieloOrder = Db::getInstance()->getRow($sql);
        if($cieloOrder['id']){
           $this->updateOrder($reference,$total,$tid,$auth,$cieloOrder['id']);           
        }else{
           $this->saveOrder($reference,$total,$tid,$auth);
        }

    }
	
    private function updateOrder($reference, $total, $tid, $auth = NULL, $idCielo)
    {
        $sql = 'UPDATE `' . _DB_PREFIX_ . 'cielo_order`
        SET `reference` = \'' . $reference . '\',
        `total` = \'' . $total . '\',
        `tid` = \'' . $tid . '\',
        `auth` = \'' . $auth . '\'
        WHERE `id` = \'' . (int) $idCielo . '\';';

        if (! Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute($sql)) {
            die(Tools::displayError('Error when updating Transaction Code from Cielo in database'));
        }
    }

    private function saveOrder($reference, $total, $tid, $auth= NULL){
        $sql = 'INSERT INTO `' . _DB_PREFIX_ . 'cielo_order` (`reference`, `total`, `tid`, `auth`)
                VALUES (\'' . $reference . '\', \'' . $total . '\',\'' . $tid . '\',\'' . $auth . '\')';
        if (! Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute($sql)) {
            die(Tools::displayError('Error when updating Transaction Code from Cielo in database'));
        }
    }

    public static function createAddOrderHistory($idOrder, $status){
       $order_history = new OrderHistory();
       $order_history->id_order = $idOrder;
       $order_history->changeIdOrderState($status, $idOrder);
       $order_history->addWithemail();
       return true;
    }


    private function getStatusCode($status){
       $sql = "SELECT `id_order_state` FROM `" . _DB_PREFIX_ . "order_state_lang` WHERE `name` = '".$status."'";
       $statusCode = Db::getInstance()->getRow($sql);
       return $statusCode['id_order_state'];
    }

    private function getIdOrderByTId($tid){
       $sql = "SELECT `id_order` FROM `" . _DB_PREFIX_ . "orders` WHERE `id_transaction` = '".$tid."'";
       $idOrder = Db::getInstance()->getRow($sql);
       return $idOrder['id_order'];
    } 

    private function getReferenceByTId($tid){
       $sql = "SELECT `reference` FROM `" . _DB_PREFIX_ . "orders` WHERE `id_transaction` = '".$tid."'";
       $reference = Db::getInstance()->getRow($sql);
       return $reference['reference'];
    } 

}
    
    

