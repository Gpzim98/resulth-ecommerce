	<?php

	/*
	* 2007-2014 PrestaShop
	*
	* NOTICE OF LICENSE
	*
	* This source file is subject to the Academic Free License (AFL 3.0)
	* that is bundled with this package in the file LICENSE.txt.
	* It is also available through the world-wide-web at this URL:
	* http://opensource.org/licenses/afl-3.0.php
	* If you did not receive a copy of the license and are unable to
	* obtain it through the world-wide-web, please send an email
	* to license@prestashop.com so we can send you a copy immediately.
	*
	* DISCLAIMER
	*
	* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
	* versions in the future. If you wish to customize PrestaShop for your
	* needs please refer to http://www.prestashop.com for more information.
	*
	*  @author PrestaShop SA <contact@prestashop.com>
	*  @copyright  2007-2014 PrestaShop SA
	*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
	*  International Registered Trademark & Property of PrestaShop SA
	*/

	/**
	 * @since 1.5.0
	 */
	include_once dirname(__FILE__) . '/../../cielo.php';
	include_once dirname(__FILE__) . '/../../includes/include.php';
	class CieloValidationModuleFrontController extends ModuleFrontController
	{
		/**
		 * @see FrontController::postProcess()
		 */
		public function postProcess(){
		    //conteúdo do carrinho
		   $cart = $this->context->cart;
		   //se buypage loja
		   if(Configuration::get('CIELO_BUYPAGE') == 1)
		   	  $this->storeRequest($cart);
		   else
		   	  $this->cieloRequest($cart); //buypage cielo
		   
		}


		//buypage cielo
		function cieloRequest($cart){
			//valida carrinho e cria pedido
		   $this->validateCieloOrder($cart);
		   //total do pedido
		   $total = $cart->getOrderTotal(true, Cart::BOTH);
		   //retira "." e "," 
		   $valor = number_format($total,2);
		  
		   $Pedido = new Pedido();		
		   // Lê dados do $_POST
		   $Pedido->formaPagamentoBandeira = $_POST["codigoBandeira"]; 
		   if($_POST["formaPagamento"] != "A" && $_POST["formaPagamento"] != "1"){
		      $Pedido->formaPagamentoProduto =   Configuration::get('CIELO_INSTALLMENT_TYPE');
			  $Pedido->formaPagamentoParcelas =  $_POST["formaPagamento"];
		   } 
		   else {
		      $Pedido->formaPagamentoProduto = $_POST["formaPagamento"];
			  $Pedido->formaPagamentoParcelas = 1;
		   }
		   
		   $Pedido->dadosEcNumero = Configuration::get('CIELO');
		   $Pedido->dadosEcChave = Configuration::get('CIELO_KEY');
		
		   $Pedido->capturar = Configuration::get('CIELO_AUTOMATICALLY_CAPTURE');	
		   $Pedido->autorizar = Configuration::get('CIELO_AUTHORIZE_AUTHOMATIC');
		
		   $Pedido->dadosPedidoNumero = rand(1000000, 9999999); 
		   $Pedido->dadosPedidoValor = $this->moneyRemove($valor);

		   $Pedido->urlRetorno = ReturnURL();
		   		   
		   // ENVIA REQUISIÇÃO SITE CIELO
		   $objResposta = $Pedido->RequisicaoTransacao(false);

		   $Pedido->tid = $objResposta->tid;
		   $Pedido->pan = $objResposta->pan;
		   $Pedido->status = $objResposta->status;
		
		   $urlAutenticacao = "url-autenticacao";
		   $Pedido->urlAutenticacao = $objResposta->$urlAutenticacao;

		   $this->saveTransactionId($Pedido);

		   // Serializa Pedido e guarda na SESSION
		   $StrPedido = $Pedido->ToString();
		   $_SESSION["pedidos"]->append($StrPedido);

		   //redireciona requisição para o site da cielo
		   Tools::redirect($Pedido->urlAutenticacao);
		   #echo $Pedido->urlAutenticacao;
		}

		//buypage loja
		function storeRequest($cart){
		   //valida carrinho e cria pedido
		   $this->validateCieloOrder($cart);
		   //total do pedido
		   $total = $cart->getOrderTotal(true, Cart::BOTH);
		   //retira "." e "," 
		   $valor = number_format($total,2);

		   $Pedido = new Pedido();	
	       // Lê dados do $_POST
	       $Pedido->formaPagamentoBandeira = $_POST["codigoBandeira"]; 
	       if($_POST["formaPagamento"] != "A" && $_POST["formaPagamento"] != "1"){
		      $Pedido->formaPagamentoProduto = Configuration::get('CIELO_INSTALLMENT_TYPE');
		      $Pedido->formaPagamentoParcelas = $_POST["formaPagamento"];
	       } 
	       else {
		      $Pedido->formaPagamentoProduto = $_POST["formaPagamento"];
		      $Pedido->formaPagamentoParcelas = 1;
	       }
	
	       $Pedido->dadosEcNumero = Configuration::get('CIELO_STORE'); // STORE
	       $Pedido->dadosEcChave =  Configuration::get('CIELO_STORE_KEY'); // STORE_KEY
	
	       $Pedido->capturar = Configuration::get('CIELO_AUTOMATICALLY_CAPTURE');	
		   $Pedido->autorizar = Configuration::get('CIELO_AUTHORIZE_AUTHOMATIC');	
	       $Pedido->dadosPortadorNumero = $_POST["card_number"]; // cartaoNumero
	       $Pedido->dadosPortadorVal = $_POST["expiration_date"]; // cartaoValidade

	       // Verifica se Código de Segurança foi informado e ajusta o indicador corretamente Old: cartaoCodigoSeguranca 
	       if ($_POST["secure_code"] == null || $_POST["secure_code"] == ""){
		      $Pedido->dadosPortadorInd = "0";
	       }
	       else if ($Pedido->formaPagamentoBandeira == "mastercard"){
		      $Pedido->dadosPortadorInd = "1";
	       }
	       else {
		      $Pedido->dadosPortadorInd = "1";
	       }
	       $Pedido->dadosPortadorCodSeg = $_POST["secure_code"];
	
	       $Pedido->dadosPedidoNumero = rand(1000000, 9999999); 
	       $Pedido->dadosPedidoValor = $this->moneyRemove($valor);
	
	       $Pedido->urlRetorno = ReturnURL();
	       $objResposta = $Pedido->RequisicaoTid();
		
		   $Pedido->tid = $objResposta->tid;
		   $Pedido->pan = $objResposta->pan;
		   $Pedido->status = $objResposta->status;

		   $objResposta = $Pedido->RequisicaoAutorizacaoPortador();
	
	       $Pedido->tid = $objResposta->tid;
	       $Pedido->pan = $objResposta->pan;
	       $Pedido->status = $objResposta->status;
	       $this->saveTransactionId($Pedido);
	
	       // Serializa Pedido e guarda na SESSION
	       $StrPedido = $Pedido->ToString();
	       $_SESSION["pedidos"]->append($StrPedido);
	       Tools::redirect(ReturnURL());
		}


        //remove "," e "." do valor
	    function moneyRemove($valor){
	       $pontos = array(",", ".");
	       $result = str_replace($pontos, "", $valor);
	       return $result;
        }

        //validate a cart and create a order
        function validateCieloOrder($cart){
           if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active)
		      Tools::redirect('index.php?controller=order&step=1');

		   // Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
		   $authorized = false;
		   foreach (Module::getPaymentModules() as $module)
			   if ($module['name'] == 'cielo'){
			      $authorized = true;
			      break;
			   }
		   if (!$authorized)
		      die($this->module->l('This payment method is not available.', 'validation'));

		   $customer = new Customer($cart->id_customer);
		   if (!Validate::isLoadedObject($customer))
		      Tools::redirect('index.php?controller=order&step=1');

		   $currency = $this->context->currency;
		   $total = (float)$cart->getOrderTotal(true, Cart::BOTH);
		   $cielo = new Cielo();
		   $statusCode = $cielo->returnIdOrderByStatusCielo("Criada");

		   $this->module->validateOrder($cart->id, $statusCode, $total, $this->module->displayName);	
		}

		function saveTransactionId($Pedido){
			if(!isset($Pedido))
				return;
			
			$sql = 'UPDATE `' . _DB_PREFIX_ . 'orders`
                    SET `installmentcounts` = \'' . (int) $Pedido->formaPagamentoParcelas . '\',
                   `id_transaction` = \'' . $Pedido->tid . '\'
   		           WHERE `reference` = \'' .$this->module->currentOrderReference . '\';';
            if (! Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute($sql)) {
               die(Tools::displayError('Error when updating InstallmentCount from Cielo in database'));
            }
        } 


			


		}

		





