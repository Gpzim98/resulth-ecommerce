{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
	<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html':'UTF-8'}" title="{l s='Go back to the Checkout' mod='cielo'}">{l s='Checkout' mod='cielo'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='Cielo payment' mod='cielo'}
{/capture}

{include file="$tpl_dir./breadcrumb.tpl"}

   <div class="box">
      <h1 class="page-subheading">{l s='Pay with Cielo'}</h1>
      {assign var='current_step' value='payment'}
      {include file="$tpl_dir./order-steps.tpl"}
      {if $nbProducts <= 0}
         <p class="warning">{l s='Your shopping cart is empty.' mod='cielo'}</p>
      {else}
      <p class="required"><sup>*</sup>{l s='Required field'}</p>
      <form action="{$link->getModuleLink('cielo', 'validation', [], true)|escape:'html'}" class="std" method="post" >
         <label> 
         <p style="margin-top:20px;">{l s='The total amount of your order is' mod='cielo'}
	        <span id="amount" class="price">{displayPrice price=$total}</span>
	        {if $use_taxes == 1}
    	       {l s='(tax incl.)' mod='cielo'}
            {/if}
         </p>   
	   
	   </label>

	   	<div class="required form-group" style="width: 60%">
	    	<label for="credit_cart">{l s='Credit card:'}</label>
	      	<select class="form-control" name="codigoBandeira" id="codigoBandeira" required="required">
	      		<option value=""></option>
	      		{foreach from=$flags item=flag}
					<option value="{$flag}">{strtoupper($flag)}</option>
				{/foreach}
	      	</select>
	   	</div>

		<div class="required form-group" style="width: 60%">
			<label for="installment">{l s='Choose your installments:'}</label>
			<select class="form-control" name="formaPagamento" id="formaPagamento">
				<option value=""></option>
				{for $foo=1 to $qtdParcel}
				    <option value="{$foo}">{if $foo eq 1}Crédito à Vista{else}{$foo}x{/if}</option>
				{/for}
			</select>
		</div>

	   <div class="required form-group">
	      <label for="card_number">{l s="Card number"}</label>
	      <input name="card_number" class="is_required validate form-control" type="text" placeholder="{l s="Card Number"}"/>
	   </div>
	   <div class="required form-group">
	      <label for="expiration_date">{l s="Expiration Date"}</label>
	      <input name="expiration_date" class="required form-control" placeholder="{l s="Expiration date"}"/>
	   </div>
	   <div class="required form-group">
	      <label for="secure_code">{l s="Secure Code"}</label>
	      <input name="secure_code" class="required form-control" placeholder="{l s="Secure code"}"/>
	   </div>
    <p class="cart_navigation" id="cart_navigation">
	   <input type="submit" value="{l s='Pay' mod='cielo'}" class="button-medium" />
	   <a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html'}" class="button_large">{l s='Other payment methods' mod='cielo'}</a>
     </p>
</form>
</div>

<!-- Atualiza forma de pagamento de acordo com a bandeira selecionada -->
<script type="text/javascript">
$(document).ready(function() {
	$("select[name='codigoBandeira']").bind("change", function (event) {
		var bandeira = $("select[name='codigoBandeira']").val();
		if ((bandeira == 'mastercard') || (bandeira == 'visa')) {
			if ( $("select[name='formaPagamento'] option[value=A]").length == 0 ) {
				$("select[name='formaPagamento'] option:eq(0)").after("<option value='A'>Débito</option>");
			}
		} else {
			$("select[name='formaPagamento'] option[value='A']").remove(); 
		}
		$("select[name='formaPagamento'] option[value='']").attr('selected','selected');
		$.uniform.update("select[name='formaPagamento']");
	});
});
</script>

{/if}