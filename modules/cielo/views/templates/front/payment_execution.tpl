{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
	<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html':'UTF-8'}" title="{l s='Go back to the Checkout' mod='cielo'}">{l s='Checkout' mod='cielo'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='Cielo payment' mod='cielo'}
{/capture}

{include file="$tpl_dir./breadcrumb.tpl"}

<h2>{l s='Order summary' mod='cielo'}</h2>

{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

{if $nbProducts <= 0}
	<p class="warning">{l s='Your shopping cart is empty.' mod='cielo'}</p>
{else}


<h3>{l s='Cielo payment' mod='cielo'}</h3>
<form action="{$link->getModuleLink('cielo', 'validation', [], true)|escape:'html'}" method="post" >
   <p>
        <img src="{$this_path_bw}/img/cielo120x40.png" alt="{l s='cielo' mod='cielo'}" width="120" height="40" style="float:left; margin: 0px 10px 5px 0px;" />
        {l s='You have chosen to pay by Cielo' mod='pagseguro'}
        <br/><br />
        {l s='Here is a short summary of your order:' mod='pagseguro'}
	</p>
	<p style="margin-top:20px;">
	   - {l s='The total amount of your order is' mod='cielo'}
	   <span id="amount" class="price">{displayPrice price=$total}</span>
	      {if $use_taxes == 1}
    	     {l s='(tax incl.)' mod='cielo'}
          {/if}
    </p>
    <p>
	-
	   {if $currencies|@count > 1}
		   {l s='We allow several currencies to be sent via cielo.' mod='cielo'}
		   <br /><br />
		   {l s='Choose one of the following:' mod='cielo'}
	    <select id="currency_payement" name="currency_payement" onchange="setCurrency($('#currency_payement').val());">
			{foreach from=$currencies item=currency}
				<option value="{$currency.id_currency}" {if $currency.id_currency == $cust_currency}selected="selected"{/if}>{$currency.name}</option>
			{/foreach}
		</select>
	   {else}
		   {l s='We allow the following currency to be sent via cielo:' mod='cielo'}&nbsp;<b>{$currencies.0.name}</b>
		   <input type="hidden" name="currency_payement" value="{$currencies.0.id_currency}" />
	   {/if}
    </p>

    <p>
	   {l s='Choose your credit card, the installments and confirm by clicking "I confirm my order".' mod='cielo'}
	   <br /><br />	   
	   {l s='Choose your credit card:' mod='cielo'}
	   <select name="codigoBandeira">
		    {foreach from=$flags item=flag}
				<option value="{$flag}">{strtoupper($flag)}</option>
			{/foreach}
	   </select>
	   <br/><br/>
	   {l s='Choose your installments:' mod='cielo'}
	   		<span id="debito">
		   		<br><input type="radio" name="formaPagamento" value="A"> Débito
		   	</span>
		   	{for $foo=1 to $qtdParcel}
		   		<br><input type="radio" name="formaPagamento" value="{$foo}"> {if $foo eq 1}Crédito à Vista{else}{$foo}x{/if}
			{/for}
	   
    </p>    
    <p class="cart_navigation" id="cart_navigation">
	   <input type="submit" value="{l s='I confirm my order' mod='cielo'}" class="exclusive_large" />
	   <a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html'}" class="button_large">{l s='Other payment methods' mod='cielo'}</a>
     </p>
</form>

<!-- Atualiza forma de pagamento de acordo com a bandeira selecionada -->
<script type="text/javascript">
$(document).ready(function() {
	$("select[name='codigoBandeira']").bind("change", function (event) {
		var bandeira = $("select[name='codigoBandeira']").val();
		if ((bandeira == 'mastercard') || (bandeira == 'visa')) {
			$('#debito').show();
		} else {
			$("input[name='formaPagamento']").removeAttr('checked');
			$('#debito').hide();
		}
	});
});
</script>

{/if}