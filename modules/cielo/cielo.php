    <?php
       
       if (!defined('_PS_VERSION_'))
          exit;



       
      class Cielo extends PaymentModule{

        private $email;
        private $number;
        private $token;
        private $_html = '';
        private $_postErrors = array();

        private $criada = array(
          'criada' => 'Criada'
        );
        private $emAndamento = array(
          'em_andamento' => 'Em andamento'
        );
        private $autenticada = array(
          'autenticada' => 'Autenticada',
        );
        private $naoAutenticada = array(
          'nao_autenticada' => 'Nao Autenticada',
        );
        private $autorizada = array(
          'autorizada' => 'Autorizada',
        );
        private $naoAutorizada = array(
          'nao_autorizada' => 'Nao Autorizada',
        );
        private $capturada = array(
          'capturada' => 'Capturada',
        );
        private $naoCapturada = array(
          'nao_capturada' => 'Nao Capturada',
        );
        private $cancelada = array(
          'cancelada' => 'Cancelada',
        );
        private $emAutenticacao = array(
          'em_autenticacao' => 'Em Autenticacao',
        );


      	public function __construct(){
      		
           $this->name = 'cielo';
      		 $this->tab = 'payments_gateways';
      		 $this->version = '1.2.0';
      		 $this->author = 'ATS Informática';
           $this->controllers = array('payment','validation');
      		 $this->currencies = true;      		 
           $this->currencies_mode = 'checkbox';

      		 $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_); 
           $this->bootstrap = true;

           parent::__construct();

           $this->displayName = $this->l('Cielo');
    		   $this->description = $this->l('Pay with cielo.');
    		   $this->confirmUninstall = $this->l('Are you sure to remove this module?'); 
           if (!count(Currency::checkPaymentCurrencies($this->id)))
              $this->warning = $this->l('No currency has been set for this module.');

           $this->setContext();
      	}


      	//basic of install a module
      	public function install(){
      	   if (!parent::install() OR !$this->registerHook('payment') OR !$this->registerHook('paymentReturn') OR
           !$this->installConfiguration())
              return false;
           return true;
      	}

        //create table cielo order
        public function installConfiguration(){  

          if (!Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'CIELO_ORDER` (
                  `id`        int(11) unsigned NOT NULL AUTO_INCREMENT,
                  `reference` varchar(50)   NOT NULL,
                  `total`     Double NOT NULL,
                  `tid`       varchar(150)  NOT NULL,
                  `auth`      varchar(100),                
                  PRIMARY KEY (`id`)
              ) ENGINE= '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8'))
             return false;


          $this->createCieloPaymentStatus($this->criada, '#01AEF0', '', false, false, false, false);
          $this->createCieloPaymentStatus($this->emAndamento, '#01AEF0', '', false, false, false, false);
          $this->createCieloPaymentStatus($this->autenticada, '#01AEF0', '', false, false, false, false);
          $this->createCieloPaymentStatus($this->naoAutenticada, '#01AEF0', '', false, false, false, false);
          $this->createCieloPaymentStatus($this->autorizada, '#01AEF0', '', false, false, false, false);
          $this->createCieloPaymentStatus($this->naoAutorizada, '#01AEF0', '', false, false, false, false);
          $this->createCieloPaymentStatus($this->capturada, '#01AEF0', '', false, false, false, false);
          $this->createCieloPaymentStatus($this->naoCapturada, '#01AEF0', '', false, false, false, false);
          $this->createCieloPaymentStatus($this->cancelada, '#01AEF0', '', false, false, false, false);
          $this->createCieloPaymentStatus($this->emAutenticacao, '#01AEF0', '', false, false, false, false);
          
          return true;
      }


        //basic of uninstall a module
      	public function uninstall(){
      	   if (!Configuration::deleteByName('CIELO_KEY')
              || !Configuration::deleteByName('CIELO_STORE')
              || !Configuration::deleteByName('CIELO')
              || !Configuration::deleteByName('CIELO_STORE_KEY')
              || !Configuration::deleteByName('CIELO_INSTALLMENT_TYPE')
              || !Configuration::deleteByName('CIELO_AUTOMATICALLY_CAPTURE')
              || !Configuration::deleteByName('CIELO_AUTHORIZE_AUTHOMATIC')
              || !Configuration::deleteByName('CIELO_BUYPAGE')
              || !Configuration::deleteByName('CIELO_FLAGS')
              || !Configuration::deleteByName('CIELO_PARCEL')
              || !parent::uninstall() 
              || !$this->dropCielo())
              return false;
           return true;
      	}

        //drop table cielo order
        public function dropCielo(){
          if (!Db::getInstance()->Execute('DROP TABLE `'._DB_PREFIX_.'CIELO_ORDER`'))
              return false;
          return true;
        }

      	private function setContext() {
            $this->context = Context::getContext();
        }

        private function _postValidation(){

           if (Tools::isSubmit('btnSubmit')){
             if (!Tools::getValue('CIELO_KEY'))
                $this->_postErrors[] = $this->l('Cielo key is required.');
             elseif(!Tools::getValue('CIELO'))
                $this->_postErrors[] = $this->l('Cielo is required');
          }
        }

        private function _postProcess(){  
           if (Tools::isSubmit('btnSubmit')){

              $flags = implode(',', Tools::getValue('CIELO_FLAGS'));
              Configuration::updateValue('CIELO_KEY', Tools::getValue('CIELO_KEY'));
              Configuration::updateValue('CIELO_STORE', Tools::getValue('CIELO_STORE'));
              Configuration::updateValue('CIELO', Tools::getValue('CIELO'));
              Configuration::updateValue('CIELO_STORE_KEY', Tools::getValue('CIELO_STORE_KEY'));
              Configuration::updateValue('CIELO_INSTALLMENT_TYPE', Tools::getValue('CIELO_INSTALLMENT_TYPE'));
              Configuration::updateValue('CIELO_AUTOMATICALLY_CAPTURE', Tools::getValue('CIELO_AUTOMATICALLY_CAPTURE'));
              Configuration::updateValue('CIELO_AUTHORIZE_AUTHOMATIC', Tools::getValue('CIELO_AUTHORIZE_AUTHOMATIC'));
              Configuration::updateValue('CIELO_BUYPAGE', Tools::getValue('CIELO_BUYPAGE'));
              Configuration::updateValue('CIELO_FLAGS', $flags);
              Configuration::updateValue('CIELO_PARCEL', Tools::getValue('CIELO_PARCEL'));
            }
           $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
        }

        private function _displayCielo(){
           return $this->display(__FILE__, 'infos.tpl');
        }

        public function getContent() {
        
        if (Tools::isSubmit('btnSubmit')){
          $this->_postValidation();
          if (!count($this->_postErrors))
            $this->_postProcess();
          else
            foreach ($this->_postErrors as $err)
              $this->_html .= $this->displayError($err);
        }
        else
          $this->_html .= '<br />';

        $this->_html .= $this->_displayCielo();
        $this->_html .= $this->displayForm();

        return $this->_html;
      }

      public function hookPayment($params){
         if (!$this->active)
            return;
         if (!$this->checkCurrency($params['cart']))
            return;

       // p('registrando pagamento');  
        $this->smarty->assign(array(
          'this_path' => $this->_path,
          'this_path_bw' => $this->_path,
          'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/',
          'qtdParcel' => Configuration::get('CIELO_PARCEL')
        ));
        return $this->display(__PS_BASE_URI__.'modules/cielo', '/views/templates/hook/payment.tpl');
      }

      
      public function hookPaymentReturn($params){                        
        if (!$this->active)
           return null;
        return $this->display(__PS_BASE_URI__ . 'modules/cielo', '/views/templates/hook/confirmation.tpl');
      }


      public function checkCurrency($cart)
      {
        $currency_order = new Currency($cart->id_currency);
        $currencies_module = $this->getCurrency($cart->id_currency);

        if (is_array($currencies_module))
          foreach ($currencies_module as $currency_module)
            if ($currency_order->id == $currency_module['id_currency'])
              return true;
        return false;
      }

      public function displayForm()
      {
        $options = array(
                array(
                  'id_option' => 2, 
                  'name' => 'Store' 
                ),
                array(
                  'id_option' => 3,
                  'name' => 'Credit card company'
                ),
         );
        $fields_form = array(
          'form' => array(
            'legend' => array(
              'title' => $this->l('Contact details'),
              'icon' => 'icon-list-alt'
            ),
            'input' => array(
              array(
                'type' => 'text',
                'label' => $this->l('CIELO'),
                'name' => 'CIELO',
                'required' => true
              ),
              array(
                'type' => 'text',
                'label' => $this->l('CIELO KEY'),
                'name' => 'CIELO_KEY',
                'required' => true
              ),

              array(  
                'type' => 'text',
                'label' => $this->l('STORE'),
                'name' => 'CIELO_STORE',
              ),
              array(
                'type' => 'text',
                'label'=> $this->l('STORE KEY'),
                'name' => 'CIELO_STORE_KEY'                
              ),
                                          
              array(
                'type' => 'select',
                'label'=> $this->l('TYPE OF INSTALLMENT'),
                'name' => 'CIELO_INSTALLMENT_TYPE',
                'options' => array(
                    'query' => $this->getTypeInstallment(),
                    'id' => 'id_option',
                    'name' => 'name'
                  )              
              ),
              array(
                'type' => 'select',
                'label'=> $this->l('AUTOMATICALLY CAPTURE'),
                'name' => 'CIELO_AUTOMATICALLY_CAPTURE',
                'options' => array(
                                'query' => $this->getAuthomaticCapture(),
                                'id' => 'id_option',
                                'name' => 'name'
                             )            
                
                ),
              array ('type' => 'select',
                'label'=> $this->l('AUTHORIZE AUTHOMATIC'),
                'name' => 'CIELO_AUTHORIZE_AUTHOMATIC',
                'default_value' => 3,
                'options' => array(
                                'query' => $this->getAuthorizeAuthomatic(),
                                'id' => 'id_option',
                                'name' => 'name'
                             )                                           
                ),

               array ('type' => 'select',
                'label'=> $this->l('BUY PAGE'),
                'name' => 'CIELO_BUYPAGE',
                'options' => array(
                                'query' => $this->getBuyPage(),
                                'id' => 'id_option',
                                'name' => 'name'
                             )                                           
                ),

                array(
                  'type' => 'select',
                  'multiple' => true,
                  'label' => $this->l('FLAGS'),
                  'name' => 'CIELO_FLAGS[]',
                  'options' => array(
                      'query' => $this->getFlags(),
                      'id' => 'id_option',
                      'name' => 'name'
                  ),
                  'size' => 8,
                  'desc' => $this->l('Flags accepted as payment.'),
                  'required' => true
                ),

                array(
                  'type' => 'text',
                  'label' => $this->l('NUMBER OF INSTALLMENTS'),
                  'name' => 'CIELO_PARCEL',
                  'desc' =>  $this->l('Maximum number of installments accepted in payment.'),
                  'required' => true
                )

             ),
            'submit' => array(
              'title' => $this->l('Save'),
            )
          ),

        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
          'fields_value' => $this->getConfigFieldsValues(),
          'languages' => $this->context->controller->getLanguages(),
          'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
      }

      public function getConfigFieldsValues()
      {
        return array(
          'CIELO_KEY' => Tools::getValue('CIELO_KEY', Configuration::get('CIELO_KEY')),
          'CIELO_STORE' => Tools::getValue('CIELO_STORE', Configuration::get('CIELO_STORE')),
          'CIELO' => Tools::getValue('CIELO', Configuration::get('CIELO')),
          'CIELO_STORE_KEY' => Tools::getValue('CIELO_STORE_KEY', Configuration::get('CIELO_STORE_KEY')),
          'CIELO_INSTALLMENT_TYPE' => Tools::getValue('CIELO_INSTALLMENT_TYPE', Configuration::get('CIELO_INSTALLMENT_TYPE')),
          'CIELO_AUTOMATICALLY_CAPTURE' => Tools::getValue('CIELO_AUTOMATICALLY_CAPTURE', Configuration::get('CIELO_AUTOMATICALLY_CAPTURE')),
          'CIELO_AUTHORIZE_AUTHOMATIC' => Tools::getValue('CIELO_AUTHORIZE_AUTHOMATIC', Configuration::get('CIELO_AUTHORIZE_AUTHOMATIC')),
          'CIELO_BUYPAGE' => Tools::getValue('CIELO_BUYPAGE', Configuration::get('CIELO_BUYPAGE')),
          'CIELO_FLAGS[]' => explode(',', Tools::getValue('CIELO_FLAGS[]', Configuration::get('CIELO_FLAGS'))),
          'CIELO_PARCEL' => Tools::getValue('CIELO_PARCEL', Configuration::get('CIELO_PARCEL'))
        );
      }

      public function getFlags(){     
        return array(
          array(
            'id_option' => 'visa', 
            'name' => $this->l('Visa') 
          ),
          array(
            'id_option' => 'mastercard',
            'name' => $this->l('Mastercard')
          ),
          array(
            'id_option' => 'diners',
            'name' => $this->l('Diners')
          ),
          array(
            'id_option' => 'discover',
            'name' => $this->l('Discover')
          ),
          array(
            'id_option' => 'elo',
            'name' => $this->l('Elo')
          ),
          array(
            'id_option' => 'amex',
            'name' => $this->l('American Express')
          ),
          array(
            'id_option' => 'jcb',
            'name' => $this->l('JCB')
          ),
          array(
            'id_option' => 'aura',
            'name' => $this->l('Aura')
          )
        );
      }

      public function getTypeInstallment(){     
        return array(
                  array(
                     'id_option' => 2, 
                     'name' => $this->l('Store') 
                  ),
                  array(
                     'id_option' => 3,
                     'name' => $this->l('Credit card company')
                  ),
                );
      }


      public function getAuthomaticCapture(){
        return array(
                  array(
                    'id_option' => 'false',
                    'name' => $this->l('No')

                  ),
                  array(
                    'id_option' => 'true',
                    'name' => $this->l('Yes')
                  ),

               );
      }

      public function getAuthorizeAuthomatic(){
        return array(
                  array(
                    'id_option' => 0,
                    'name' =>  $this->l('Only authorize transaction')
                  ),
                  array(
                    'id_option' => 1,
                    'name' => $this->l('Authorize transaction if only authentic')
                  ),
                  array(
                    'id_option' => 2,
                    'name' => $this->l('Authorize authentic transaction and unauthenticated')
                  ),
                  array(
                    'id_option' => 3,
                    'name' =>  $this->l('Authorize direct'),
                    'selected' => 'selected'
                  ),
                  
               );
      }

      public function getBuyPage(){
        return array(
                  array(
                    'id_option' => 1,
                    'name' => $this->l('Store')
                    ),
                  array(
                    'id_option' => 2,
                    'name' => $this->l('Cielo') 
                    )
                );
      }



      /**
      * create new order statuses
      *
      * @param $array
      * @param $color
      * @param $template
      * @param $invoice
      * @param $send_email
      * @param $paid
      * @param $logable
      */
      public function createCieloPaymentStatus($array, $color, $template, $invoice, $send_email, $paid, $logable){
         
         foreach ($array as $key => $value){
            $ow_status = Configuration::get($key);
            if ($ow_status === false){
               $order_state = new OrderState();
            }else
               $order_state = new OrderState((int)$ow_status);

               $langs = Language::getLanguages();

            foreach ($langs as $lang)
               $order_state->name[$lang['id_lang']] = utf8_encode(html_entity_decode($value));

            $order_state->invoice = $invoice;
            $order_state->send_email = $send_email;

            if ($template != '')
               $order_state->template = $template;

            if ($paid != '')
               $order_state->paid = $paid;

            $order_state->logable = $logable;
            $order_state->color = $color;
            $order_state->module_name = 'cielo';
            $order_state->save();
            $image = _PS_ROOT_DIR_.'/img/cielo.gif';
            $file = _PS_ROOT_DIR_ . '/img/os/' . (int) $order_state->id . '.gif';
            //$file  = _PS_ROOT_DIR_.'/img/os/'.(int)$order_state->id.'.gif' 
            copy($image, $file);
            Configuration::updateValue($key, (int)$order_state->id);            
         }
      }

      public function returnIdOrderByStatusCielo($nome_status){        
         $isDeleted = version_compare(_PS_VERSION_, '1.5', '<') ? '' : 'WHERE deleted = 0';
        
         $sql = 'SELECT distinct os.`id_order_state`
             FROM `' . _DB_PREFIX_ . 'order_state` os
             INNER JOIN `' . _DB_PREFIX_ . 'order_state_lang` osl
             ON (os.`id_order_state` = osl.`id_order_state` AND osl.`name` = \'' .
              pSQL($nome_status) . '\')' . $isDeleted;
        
         $id_order_state = (Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql));
        
         return $id_order_state[0]['id_order_state'];
      }



      public function returnPaymentConfiguration($params){        
         $this->context = Context::getContext();       
         if (! Tools::isEmpty($params['objOrder']) && $params['objOrder']->module === 'cielo') {        
            $this->context->smarty->assign(
            array(
                  'total_to_pay' => Tools::displayPrice(
                      $params['objOrder']->total_paid,
                      $this->context->currency->id,
                      false
                  ),
                  'status' => 'ok',
                  'id_order' => (int) $params['objOrder']->id
                )
            );            
            if (isset($params['objOrder']->reference) && ! Tools::isEmpty($params['objOrder']->reference)) {
               $this->context->smarty->assign('reference', $params['objOrder']->reference);
            }
          } else {
            $this->context->smarty->assign('status', 'failed');
        }
    }

}