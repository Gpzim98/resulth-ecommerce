<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{debug}prestashop>debug_ad42f6697b035b7580e4fef93be20b4d'] = 'Debogueur';
$_MODULE['<{debug}prestashop>debug_209f96a75fbca48a4d05097af7b94d66'] = 'Affiche la console de debugage smarty';
$_MODULE['<{debug}prestashop>debug_5f827a1edc69f87672cb16c43c543726'] = 'Etes vous sûres de vouloir effacer le module de debogage.';
$_MODULE['<{debug}prestashop>debug_432dbf2122d4aaf3c4c5b41534bebb78'] = 'Ce module vous permet d\'afficher la console smarty sur votre boutique pour une ou plusieurs adresse IP, vous pouvez donner un nom pour une IP donnée.';
$_MODULE['<{debug}prestashop>debug_e4d0eb405d64edb059c1f21fdf922ddd'] = 'Un adminstrateur a été enregistré avec succès';
$_MODULE['<{debug}prestashop>debug_c8ff5758aeb84a7d775597379ee56f05'] = 'Une erreur est survenue lors de la création du module debug';
$_MODULE['<{debug}prestashop>debug_ad49a2577a7947b46529c72c84471f83'] = 'Paramétrez votre module';
$_MODULE['<{debug}prestashop>debug_49ee3087348e8d44e1feda1917443987'] = 'Nom ';
$_MODULE['<{debug}prestashop>debug_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Actif';
$_MODULE['<{debug}prestashop>debug_f2a6c498fb90ee345d997f888fce3b18'] = 'Effacer';
$_MODULE['<{debug}prestashop>debug_06dfd2a635d2230612ba5a1b22061e9a'] = 'Ajouter un administrateur : ';
$_MODULE['<{debug}prestashop>debug_d391ae0ac8a316e124c952d360e946e9'] = 'IP ';
$_MODULE['<{debug}prestashop>debug_8ca81dc4a7a5774d5cb1ff33e4e54024'] = 'Nom ';
$_MODULE['<{debug}prestashop>debug_c9cc8cce247e49bae79f15173ce97354'] = 'Sauvegarder';
