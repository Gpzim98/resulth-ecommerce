{*
* History:
*
* 1.0 - First version
* 2.0 - delete help_debug.php, debug for all pages with ip detection
* 2.1 - The first administrator was not recognized, problem solved
* 2.2 - update
*
*  @author    Vincent MASSON <contact@coeos.pro>
*  @copyright Vincent MASSON <www.coeos.pro>
*  @license   http://www.coeos.pro/boutique/fr/content/3-conditions-generales-de-ventes
*}
{if $debug_by_coeos_dot_pro}{debug}{/if}