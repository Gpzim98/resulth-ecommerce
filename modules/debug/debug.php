<?php
/**
* History:
*
* 1.0 - First version
* 2.0 - delete help_debug.php, debug for all pages with ip detection
* 2.1 - The first administrator was not recognized, problem solved
* 2.2 - update
*
*  @author    Vincent MASSON <contact@coeos.pro>
*  @copyright Vincent MASSON <www.coeos.pro>
*  @license   http://www.coeos.pro/boutique/fr/content/3-conditions-generales-de-ventes
*/

if (!defined('_PS_VERSION_'))
	exit;

class Debug extends Module
{
	/* @var boolean error */
	protected $error = false;

	public function __construct()
	{
		$this->name = 'debug';
		$this->tab = 'front_office_features';
		$this->version = '2.2';
		$this->ps_versions_compliancy['min'] = '1.5';
		$this->author = 'www.coeos.pro';

		parent::__construct();

		$this->displayName = $this->l('debug');
		$this->description = $this->l('Adds a module debugging');
		$this->confirmUninstall = $this->l('Are you sure you want to delete the module debugging ?');
	}
	public function install()
	{
		if (!parent::install() || !$this->registerHook('displayFooter'))
			return false;
		$query = 'CREATE TABLE `'.$this->table().'` (
			`id` int(10) NOT NULL AUTO_INCREMENT,
			`ip` varchar(40) NOT NULL,
			`name` varchar(40) NOT NULL,
			`active` TINYINT(1) NOT NULL,
		PRIMARY KEY(`id`)) ENGINE=MyISAM default CHARSET=utf8';
		if (!Db::getInstance()->Execute($query))
			return false;
		return true;
	}
	public function table($prefix = true)
	{
		return ($prefix)? _DB_PREFIX_.'debug_by_coeos_dot_pro' : 'debug_by_coeos_dot_pro';
	}
	public function uninstall()
	{
		if (!parent::uninstall() || !Db::getInstance()->Execute('DROP TABLE `'.$this->table().'`'))
			return false;
		return true;
	}
	public function hookDisplayFooter()
	{
		$active = $this->getDebug($_SERVER['REMOTE_ADDR']);
		$this->context->smarty->assign('debug_by_coeos_dot_pro', $active);
		return $this->display(__FILE__, 'debug.tpl');
	}
	public function getDebug($ip)
	{
		return (int)Db::getInstance()->getValue('SELECT `active` FROM `'.$this->table().'` WHERE `ip`=\''.$ip.'\' AND `active`=\'1\'');
	}
	public function addDebug()
	{
		$datas = Db::getInstance()->ExecuteS('SELECT * FROM `'.$this->table().'`');
		if (!Db::getInstance()->delete($this->table(false)))
			return false;
		if ($datas)
			foreach ($datas as $data)
				if (!Tools::getValue('delete_'.$data['id']))
				{
					$values = array();
					$values['ip'] = Tools::getValue('ip_'.$data['id']);
					$values['name'] = Tools::getValue('name_'.$data['id']);
					$values['active'] = (int)Tools::getValue('active_'.$data['id']);
					if (!$this->saveDatas($values))
						return false;
				}
		$values = array();
		if ($values['ip'] = Tools::getValue('new_ip'))
		{
			$values['name'] = Tools::getValue('new_name');
			$values['active'] = (int)Tools::getValue('new_active');
			if (!$this->saveDatas($values))
				return false;
		}
		return true;
	}
	private function saveDatas($values)
	{
		if (!Db::getInstance()->insert($this->table(false), $values))
			return false;
		return true;
	}
	private function displayHelp()
	{
		$this->_html .= '
		<p style="float:right"><a href="http://www.coeos.pro/boutique/9-modules-prestashop"><img src="'.$this->_path.'/img/coeos_logo.jpg"/></a></p>
		<b>'.$this->l('This module allows you to display the database').'</b><br /><br />';
		$this->_html .= '<br /><br />MASSON Vincent<p><a href="http://www.coeos.pro/boutique/9-modules-prestashop">www.coeos.pro</a></p>
		<p><a href="mailto:contact@coeos.pro">contact@coeos.pro</a></p>';
	}
	public function getContent()
	{
		$this->_html = '<h2>'.$this->displayName.'</h2>';
		if (Tools::getValue('submitDebugAdd'))
		{
			if ($this->addDebug())
				$this->_html .= $this->displayConfirmation($this->l('The debug has been added successfully'));
			else
				$this->_html .= $this->displayError($this->l('An error occured during debug creation'));
		}
		$this->displayHelp();
		$this->displayForm();
		return $this->_html;
	}
	private function displayForm()
	{
		$datas = Db::getInstance()->ExecuteS('SELECT * FROM `'.$this->table().'`');
		$this->_html .= '
		<style type="text/css">
			table td, table th{border:1px solid black;padding:10px;}
			table{border-collapse:collapse;text-align:center; width:100%}
			
			a.pagination{margin-right:20px;border:1px solid #000;padding:5px;text-decoration:none}
		</style>
		<fieldset>
		<legend><img src="'.$this->_path.'logo.gif" alt="" title="" /> '.$this->l('Manage your debug').'</legend>
		<form method="post" action="'.$_SERVER['REQUEST_URI'].'">
		<table>
		<tr>
			<th>IP</th>
			<th>'.$this->l('Name').'</th>
			<th>'.$this->l('Actif').'</th>
			<th>'.$this->l('Delete').'</th>
		</tr>';
		if ($datas)
			foreach ($datas as $data)
				$this->_html .= '
				<tr>
					<td><input type="text" name="ip_'.$data['id'].'" value="'.$data['ip'].'"/></td>
					<td><input type="text" name="name_'.$data['id'].'" value="'.$data['name'].'"/></td>
					<td><input type="checkbox" name="active_'.$data['id'].'" value="1" '.(($data['active'] == 1)? 'checked="checked"' : '').'/></td>
					<td><input type="checkbox" name="delete_'.$data['id'].'" value="1"/></td>
				</tr>';
		$this->_html .= '
			<tr>
				<td><input type="text" name="new_ip" id="new_ip" value=""/> <input type ="button" onclick="addMyIp()" value=" IP "/></td>
				<td><input type="text" name="new_name" value=""/></td>
				<td><input type="checkbox" name="new_active" value="1"/></td>
				<td></td>
			</tr>
			</table>
				<div class="margin-form">';
					$this->_html .= '<input type="submit" class="button" name="submitDebugAdd" value="'.$this->l('Save').'" id="submitDebugAdd"/>
				</div>
			</form>
		</fieldset>
		<script type="text/javascript">
		function addMyIp()
		{
			document.getElementById("new_ip").value = "'.$_SERVER['REMOTE_ADDR'].'";
		}
		</script>
		';
	}
}