<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blocktags}prestashop>blocktags_f2568a62d4ac8d1d5b532556379772ba'] = 'ট্যাগ ব্লক';
$_MODULE['<{blocktags}prestashop>blocktags_b2de1a21b938fcae9955206a4ca11a12'] = 'আপনার পণ্য ট্যাগ ধারণকারী একটি ব্লক যুক্ত করে.';
$_MODULE['<{blocktags}prestashop>blocktags_8d731d453cacf8cff061df22a269b82b'] = '"প্রদর্শীত ট্যাগ" ক্ষেত্র সম্পন্ন করুন';
$_MODULE['<{blocktags}prestashop>blocktags_73293a024e644165e9bf48f270af63a0'] = 'ভুল সংখ্যা.';
$_MODULE['<{blocktags}prestashop>blocktags_e5b04b8e686367fd0d34e110eb6598c8'] = '';
$_MODULE['<{blocktags}prestashop>blocktags_9b374b52b3bbb02ac62cf53d4db5a075'] = '';
$_MODULE['<{blocktags}prestashop>blocktags_08d5df9c340804cbdd62c0afc6afa784'] = '';
$_MODULE['<{blocktags}prestashop>blocktags_5c35c840e2d37e5cb3b6e1cf8aa78880'] = '';
$_MODULE['<{blocktags}prestashop>blocktags_c888438d14855d7d96a2724ee9c306bd'] = ' আপডেট সেটিংস হচ্ছে ';
$_MODULE['<{blocktags}prestashop>blocktags_f4f70727dc34561dfde1a3c529b6205c'] = 'সেটিংস';
$_MODULE['<{blocktags}prestashop>blocktags_726cefc6088fc537bc5b18f333357724'] = 'প্রদর্শিত ট্যাগ ';
$_MODULE['<{blocktags}prestashop>blocktags_34a51d24608287f9b34807c3004b39d9'] = '';
$_MODULE['<{blocktags}prestashop>blocktags_81cd72634d64b8ca3c138a9eb14eac51'] = '';
$_MODULE['<{blocktags}prestashop>blocktags_d187fc236d189e377e0611a7622e5916'] = '';
$_MODULE['<{blocktags}prestashop>blocktags_ac5bf3b321fa29adf8af5c825d670e76'] = '';
$_MODULE['<{blocktags}prestashop>blocktags_d4a9f41f7f8d2a65cda97d8b5eb0c9d5'] = '';
$_MODULE['<{blocktags}prestashop>blocktags_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'সক্রিয়';
$_MODULE['<{blocktags}prestashop>blocktags_b9f5c797ebbf55adccdd8539a65a0241'] = 'নিষ্ক্রিয়';
$_MODULE['<{blocktags}prestashop>blocktags_c9cc8cce247e49bae79f15173ce97354'] = 'সংরক্ষণ করুন';
$_MODULE['<{blocktags}prestashop>blocktags_189f63f277cd73395561651753563065'] = 'ট্যাগ';
$_MODULE['<{blocktags}prestashop>blocktags_49fa2426b7903b3d4c89e2c1874d9346'] = 'আরও বিস্তারিত';
$_MODULE['<{blocktags}prestashop>blocktags_4e6307cfde762f042d0de430e82ba854'] = 'কোন ট্যাগ এখন পর্যন্ত  নির্দিষ্ট করা হয়নি';
$_MODULE['<{blocktags}prestashop>blocktags_70d5e9f2bb7bcb17339709134ba3a2c6'] = 'কোন ট্যাগ এখন পর্যন্ত  নির্দিষ্ট করা হয়নি';


return $_MODULE;
