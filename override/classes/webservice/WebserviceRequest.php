<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Prestashop SA <contact@prestashop.com>
*  @copyright  2007-2010 Prestashop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class WebserviceRequest extends WebserviceRequestCore
{

	public function fetch($key, $method, $url, $params, $bad_class_name, $inputXml = NULL) {
		if ($url == 'test_connection') {
			die("1"); // Conectado com sucesso
		}

		return parent::fetch($key, $method, $url, $params, $bad_class_name, $inputXml);
	}

	public static function getResources() {
		$resources = parent::getResources();
		$resources['order_pending'] = array('description' => 'The Customers orders pending','class' => 'OrderPending');

		ksort($resources);
		return $resources;
	}

	protected function getOutputObject($type)
	{
		switch ($type)
		{
			case 'JSON' :
                $obj_render = new WebserviceOutputJSON();
                break;
				
			case 'XML' :
			default :
				$obj_render = new WebserviceOutputXML();
				break;
		}
		return $obj_render;
	}
}
