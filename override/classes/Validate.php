<?php

class Validate extends ValidateCore {

	public static function isCPF($cpf) {
		$cpf = preg_replace("/[^0-9]/", "", trim($cpf));
   		if (strlen($cpf) != 11) {
	        return "Informe os 11 digitos do CPF.";
	    }

	    $tudoIgual = true;
	    for ($i = 0; $i < 10; $i++) {
	        if ($cpf{$i} != $cpf{($i + 1)}) {
	            $tudoIgual = false;
	            break;
	        }
	    }
	    /* Se todos os digitos forem iguais, o CPF é inválido */
	    if ($tudoIgual == true) {
	        return "CPF inválido.";
	    }

	    $digitosAtuais = $cpf{9} . $cpf{10};
	    $soma1 = 0;
	    $soma2 = 0;
	    $vlr = 11;

	    for($i = 0; $i < 9; $i++){
	        $soma1 += ($cpf{$i} * ($vlr - 1));
	        $soma2 += ($cpf{$i} * $vlr);
	        $vlr--;
	    }
	    $soma1 = ((($soma1 * 10) % 11) == 10 ? 0 : (($soma1 * 10) % 11));
	    $soma2 = ((($soma2 + (2 * $soma1)) * 10) % 11);

	    if (!((($soma1 * 10) + $soma2) == $digitosAtuais)) {
	        return "CPF inválido.";
	    }

	    return true;
	}

	public static function isCNPJ($cnpj){
		$cnpj = preg_replace("/[^0-9]/", "", trim($cnpj));
   		if (strlen($cnpj) != 14) {
	        return "Informe os 14 digitos do CNPJ.";
	    }

	    $tudoIgual = true;
	    for ($i = 0; $i < 13; $i++) {
	        if ($cnpj{$i} != $cnpj{($i + 1)}) {
	            $tudoIgual = false;
	            break;
	        }
	    }
	    /* Se todos os digitos forem iguais, o CPF é inválido */
	    if ($tudoIgual) {
	        return "CNPJ inválido.";
	    }

	    $valida = array(6,5,4,3,2,9,8,7,6,5,4,3,2);
	    $dig1 = 0;
	    $dig2 = 0;
	    $digito = $cnpj{12} . $cnpj{13};

	    for($i = 0; $i < sizeOf($valida); $i++){
	        $dig1 += ($i > 0 ? ($cnpj{($i - 1)} * $valida[$i]) : 0);
	        $dig2 += $cnpj{$i} * $valida[$i];
	    }
	    $dig1 = ((($dig1 % 11) < 2) ? 0 : (11 - ($dig1 % 11)));
	    $dig2 = ((($dig2 % 11) < 2) ? 0 : (11 - ($dig2 % 11)));

	    if (!((($dig1 * 10) + $dig2) == $digito)) {
	        return "CNPJ inválido.";
	    }

	    return true;
	}
}
