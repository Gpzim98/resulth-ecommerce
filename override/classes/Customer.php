<?php

class Customer extends CustomerCore {

	public $cpf;

    /** Valores possíveis: "fisica" e "juridica" */
    public $tipo_pessoa;

    public $rg;

	public $nome_fantasia;

    private $new_fields = array();

    public  function __construct($id_customer = NULL, $id_lang = NULL){
		/** Não altere a ordem desses campos */
		$this->nd_addField('tipo_pessoa', parent::TYPE_STRING, null,null,true);
        $this->nd_addField('cpf', parent::TYPE_STRING, null, null, true);
        $this->nd_addField('rg', parent::TYPE_STRING, null,null,false);
        $this->nd_addField('nome_fantasia', parent::TYPE_STRING, null,null,false);

        /** Adiciona campos no WebService */
        $webserviceParameters['fields']["tipo_pessoa"] = array();
        $webserviceParameters['fields']["cpf"] = array();
        $webserviceParameters['fields']["rg"] = array();
        $webserviceParameters['fields']["nome_fantasia"] = array();

        /** Desabilita a validação padrão */
        parent::$definition['fields']['siret']['validate'] = 'isGenericName';
        parent::$definition['fields']['ape']['validate'] = 'isGenericName';

        $this->nd_addNewFields();
        parent::__construct($id_customer);
    }

    private function nd_addNewFields(){
        if(!empty($this->new_fields)){
            foreach($this->new_fields as $name=>$def){
                $this->$name = '';
                parent::$definition['fields'][$name] = $def;
            }
        }
    }

    private function nd_addField($fieldname, $type, $validate, $size=null, $required=false){
        $this->new_fields[$fieldname] = array(
            'type' => $type,
            'validate' => $validate,
            'required' => $required,
            'size' => $size
        );
    }

    public function validateField($field, $value, $id_lang = null, $skip = array(), $human_errors = false){
		$ret = parent::validateField($field, $value, $id_lang, $skip, $human_errors);

		/**
		 * Por limitações do prestashop, é necessário fazer a validação desses camos
		 * de forma específica.
		 *
		 * TODO: CPF/CNPJ e RG;IE devem ser validados depois do tipo_pessoa, vide explicação abaixo.
		 */
		if ($ret === true) {
			if (Tools::strtolower($field) == 'tipo_pessoa'){
				/**
				 * TODO: O prestashop instancia um novo objeto para poder fazer as validações
				 * sendo assim, é necessário armazenar o tipo de pessoa no objeto temporário
				 * para que ao validar CPF/CNPJ tenhamos acesso a essa informação. Infelizmente
				 * se a ordem desses campos forem alterados irá provocar problemas, mas é a
				 * única solução no momento.
				 */
				$this->tipo_pessoa = $value;

				if (Tools::strtolower($value) != 'fisica'
					&& Tools::strtolower($value) != 'juridica') {

					$ret = sprintf(Tools::displayError('O campo %s foi informado com um valor inválido.'),
						$this->displayFieldName($field, get_class($this)));
				}

	    	} else if (Tools::strtolower($field) == 'cpf') {
	 			$valid = Validate::isCPF($value);
	 			if ($valid !== true) {
					$ret = Tools::displayError($valid);
				}

	    	} else if (Tools::strtolower($field) == 'siret'
	    		&& Tools::strtolower($this->tipo_pessoa) == 'juridica') {

	 			$valid = Validate::isCNPJ($value);
				if ($valid !== true) {
					$ret = Tools::displayError($valid);
				}

	    	} else if (Tools::strtolower($field) == 'ape'
	    		&& Tools::strtolower($this->tipo_pessoa) == 'juridica') {

				if (strlen($value) != 11 && strlen($value) != 13
					&& Tools::strtolower($value) !== "isento") {

					$ret = Tools::displayError('O campo Número de IE/CFDF não possui o tamanho correto.');
	            }
	    	}
		}

     	return $ret;
    }

	/**
	 * Devido a um problema estrutural, essas validações ocorrem de
	 * forma diferente, mais informações sobre essas validações podem
	 * ser encontradas no método, validateField desta classe
	 */
	public function validateController($htmlentities = true){
		$errors = parent::validateController($htmlentities);

		if (Tools::strtolower($this->tipo_pessoa) != 'fisica'
			&& Tools::strtolower($this->tipo_pessoa) != 'juridica') {

			$errors['tipo_pessoa'] = Tools::displayError('O campo tipo de pessoa foi informado com um valor inválido.');
		} else {
			/** Valida CPF */
			$valid = Validate::isCPF($this->cpf);
			if ($valid !== true) {
				$errors['cpf'] = Tools::displayError($valid);
			}

			if (Tools::strtolower($this->tipo_pessoa) == 'juridica') {
				$valid = Validate::isCNPJ($this->siret);
				if ($valid !== true) {
					$errors['siret'] = Tools::displayError($valid);
				}

				if (strlen($this->ape) != 11 && strlen($this->ape) != 13
					&& Tools::strtolower($this->ape) !== "isento") {
					$errors['ape'] = Tools::displayError('O campo Número de IE/CFDF não possui o tamanho correto.');
				}
			}
		}

		return $errors;
	}
}
