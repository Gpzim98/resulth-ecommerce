<?php

class Product extends ProductCore {


	public function addWs($autodate = true, $null_values = false) {
		$return = parent::addWs($autodate, $null_values);
		if($return){
			$this->processTags();
			$this->cleanCache();
		}
		return $return;
	}

	public function updateWs($null_values = false) {
		// Evita que a data de inclusão do produto seja perdida
		// Chamado: 28031
		if (isset($this->id)) {
			$prod_temp = new Product($this->id);
			$this->date_add = $prod_temp->date_add;
		}

		$return = parent::updateWs($null_values);
		if($return){
			$this->processTags();
			$this->cleanCache();
		}
		return $return;
	}

	private function processTags(){
		$key_array = $this->meta_keywords;
		if (empty($key_array)){
			return;
		}

		foreach ($key_array as $k => $keywords) {
			if (empty($keywords)){
				continue;
			}
			$this->deleteTags();
			Tag::addTags($k, $this->id, $keywords, ",");
		}
	}

	private function cleanCache() {
		// Atualiza cache do bloco de novos produtos após adicionar o produto
		// Chamado: 28031
		if(@require "../modules/blocknewproducts/blocknewproducts.php") {
			$block = new BlockNewProducts();
			$block->_clearCache('*');
		}
	}

}
